Consultar todos os funcionários:
SELECT * FROM VENDEDORES

Consultar todos os funcionários que ganham acima de R$1000:
SELECT * FROM VENDEDORES

WHERE salario >= 1000

Consultar a QUANTIDADE de funcionários que ganham acima de R$2000:
SELECT COUNT(*) FROM VENDEDORES

WHERE salario >= 2000

Consultar qual o total da soma dos salários dos funcionários:
SELECT SUM(salario) FROM VENDEDORES


Ordenar os salários dos funcionários do Menor para o Maior:
SELECT * FROM VENDEDORES

ORDER BY salario ASC

Ordenar os salários dos funcionários do Maior par o Menor:
SELECT * FROM VENDEDORES

ORDER BY salario DESC

Consultar todos os produtos da loja:
SELECT * FROM PRODUTOS

Consultar todos os produtos que custam acima de R$1000:
SELECT * FROM PRODUTOS
 WHERE preco >= 1000

Ordenar os preços dos produtos do Menor para o Maior:
SELECT * FROM PRODUTOS
 ORDER BY PRECO ASC

Verificar QUANTOS produtos temos cadastrados:
SELECT COUNT(*) FROM PRODUTOS

Consultar QUANTAS vendas foram efetuadas no total:
SELECT COUNT(*) FROM VENDAS

Consultar o TOTAL de LUCRO BRUTO das vendas efetuadas:
SELECT SUM(preco) FROM VENDAS

INNER JOIN PRODUTOS 

ON vendas.id_produto = produtos.id_produto





