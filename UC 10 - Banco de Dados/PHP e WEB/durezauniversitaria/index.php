<!DOCTYPE HTML>
<HTML>
	<HEAD>
	
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="css/estilo.css">
		<title>Dureza Universitária</title>
		
	</HEAD>
	
	<BODY>
		<div id="menu">
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="about.php">About</a></li>
				<li><a href="contact.php">Contact</a></li>
			</ul>		
		</div>
		
		
		<div id="paginas">
		
			<h1>
			CONTEÚDO ACADÊMICO<br /> 
			ENGENHARIA BÁSICO <br />
			EB4P52-EB4Q52	  <br />
			</h1>
		
			
			<ul>
				<li>
					<a href="http://durezauniversitaria.com/pasta/eqdif.html"> EQUACOES DIFERENCIAIS </a>
				</li>
				
				<li>
					<a href="http://durezauniversitaria.com/pasta/dinamica.html"> DINAMICA DOS SOLIDOS </a>
				</li>
				
				<li>
					<a href="http://durezauniversitaria.com/pasta/cfisica.html"> COMPLEMENTOS DE FISICA </a>
				</li>
				
				<li>
					<a href="http://durezauniversitaria.com/pasta/estatisticaind.html"> ESTATISTICA INDUTIVA </a>
				</li>
				
			</ul>
			
		</div>
		
	</BODY>

</HTML>
