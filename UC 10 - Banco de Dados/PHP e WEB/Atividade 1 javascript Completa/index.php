<!DOCTYPE HTML>
<HTML>

	<HEAD>
		<meta charset="utf-8">
		<title> Aula de Javascript </title>
		<style>
			input[type=text], input[type=NUMBER], input[type=EMAIL], select {
			width: 100%;
			padding: 12px 20px;
			margin: 8px 0;
			display: inline-block;
			border: 1px solid #ccc;
			border-radius: 4px;
			box-sizing: border-box;
			}

			button {
			width: 100%;
			background-color: #4CAF50;
			color: white;
			padding: 14px 20px;
			margin: 8px 0;
			border: none;
			border-radius: 4px;
			cursor: pointer;
			}
	
			button:hover {
			background-color: #45a049;
			}

			div {
			border-radius: 5px;
			background-color: #f2f2f2;
			padding: 20px;
			width: 500px;
			margin-left: 500px;
			margin-top: 100px;
		-webkit-box-shadow: 6px 6px 28px -3px rgba(0,0,0,1);
		-moz-box-shadow: 6px 6px 28px -3px rgba(0,0,0,1);
		box-shadow: 6px 6px 28px -3px rgba(0,0,0,1);
			}
		
			body{
				background-image: url("https://i.pinimg.com/originals/a9/9d/34/a99d345079abf7af4a51b42c165e675a.jpg");
				background-size: 100%;
			}
		</style>
	</HEAD>
	
	<BODY>
		<div>
		<label>Nome:</label>
		<input type="text" id="valorNome"></input>
		
		<label>Idade:</label>
		<input type="number" id="valorIdade"></input>
		
		<label>E-mail:</label>
		<input type="email" id="valorEmail"></input>
		
		<label>Sexo:</label>
		<select id="valorSexo">
			<option value="masculino">Masculino</option>
			<option value="feminino">Feminino</option>		
		</select>
		
		<label>Endereço:</label>
		<input type="text" id="valorEndereco"></input>
		
		
		<button onclick="mostrarNome()">Ok</button>
		</div>

		<script>					
			
			function mostrarNome(){
				
				var nome, idade, email, sexoLista, sexo, endereco;
				
				nome = document.getElementById('valorNome').value;
				idade = document.getElementById('valorIdade').value;
				email = document.getElementById('valorEmail').value;
				sexoLista = document.getElementById('valorSexo');
				sexo = sexoLista.options[sexoLista.selectedIndex].value;
				endereco = document.getElementById('valorEndereco').value;
				
				alert('Seu nome é: ' 		  + nome 
							+ '\nSua idade é: '   + idade
							+ '\nSeu e-mail é: '   + email 
							+ '\nSeu sexo é : '    + sexo
							+ '\nSeu endereço é: ' + endereco);				
				
			}
			
			
		</script>
	</BODY>
	
</HTML>

