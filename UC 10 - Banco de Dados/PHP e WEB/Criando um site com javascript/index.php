<!DOCTYPE HTML>
<HTML>

	<HEAD>
		<meta charset="utf-8">
		<title> Aula de Javascript </title>
		
	</HEAD>
	
	<BODY>
		<script>
			//Escreve um registro no console do site
			console.log('Hello world!');
			
			//Exibe um alerta para o usuário
			alert('Hello, World!');
			
			//Declaração e atribuição de variáveis no javascript
			var nome;
			var idade;
			
			nome = 'Leonardo';
			idade = 18;
			
			//exemplo de IF no javascript
			if(idade >= 18){
				
				alert('Você é maior de idade!!!');
				
			}else{
				
				alert('Você não é maior de idade!');
			}			
		
			
		</script>
	</BODY>
	
</HTML>

