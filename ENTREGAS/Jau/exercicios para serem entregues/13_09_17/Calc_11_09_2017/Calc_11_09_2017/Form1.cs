﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calc_11_09_2017
{
    public partial class Fom1 : Form
    {
        //Declaração de variaveis
        string operacao;
        double valor1, valor2; // Double é para a calculadora trabalhar com numeros fracionários
        double multiplica, divide, raiz1, raiz2, potencia;
        public Fom1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
            
        {

        }

        //botao soma
        private void btnSoma_Click(object sender, EventArgs e)
        {
            operacao = "soma";
        }

        //botão subtração
        private void button2_Click(object sender, EventArgs e)
        {
            operacao = "subtracao";
        }

        //botão multiplicação
        private void btnMult_Click(object sender, EventArgs e)
        {
            operacao = "multiplicacao";
        }

        //botão divisão
        private void btnDiv_Click(object sender, EventArgs e)
        {
            operacao = "divisao";
        }

        //botão raiz quadrada de valor1 e valor2
        private void btnRaiz_Click(object sender, EventArgs e)
        {
            operacao = "raiz";
        }

        //botão potencia valor1 ^ valor2
        private void btnPotencia_Click(object sender, EventArgs e)
        {
            operacao = "potencia";
        }

        private void btnIgual_Click(object sender, EventArgs e)
        {

            try // extrutura try/catch para tratar o código abaixo, evitando de fazer operações com letras
            { 

            valor1 = Convert.ToDouble(txtNum1.Text);
            valor2 = Convert.ToDouble(txtNum2.Text);

           

                //operação soma
                if (operacao == "soma")
                {
                    MessageBox.Show(valor1 + " + " + valor2 + " = " + (valor1 + valor2));

                }


                //operação subtração

                if (operacao == "subtracao")
                {
                    MessageBox.Show(valor1 + " - " + valor2 + " = " + (valor1 - valor2));

                }


                //operação divisão

                if (operacao == "divisao")
                {
                    if (valor1 == 0 || valor2 == 0) //testa se valor1 ou valor2 é igual a zero

                        MessageBox.Show("Não é possivel dividir por zero");

                    else

                        divide = valor1 / valor2;
                        MessageBox.Show(valor1 + " / " + valor2 + " = " +  Math.Round(divide, 4));

                }


                //operação multiplicação

                if (operacao == "multiplicacao")
                {
                    multiplica = valor1 * valor2;

                    MessageBox.Show(valor1 + " X " + valor2 + " = " + Math.Round(multiplica,4));


                }

                //operação raiz quadrada, √
                if (operacao == "raiz")
                {
                    raiz1 = Math.Sqrt(valor1);
                    raiz2 = Math.Sqrt(valor2);

                    MessageBox.Show(" √ " + valor1 + " = " + Math.Round(raiz1, 4) + " , " + " √ " + valor2 + " = " + Math.Round(raiz2, 4));

                }

                // operação potencia

                if (operacao == "potencia")
                {
                    potencia = Math.Pow(valor1, valor2);
                    MessageBox.Show(valor1 + " ^ " + valor2 + " = " + Math.Round(potencia, 4));

                }
            }
            catch (Exception) // extrutura try/catch, se digitado letras dá a seguinte mensagem
            {
                MessageBox.Show(" Erro !!!!" +
                                " \n Digite apenas números nos campos Num1 e Num2" +
                                " \n Use virgula para números fracionários, ex: 25,68. ");
            }


        }
    }
}
