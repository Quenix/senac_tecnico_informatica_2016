﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Caixa_eletronico
{
    public partial class saque : Form
    {
        public saque()
        {
            InitializeComponent();
        }

        private void btnSaque_Click(object sender, EventArgs e)
        {
            double saque = Convert.ToInt32(txtSaque.Text);

            Form1.saldo = Form1.saldo - saque;

            MessageBox.Show("Olá, " + Form1.nome_cliente + "\nO Seu Saque foi de R$: " + saque +
                             "\nO Seu Saldo atual é R$: " + Form1.saldo);

        }
    }
}
