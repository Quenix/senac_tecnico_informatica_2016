﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Caixa_eletronico
{
    public partial class Identificacao : Form
    {
        

        public Identificacao()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1.nome_cliente = txtNome.Text;
            Form1.agencia = txtAgencia.Text;
            Form1.conta = Convert.ToInt32(txtConta.Text);

            MessageBox.Show("Olá, " + Form1.nome_cliente + "\nSua Agência é: " + Form1.agencia + "\nSua Conta é Nº: " + Form1.conta);
        }

        private void Identificacao_Load(object sender, EventArgs e)
        {
            
        }
    }
}
