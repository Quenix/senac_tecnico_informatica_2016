﻿namespace Piano
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnKeyc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnKeyc
            // 
            this.btnKeyc.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnKeyc.Location = new System.Drawing.Point(3, 170);
            this.btnKeyc.Name = "btnKeyc";
            this.btnKeyc.Size = new System.Drawing.Size(26, 86);
            this.btnKeyc.TabIndex = 0;
            this.btnKeyc.Text = "button1";
            this.btnKeyc.UseVisualStyleBackColor = false;
            this.btnKeyc.Click += new System.EventHandler(this.btnKeyc_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnKeyc);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnKeyc;
    }
}

