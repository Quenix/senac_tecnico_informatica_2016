﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Toub.Sound.Midi;

namespace Piano
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            MidiPlayer.OpenMidi();
        }

        private void btnKeyc_Click(object sender, EventArgs e)
        {
            //nota 1 - Do
            MidiPlayer.Play(new NoteOn(0, 1, "C4", 127));
            System.Threading.Thread.Sleep(500);

            //nota 2 - Ré
            MidiPlayer.Play(new NoteOn(0, 1, "D4", 127));
            System.Threading.Thread.Sleep(500);

            //nota 3 - Mi
            MidiPlayer.Play(new NoteOn(0, 1, "E4", 127));
            System.Threading.Thread.Sleep(500);

            //nota 4 - Fá
            MidiPlayer.Play(new NoteOn(0, 1, "F4", 127));
            System.Threading.Thread.Sleep(1000);

            //nota 5 - Fá
            MidiPlayer.Play(new NoteOn(0, 1, "F4", 127));
            System.Threading.Thread.Sleep(500);

            //nota 6 - Fá
            MidiPlayer.Play(new NoteOn(0, 1, "F4", 127));
            System.Threading.Thread.Sleep(1000);

            //nota 7 - Do
            MidiPlayer.Play(new NoteOn(0, 1, "C4", 127));
            System.Threading.Thread.Sleep(500);

            //nota 8 - Ré
            MidiPlayer.Play(new NoteOn(0, 1, "D4", 127));
            System.Threading.Thread.Sleep(500);

            //nota 9 - Do
            MidiPlayer.Play(new NoteOn(0, 1, "C4", 127));
            System.Threading.Thread.Sleep(500);

            //nota 10 - Ré
            MidiPlayer.Play(new NoteOn(0, 1, "D4", 127));
            System.Threading.Thread.Sleep(1000);

            //nota 11 - Ré
            MidiPlayer.Play(new NoteOn(0, 1, "D4", 127));
            System.Threading.Thread.Sleep(500);

            //nota 12 - Ré
            MidiPlayer.Play(new NoteOn(0, 1, "D4", 127));
            System.Threading.Thread.Sleep(1000);

            //nota 13 - Do
            MidiPlayer.Play(new NoteOn(0, 1, "C4", 127));
            System.Threading.Thread.Sleep(500);

            //nota 14 - Sol
            MidiPlayer.Play(new NoteOn(0, 1, "G4", 127));
            System.Threading.Thread.Sleep(1000);

            //nota 15 - Fá
            MidiPlayer.Play(new NoteOn(0, 1, "F4", 127));
            System.Threading.Thread.Sleep(1000);

            //nota 16 - Mi
            MidiPlayer.Play(new NoteOn(0, 1, "E4", 127));
            System.Threading.Thread.Sleep(1000);

            //nota 17 - Mi
            MidiPlayer.Play(new NoteOn(0, 1, "E4", 127));
            System.Threading.Thread.Sleep(500);

            //nota 18 - Mi
            MidiPlayer.Play(new NoteOn(0, 1, "E4", 127));
            System.Threading.Thread.Sleep(1000);

            //nota 19 - Do
            MidiPlayer.Play(new NoteOn(0, 1, "C4", 127));
            System.Threading.Thread.Sleep(500);

            //nota 20 - Ré
            MidiPlayer.Play(new NoteOn(0, 1, "D4", 127));
            System.Threading.Thread.Sleep(500);

            //nota 21 - Mi
            MidiPlayer.Play(new NoteOn(0, 1, "E4", 127));
            System.Threading.Thread.Sleep(500);

            //nota 22 - Fá
            MidiPlayer.Play(new NoteOn(0, 1, "F4", 127));
            System.Threading.Thread.Sleep(1000);

            //nota 23 - Fá
            MidiPlayer.Play(new NoteOn(0, 1, "F4", 127));
            System.Threading.Thread.Sleep(500);

            //nota 24 - Fá
            MidiPlayer.Play(new NoteOn(0, 1, "F4", 127));
            System.Threading.Thread.Sleep(500);


        }
    }
}
