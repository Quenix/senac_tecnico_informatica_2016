﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IMC
{
    public partial class Form1 : Form
    {
        string nome;
        double peso, altura;
        double IMC, IMC1;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void btnCalc_Click(object sender, EventArgs e)
        {

            nome = txtNome.Text;
            peso = Math.Round(Convert.ToDouble(txtPeso.Text), 2);
            altura = Math.Round(Convert.ToDouble(txtAlt.Text), 2);

            IMC = peso / (altura * altura);
            IMC1 = Math.Round(IMC, 2);

            txtImc.Text = Convert.ToString("  ") + Convert.ToString(IMC1);

            if (IMC <= 16)
            txtClass.Text = "Peso muito Baixo (Grave)";

            if ((IMC <= 16.99) && (IMC >= 16))
            txtClass.Text = "Peso Baixo, Grave";

            if ((IMC <= 18.49) && (IMC >= 17))
            txtClass.Text = "Peso Baixo";

            if ((IMC <= 24.99) && (IMC >= 18.50))
            txtClass.Text = "Peso Normal ";

            if ((IMC <= 29.99) && (IMC >= 25))
            txtClass.Text = "Sobrepeso";

            if ((IMC <= 34.99) && (IMC >= 30))
            txtClass.Text = "Obsidade Grau 1";

            if ((IMC <= 39.99) && (IMC >= 35))
            txtClass.Text = "Obsidade Grau 2";

            if (IMC >= 40)
            txtClass.Text = "Obsidade Grau 3 (Morbida)";
        }
    }
}
