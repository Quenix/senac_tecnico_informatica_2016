﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calc_11_09_2017
{
    public partial class Fom1 : Form
    {
        //Declaração de variaveis
        string operacao;
        double valor1, valor2; //Double para trabalhar com numeros fracionários

        public Fom1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
            
        {

        }

        //botao soma
        private void btnSoma_Click(object sender, EventArgs e)
        {
            operacao = "soma";
        }

        //botão subtração
        private void button2_Click(object sender, EventArgs e)
        {
            operacao = "subtracao";
        }

        //botão multiplicação
        private void btnMult_Click(object sender, EventArgs e)
        {
            operacao = "multiplicacao";
        }

        //botão divisão
        private void btnDiv_Click(object sender, EventArgs e)
        {
            operacao = "divisao";
        }

        private void btnIgual_Click(object sender, EventArgs e)
        {

            valor1 = Convert.ToDouble(txtNum1.Text);
            valor2 = Convert.ToDouble(txtNum2.Text);

            //operação soma
            if (operacao == "soma")
            {
                MessageBox.Show("Resultado: " + (valor1 + valor2));

            }
           

            //operação subtração

            if (operacao == "subtracao")
            {
                MessageBox.Show("Resultado: " + (valor1 - valor2));

            }
            

            //operação divisão

            if (operacao == "divisao")
            {
                MessageBox.Show("Resultado: " + (valor1 / valor2));

            }
           

            //operação multiplicação

            if (operacao == "multiplicacao")
            {
                MessageBox.Show("Resultado: " + (valor1 * valor2));

            }
          

            

           
        }
    }
}
