﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Form1_e_Form2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void nomeEIdadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var nomeidade = new Form2();
            nomeidade.ShowDialog();
        }

        private void fraseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var mudacor = new Form3();
            mudacor.ShowDialog();
        }

        private void inverteFraseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var invertefrase = new Form4();
            invertefrase.ShowDialog();
        }
    }
}
