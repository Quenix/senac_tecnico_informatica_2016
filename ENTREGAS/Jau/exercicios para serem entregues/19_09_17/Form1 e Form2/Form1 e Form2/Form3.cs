﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Form1_e_Form2
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void btnMudacor_Click(object sender, EventArgs e)
        {
            lblFrase.ForeColor = Color.DarkRed;
            btnMudacor.ForeColor = Color.DarkBlue;
            btnMudacor.BackColor = Color.DarkSalmon;
            button1.BackColor = Color.Bisque;
            this.BackColor = Color.DarkOliveGreen;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lblFrase.ForeColor = Color.Red;
            btnMudacor.ForeColor = Color.Blue;
            btnMudacor.BackColor = Color.YellowGreen;
            this.BackColor = Color.Green;
        }
    }
}
