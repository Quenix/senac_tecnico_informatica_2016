﻿namespace Form1_e_Form2
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.nomeEIdadeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fraseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inverteFraseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nomeEIdadeToolStripMenuItem,
            this.fraseToolStripMenuItem,
            this.inverteFraseToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(284, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // nomeEIdadeToolStripMenuItem
            // 
            this.nomeEIdadeToolStripMenuItem.Name = "nomeEIdadeToolStripMenuItem";
            this.nomeEIdadeToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.nomeEIdadeToolStripMenuItem.Text = "Nome e Idade";
            this.nomeEIdadeToolStripMenuItem.Click += new System.EventHandler(this.nomeEIdadeToolStripMenuItem_Click);
            // 
            // fraseToolStripMenuItem
            // 
            this.fraseToolStripMenuItem.Name = "fraseToolStripMenuItem";
            this.fraseToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.fraseToolStripMenuItem.Text = "MudaCor";
            this.fraseToolStripMenuItem.Click += new System.EventHandler(this.fraseToolStripMenuItem_Click);
            // 
            // inverteFraseToolStripMenuItem
            // 
            this.inverteFraseToolStripMenuItem.Name = "inverteFraseToolStripMenuItem";
            this.inverteFraseToolStripMenuItem.Size = new System.Drawing.Size(85, 20);
            this.inverteFraseToolStripMenuItem.Text = "Inverte Frase";
            this.inverteFraseToolStripMenuItem.Click += new System.EventHandler(this.inverteFraseToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem nomeEIdadeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fraseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inverteFraseToolStripMenuItem;
    }
}

