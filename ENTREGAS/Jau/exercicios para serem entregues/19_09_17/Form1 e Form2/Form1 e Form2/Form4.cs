﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Form1_e_Form2
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void btnInverte_Click(object sender, EventArgs e)
        {
          
            //cria um array de caracteres da frase.
            char[] ArrayChar = txtFrase.Text.ToCharArray();

            //Inverte a ordem do array
            Array.Reverse(ArrayChar);

            //mostra o texto invertido
            string invertido = new string(ArrayChar);

            txtFrase.Text = invertido;
            

        }
    }
}
