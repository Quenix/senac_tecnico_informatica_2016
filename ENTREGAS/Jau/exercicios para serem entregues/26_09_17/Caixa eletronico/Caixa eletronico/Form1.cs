﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Caixa_eletronico
{
    public partial class Form1 : Form
    {
        
        public static string nome_cliente, agencia;
        public static double saldo = 1000;
        public static int conta;
       

        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btn_Click(object sender, EventArgs e)
        {
            var deposito = new deposito();
            deposito.ShowDialog();
        }

        private void btnIdentificação_Click(object sender, EventArgs e)
        {
            var identificacao = new Identificacao();
            identificacao.ShowDialog();
        }

        private void btnSaque_Click(object sender, EventArgs e)
        {
            var saque = new saque();
            saque.ShowDialog();
        }

        private void txtTela_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSaldo_Click(object sender, EventArgs e)
        {
            var saldo = new saldo();
            saldo.ShowDialog();
        }
    }
}
