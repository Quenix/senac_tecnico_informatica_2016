﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Caixa_eletronico
{
    public partial class saldo : Form
    {
        public saldo()
        {
            InitializeComponent();
        }

        private void btnSaldo_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Olá, " + Form1.nome_cliente + "\nO Seu Saldo atual é R$: " + Form1.saldo);
        }
    }
}
