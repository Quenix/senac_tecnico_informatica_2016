﻿namespace quizz
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblpergunta1 = new System.Windows.Forms.Label();
            this.pergunta1_resposta1 = new System.Windows.Forms.RadioButton();
            this.pergunta1_resposta2 = new System.Windows.Forms.RadioButton();
            this.pergunta1_resposta3 = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pergunta2_resposta3 = new System.Windows.Forms.RadioButton();
            this.pergunta2_resposta2 = new System.Windows.Forms.RadioButton();
            this.pergunta2_resposta1 = new System.Windows.Forms.RadioButton();
            this.lblpergunta2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pergunta3_resposta3 = new System.Windows.Forms.RadioButton();
            this.pergunta3_resposta2 = new System.Windows.Forms.RadioButton();
            this.pergunta3_resposta1 = new System.Windows.Forms.RadioButton();
            this.lblpergunta3 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.pergunta4_resposta3 = new System.Windows.Forms.RadioButton();
            this.pergunta4_resposta2 = new System.Windows.Forms.RadioButton();
            this.pergunta4_resposta1 = new System.Windows.Forms.RadioButton();
            this.lblpergunta4 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.pergunta5_resposta3 = new System.Windows.Forms.RadioButton();
            this.pergunta5_resposta2 = new System.Windows.Forms.RadioButton();
            this.pergunta5_resposta1 = new System.Windows.Forms.RadioButton();
            this.lblpergunta5 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.pergunta6_resposta3 = new System.Windows.Forms.RadioButton();
            this.pergunta6_resposta2 = new System.Windows.Forms.RadioButton();
            this.pergunta6_resposta1 = new System.Windows.Forms.RadioButton();
            this.lblpergunta6 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pergunta1_resposta3);
            this.groupBox1.Controls.Add(this.pergunta1_resposta2);
            this.groupBox1.Controls.Add(this.pergunta1_resposta1);
            this.groupBox1.Controls.Add(this.lblpergunta1);
            this.groupBox1.Location = new System.Drawing.Point(22, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(199, 164);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pergunta1";
            // 
            // lblpergunta1
            // 
            this.lblpergunta1.AutoSize = true;
            this.lblpergunta1.Location = new System.Drawing.Point(7, 22);
            this.lblpergunta1.Name = "lblpergunta1";
            this.lblpergunta1.Size = new System.Drawing.Size(35, 13);
            this.lblpergunta1.TabIndex = 0;
            this.lblpergunta1.Text = "label1";
            // 
            // pergunta1_resposta1
            // 
            this.pergunta1_resposta1.AutoSize = true;
            this.pergunta1_resposta1.Location = new System.Drawing.Point(10, 72);
            this.pergunta1_resposta1.Name = "pergunta1_resposta1";
            this.pergunta1_resposta1.Size = new System.Drawing.Size(85, 17);
            this.pergunta1_resposta1.TabIndex = 1;
            this.pergunta1_resposta1.TabStop = true;
            this.pergunta1_resposta1.Text = "radioButton1";
            this.pergunta1_resposta1.UseVisualStyleBackColor = true;
            // 
            // pergunta1_resposta2
            // 
            this.pergunta1_resposta2.AutoSize = true;
            this.pergunta1_resposta2.Location = new System.Drawing.Point(10, 95);
            this.pergunta1_resposta2.Name = "pergunta1_resposta2";
            this.pergunta1_resposta2.Size = new System.Drawing.Size(85, 17);
            this.pergunta1_resposta2.TabIndex = 2;
            this.pergunta1_resposta2.TabStop = true;
            this.pergunta1_resposta2.Text = "radioButton2";
            this.pergunta1_resposta2.UseVisualStyleBackColor = true;
            // 
            // pergunta1_resposta3
            // 
            this.pergunta1_resposta3.AutoSize = true;
            this.pergunta1_resposta3.Location = new System.Drawing.Point(10, 118);
            this.pergunta1_resposta3.Name = "pergunta1_resposta3";
            this.pergunta1_resposta3.Size = new System.Drawing.Size(85, 17);
            this.pergunta1_resposta3.TabIndex = 3;
            this.pergunta1_resposta3.TabStop = true;
            this.pergunta1_resposta3.Text = "radioButton3";
            this.pergunta1_resposta3.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pergunta2_resposta3);
            this.groupBox2.Controls.Add(this.pergunta2_resposta2);
            this.groupBox2.Controls.Add(this.pergunta2_resposta1);
            this.groupBox2.Controls.Add(this.lblpergunta2);
            this.groupBox2.Location = new System.Drawing.Point(227, 26);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(199, 164);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pergunta2";
            // 
            // pergunta2_resposta3
            // 
            this.pergunta2_resposta3.AutoSize = true;
            this.pergunta2_resposta3.Location = new System.Drawing.Point(10, 118);
            this.pergunta2_resposta3.Name = "pergunta2_resposta3";
            this.pergunta2_resposta3.Size = new System.Drawing.Size(85, 17);
            this.pergunta2_resposta3.TabIndex = 3;
            this.pergunta2_resposta3.TabStop = true;
            this.pergunta2_resposta3.Text = "radioButton3";
            this.pergunta2_resposta3.UseVisualStyleBackColor = true;
            // 
            // pergunta2_resposta2
            // 
            this.pergunta2_resposta2.AutoSize = true;
            this.pergunta2_resposta2.Location = new System.Drawing.Point(10, 95);
            this.pergunta2_resposta2.Name = "pergunta2_resposta2";
            this.pergunta2_resposta2.Size = new System.Drawing.Size(85, 17);
            this.pergunta2_resposta2.TabIndex = 2;
            this.pergunta2_resposta2.TabStop = true;
            this.pergunta2_resposta2.Text = "radioButton2";
            this.pergunta2_resposta2.UseVisualStyleBackColor = true;
            // 
            // pergunta2_resposta1
            // 
            this.pergunta2_resposta1.AutoSize = true;
            this.pergunta2_resposta1.Location = new System.Drawing.Point(10, 72);
            this.pergunta2_resposta1.Name = "pergunta2_resposta1";
            this.pergunta2_resposta1.Size = new System.Drawing.Size(85, 17);
            this.pergunta2_resposta1.TabIndex = 1;
            this.pergunta2_resposta1.TabStop = true;
            this.pergunta2_resposta1.Text = "radioButton1";
            this.pergunta2_resposta1.UseVisualStyleBackColor = true;
            // 
            // lblpergunta2
            // 
            this.lblpergunta2.AutoSize = true;
            this.lblpergunta2.Location = new System.Drawing.Point(7, 22);
            this.lblpergunta2.Name = "lblpergunta2";
            this.lblpergunta2.Size = new System.Drawing.Size(35, 13);
            this.lblpergunta2.TabIndex = 0;
            this.lblpergunta2.Text = "label2";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(246, 396);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(166, 44);
            this.button1.TabIndex = 2;
            this.button1.Text = "Executar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.pergunta3_resposta3);
            this.groupBox3.Controls.Add(this.pergunta3_resposta2);
            this.groupBox3.Controls.Add(this.pergunta3_resposta1);
            this.groupBox3.Controls.Add(this.lblpergunta3);
            this.groupBox3.Location = new System.Drawing.Point(22, 206);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(199, 164);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Pergunta3";
            // 
            // pergunta3_resposta3
            // 
            this.pergunta3_resposta3.AutoSize = true;
            this.pergunta3_resposta3.Location = new System.Drawing.Point(10, 118);
            this.pergunta3_resposta3.Name = "pergunta3_resposta3";
            this.pergunta3_resposta3.Size = new System.Drawing.Size(85, 17);
            this.pergunta3_resposta3.TabIndex = 3;
            this.pergunta3_resposta3.TabStop = true;
            this.pergunta3_resposta3.Text = "radioButton3";
            this.pergunta3_resposta3.UseVisualStyleBackColor = true;
            // 
            // pergunta3_resposta2
            // 
            this.pergunta3_resposta2.AutoSize = true;
            this.pergunta3_resposta2.Location = new System.Drawing.Point(10, 95);
            this.pergunta3_resposta2.Name = "pergunta3_resposta2";
            this.pergunta3_resposta2.Size = new System.Drawing.Size(85, 17);
            this.pergunta3_resposta2.TabIndex = 2;
            this.pergunta3_resposta2.TabStop = true;
            this.pergunta3_resposta2.Text = "radioButton2";
            this.pergunta3_resposta2.UseVisualStyleBackColor = true;
            // 
            // pergunta3_resposta1
            // 
            this.pergunta3_resposta1.AutoSize = true;
            this.pergunta3_resposta1.Location = new System.Drawing.Point(10, 72);
            this.pergunta3_resposta1.Name = "pergunta3_resposta1";
            this.pergunta3_resposta1.Size = new System.Drawing.Size(85, 17);
            this.pergunta3_resposta1.TabIndex = 1;
            this.pergunta3_resposta1.TabStop = true;
            this.pergunta3_resposta1.Text = "radioButton1";
            this.pergunta3_resposta1.UseVisualStyleBackColor = true;
            // 
            // lblpergunta3
            // 
            this.lblpergunta3.AutoSize = true;
            this.lblpergunta3.Location = new System.Drawing.Point(7, 22);
            this.lblpergunta3.Name = "lblpergunta3";
            this.lblpergunta3.Size = new System.Drawing.Size(35, 13);
            this.lblpergunta3.TabIndex = 0;
            this.lblpergunta3.Text = "label3";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.pergunta4_resposta3);
            this.groupBox4.Controls.Add(this.pergunta4_resposta2);
            this.groupBox4.Controls.Add(this.pergunta4_resposta1);
            this.groupBox4.Controls.Add(this.lblpergunta4);
            this.groupBox4.Location = new System.Drawing.Point(227, 206);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(199, 164);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Pergunta4";
            // 
            // pergunta4_resposta3
            // 
            this.pergunta4_resposta3.AutoSize = true;
            this.pergunta4_resposta3.Location = new System.Drawing.Point(10, 118);
            this.pergunta4_resposta3.Name = "pergunta4_resposta3";
            this.pergunta4_resposta3.Size = new System.Drawing.Size(85, 17);
            this.pergunta4_resposta3.TabIndex = 3;
            this.pergunta4_resposta3.TabStop = true;
            this.pergunta4_resposta3.Text = "radioButton3";
            this.pergunta4_resposta3.UseVisualStyleBackColor = true;
            // 
            // pergunta4_resposta2
            // 
            this.pergunta4_resposta2.AutoSize = true;
            this.pergunta4_resposta2.Location = new System.Drawing.Point(10, 95);
            this.pergunta4_resposta2.Name = "pergunta4_resposta2";
            this.pergunta4_resposta2.Size = new System.Drawing.Size(85, 17);
            this.pergunta4_resposta2.TabIndex = 2;
            this.pergunta4_resposta2.TabStop = true;
            this.pergunta4_resposta2.Text = "radioButton2";
            this.pergunta4_resposta2.UseVisualStyleBackColor = true;
            // 
            // pergunta4_resposta1
            // 
            this.pergunta4_resposta1.AutoSize = true;
            this.pergunta4_resposta1.Location = new System.Drawing.Point(10, 72);
            this.pergunta4_resposta1.Name = "pergunta4_resposta1";
            this.pergunta4_resposta1.Size = new System.Drawing.Size(85, 17);
            this.pergunta4_resposta1.TabIndex = 1;
            this.pergunta4_resposta1.TabStop = true;
            this.pergunta4_resposta1.Text = "radioButton1";
            this.pergunta4_resposta1.UseVisualStyleBackColor = true;
            // 
            // lblpergunta4
            // 
            this.lblpergunta4.AutoSize = true;
            this.lblpergunta4.Location = new System.Drawing.Point(7, 22);
            this.lblpergunta4.Name = "lblpergunta4";
            this.lblpergunta4.Size = new System.Drawing.Size(35, 13);
            this.lblpergunta4.TabIndex = 0;
            this.lblpergunta4.Text = "label1";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.pergunta5_resposta3);
            this.groupBox5.Controls.Add(this.pergunta5_resposta2);
            this.groupBox5.Controls.Add(this.pergunta5_resposta1);
            this.groupBox5.Controls.Add(this.lblpergunta5);
            this.groupBox5.Location = new System.Drawing.Point(432, 26);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(199, 164);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Pergunta5";
            // 
            // pergunta5_resposta3
            // 
            this.pergunta5_resposta3.AutoSize = true;
            this.pergunta5_resposta3.Location = new System.Drawing.Point(10, 118);
            this.pergunta5_resposta3.Name = "pergunta5_resposta3";
            this.pergunta5_resposta3.Size = new System.Drawing.Size(85, 17);
            this.pergunta5_resposta3.TabIndex = 3;
            this.pergunta5_resposta3.TabStop = true;
            this.pergunta5_resposta3.Text = "radioButton3";
            this.pergunta5_resposta3.UseVisualStyleBackColor = true;
            // 
            // pergunta5_resposta2
            // 
            this.pergunta5_resposta2.AutoSize = true;
            this.pergunta5_resposta2.Location = new System.Drawing.Point(10, 95);
            this.pergunta5_resposta2.Name = "pergunta5_resposta2";
            this.pergunta5_resposta2.Size = new System.Drawing.Size(85, 17);
            this.pergunta5_resposta2.TabIndex = 2;
            this.pergunta5_resposta2.TabStop = true;
            this.pergunta5_resposta2.Text = "radioButton2";
            this.pergunta5_resposta2.UseVisualStyleBackColor = true;
            // 
            // pergunta5_resposta1
            // 
            this.pergunta5_resposta1.AutoSize = true;
            this.pergunta5_resposta1.Location = new System.Drawing.Point(10, 72);
            this.pergunta5_resposta1.Name = "pergunta5_resposta1";
            this.pergunta5_resposta1.Size = new System.Drawing.Size(85, 17);
            this.pergunta5_resposta1.TabIndex = 1;
            this.pergunta5_resposta1.TabStop = true;
            this.pergunta5_resposta1.Text = "radioButton1";
            this.pergunta5_resposta1.UseVisualStyleBackColor = true;
            // 
            // lblpergunta5
            // 
            this.lblpergunta5.AutoSize = true;
            this.lblpergunta5.Location = new System.Drawing.Point(7, 22);
            this.lblpergunta5.Name = "lblpergunta5";
            this.lblpergunta5.Size = new System.Drawing.Size(35, 13);
            this.lblpergunta5.TabIndex = 0;
            this.lblpergunta5.Text = "label1";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.pergunta6_resposta3);
            this.groupBox6.Controls.Add(this.pergunta6_resposta2);
            this.groupBox6.Controls.Add(this.pergunta6_resposta1);
            this.groupBox6.Controls.Add(this.lblpergunta6);
            this.groupBox6.Location = new System.Drawing.Point(432, 206);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(199, 164);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Pergunta6";
            // 
            // pergunta6_resposta3
            // 
            this.pergunta6_resposta3.AutoSize = true;
            this.pergunta6_resposta3.Location = new System.Drawing.Point(10, 118);
            this.pergunta6_resposta3.Name = "pergunta6_resposta3";
            this.pergunta6_resposta3.Size = new System.Drawing.Size(85, 17);
            this.pergunta6_resposta3.TabIndex = 3;
            this.pergunta6_resposta3.TabStop = true;
            this.pergunta6_resposta3.Text = "radioButton3";
            this.pergunta6_resposta3.UseVisualStyleBackColor = true;
            // 
            // pergunta6_resposta2
            // 
            this.pergunta6_resposta2.AutoSize = true;
            this.pergunta6_resposta2.Location = new System.Drawing.Point(10, 95);
            this.pergunta6_resposta2.Name = "pergunta6_resposta2";
            this.pergunta6_resposta2.Size = new System.Drawing.Size(85, 17);
            this.pergunta6_resposta2.TabIndex = 2;
            this.pergunta6_resposta2.TabStop = true;
            this.pergunta6_resposta2.Text = "radioButton2";
            this.pergunta6_resposta2.UseVisualStyleBackColor = true;
            // 
            // pergunta6_resposta1
            // 
            this.pergunta6_resposta1.AutoSize = true;
            this.pergunta6_resposta1.Location = new System.Drawing.Point(10, 72);
            this.pergunta6_resposta1.Name = "pergunta6_resposta1";
            this.pergunta6_resposta1.Size = new System.Drawing.Size(85, 17);
            this.pergunta6_resposta1.TabIndex = 1;
            this.pergunta6_resposta1.TabStop = true;
            this.pergunta6_resposta1.Text = "radioButton1";
            this.pergunta6_resposta1.UseVisualStyleBackColor = true;
            // 
            // lblpergunta6
            // 
            this.lblpergunta6.AutoSize = true;
            this.lblpergunta6.Location = new System.Drawing.Point(7, 22);
            this.lblpergunta6.Name = "lblpergunta6";
            this.lblpergunta6.Size = new System.Drawing.Size(35, 13);
            this.lblpergunta6.TabIndex = 0;
            this.lblpergunta6.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(685, 465);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton pergunta1_resposta3;
        private System.Windows.Forms.RadioButton pergunta1_resposta2;
        private System.Windows.Forms.RadioButton pergunta1_resposta1;
        private System.Windows.Forms.Label lblpergunta1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton pergunta2_resposta3;
        private System.Windows.Forms.RadioButton pergunta2_resposta2;
        private System.Windows.Forms.RadioButton pergunta2_resposta1;
        private System.Windows.Forms.Label lblpergunta2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton pergunta3_resposta3;
        private System.Windows.Forms.RadioButton pergunta3_resposta2;
        private System.Windows.Forms.RadioButton pergunta3_resposta1;
        private System.Windows.Forms.Label lblpergunta3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton pergunta4_resposta3;
        private System.Windows.Forms.RadioButton pergunta4_resposta2;
        private System.Windows.Forms.RadioButton pergunta4_resposta1;
        private System.Windows.Forms.Label lblpergunta4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton pergunta5_resposta3;
        private System.Windows.Forms.RadioButton pergunta5_resposta2;
        private System.Windows.Forms.RadioButton pergunta5_resposta1;
        private System.Windows.Forms.Label lblpergunta5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton pergunta6_resposta3;
        private System.Windows.Forms.RadioButton pergunta6_resposta2;
        private System.Windows.Forms.RadioButton pergunta6_resposta1;
        private System.Windows.Forms.Label lblpergunta6;
    }
}

