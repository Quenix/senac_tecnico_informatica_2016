﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace quizz
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //pergunta1
            lblpergunta1.Text = "A PEDRA cumprimentou uma TORA. \n Que horas são?";
            pergunta1_resposta1.Text = "2h30";
            pergunta1_resposta2.Text = "7h45";
            pergunta1_resposta3.Text = "8h00"; //resposta certa

            // pergunta2
            lblpergunta2.Text = "Qual é a cor do carro do OI SUMIDO?";
            pergunta2_resposta1.Text = "Branco";
            pergunta2_resposta2.Text = "Preto"; // resposta certa
            pergunta2_resposta3.Text = "Cinza";

            //pergunta3
            lblpergunta3.Text = "Qual é o centro da metade do meio?";
            pergunta3_resposta1.Text = "1/8"; // resposta certa
            pergunta3_resposta2.Text = "1/4"; 
            pergunta3_resposta3.Text = "1/2";

            //pergunta4
            lblpergunta4.Text = "Meu avô tem 4 filhos,\ncada filho tem 4 filhos,\nQuantos primos eu tenho?";
            pergunta4_resposta1.Text = "8"; 
            pergunta4_resposta2.Text = "12"; // resposta certa
            pergunta4_resposta3.Text = "15";

            //pergunta5
            lblpergunta5.Text = "Qual é o proximo numero da sequencia: 2,10,12,16,17,18,19,...";
            pergunta5_resposta1.Text = "20";
            pergunta5_resposta2.Text = "40"; 
            pergunta5_resposta3.Text = "200"; // resposta certa

            //pergunta6
            lblpergunta6.Text = "Qual o proximo numero:\n 25,24,21,16,09,..";
            pergunta6_resposta1.Text = "02";
            pergunta6_resposta2.Text = "01"; 
            pergunta6_resposta3.Text = "00"; // resposta certa

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int pontos;

            pontos = 0;
            //pergunta1
            if (pergunta1_resposta3.Checked == true)
            {
                pontos += 10;

                pergunta1_resposta1.ForeColor = Color.Red;
                pergunta1_resposta2.ForeColor = Color.Red;
                pergunta1_resposta3.ForeColor = Color.Green;

            }
            //pergunta2
            if (pergunta2_resposta2.Checked == true)
            {
                pontos += 10;

                pergunta2_resposta1.ForeColor = Color.Red;
                pergunta2_resposta2.ForeColor = Color.Green;
                pergunta2_resposta3.ForeColor = Color.Red;

            }

            //pergunta3
            if (pergunta3_resposta1.Checked == true)
            {
                pontos += 10;

                pergunta3_resposta1.ForeColor = Color.Green;
                pergunta3_resposta2.ForeColor = Color.Red;
                pergunta3_resposta3.ForeColor = Color.Red;

            }

            //pergunta4
            if (pergunta4_resposta2.Checked == true)
            {
                pontos += 10;

                pergunta4_resposta1.ForeColor = Color.Red;
                pergunta4_resposta2.ForeColor = Color.Green;
                pergunta4_resposta3.ForeColor = Color.Red;

            }

            //pergunta5
            if (pergunta5_resposta3.Checked == true)
            {
                pontos += 10;

                pergunta5_resposta1.ForeColor = Color.Red;
                pergunta5_resposta2.ForeColor = Color.Red;
                pergunta5_resposta3.ForeColor = Color.Green;

            }

            //pergunta6
            if (pergunta6_resposta3.Checked == true)
            {
                pontos += 10;

                pergunta6_resposta1.ForeColor = Color.Red;
                pergunta6_resposta2.ForeColor = Color.Red;
                pergunta6_resposta3.ForeColor = Color.Green;

            }

            MessageBox.Show("Parabens, você marcou " + pontos + " pontos!");
        }
    }
}
