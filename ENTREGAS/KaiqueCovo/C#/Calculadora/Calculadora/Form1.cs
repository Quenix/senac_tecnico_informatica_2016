﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnmulti_Click(object sender, EventArgs e)
        {
            //OBTEM MULTIPLICAÇÃO DOS NÚMEROS

            Double multi, valor1, valor2;
       

            valor1 = Convert.ToDouble(textcamp1.Text);
            valor2 = Convert.ToDouble(textcamp2.Text);
            multi = valor1 * valor2;
            lblresul.Text = Convert.ToString(multi);
            
            
        }

        private void btndivi_Click(object sender, EventArgs e)
        {

            //OBTEM DIVISÃO DOS NÚMEROS

            Double multi, valor1, valor2;


            valor1 = Convert.ToDouble(textcamp1.Text);
            valor2 = Convert.ToDouble(textcamp2.Text);
            multi = valor1 / valor2;
            lblresul.Text = Convert.ToString(multi);
        }

        private void btnsub_Click(object sender, EventArgs e)
        {

            //OBTEM SUBTRAÇÃO NÚMEROS

            Double multi, valor1, valor2;


            valor1 = Convert.ToDouble(textcamp1.Text);
            valor2 = Convert.ToDouble(textcamp2.Text);
            multi = valor1 - valor2;
            lblresul.Text = Convert.ToString(multi);
        }

        private void btnadi_Click(object sender, EventArgs e)
        {

            //OBTEM ADIÇÃO DOS NÚMEROS

            Double multi, valor1, valor2;

      
            valor1 = Convert.ToDouble(textcamp1.Text);
            valor2 = Convert.ToDouble(textcamp2.Text);
            multi = valor1 + valor2;
            lblresul.Text = Convert.ToString(multi);
        }

        private void button1_Click(object sender, EventArgs e)
        {

            //CALCULA POTENCIAÇÃO

            Double multi, valor1, valor2;

            valor1 = Convert.ToDouble(textcamp1.Text);
            valor2 = Convert.ToDouble(textcamp2.Text);
            multi = Convert.ToDouble(Math.Pow(valor1, valor2));
            lblresul.Text = Convert.ToString(multi);
        }

        private void btnraiz_Click(object sender, EventArgs e)
        {

            //OBTEM O VALOR DA RAIZ

            Double multi, valor1;

            valor1 = Convert.ToDouble(textcamp1.Text);
            multi = Convert.ToDouble(Math.Sqrt(valor1));
            lblresul.Text = Convert.ToString(multi);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {

            //CALCULA VALOR DE PI

            Double multi, valor1;

            valor1 = Convert.ToDouble(textcamp1.Text);
            multi = Convert.ToDouble(Math.Sqrt(valor1));
            lblresul.Text = Convert.ToString(multi);

        }
    }
    }

