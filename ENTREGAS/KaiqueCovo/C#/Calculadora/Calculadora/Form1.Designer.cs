﻿namespace Calculadora
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.textcamp1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textcamp2 = new System.Windows.Forms.TextBox();
            this.btnmulti = new System.Windows.Forms.Button();
            this.btnsub = new System.Windows.Forms.Button();
            this.btnadi = new System.Windows.Forms.Button();
            this.btndivi = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.lblresul = new System.Windows.Forms.Label();
            this.btnpoten = new System.Windows.Forms.Button();
            this.btnraiz = new System.Windows.Forms.Button();
            this.btnpi = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textcamp1
            // 
            this.textcamp1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textcamp1.Location = new System.Drawing.Point(12, 68);
            this.textcamp1.Name = "textcamp1";
            this.textcamp1.Size = new System.Drawing.Size(100, 20);
            this.textcamp1.TabIndex = 0;
            this.textcamp1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(79, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "Calculadora";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Primeiro nº";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Segundo nº";
            // 
            // textcamp2
            // 
            this.textcamp2.Location = new System.Drawing.Point(12, 114);
            this.textcamp2.Name = "textcamp2";
            this.textcamp2.Size = new System.Drawing.Size(100, 20);
            this.textcamp2.TabIndex = 4;
            this.textcamp2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // btnmulti
            // 
            this.btnmulti.Location = new System.Drawing.Point(154, 65);
            this.btnmulti.Name = "btnmulti";
            this.btnmulti.Size = new System.Drawing.Size(56, 23);
            this.btnmulti.TabIndex = 5;
            this.btnmulti.Text = "X";
            this.btnmulti.UseVisualStyleBackColor = true;
            this.btnmulti.Click += new System.EventHandler(this.btnmulti_Click);
            // 
            // btnsub
            // 
            this.btnsub.Location = new System.Drawing.Point(154, 97);
            this.btnsub.Name = "btnsub";
            this.btnsub.Size = new System.Drawing.Size(56, 23);
            this.btnsub.TabIndex = 6;
            this.btnsub.Text = "-";
            this.btnsub.UseVisualStyleBackColor = true;
            this.btnsub.Click += new System.EventHandler(this.btnsub_Click);
            // 
            // btnadi
            // 
            this.btnadi.Location = new System.Drawing.Point(216, 97);
            this.btnadi.Name = "btnadi";
            this.btnadi.Size = new System.Drawing.Size(56, 23);
            this.btnadi.TabIndex = 7;
            this.btnadi.Text = "+";
            this.btnadi.UseVisualStyleBackColor = true;
            this.btnadi.Click += new System.EventHandler(this.btnadi_Click);
            // 
            // btndivi
            // 
            this.btndivi.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btndivi.Location = new System.Drawing.Point(216, 66);
            this.btndivi.Name = "btndivi";
            this.btndivi.Size = new System.Drawing.Size(56, 23);
            this.btndivi.TabIndex = 8;
            this.btndivi.Text = "/";
            this.btndivi.UseVisualStyleBackColor = false;
            this.btndivi.Click += new System.EventHandler(this.btndivi_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 228);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 24);
            this.label4.TabIndex = 9;
            this.label4.Text = "Resultado :";
            // 
            // lblresul
            // 
            this.lblresul.AutoSize = true;
            this.lblresul.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblresul.Location = new System.Drawing.Point(150, 232);
            this.lblresul.Name = "lblresul";
            this.lblresul.Size = new System.Drawing.Size(0, 20);
            this.lblresul.TabIndex = 10;
            this.lblresul.Click += new System.EventHandler(this.label5_Click);
            // 
            // btnpoten
            // 
            this.btnpoten.Location = new System.Drawing.Point(154, 127);
            this.btnpoten.Name = "btnpoten";
            this.btnpoten.Size = new System.Drawing.Size(56, 23);
            this.btnpoten.TabIndex = 11;
            this.btnpoten.Text = "x²";
            this.btnpoten.UseVisualStyleBackColor = true;
            this.btnpoten.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnraiz
            // 
            this.btnraiz.Location = new System.Drawing.Point(216, 127);
            this.btnraiz.Name = "btnraiz";
            this.btnraiz.Size = new System.Drawing.Size(55, 23);
            this.btnraiz.TabIndex = 12;
            this.btnraiz.Text = "Raiz";
            this.btnraiz.UseVisualStyleBackColor = true;
            this.btnraiz.Click += new System.EventHandler(this.btnraiz_Click);
            // 
            // btnpi
            // 
            this.btnpi.Location = new System.Drawing.Point(154, 157);
            this.btnpi.Name = "btnpi";
            this.btnpi.Size = new System.Drawing.Size(117, 23);
            this.btnpi.TabIndex = 13;
            this.btnpi.Text = "VALOR DE PI";
            this.btnpi.UseVisualStyleBackColor = true;
            this.btnpi.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnpi);
            this.Controls.Add(this.btnraiz);
            this.Controls.Add(this.btnpoten);
            this.Controls.Add(this.lblresul);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btndivi);
            this.Controls.Add(this.btnadi);
            this.Controls.Add(this.btnsub);
            this.Controls.Add(this.btnmulti);
            this.Controls.Add(this.textcamp2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textcamp1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textcamp1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textcamp2;
        private System.Windows.Forms.Button btnmulti;
        private System.Windows.Forms.Button btnsub;
        private System.Windows.Forms.Button btnadi;
        private System.Windows.Forms.Button btndivi;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblresul;
        private System.Windows.Forms.Button btnpoten;
        private System.Windows.Forms.Button btnraiz;
        private System.Windows.Forms.Button btnpi;
    }
}

