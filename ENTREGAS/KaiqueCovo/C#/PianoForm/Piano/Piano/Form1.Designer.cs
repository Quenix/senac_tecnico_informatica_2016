﻿namespace Piano
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnc = new System.Windows.Forms.Button();
            this.btnd = new System.Windows.Forms.Button();
            this.btne = new System.Windows.Forms.Button();
            this.btnf = new System.Windows.Forms.Button();
            this.btng = new System.Windows.Forms.Button();
            this.btna = new System.Windows.Forms.Button();
            this.btnb = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnc
            // 
            this.btnc.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnc.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnc.Location = new System.Drawing.Point(27, 105);
            this.btnc.Name = "btnc";
            this.btnc.Size = new System.Drawing.Size(27, 118);
            this.btnc.TabIndex = 0;
            this.btnc.UseVisualStyleBackColor = false;
            this.btnc.Click += new System.EventHandler(this.btn_Click);
            // 
            // btnd
            // 
            this.btnd.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnd.ForeColor = System.Drawing.SystemColors.Menu;
            this.btnd.Location = new System.Drawing.Point(53, 105);
            this.btnd.Name = "btnd";
            this.btnd.Size = new System.Drawing.Size(27, 118);
            this.btnd.TabIndex = 1;
            this.btnd.UseVisualStyleBackColor = false;
            this.btnd.Click += new System.EventHandler(this.button1_Click);
            // 
            // btne
            // 
            this.btne.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btne.Location = new System.Drawing.Point(79, 106);
            this.btne.Name = "btne";
            this.btne.Size = new System.Drawing.Size(27, 117);
            this.btne.TabIndex = 2;
            this.btne.UseVisualStyleBackColor = false;
            this.btne.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnf
            // 
            this.btnf.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnf.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnf.Location = new System.Drawing.Point(105, 106);
            this.btnf.Name = "btnf";
            this.btnf.Size = new System.Drawing.Size(27, 117);
            this.btnf.TabIndex = 3;
            this.btnf.UseVisualStyleBackColor = false;
            this.btnf.Click += new System.EventHandler(this.button3_Click);
            // 
            // btng
            // 
            this.btng.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btng.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btng.Location = new System.Drawing.Point(131, 106);
            this.btng.Name = "btng";
            this.btng.Size = new System.Drawing.Size(27, 117);
            this.btng.TabIndex = 4;
            this.btng.UseVisualStyleBackColor = false;
            this.btng.Click += new System.EventHandler(this.button4_Click);
            // 
            // btna
            // 
            this.btna.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btna.Location = new System.Drawing.Point(157, 106);
            this.btna.Name = "btna";
            this.btna.Size = new System.Drawing.Size(26, 117);
            this.btna.TabIndex = 5;
            this.btna.UseVisualStyleBackColor = false;
            this.btna.Click += new System.EventHandler(this.button5_Click);
            // 
            // btnb
            // 
            this.btnb.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnb.Location = new System.Drawing.Point(182, 105);
            this.btnb.Name = "btnb";
            this.btnb.Size = new System.Drawing.Size(27, 117);
            this.btnb.TabIndex = 6;
            this.btnb.UseVisualStyleBackColor = false;
            this.btnb.Click += new System.EventHandler(this.button6_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.Location = new System.Drawing.Point(45, 76);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(21, 92);
            this.button1.TabIndex = 7;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button2.Location = new System.Drawing.Point(72, 76);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(21, 92);
            this.button2.TabIndex = 8;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button3.Location = new System.Drawing.Point(123, 76);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(21, 92);
            this.button3.TabIndex = 9;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button4.Location = new System.Drawing.Point(175, 76);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(21, 92);
            this.button4.TabIndex = 10;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button5.Location = new System.Drawing.Point(148, 76);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(21, 92);
            this.button5.TabIndex = 11;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnb);
            this.Controls.Add(this.btna);
            this.Controls.Add(this.btng);
            this.Controls.Add(this.btnf);
            this.Controls.Add(this.btne);
            this.Controls.Add(this.btnd);
            this.Controls.Add(this.btnc);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnc;
        private System.Windows.Forms.Button btnd;
        private System.Windows.Forms.Button btne;
        private System.Windows.Forms.Button btnf;
        private System.Windows.Forms.Button btng;
        private System.Windows.Forms.Button btna;
        private System.Windows.Forms.Button btnb;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
    }
}

