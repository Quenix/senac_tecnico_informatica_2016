﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Menu
{
    public partial class MenorMaior : Form
    {
        Double Valor1;
        public MenorMaior()
        {
            InitializeComponent();
        }

        private void btnmostrar_Click(object sender, EventArgs e)
        {
            

            try {
                Valor1 = Convert.ToInt32(txtid.Text);
            }
            catch
            {
                MessageBox.Show("Digite Apenas números");
            }

            if (Valor1 >= 18)
            {
                lblmostrar.Text ="Seu nome é "+txtnome.Text+ ". Você tem " + Valor1 + " anos, maior de idade ";


            }
            else
            {
                lblmostrar.Text = "Seu nome é " + txtnome.Text + ". Você tem " + Valor1 + " anos, menor de idade";
            }
        }
    }
}
