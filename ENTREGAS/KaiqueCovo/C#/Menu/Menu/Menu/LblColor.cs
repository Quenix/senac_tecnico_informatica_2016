﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Menu
{
    public partial class LblColor : Form
    {
        Boolean cor;
        public LblColor()
        {
            InitializeComponent();
        }

        private void btnrosa_Click(object sender, EventArgs e)
        {
            lblexemplo.ForeColor = System.Drawing.Color.Fuchsia;
            lblexemplo.Text = "Você Clicou no Rosa";
            
           
        }

        private void btnblue_Click(object sender, EventArgs e)
        {
            lblexemplo.ForeColor = System.Drawing.Color.Blue;
            lblexemplo.Text = "Você Clicou no Azul";
        }

        private void btnblack_Click(object sender, EventArgs e)
        {

            lblexemplo.ForeColor = System.Drawing.Color.Black;
            lblexemplo.Text = "Você Clicou no Preto";
        }

    }
}
