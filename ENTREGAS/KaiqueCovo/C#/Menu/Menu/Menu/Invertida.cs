﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Menu
{
    public partial class Invertida : Form
    {
        public Invertida()
        {
          

            InitializeComponent();
        }

        private void btninverter_Click(object sender, EventArgs e)
        {
            char[] inverter = txtinverter.Text.ToCharArray();
            Array.Reverse(inverter);
            string mostrar = new string(inverter);
            txtresul.Text = mostrar;
            
            
   
        }

        private void txtinverter_TextChanged(object sender, EventArgs e)
        {
            
          
        }
    }
}
