﻿namespace Menu
{
    partial class Invertida
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtinverter = new System.Windows.Forms.TextBox();
            this.btninverter = new System.Windows.Forms.Button();
            this.lblresul = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtresul = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtinverter
            // 
            this.txtinverter.Location = new System.Drawing.Point(92, 39);
            this.txtinverter.Name = "txtinverter";
            this.txtinverter.Size = new System.Drawing.Size(100, 20);
            this.txtinverter.TabIndex = 0;
            this.txtinverter.TextChanged += new System.EventHandler(this.txtinverter_TextChanged);
            // 
            // btninverter
            // 
            this.btninverter.Location = new System.Drawing.Point(47, 90);
            this.btninverter.Name = "btninverter";
            this.btninverter.Size = new System.Drawing.Size(150, 25);
            this.btninverter.TabIndex = 1;
            this.btninverter.Text = "Inverter";
            this.btninverter.UseVisualStyleBackColor = true;
            this.btninverter.Click += new System.EventHandler(this.btninverter_Click);
            // 
            // lblresul
            // 
            this.lblresul.AutoSize = true;
            this.lblresul.Location = new System.Drawing.Point(44, 146);
            this.lblresul.Name = "lblresul";
            this.lblresul.Size = new System.Drawing.Size(42, 13);
            this.lblresul.TabIndex = 2;
            this.lblresul.Text = "Inverso";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Frase";
            // 
            // txtresul
            // 
            this.txtresul.Location = new System.Drawing.Point(92, 143);
            this.txtresul.Name = "txtresul";
            this.txtresul.Size = new System.Drawing.Size(100, 20);
            this.txtresul.TabIndex = 4;
            // 
            // Invertida
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.txtresul);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblresul);
            this.Controls.Add(this.btninverter);
            this.Controls.Add(this.txtinverter);
            this.Name = "Invertida";
            this.Text = "Invertida";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtinverter;
        private System.Windows.Forms.Button btninverter;
        private System.Windows.Forms.Label lblresul;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtresul;
    }
}