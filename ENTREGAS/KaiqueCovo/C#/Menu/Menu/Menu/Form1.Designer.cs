﻿namespace Menu
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.calculadoraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novoCadastroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unidadesCadastradasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.softwareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maiorOuMenorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelColoridaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stringInvertidaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.calculadoraToolStripMenuItem,
            this.softwareToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(284, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // calculadoraToolStripMenuItem
            // 
            this.calculadoraToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoCadastroToolStripMenuItem,
            this.unidadesCadastradasToolStripMenuItem});
            this.calculadoraToolStripMenuItem.Name = "calculadoraToolStripMenuItem";
            this.calculadoraToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.calculadoraToolStripMenuItem.Text = "Cadastro";
            // 
            // novoCadastroToolStripMenuItem
            // 
            this.novoCadastroToolStripMenuItem.Name = "novoCadastroToolStripMenuItem";
            this.novoCadastroToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.novoCadastroToolStripMenuItem.Text = "Novo Cadastro";
            this.novoCadastroToolStripMenuItem.Click += new System.EventHandler(this.novoCadastroToolStripMenuItem_Click);
            // 
            // unidadesCadastradasToolStripMenuItem
            // 
            this.unidadesCadastradasToolStripMenuItem.Name = "unidadesCadastradasToolStripMenuItem";
            this.unidadesCadastradasToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.unidadesCadastradasToolStripMenuItem.Text = "Unidades Cadastradas";
            this.unidadesCadastradasToolStripMenuItem.Click += new System.EventHandler(this.unidadesCadastradasToolStripMenuItem_Click);
            // 
            // softwareToolStripMenuItem
            // 
            this.softwareToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.maiorOuMenorToolStripMenuItem,
            this.labelColoridaToolStripMenuItem,
            this.stringInvertidaToolStripMenuItem});
            this.softwareToolStripMenuItem.Name = "softwareToolStripMenuItem";
            this.softwareToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.softwareToolStripMenuItem.Text = "Software";
            // 
            // maiorOuMenorToolStripMenuItem
            // 
            this.maiorOuMenorToolStripMenuItem.Name = "maiorOuMenorToolStripMenuItem";
            this.maiorOuMenorToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.maiorOuMenorToolStripMenuItem.Text = "Maior ou menor ?";
            this.maiorOuMenorToolStripMenuItem.Click += new System.EventHandler(this.maiorOuMenorToolStripMenuItem_Click);
            // 
            // labelColoridaToolStripMenuItem
            // 
            this.labelColoridaToolStripMenuItem.Name = "labelColoridaToolStripMenuItem";
            this.labelColoridaToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.labelColoridaToolStripMenuItem.Text = "Label Colorida";
            this.labelColoridaToolStripMenuItem.Click += new System.EventHandler(this.labelColoridaToolStripMenuItem_Click);
            // 
            // stringInvertidaToolStripMenuItem
            // 
            this.stringInvertidaToolStripMenuItem.Name = "stringInvertidaToolStripMenuItem";
            this.stringInvertidaToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.stringInvertidaToolStripMenuItem.Text = "String Invertida";
            this.stringInvertidaToolStripMenuItem.Click += new System.EventHandler(this.stringInvertidaToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem calculadoraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novoCadastroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unidadesCadastradasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem softwareToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maiorOuMenorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem labelColoridaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stringInvertidaToolStripMenuItem;
    }
}

