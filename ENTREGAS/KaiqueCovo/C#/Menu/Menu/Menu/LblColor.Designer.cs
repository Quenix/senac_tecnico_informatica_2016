﻿namespace Menu
{
    partial class LblColor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblexemplo = new System.Windows.Forms.Label();
            this.btnrosa = new System.Windows.Forms.Button();
            this.btnblue = new System.Windows.Forms.Button();
            this.btnblack = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblexemplo
            // 
            this.lblexemplo.AutoSize = true;
            this.lblexemplo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblexemplo.Location = new System.Drawing.Point(12, 195);
            this.lblexemplo.Name = "lblexemplo";
            this.lblexemplo.Size = new System.Drawing.Size(232, 25);
            this.lblexemplo.TabIndex = 0;
            this.lblexemplo.Text = "ISSO MUDA DE COR";
            // 
            // btnrosa
            // 
            this.btnrosa.ForeColor = System.Drawing.Color.Fuchsia;
            this.btnrosa.Location = new System.Drawing.Point(179, 107);
            this.btnrosa.Name = "btnrosa";
            this.btnrosa.Size = new System.Drawing.Size(75, 23);
            this.btnrosa.TabIndex = 1;
            this.btnrosa.Text = "Rosa";
            this.btnrosa.UseVisualStyleBackColor = true;
            this.btnrosa.Click += new System.EventHandler(this.btnrosa_Click);
            // 
            // btnblue
            // 
            this.btnblue.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnblue.Location = new System.Drawing.Point(17, 107);
            this.btnblue.Name = "btnblue";
            this.btnblue.Size = new System.Drawing.Size(75, 23);
            this.btnblue.TabIndex = 2;
            this.btnblue.Text = "Azul";
            this.btnblue.UseVisualStyleBackColor = true;
            this.btnblue.Click += new System.EventHandler(this.btnblue_Click);
            // 
            // btnblack
            // 
            this.btnblack.Location = new System.Drawing.Point(98, 107);
            this.btnblack.Name = "btnblack";
            this.btnblack.Size = new System.Drawing.Size(75, 23);
            this.btnblack.TabIndex = 3;
            this.btnblack.Text = "Preto";
            this.btnblack.UseVisualStyleBackColor = true;
            this.btnblack.Click += new System.EventHandler(this.btnblack_Click);
            // 
            // LblColor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnblack);
            this.Controls.Add(this.btnblue);
            this.Controls.Add(this.btnrosa);
            this.Controls.Add(this.lblexemplo);
            this.Name = "LblColor";
            this.Text = "LblColor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblexemplo;
        private System.Windows.Forms.Button btnrosa;
        private System.Windows.Forms.Button btnblue;
        private System.Windows.Forms.Button btnblack;
    }
}