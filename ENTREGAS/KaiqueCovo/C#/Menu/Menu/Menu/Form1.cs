﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Menu
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void maiorOuMenorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var menormaior = new MenorMaior();
            menormaior.Show();

        }

        private void labelColoridaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var lblcolor = new LblColor();
            lblcolor.Show();
        }

        private void novoCadastroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Error: 8048, CONTATE UM PROGRAMADOR");
        }

        private void unidadesCadastradasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Error: 8048, CONTATE UM PROGRAMADOR");
        }

        private void stringInvertidaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var invertida = new Invertida();
            invertida.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("ERROR 8048: BOTÃO NÃO CADASTRADO");
        }
    }
}
