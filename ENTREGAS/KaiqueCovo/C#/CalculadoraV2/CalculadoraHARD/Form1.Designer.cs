﻿namespace CalculadoraHARD
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.ponto = new System.Windows.Forms.Button();
            this.clear = new System.Windows.Forms.Button();
            this.btnadi = new System.Windows.Forms.Button();
            this.btnsub = new System.Windows.Forms.Button();
            this.btnmulti = new System.Windows.Forms.Button();
            this.btndiv = new System.Windows.Forms.Button();
            this.txtresul = new System.Windows.Forms.TextBox();
            this.btnclear = new System.Windows.Forms.Button();
            this.lblale = new System.Windows.Forms.Label();
            this.ap = new System.Windows.Forms.Label();
            this.btnraiz = new System.Windows.Forms.Button();
            this.btnpot = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnporc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(12, 128);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(75, 23);
            this.btn1.TabIndex = 1;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(12, 157);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(75, 23);
            this.btn2.TabIndex = 1;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btn3
            // 
            this.btn3.Location = new System.Drawing.Point(12, 186);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(75, 23);
            this.btn3.TabIndex = 2;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = true;
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btn4
            // 
            this.btn4.Location = new System.Drawing.Point(12, 215);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(75, 23);
            this.btn4.TabIndex = 3;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = true;
            this.btn4.Click += new System.EventHandler(this.btn4_Click);
            // 
            // btn6
            // 
            this.btn6.Location = new System.Drawing.Point(93, 128);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(75, 23);
            this.btn6.TabIndex = 5;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = true;
            this.btn6.Click += new System.EventHandler(this.btn6_Click);
            // 
            // btn5
            // 
            this.btn5.Location = new System.Drawing.Point(12, 244);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(75, 23);
            this.btn5.TabIndex = 6;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = true;
            this.btn5.Click += new System.EventHandler(this.btn5_Click);
            // 
            // btn7
            // 
            this.btn7.Location = new System.Drawing.Point(93, 157);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(75, 23);
            this.btn7.TabIndex = 7;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = true;
            this.btn7.Click += new System.EventHandler(this.btn7_Click);
            // 
            // btn8
            // 
            this.btn8.Location = new System.Drawing.Point(93, 186);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(75, 23);
            this.btn8.TabIndex = 8;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = true;
            this.btn8.Click += new System.EventHandler(this.btn8_Click);
            // 
            // btn9
            // 
            this.btn9.Location = new System.Drawing.Point(93, 215);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(75, 23);
            this.btn9.TabIndex = 9;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = true;
            this.btn9.Click += new System.EventHandler(this.btn9_Click);
            // 
            // btn0
            // 
            this.btn0.Location = new System.Drawing.Point(94, 243);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(75, 23);
            this.btn0.TabIndex = 10;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = true;
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // ponto
            // 
            this.ponto.Location = new System.Drawing.Point(12, 271);
            this.ponto.Name = "ponto";
            this.ponto.Size = new System.Drawing.Size(75, 23);
            this.ponto.TabIndex = 11;
            this.ponto.Text = ".";
            this.ponto.UseVisualStyleBackColor = true;
            this.ponto.Click += new System.EventHandler(this.ponto_Click);
            // 
            // clear
            // 
            this.clear.Location = new System.Drawing.Point(94, 272);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(75, 23);
            this.clear.TabIndex = 12;
            this.clear.Text = "CLEAR";
            this.clear.UseVisualStyleBackColor = true;
            this.clear.Click += new System.EventHandler(this.clear_Click);
            // 
            // btnadi
            // 
            this.btnadi.Location = new System.Drawing.Point(252, 129);
            this.btnadi.Name = "btnadi";
            this.btnadi.Size = new System.Drawing.Size(53, 23);
            this.btnadi.TabIndex = 13;
            this.btnadi.Text = "+";
            this.btnadi.UseVisualStyleBackColor = true;
            this.btnadi.Click += new System.EventHandler(this.btnadi_Click);
            // 
            // btnsub
            // 
            this.btnsub.Location = new System.Drawing.Point(311, 129);
            this.btnsub.Name = "btnsub";
            this.btnsub.Size = new System.Drawing.Size(53, 23);
            this.btnsub.TabIndex = 14;
            this.btnsub.Text = "-";
            this.btnsub.UseVisualStyleBackColor = true;
            this.btnsub.Click += new System.EventHandler(this.btnsub_Click);
            // 
            // btnmulti
            // 
            this.btnmulti.Location = new System.Drawing.Point(252, 158);
            this.btnmulti.Name = "btnmulti";
            this.btnmulti.Size = new System.Drawing.Size(53, 23);
            this.btnmulti.TabIndex = 15;
            this.btnmulti.Text = "X";
            this.btnmulti.UseVisualStyleBackColor = true;
            this.btnmulti.Click += new System.EventHandler(this.btnmulti_Click);
            // 
            // btndiv
            // 
            this.btndiv.Location = new System.Drawing.Point(311, 158);
            this.btndiv.Name = "btndiv";
            this.btndiv.Size = new System.Drawing.Size(53, 23);
            this.btndiv.TabIndex = 16;
            this.btndiv.Text = "÷";
            this.btndiv.UseVisualStyleBackColor = true;
            this.btndiv.Click += new System.EventHandler(this.btndiv_Click);
            // 
            // txtresul
            // 
            this.txtresul.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtresul.Location = new System.Drawing.Point(110, 52);
            this.txtresul.Name = "txtresul";
            this.txtresul.Size = new System.Drawing.Size(172, 31);
            this.txtresul.TabIndex = 0;
            this.txtresul.TextChanged += new System.EventHandler(this.txtresul_TextChanged);
            this.txtresul.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtresul_KeyPress);
            this.txtresul.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtresul_KeyUp);
            // 
            // btnclear
            // 
            this.btnclear.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnclear.Location = new System.Drawing.Point(55, 311);
            this.btnclear.Name = "btnclear";
            this.btnclear.Size = new System.Drawing.Size(75, 23);
            this.btnclear.TabIndex = 18;
            this.btnclear.Text = "=";
            this.btnclear.UseVisualStyleBackColor = true;
            this.btnclear.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // lblale
            // 
            this.lblale.AutoSize = true;
            this.lblale.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblale.Location = new System.Drawing.Point(93, 64);
            this.lblale.Name = "lblale";
            this.lblale.Size = new System.Drawing.Size(0, 16);
            this.lblale.TabIndex = 19;
            this.lblale.Click += new System.EventHandler(this.label1_Click);
            // 
            // ap
            // 
            this.ap.AutoSize = true;
            this.ap.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ap.Location = new System.Drawing.Point(104, 28);
            this.ap.Name = "ap";
            this.ap.Size = new System.Drawing.Size(0, 13);
            this.ap.TabIndex = 20;
            // 
            // btnraiz
            // 
            this.btnraiz.Location = new System.Drawing.Point(252, 185);
            this.btnraiz.Name = "btnraiz";
            this.btnraiz.Size = new System.Drawing.Size(53, 23);
            this.btnraiz.TabIndex = 21;
            this.btnraiz.Text = "√ ";
            this.btnraiz.UseVisualStyleBackColor = true;
            this.btnraiz.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnpot
            // 
            this.btnpot.Location = new System.Drawing.Point(312, 185);
            this.btnpot.Name = "btnpot";
            this.btnpot.Size = new System.Drawing.Size(52, 23);
            this.btnpot.TabIndex = 22;
            this.btnpot.Text = "x²";
            this.btnpot.UseVisualStyleBackColor = true;
            this.btnpot.Click += new System.EventHandler(this.button3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(105, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 25);
            this.label1.TabIndex = 24;
            this.label1.Text = "CALCULADORA";
            // 
            // btnporc
            // 
            this.btnporc.Location = new System.Drawing.Point(252, 215);
            this.btnporc.Name = "btnporc";
            this.btnporc.Size = new System.Drawing.Size(53, 23);
            this.btnporc.TabIndex = 25;
            this.btnporc.Text = "%";
            this.btnporc.UseVisualStyleBackColor = true;
            this.btnporc.Click += new System.EventHandler(this.btnporc_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 361);
            this.Controls.Add(this.btnporc);
            this.Controls.Add(this.txtresul);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnpot);
            this.Controls.Add(this.btnraiz);
            this.Controls.Add(this.ap);
            this.Controls.Add(this.lblale);
            this.Controls.Add(this.btnclear);
            this.Controls.Add(this.btndiv);
            this.Controls.Add(this.btnmulti);
            this.Controls.Add(this.btnsub);
            this.Controls.Add(this.btnadi);
            this.Controls.Add(this.clear);
            this.Controls.Add(this.ponto);
            this.Controls.Add(this.btn0);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn7);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(400, 400);
            this.MinimumSize = new System.Drawing.Size(400, 400);
            this.Name = "Form1";
            this.Text = "menu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button ponto;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.Button btnadi;
        private System.Windows.Forms.Button btnsub;
        private System.Windows.Forms.Button btnmulti;
        private System.Windows.Forms.Button btndiv;
        private System.Windows.Forms.TextBox txtresul;
        private System.Windows.Forms.Button btnclear;
        private System.Windows.Forms.Label lblale;
        private System.Windows.Forms.Label ap;
        private System.Windows.Forms.Button btnraiz;
        private System.Windows.Forms.Button btnpot;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnporc;
    }
}

