﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculadoraHARD
{
    public partial class Form1 : Form
    {
        //VÁRIAVEIS

        Double Valor1, Valor2, guarde1, guarde2, resultado;
        String Op;
        Boolean teclado;
       

        public Form1()
        {

            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtresul.Text += "1";
        }

        private void txtresul_TextChanged(object sender, EventArgs e)
        {

     
        }
        /* \/ \/ \/  NÚMEROS DO TECLADO DA CALC  \/ \/ \/ */

        private void btn2_Click(object sender, EventArgs e)
        {
            txtresul.Text += "2";
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            txtresul.Text += "3";
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            txtresul.Text += "4";
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            txtresul.Text += "5";
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            txtresul.Text += "6";
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            txtresul.Text += "7";
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            txtresul.Text += "8";
        }

        /* /\ /\ /\ NÚMEROS DO TECLADO CALC /\ /\ /\ */

        private void btnsub_Click(object sender, EventArgs e)
        {
            //VALIDA O TECLADO

            if (teclado)
            {
                txtresul.Text = txtresul.Text.Remove(txtresul.Text.Length - 1);
            }

            //SUBTRAÇÃO

            Valor1 = Convert.ToDouble(txtresul.Text);
            guarde1 = Valor1;
            txtresul.Text = "";
            Op = "sub";
        }

        private void btnmulti_Click(object sender, EventArgs e)
        {
            //VALIDA O TECLADO

            if (teclado)
            {
                txtresul.Text = txtresul.Text.Remove(txtresul.Text.Length - 1);
            }

            //MULTIPLICAÇÃO

            Valor1 = Convert.ToDouble(txtresul.Text);
            guarde1 = Valor1;
            txtresul.Text = "";
            lblale.Text = "X";
            ap.Text = Convert.ToString(guarde1 + " x ");
            Op = "multi";
        }

        private void btndiv_Click(object sender, EventArgs e)
        {
            //VALIDA O TECLADO

            if (teclado)
            {
                txtresul.Text = txtresul.Text.Remove(txtresul.Text.Length - 1);
            }

            //DIVISÃO

            Valor1 = Convert.ToDouble(txtresul.Text);
            guarde1 = Valor1;
            txtresul.Text = "";
            lblale.Text = "÷";
            Op = "divi";
        }

        private void clear_Click(object sender, EventArgs e)
        {   
            //VALIDA O TECLADO

            if (teclado)
            {
                txtresul.Text = txtresul.Text.Remove(txtresul.Text.Length - 1);
            }

            //CLEAR
            txtresul.Text = " ";
            lblale.Text = " ";
            ap.Text = " ";
        }

        private void ponto_Click(object sender, EventArgs e)
        {
            txtresul.Text += ",";
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //RAIZ
           
            Valor1 = Convert.ToDouble(txtresul.Text);
            guarde1 = Valor1;
            txtresul.Text = "";
            lblale.Text = "√";
            Op = "raiz";
        }

        private void button3_Click(object sender, EventArgs e)
        {
           //POTENCIA

            Valor1 = Convert.ToDouble(txtresul.Text);
            guarde1 = Valor1;
            txtresul.Text = "";
            lblale.Text = "x²";
            Op = "potencial";
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void txtresul_KeyUp(object sender, KeyEventArgs e)
        {
            //CONTROLA ATIVIDADE DAS TECLAS +, -, *, /, ENTER, ESC

            if (e.KeyCode == Keys.Add || e.KeyCode == Keys.Oemplus)
            {
                teclado = true;
                btnadi_Click(this, null);
            }
            if (e.KeyCode == Keys.Subtract || e.KeyCode == Keys.OemMinus)
            {
                teclado = true;
                btnsub_Click(this, null);
            }
            if (e.KeyCode == Keys.Multiply)
            {
                teclado = true;
                btnmulti_Click(this, null);
            }
            if(e.KeyCode == Keys.Divide)
            {
                teclado = true;
                btndiv_Click(this, null);
            }
            if(e.KeyCode == Keys.Enter)
            {
                teclado = true;
                button1_Click_1(this, null);
            }
            if(e.KeyCode == Keys.Escape)
            {
                teclado = true;
                clear_Click(this, null);
            }
         
            
        }

        private void txtresul_KeyPress(object sender, KeyPressEventArgs e)
        {
            // CONTROLE DE ATIVIDADE DENTRO DA TEXTBOX, ACEITA SOMENTE VIRGULA E NÚMERO

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void btnporc_Click(object sender, EventArgs e)
        {
            //PORCENTAGEM

            Valor1 = Convert.ToDouble(txtresul.Text);
            guarde1 = Valor1;
            txtresul.Text = "";
            lblale.Text = "%";
            Op = "porcentagem";
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            txtresul.Text += "9";
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //VALIDAÇÃO DO SEGUNDO NÚMERO DIGITADO E FAZ A OPERAÇÃO

            if (Op == "soma")
            {
                Valor2 = Convert.ToDouble(txtresul.Text);
                guarde2 = Valor2;
                txtresul.Text = "";
                lblale.Text = "+";
                ap.Text = Convert.ToString(guarde1 + " + " + guarde2 + " = ");
                resultado = (guarde1 + guarde2);
                txtresul.Text = Convert.ToString(resultado);
            }
            if (Op == "sub")
            {
                Valor2 = Convert.ToDouble(txtresul.Text);
                guarde2 = Valor2;
                txtresul.Text = "";
                lblale.Text = "-";
                ap.Text = Convert.ToString(guarde1 + " - " + guarde2 + " = ");
                resultado = (guarde1 - guarde2);
                txtresul.Text = Convert.ToString(resultado);
            }
            if (Op == "multi")
            {
                Valor2 = Convert.ToDouble(txtresul.Text);
                guarde2 = Valor2;
                txtresul.Text = "";
                lblale.Text = "X";
                ap.Text = Convert.ToString(guarde1 + " x " + guarde2 + " = ");
                resultado = (guarde1 * guarde2);
                txtresul.Text = Convert.ToString(resultado);
            }
            if (Op == "divi")
            {
                Valor2 = Convert.ToDouble(txtresul.Text);
                guarde2 = Valor2;
                txtresul.Text = "";
                lblale.Text = "÷";
                ap.Text = Convert.ToString(guarde1 + " ÷ " + guarde2 + " = ");
                resultado = (guarde1 / guarde2);
                txtresul.Text = Convert.ToString(resultado);

            }
            if (Op == "raiz")
            {
                Valor2 = Convert.ToDouble(txtresul.Text);
                guarde2 = Valor2;
                txtresul.Text = "";
                lblale.Text = "√";
                ap.Text = Convert.ToString(guarde1 + " √ " + guarde2 + " = ");
                resultado = Convert.ToDouble(Math.Sqrt(Valor1));
                txtresul.Text = Convert.ToString(resultado);
            }
            if (Op == "potencial")
            {
                Valor2 = Convert.ToDouble(txtresul.Text);
                guarde2 = Valor2;
                ap.Text = Convert.ToString(guarde1 + " ^ " + guarde2 + " = ");
                resultado = Convert.ToDouble(Math.Pow(Valor1, Valor2));
                txtresul.Text = Convert.ToString(resultado);
            }
            if (txtresul.Text == "∞")
            {
                txtresul.Text = "Deu merda";
            }
            if(Op == "porcentagem")
            {
                Valor2 = Convert.ToDouble(txtresul.Text);
                guarde2 = Valor2;
                ap.Text = Convert.ToString(guarde1 + "%" + guarde2);
                resultado = Convert.ToDouble((guarde1 * guarde2) / 100);
                lblale.Text = "=";
                txtresul.Text = Convert.ToString(resultado);
            }
           
        }



        private void btn0_Click(object sender, EventArgs e)
        {
            txtresul.Text += "0";
        }

        private void btnadi_Click(object sender, EventArgs e)
        {
            //SOMA

            if (teclado)
            {
                txtresul.Text = txtresul.Text.Remove(txtresul.Text.Length - 1);
            }
            Valor1 = Convert.ToDouble(txtresul.Text);
            guarde1 = Valor1;
            txtresul.Text = "";
            Op = "soma";




        }

    }
}

