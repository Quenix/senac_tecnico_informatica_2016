﻿namespace IMC
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtpeso = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.calc = new System.Windows.Forms.Button();
            this.lblalt = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtalt = new System.Windows.Forms.TextBox();
            this.txtid = new System.Windows.Forms.TextBox();
            this.lblresul = new System.Windows.Forms.Label();
            this.lblexp = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.chekm = new System.Windows.Forms.CheckBox();
            this.chekf = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cheklu = new System.Windows.Forms.CheckBox();
            this.chekmo = new System.Windows.Forms.CheckBox();
            this.cheks = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // txtpeso
            // 
            this.txtpeso.Location = new System.Drawing.Point(52, 60);
            this.txtpeso.Name = "txtpeso";
            this.txtpeso.Size = new System.Drawing.Size(81, 20);
            this.txtpeso.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Peso";
            // 
            // calc
            // 
            this.calc.Location = new System.Drawing.Point(94, 139);
            this.calc.Name = "calc";
            this.calc.Size = new System.Drawing.Size(100, 37);
            this.calc.TabIndex = 2;
            this.calc.Text = "Calcular";
            this.calc.UseVisualStyleBackColor = true;
            this.calc.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblalt
            // 
            this.lblalt.AutoSize = true;
            this.lblalt.Location = new System.Drawing.Point(13, 89);
            this.lblalt.Name = "lblalt";
            this.lblalt.Size = new System.Drawing.Size(34, 13);
            this.lblalt.TabIndex = 7;
            this.lblalt.Text = "Altura";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Idade";
            // 
            // txtalt
            // 
            this.txtalt.Location = new System.Drawing.Point(52, 86);
            this.txtalt.Name = "txtalt";
            this.txtalt.Size = new System.Drawing.Size(62, 20);
            this.txtalt.TabIndex = 9;
            // 
            // txtid
            // 
            this.txtid.Location = new System.Drawing.Point(52, 113);
            this.txtid.Name = "txtid";
            this.txtid.Size = new System.Drawing.Size(34, 20);
            this.txtid.TabIndex = 10;
            // 
            // lblresul
            // 
            this.lblresul.AutoSize = true;
            this.lblresul.Location = new System.Drawing.Point(95, 197);
            this.lblresul.Name = "lblresul";
            this.lblresul.Size = new System.Drawing.Size(19, 13);
            this.lblresul.TabIndex = 14;
            this.lblresul.Text = "....";
            // 
            // lblexp
            // 
            this.lblexp.AutoSize = true;
            this.lblexp.Location = new System.Drawing.Point(120, 239);
            this.lblexp.Name = "lblexp";
            this.lblexp.Size = new System.Drawing.Size(13, 13);
            this.lblexp.TabIndex = 15;
            this.lblexp.Text = "..";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(28, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "SEQSU";
            // 
            // chekm
            // 
            this.chekm.AutoSize = true;
            this.chekm.Location = new System.Drawing.Point(12, 27);
            this.chekm.Name = "chekm";
            this.chekm.Size = new System.Drawing.Size(35, 17);
            this.chekm.TabIndex = 12;
            this.chekm.Text = "M";
            this.chekm.UseVisualStyleBackColor = true;
            // 
            // chekf
            // 
            this.chekf.AutoSize = true;
            this.chekf.Location = new System.Drawing.Point(56, 27);
            this.chekf.Name = "chekf";
            this.chekf.Size = new System.Drawing.Size(32, 17);
            this.chekf.TabIndex = 13;
            this.chekf.Text = "F";
            this.chekf.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(180, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Atividade Diária";
            // 
            // cheklu
            // 
            this.cheklu.AutoSize = true;
            this.cheklu.Location = new System.Drawing.Point(183, 108);
            this.cheklu.Name = "cheklu";
            this.cheklu.Size = new System.Drawing.Size(55, 17);
            this.cheklu.TabIndex = 6;
            this.cheklu.Text = "Lucas";
            this.cheklu.UseVisualStyleBackColor = true;
            // 
            // chekmo
            // 
            this.chekmo.AutoSize = true;
            this.chekmo.Location = new System.Drawing.Point(183, 85);
            this.chekmo.Name = "chekmo";
            this.chekmo.Size = new System.Drawing.Size(74, 17);
            this.chekmo.TabIndex = 5;
            this.chekmo.Text = "Moderada";
            this.chekmo.UseVisualStyleBackColor = true;
            // 
            // cheks
            // 
            this.cheks.AutoSize = true;
            this.cheks.Location = new System.Drawing.Point(183, 60);
            this.cheks.Name = "cheks";
            this.cheks.Size = new System.Drawing.Size(77, 17);
            this.cheks.TabIndex = 4;
            this.cheks.Text = "Sedentario";
            this.cheks.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.lblexp);
            this.Controls.Add(this.lblresul);
            this.Controls.Add(this.chekf);
            this.Controls.Add(this.chekm);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtid);
            this.Controls.Add(this.txtalt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblalt);
            this.Controls.Add(this.cheklu);
            this.Controls.Add(this.chekmo);
            this.Controls.Add(this.cheks);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.calc);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtpeso);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtpeso;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button calc;
        private System.Windows.Forms.Label lblalt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtalt;
        private System.Windows.Forms.TextBox txtid;
        private System.Windows.Forms.Label lblresul;
        private System.Windows.Forms.Label lblexp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chekm;
        private System.Windows.Forms.CheckBox chekf;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox cheklu;
        private System.Windows.Forms.CheckBox chekmo;
        private System.Windows.Forms.CheckBox cheks;
    }
}

