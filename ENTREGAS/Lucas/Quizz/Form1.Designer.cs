﻿namespace Quizz
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pergunta1_resposta3 = new System.Windows.Forms.RadioButton();
            this.pergunta1_resposta2 = new System.Windows.Forms.RadioButton();
            this.pergunta1_resposta1 = new System.Windows.Forms.RadioButton();
            this.lblPergunta1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pergunta2_resposta3 = new System.Windows.Forms.RadioButton();
            this.pergunta2_resposta2 = new System.Windows.Forms.RadioButton();
            this.pergunta2_resposta1 = new System.Windows.Forms.RadioButton();
            this.lblPergunta2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pergunta1_resposta3);
            this.groupBox1.Controls.Add(this.pergunta1_resposta2);
            this.groupBox1.Controls.Add(this.pergunta1_resposta1);
            this.groupBox1.Controls.Add(this.lblPergunta1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(265, 298);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pergunta 1";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // pergunta1_resposta3
            // 
            this.pergunta1_resposta3.AutoSize = true;
            this.pergunta1_resposta3.Location = new System.Drawing.Point(6, 144);
            this.pergunta1_resposta3.Name = "pergunta1_resposta3";
            this.pergunta1_resposta3.Size = new System.Drawing.Size(85, 17);
            this.pergunta1_resposta3.TabIndex = 3;
            this.pergunta1_resposta3.TabStop = true;
            this.pergunta1_resposta3.Text = "radioButton3";
            this.pergunta1_resposta3.UseVisualStyleBackColor = true;
            // 
            // pergunta1_resposta2
            // 
            this.pergunta1_resposta2.AutoSize = true;
            this.pergunta1_resposta2.Location = new System.Drawing.Point(6, 121);
            this.pergunta1_resposta2.Name = "pergunta1_resposta2";
            this.pergunta1_resposta2.Size = new System.Drawing.Size(85, 17);
            this.pergunta1_resposta2.TabIndex = 2;
            this.pergunta1_resposta2.TabStop = true;
            this.pergunta1_resposta2.Text = "radioButton2";
            this.pergunta1_resposta2.UseVisualStyleBackColor = true;
            // 
            // pergunta1_resposta1
            // 
            this.pergunta1_resposta1.AutoSize = true;
            this.pergunta1_resposta1.Location = new System.Drawing.Point(6, 98);
            this.pergunta1_resposta1.Name = "pergunta1_resposta1";
            this.pergunta1_resposta1.Size = new System.Drawing.Size(85, 17);
            this.pergunta1_resposta1.TabIndex = 1;
            this.pergunta1_resposta1.TabStop = true;
            this.pergunta1_resposta1.Text = "radioButton1";
            this.pergunta1_resposta1.UseVisualStyleBackColor = true;
            // 
            // lblPergunta1
            // 
            this.lblPergunta1.AutoSize = true;
            this.lblPergunta1.Location = new System.Drawing.Point(7, 20);
            this.lblPergunta1.Name = "lblPergunta1";
            this.lblPergunta1.Size = new System.Drawing.Size(35, 13);
            this.lblPergunta1.TabIndex = 0;
            this.lblPergunta1.Text = "label1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pergunta2_resposta3);
            this.groupBox2.Controls.Add(this.pergunta2_resposta2);
            this.groupBox2.Controls.Add(this.pergunta2_resposta1);
            this.groupBox2.Controls.Add(this.lblPergunta2);
            this.groupBox2.Location = new System.Drawing.Point(283, 12);
            this.groupBox2.MaximumSize = new System.Drawing.Size(432, 298);
            this.groupBox2.MinimumSize = new System.Drawing.Size(432, 298);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(432, 298);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pergunta 2";
            // 
            // pergunta2_resposta3
            // 
            this.pergunta2_resposta3.AutoSize = true;
            this.pergunta2_resposta3.Location = new System.Drawing.Point(6, 144);
            this.pergunta2_resposta3.Name = "pergunta2_resposta3";
            this.pergunta2_resposta3.Size = new System.Drawing.Size(85, 17);
            this.pergunta2_resposta3.TabIndex = 3;
            this.pergunta2_resposta3.TabStop = true;
            this.pergunta2_resposta3.Text = "radioButton3";
            this.pergunta2_resposta3.UseVisualStyleBackColor = true;
            // 
            // pergunta2_resposta2
            // 
            this.pergunta2_resposta2.AutoSize = true;
            this.pergunta2_resposta2.Location = new System.Drawing.Point(6, 121);
            this.pergunta2_resposta2.Name = "pergunta2_resposta2";
            this.pergunta2_resposta2.Size = new System.Drawing.Size(85, 17);
            this.pergunta2_resposta2.TabIndex = 2;
            this.pergunta2_resposta2.TabStop = true;
            this.pergunta2_resposta2.Text = "radioButton2";
            this.pergunta2_resposta2.UseVisualStyleBackColor = true;
            // 
            // pergunta2_resposta1
            // 
            this.pergunta2_resposta1.AutoSize = true;
            this.pergunta2_resposta1.Location = new System.Drawing.Point(6, 98);
            this.pergunta2_resposta1.Name = "pergunta2_resposta1";
            this.pergunta2_resposta1.Size = new System.Drawing.Size(85, 17);
            this.pergunta2_resposta1.TabIndex = 1;
            this.pergunta2_resposta1.TabStop = true;
            this.pergunta2_resposta1.Text = "radioButton1";
            this.pergunta2_resposta1.UseVisualStyleBackColor = true;
            // 
            // lblPergunta2
            // 
            this.lblPergunta2.AutoSize = true;
            this.lblPergunta2.Location = new System.Drawing.Point(7, 20);
            this.lblPergunta2.Name = "lblPergunta2";
            this.lblPergunta2.Size = new System.Drawing.Size(35, 13);
            this.lblPergunta2.TabIndex = 0;
            this.lblPergunta2.Text = "label1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(576, 345);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(127, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Terminar Questão 1 e 2";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 398);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximumSize = new System.Drawing.Size(731, 437);
            this.MinimumSize = new System.Drawing.Size(731, 437);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton pergunta1_resposta1;
        private System.Windows.Forms.Label lblPergunta1;
        private System.Windows.Forms.RadioButton pergunta1_resposta3;
        private System.Windows.Forms.RadioButton pergunta1_resposta2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton pergunta2_resposta3;
        private System.Windows.Forms.RadioButton pergunta2_resposta2;
        private System.Windows.Forms.RadioButton pergunta2_resposta1;
        private System.Windows.Forms.Label lblPergunta2;
        private System.Windows.Forms.Button button1;
    }
}

