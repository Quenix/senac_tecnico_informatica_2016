﻿namespace Quizz
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pergunta3_resposta3 = new System.Windows.Forms.RadioButton();
            this.pergunta3_resposta2 = new System.Windows.Forms.RadioButton();
            this.pergunta3_resposta1 = new System.Windows.Forms.RadioButton();
            this.lblPergunta3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pergunta4_resposta3 = new System.Windows.Forms.RadioButton();
            this.pergunta4_resposta2 = new System.Windows.Forms.RadioButton();
            this.pergunta4_resposta1 = new System.Windows.Forms.RadioButton();
            this.lblPergunta4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pergunta3_resposta3);
            this.groupBox1.Controls.Add(this.pergunta3_resposta2);
            this.groupBox1.Controls.Add(this.pergunta3_resposta1);
            this.groupBox1.Controls.Add(this.lblPergunta3);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(265, 298);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pergunta 3";
            // 
            // pergunta3_resposta3
            // 
            this.pergunta3_resposta3.AutoSize = true;
            this.pergunta3_resposta3.Location = new System.Drawing.Point(6, 144);
            this.pergunta3_resposta3.Name = "pergunta3_resposta3";
            this.pergunta3_resposta3.Size = new System.Drawing.Size(85, 17);
            this.pergunta3_resposta3.TabIndex = 3;
            this.pergunta3_resposta3.TabStop = true;
            this.pergunta3_resposta3.Text = "radioButton3";
            this.pergunta3_resposta3.UseVisualStyleBackColor = true;
            // 
            // pergunta3_resposta2
            // 
            this.pergunta3_resposta2.AutoSize = true;
            this.pergunta3_resposta2.Location = new System.Drawing.Point(6, 121);
            this.pergunta3_resposta2.Name = "pergunta3_resposta2";
            this.pergunta3_resposta2.Size = new System.Drawing.Size(85, 17);
            this.pergunta3_resposta2.TabIndex = 2;
            this.pergunta3_resposta2.TabStop = true;
            this.pergunta3_resposta2.Text = "radioButton2";
            this.pergunta3_resposta2.UseVisualStyleBackColor = true;
            // 
            // pergunta3_resposta1
            // 
            this.pergunta3_resposta1.AutoSize = true;
            this.pergunta3_resposta1.Location = new System.Drawing.Point(6, 98);
            this.pergunta3_resposta1.Name = "pergunta3_resposta1";
            this.pergunta3_resposta1.Size = new System.Drawing.Size(85, 17);
            this.pergunta3_resposta1.TabIndex = 1;
            this.pergunta3_resposta1.TabStop = true;
            this.pergunta3_resposta1.Text = "radioButton1";
            this.pergunta3_resposta1.UseVisualStyleBackColor = true;
            // 
            // lblPergunta3
            // 
            this.lblPergunta3.AutoSize = true;
            this.lblPergunta3.Location = new System.Drawing.Point(7, 20);
            this.lblPergunta3.Name = "lblPergunta3";
            this.lblPergunta3.Size = new System.Drawing.Size(35, 13);
            this.lblPergunta3.TabIndex = 0;
            this.lblPergunta3.Text = "label1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pergunta4_resposta3);
            this.groupBox2.Controls.Add(this.pergunta4_resposta2);
            this.groupBox2.Controls.Add(this.pergunta4_resposta1);
            this.groupBox2.Controls.Add(this.lblPergunta4);
            this.groupBox2.Location = new System.Drawing.Point(294, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(415, 298);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pergunta 4";
            // 
            // pergunta4_resposta3
            // 
            this.pergunta4_resposta3.AutoSize = true;
            this.pergunta4_resposta3.Location = new System.Drawing.Point(6, 144);
            this.pergunta4_resposta3.Name = "pergunta4_resposta3";
            this.pergunta4_resposta3.Size = new System.Drawing.Size(85, 17);
            this.pergunta4_resposta3.TabIndex = 3;
            this.pergunta4_resposta3.TabStop = true;
            this.pergunta4_resposta3.Text = "radioButton3";
            this.pergunta4_resposta3.UseVisualStyleBackColor = true;
            // 
            // pergunta4_resposta2
            // 
            this.pergunta4_resposta2.AutoSize = true;
            this.pergunta4_resposta2.Location = new System.Drawing.Point(6, 121);
            this.pergunta4_resposta2.Name = "pergunta4_resposta2";
            this.pergunta4_resposta2.Size = new System.Drawing.Size(85, 17);
            this.pergunta4_resposta2.TabIndex = 2;
            this.pergunta4_resposta2.TabStop = true;
            this.pergunta4_resposta2.Text = "radioButton2";
            this.pergunta4_resposta2.UseVisualStyleBackColor = true;
            // 
            // pergunta4_resposta1
            // 
            this.pergunta4_resposta1.AutoSize = true;
            this.pergunta4_resposta1.Location = new System.Drawing.Point(6, 98);
            this.pergunta4_resposta1.Name = "pergunta4_resposta1";
            this.pergunta4_resposta1.Size = new System.Drawing.Size(85, 17);
            this.pergunta4_resposta1.TabIndex = 1;
            this.pergunta4_resposta1.TabStop = true;
            this.pergunta4_resposta1.Text = "radioButton1";
            this.pergunta4_resposta1.UseVisualStyleBackColor = true;
            // 
            // lblPergunta4
            // 
            this.lblPergunta4.AutoSize = true;
            this.lblPergunta4.Location = new System.Drawing.Point(7, 20);
            this.lblPergunta4.Name = "lblPergunta4";
            this.lblPergunta4.Size = new System.Drawing.Size(35, 13);
            this.lblPergunta4.TabIndex = 0;
            this.lblPergunta4.Text = "label1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(578, 346);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(122, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Terminar";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 402);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximumSize = new System.Drawing.Size(728, 441);
            this.MinimumSize = new System.Drawing.Size(728, 441);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton pergunta3_resposta3;
        private System.Windows.Forms.RadioButton pergunta3_resposta2;
        private System.Windows.Forms.RadioButton pergunta3_resposta1;
        private System.Windows.Forms.Label lblPergunta3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton pergunta4_resposta3;
        private System.Windows.Forms.RadioButton pergunta4_resposta2;
        private System.Windows.Forms.RadioButton pergunta4_resposta1;
        private System.Windows.Forms.Label lblPergunta4;
        private System.Windows.Forms.Button button1;
    }
}