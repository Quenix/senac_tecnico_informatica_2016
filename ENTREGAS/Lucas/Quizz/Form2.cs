﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quizz
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            lblPergunta3.Text = "Uma mãe tem 30 euros para divir para suas duas filhas.Que horas?";
            pergunta3_resposta1.Text = "2h30";
            pergunta3_resposta2.Text = "4h50";
            pergunta3_resposta3.Text = "1h45"; //CORRETO

            lblPergunta4.Text = "";
            pergunta4_resposta1.Text = "";
            pergunta4_resposta2.Text = "";
            pergunta4_resposta3.Text = "";
        }
    }
}
