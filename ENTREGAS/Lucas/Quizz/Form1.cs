﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quizz
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        

        private void Form1_Load(object sender, EventArgs e)
        {
            lblPergunta1.Text = "A pedra cumprimentou uma tora. Que horas são?";
            pergunta1_resposta1.Text = "2h30";
            pergunta1_resposta2.Text = "7h45";
            pergunta1_resposta3.Text = "8h00"; //CORRETO

            lblPergunta2.Text = "Em um avião tinham 4 romanos e 1 inglês. Sabendo disso, qual o nome da aeromoça?";
            pergunta2_resposta1.Text = "Judite";
            pergunta2_resposta2.Text = "Ivone"; //CORRETO
            pergunta2_resposta3.Text = "Maria";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int pontos;

            pontos = 0;

            if (pergunta1_resposta3.Checked == true)
            {
                pontos += 10;
            }

            if (pergunta2_resposta2.Checked == true)
            {
                pontos += 10;
            }


            MessageBox.Show(pontos + " pontos");

            var epo = new Form2();
            epo.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
         
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}


        
    

