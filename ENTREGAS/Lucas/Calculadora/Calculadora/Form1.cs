﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Form1 : Form
    {
        //VARIAVEIS

        double valor1, valor2, resultado, resultado2, resultado3, resultado4;
        string operacao;
        
        public Form1()
        {
            InitializeComponent();
        }

        private void txtValor1_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void btnDivisao_Click(object sender, EventArgs e)
        {
            operacao = "divisao";
        }

        private void btnMultiplicacao_Click(object sender, EventArgs e)
        {
            operacao = "multiplicacao";
        }

        private void txtValor1_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void txtValor1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtValor2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtValor2_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void btnSoma_Click(object sender, EventArgs e)
        {
            operacao = "soma";
        }

        private void btnSubtracao_Click(object sender, EventArgs e)
        {
            operacao = "subtracao";
        }           
                 
        private void btnResultado_Click(object sender, EventArgs e)
        {
            valor1 = Convert.ToDouble(txtValor1.Text);
            valor2 = Convert.ToDouble(txtValor2.Text);

            if (operacao == "soma")
            {
                resultado = valor1 + valor2;
                lblResult.Text = Convert.ToString(resultado);
            }
            if (operacao == "subtracao")
            {
                resultado2 = valor1 - valor2;
                lblResult.Text = Convert.ToString(resultado2);
            }
            if (operacao == "divisao")
            {
                if (valor1 == 0 || valor2 == 0)
                {
                    lblResult.Text = "Impossível dividir";
                }
                else
                {
                    resultado3 = valor1 / valor2;
                    lblResult.Text = Convert.ToString(resultado3);
                }
                
            }
            if (operacao == "multiplicacao")
            {
                resultado4 = valor1 * valor2;
                lblResult.Text = Convert.ToString(resultado4);
            }
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
