﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void btnVerde_Click(object sender, EventArgs e)
        {
            lblFrase.ForeColor = System.Drawing.Color.Green;
        }

        private void btnAzul_Click(object sender, EventArgs e)
        {
            lblFrase.ForeColor = System.Drawing.Color.Blue;
        }

        private void btnVermelho_Click(object sender, EventArgs e)
        {
            lblFrase.ForeColor = System.Drawing.Color.Red;
        }
    }
}
