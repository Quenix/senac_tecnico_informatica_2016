﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        public object Reverse { get; private set; }

        private void button1_Click(object sender, EventArgs e)
        {
            char[] invertfon = txtInvert.Text.ToCharArray();
            Array.Reverse(invertfon);
            string reverter = new string(invertfon);
            txtInvertido.Text = reverter;
        }
    }
}
