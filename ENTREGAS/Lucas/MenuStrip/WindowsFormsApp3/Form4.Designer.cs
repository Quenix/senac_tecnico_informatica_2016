﻿namespace WindowsFormsApp3
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnInvert = new System.Windows.Forms.Button();
            this.txtInvert = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtInvertido = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Digite sua frase: ";
            // 
            // btnInvert
            // 
            this.btnInvert.Location = new System.Drawing.Point(110, 116);
            this.btnInvert.Name = "btnInvert";
            this.btnInvert.Size = new System.Drawing.Size(75, 23);
            this.btnInvert.TabIndex = 2;
            this.btnInvert.Text = "Inverter";
            this.btnInvert.UseVisualStyleBackColor = true;
            this.btnInvert.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtInvert
            // 
            this.txtInvert.Location = new System.Drawing.Point(104, 26);
            this.txtInvert.Name = "txtInvert";
            this.txtInvert.Size = new System.Drawing.Size(168, 20);
            this.txtInvert.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Frase Invertida: ";
            // 
            // txtInvertido
            // 
            this.txtInvertido.Location = new System.Drawing.Point(104, 64);
            this.txtInvertido.Name = "txtInvertido";
            this.txtInvertido.Size = new System.Drawing.Size(168, 20);
            this.txtInvertido.TabIndex = 6;
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.txtInvertido);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtInvert);
            this.Controls.Add(this.btnInvert);
            this.Controls.Add(this.label1);
            this.Name = "Form4";
            this.Text = "Inverter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnInvert;
        private System.Windows.Forms.TextBox txtInvert;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtInvertido;
    }
}