﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estrutura_repetição
{
    class Program
    {
        static void Main(string[] args)
        {
            Boolean lampada;
            string resposta;

            lampada = true;

            while(lampada == true)
            {
                Console.WriteLine("luz na lampada");
                Console.WriteLine("Digite apagar para apagar a lampada");
                resposta = Console.ReadLine();

                if (resposta == "apagar")
                    Console.WriteLine("lampada sem luz");
                {
                    lampada = false;
                }
            }
        }
    }
}
