﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IMC
{
    public partial class Form1 : Form
    {
        double peso, altura, imc;

        private void lblResultado_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public Form1()
        {
            InitializeComponent();
        }

        private void btnResult_Click(object sender, EventArgs e)
        {
            peso = Convert.ToDouble(txtPeso.Text);
            altura = Convert.ToDouble(txtAltura.Text);
            imc = peso / (altura * altura);

            lblResultado.Text = ("IMC: " + Math.Round(imc, 2));

            if (imc <= 18.5)
            {
                lblResultado2.Text = ("Abaixo do Peso");
            }

            if (imc >= 18.5 && imc <= 24.9)
            {
                lblResultado2.Text = ("Peso ideal");            
            }

            if (imc >= 25.0 && imc <= 29.9)
            {
                lblResultado2.Text = ("Pouco acima do peso");
            }

            if (imc >= 30.0 && imc <= 34.9)
            {
                lblResultado2.Text = ("Obesidade Grau I");
            }

            if (imc >= 35.0 && imc <= 39.9)
            {
                lblResultado2.Text = ("Obesidade Grau II");
            }

            if (imc >= 40.0)
            {
                lblResultado2.Text = ("Obesidade Grau III");
            }
        }
    }
}
