﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Toub.Sound.Midi;

namespace Piano
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            MidiPlayer.OpenMidi();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "C#4", 127));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "D#4", 127));
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "F#4", 127));
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "F#4", 127));
        }

        private void button5_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "G#4", 127));
        }

        private void button6_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "A#4", 127));
        }


        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            
        }

        private void button9_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "D4", 127));
        }

        private void button8_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "C4", 127));
        }

        private void button10_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "E4", 127));
        }

        private void button11_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "F4", 127));
        }

        private void button12_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "G4", 127));
        }

        private void button13_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "A4", 127));
        }

        private void button14_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "B4", 127));
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "C4", 127));
        }
    }
}
