<!DOCTYPE HTML>
<HTML>
	<HEAD>
		<meta charset="utf-8">
		<title> Aula de Javascript </title>
	</HEAD>

	<BODY>
		
		<label>Nome: </label>
		<input type="text" id="Nome"></input>
		<label>Idade: </label>
		<input type="text" id="Idade"></input>
		<label>E-mail: </label>
		<input type="email" id="Email"></input>
		<label>Sexo: </label>
		<select type="text" id="Sexo">
			<option>Homem</option>
			<option>Mulher</option>
		</select>
		<label>Endereço: </label>
		<input type="text" id="Endereço"></input>
		<button onclick="Mostrar()">Yeah!</button>
		
		<script>
		
			function Mostrar(){
				
				var nome, idade, email, sexo, endereco;
				
				nome = document.getElementById('Nome').value;
				idade = document.getElementById('Idade').value;
				email = document.getElementById('Email').value;
				sexo = document.getElementById('Sexo').value;
				endereco = document.getElementById('Endereço').value;
				
				alert('Nome: ' + nome + '\nIdade: ' + idade + '\nE-mail: ' + email + '\nSexo: ' + sexo + '\nEndereço:' + endereco);
				
			}
		
		</script>
		
	</BODY>

</HTML>