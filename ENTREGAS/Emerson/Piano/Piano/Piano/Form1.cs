﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Toub.Sound.Midi;

namespace Piano
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            MidiPlayer.OpenMidi();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "C4", 127));
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "E4", 127));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "D4", 127));
        }
    }
}
