﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MultiplasJanelas
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            char[] arrChar = txtFrase.Text.ToCharArray();
            Array.Reverse(arrChar);
            string invertida = new String(arrChar);

            MessageBox.Show(invertida);
        }
    }
}
