﻿namespace MultiplasJanelas
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ldlNome = new System.Windows.Forms.Label();
            this.ldlIdade = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.txtIdade = new System.Windows.Forms.TextBox();
            this.lblCadastrar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ldlNome
            // 
            this.ldlNome.AutoSize = true;
            this.ldlNome.Location = new System.Drawing.Point(26, 31);
            this.ldlNome.Name = "ldlNome";
            this.ldlNome.Size = new System.Drawing.Size(42, 13);
            this.ldlNome.TabIndex = 0;
            this.ldlNome.Text = "NOME:";
            // 
            // ldlIdade
            // 
            this.ldlIdade.AutoSize = true;
            this.ldlIdade.Location = new System.Drawing.Point(26, 59);
            this.ldlIdade.Name = "ldlIdade";
            this.ldlIdade.Size = new System.Drawing.Size(43, 13);
            this.ldlIdade.TabIndex = 1;
            this.ldlIdade.Text = "IDADE:";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(74, 24);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(100, 20);
            this.txtNome.TabIndex = 2;
            this.txtNome.TextChanged += new System.EventHandler(this.txtNome_TextChanged);
            // 
            // txtIdade
            // 
            this.txtIdade.Location = new System.Drawing.Point(75, 56);
            this.txtIdade.Name = "txtIdade";
            this.txtIdade.Size = new System.Drawing.Size(100, 20);
            this.txtIdade.TabIndex = 3;
            // 
            // lblCadastrar
            // 
            this.lblCadastrar.Location = new System.Drawing.Point(84, 155);
            this.lblCadastrar.Name = "lblCadastrar";
            this.lblCadastrar.Size = new System.Drawing.Size(75, 23);
            this.lblCadastrar.TabIndex = 4;
            this.lblCadastrar.Text = "Cadastrar";
            this.lblCadastrar.UseVisualStyleBackColor = true;
            this.lblCadastrar.Click += new System.EventHandler(this.lblCadastrar_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.lblCadastrar);
            this.Controls.Add(this.txtIdade);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.ldlIdade);
            this.Controls.Add(this.ldlNome);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ldlNome;
        private System.Windows.Forms.Label ldlIdade;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.TextBox txtIdade;
        private System.Windows.Forms.Button lblCadastrar;
    }
}