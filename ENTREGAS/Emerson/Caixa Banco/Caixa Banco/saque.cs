﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Caixa_Banco
{
    public partial class saque : Form
    {
        public saque()
        {
            InitializeComponent();
        }

        private void btn20_Click(object sender, EventArgs e)
        {
            Form1.saldo -= 20;
            MessageBox.Show("Novo Saldo é: " + Form1.saldo);
        }

        private void btn40_Click(object sender, EventArgs e)
        {
            Form1.saldo -= 40;
            MessageBox.Show("Novo Saldo é: " + Form1.saldo);
        }

        private void btn50_Click(object sender, EventArgs e)
        {
            Form1.saldo -= 50;
            MessageBox.Show("Novo Saldo é: " + Form1.saldo);
        }

        private void btn100_Click(object sender, EventArgs e)
        {
            Form1.saldo -= 100;
            MessageBox.Show(" Novo Saldo é: " + Form1.saldo);
        }
    }
}
