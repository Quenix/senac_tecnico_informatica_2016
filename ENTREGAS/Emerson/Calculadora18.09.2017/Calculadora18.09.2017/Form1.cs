﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora18._09._2017
{
    public partial class Form1 : Form
    {
        string operacao;
        int valorA, valorB, resultado;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            operacao = "subtracao";
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnMULTIPLICACAO_Click(object sender, EventArgs e)
        {
            operacao = "multiplicacao";
        }

        private void btnDIVISAO_Click(object sender, EventArgs e)
        {
            operacao = "divisao";
        }

        private void btnResultado_Click(object sender, EventArgs e)
        {
            valorA = Convert.ToInt32(txtValorA.Text);
            valorB = Convert.ToInt32(txtValorB.Text);



            if (operacao == "soma")
            {
                resultado = valorA + valorB;
                txtResultado.Text = Convert.ToString(resultado);
            }

            if (operacao == "multiplicacao")
            {
                resultado = valorA * valorB;
                txtResultado.Text = Convert.ToString(resultado);

            }
            if (operacao == "subtracao")
            {
                resultado = valorA - valorB;
                txtResultado.Text = Convert.ToString(resultado);
            }
                
            if (operacao == "divisao")
            {
                resultado = valorA / valorB;
                txtResultado.Text = Convert.ToString(resultado);

            }


        }

        private void lblResultado_Click(object sender, EventArgs e)
        {

        }

        private void btnSOMA_Click(object sender, EventArgs e)
        {
            operacao = "soma";
        }
    }
}
