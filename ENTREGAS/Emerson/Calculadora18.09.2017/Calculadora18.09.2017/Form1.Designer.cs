﻿namespace Calculadora18._09._2017
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSOMA = new System.Windows.Forms.Button();
            this.btnSUBTRACAO = new System.Windows.Forms.Button();
            this.btnMULTIPLICACAO = new System.Windows.Forms.Button();
            this.btnDIVISAO = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtValorA = new System.Windows.Forms.TextBox();
            this.txtValorB = new System.Windows.Forms.TextBox();
            this.lblResultado = new System.Windows.Forms.Label();
            this.txtResultado = new System.Windows.Forms.TextBox();
            this.btnResultado = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSOMA
            // 
            this.btnSOMA.Location = new System.Drawing.Point(51, 138);
            this.btnSOMA.Name = "btnSOMA";
            this.btnSOMA.Size = new System.Drawing.Size(75, 23);
            this.btnSOMA.TabIndex = 0;
            this.btnSOMA.Text = "+";
            this.btnSOMA.UseVisualStyleBackColor = true;
            this.btnSOMA.Click += new System.EventHandler(this.btnSOMA_Click);
            // 
            // btnSUBTRACAO
            // 
            this.btnSUBTRACAO.Location = new System.Drawing.Point(51, 185);
            this.btnSUBTRACAO.Name = "btnSUBTRACAO";
            this.btnSUBTRACAO.Size = new System.Drawing.Size(75, 23);
            this.btnSUBTRACAO.TabIndex = 1;
            this.btnSUBTRACAO.Text = "-";
            this.btnSUBTRACAO.UseVisualStyleBackColor = true;
            this.btnSUBTRACAO.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnMULTIPLICACAO
            // 
            this.btnMULTIPLICACAO.Location = new System.Drawing.Point(163, 138);
            this.btnMULTIPLICACAO.Name = "btnMULTIPLICACAO";
            this.btnMULTIPLICACAO.Size = new System.Drawing.Size(75, 23);
            this.btnMULTIPLICACAO.TabIndex = 2;
            this.btnMULTIPLICACAO.Text = "x";
            this.btnMULTIPLICACAO.UseVisualStyleBackColor = true;
            this.btnMULTIPLICACAO.Click += new System.EventHandler(this.btnMULTIPLICACAO_Click);
            // 
            // btnDIVISAO
            // 
            this.btnDIVISAO.Location = new System.Drawing.Point(163, 185);
            this.btnDIVISAO.Name = "btnDIVISAO";
            this.btnDIVISAO.Size = new System.Drawing.Size(75, 23);
            this.btnDIVISAO.TabIndex = 3;
            this.btnDIVISAO.Text = "/";
            this.btnDIVISAO.UseVisualStyleBackColor = true;
            this.btnDIVISAO.Click += new System.EventHandler(this.btnDIVISAO_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(60, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Valor A";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(60, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Valor B";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtValorA
            // 
            this.txtValorA.Location = new System.Drawing.Point(107, 32);
            this.txtValorA.Name = "txtValorA";
            this.txtValorA.Size = new System.Drawing.Size(100, 20);
            this.txtValorA.TabIndex = 6;
            // 
            // txtValorB
            // 
            this.txtValorB.Location = new System.Drawing.Point(107, 61);
            this.txtValorB.Name = "txtValorB";
            this.txtValorB.Size = new System.Drawing.Size(100, 20);
            this.txtValorB.TabIndex = 7;
            // 
            // lblResultado
            // 
            this.lblResultado.AutoSize = true;
            this.lblResultado.Location = new System.Drawing.Point(60, 88);
            this.lblResultado.Name = "lblResultado";
            this.lblResultado.Size = new System.Drawing.Size(58, 13);
            this.lblResultado.TabIndex = 8;
            this.lblResultado.Text = "Resultado:";
            this.lblResultado.Click += new System.EventHandler(this.lblResultado_Click);
            // 
            // txtResultado
            // 
            this.txtResultado.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtResultado.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtResultado.ForeColor = System.Drawing.Color.Black;
            this.txtResultado.Location = new System.Drawing.Point(124, 87);
            this.txtResultado.Name = "txtResultado";
            this.txtResultado.ReadOnly = true;
            this.txtResultado.Size = new System.Drawing.Size(100, 13);
            this.txtResultado.TabIndex = 9;
            this.txtResultado.TabStop = false;
            // 
            // btnResultado
            // 
            this.btnResultado.Location = new System.Drawing.Point(51, 226);
            this.btnResultado.Name = "btnResultado";
            this.btnResultado.Size = new System.Drawing.Size(187, 23);
            this.btnResultado.TabIndex = 10;
            this.btnResultado.Text = "CalcularResultado";
            this.btnResultado.UseVisualStyleBackColor = true;
            this.btnResultado.Click += new System.EventHandler(this.btnResultado_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnResultado);
            this.Controls.Add(this.txtResultado);
            this.Controls.Add(this.lblResultado);
            this.Controls.Add(this.txtValorB);
            this.Controls.Add(this.txtValorA);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnDIVISAO);
            this.Controls.Add(this.btnMULTIPLICACAO);
            this.Controls.Add(this.btnSUBTRACAO);
            this.Controls.Add(this.btnSOMA);
            this.Name = "Form1";
            this.Text = "te";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSOMA;
        private System.Windows.Forms.Button btnSUBTRACAO;
        private System.Windows.Forms.Button btnMULTIPLICACAO;
        private System.Windows.Forms.Button btnDIVISAO;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtValorA;
        private System.Windows.Forms.TextBox txtValorB;
        private System.Windows.Forms.Label lblResultado;
        private System.Windows.Forms.Button btnResultado;
        private System.Windows.Forms.TextBox txtResultado;
    }
}

