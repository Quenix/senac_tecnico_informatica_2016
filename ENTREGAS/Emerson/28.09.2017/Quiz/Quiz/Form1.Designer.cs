﻿namespace Quiz
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Pergunta1_resposta4 = new System.Windows.Forms.RadioButton();
            this.Pergunta1_resposta3 = new System.Windows.Forms.RadioButton();
            this.Pergunta1_resposta2 = new System.Windows.Forms.RadioButton();
            this.Pergunta1_resposta1 = new System.Windows.Forms.RadioButton();
            this.lblpergunta1 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.btnFinalizar = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Pergunta2_resposta4 = new System.Windows.Forms.RadioButton();
            this.Pergunta2_resposta3 = new System.Windows.Forms.RadioButton();
            this.Pergunta2_resposta2 = new System.Windows.Forms.RadioButton();
            this.Pergunta2_resposta1 = new System.Windows.Forms.RadioButton();
            this.lblPergunta2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Pergunta3_resposta4 = new System.Windows.Forms.RadioButton();
            this.Pergunta3_resposta3 = new System.Windows.Forms.RadioButton();
            this.Pergunta3_resposta2 = new System.Windows.Forms.RadioButton();
            this.Pergunta3_resposta1 = new System.Windows.Forms.RadioButton();
            this.lblpergunta3 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.Pergunta6_resposta4 = new System.Windows.Forms.RadioButton();
            this.Pergunta6_resposta3 = new System.Windows.Forms.RadioButton();
            this.Pergunta6_resposta2 = new System.Windows.Forms.RadioButton();
            this.Pergunta6_resposta1 = new System.Windows.Forms.RadioButton();
            this.lblpergunta6 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.Pergunta5_resposta4 = new System.Windows.Forms.RadioButton();
            this.Pergunta5_resposta3 = new System.Windows.Forms.RadioButton();
            this.Pergunta5_resposta2 = new System.Windows.Forms.RadioButton();
            this.Pergunta5_resposta1 = new System.Windows.Forms.RadioButton();
            this.lblpergunta5 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.Pergunta4_resposta4 = new System.Windows.Forms.RadioButton();
            this.Pergunta4_resposta3 = new System.Windows.Forms.RadioButton();
            this.Pergunta4_resposta2 = new System.Windows.Forms.RadioButton();
            this.Pergunta4_resposta1 = new System.Windows.Forms.RadioButton();
            this.lblpergunta4 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.Pergunta8_resposta4 = new System.Windows.Forms.RadioButton();
            this.Pergunta8_resposta3 = new System.Windows.Forms.RadioButton();
            this.Pergunta8_resposta2 = new System.Windows.Forms.RadioButton();
            this.Pergunta8_resposta1 = new System.Windows.Forms.RadioButton();
            this.lblpergunta8 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.Pergunta9_resposta4 = new System.Windows.Forms.RadioButton();
            this.Pergunta9_resposta3 = new System.Windows.Forms.RadioButton();
            this.Pergunta9_resposta2 = new System.Windows.Forms.RadioButton();
            this.Pergunta9_resposta1 = new System.Windows.Forms.RadioButton();
            this.lblpergunta9 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.Pergunta7_resposta4 = new System.Windows.Forms.RadioButton();
            this.Pergunta7_resposta3 = new System.Windows.Forms.RadioButton();
            this.Pergunta7_resposta2 = new System.Windows.Forms.RadioButton();
            this.Pergunta7_resposta1 = new System.Windows.Forms.RadioButton();
            this.lblpergunta7 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.Pergunta10_resposta4 = new System.Windows.Forms.RadioButton();
            this.Pergunta10_resposta3 = new System.Windows.Forms.RadioButton();
            this.Pergunta10_resposta2 = new System.Windows.Forms.RadioButton();
            this.Pergunta10_resposta1 = new System.Windows.Forms.RadioButton();
            this.lblpergunta10 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Pergunta1_resposta4);
            this.groupBox1.Controls.Add(this.Pergunta1_resposta3);
            this.groupBox1.Controls.Add(this.Pergunta1_resposta2);
            this.groupBox1.Controls.Add(this.Pergunta1_resposta1);
            this.groupBox1.Controls.Add(this.lblpergunta1);
            this.groupBox1.Location = new System.Drawing.Point(36, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(256, 196);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pergunta 1";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // Pergunta1_resposta4
            // 
            this.Pergunta1_resposta4.AutoSize = true;
            this.Pergunta1_resposta4.Location = new System.Drawing.Point(9, 160);
            this.Pergunta1_resposta4.Name = "Pergunta1_resposta4";
            this.Pergunta1_resposta4.Size = new System.Drawing.Size(40, 17);
            this.Pergunta1_resposta4.TabIndex = 4;
            this.Pergunta1_resposta4.TabStop = true;
            this.Pergunta1_resposta4.Text = "Tia";
            this.Pergunta1_resposta4.UseVisualStyleBackColor = true;
            // 
            // Pergunta1_resposta3
            // 
            this.Pergunta1_resposta3.AutoSize = true;
            this.Pergunta1_resposta3.Location = new System.Drawing.Point(9, 137);
            this.Pergunta1_resposta3.Name = "Pergunta1_resposta3";
            this.Pergunta1_resposta3.Size = new System.Drawing.Size(44, 17);
            this.Pergunta1_resposta3.TabIndex = 3;
            this.Pergunta1_resposta3.TabStop = true;
            this.Pergunta1_resposta3.Text = "Avó";
            this.Pergunta1_resposta3.UseVisualStyleBackColor = true;
            // 
            // Pergunta1_resposta2
            // 
            this.Pergunta1_resposta2.AutoSize = true;
            this.Pergunta1_resposta2.Location = new System.Drawing.Point(9, 114);
            this.Pergunta1_resposta2.Name = "Pergunta1_resposta2";
            this.Pergunta1_resposta2.Size = new System.Drawing.Size(47, 17);
            this.Pergunta1_resposta2.TabIndex = 2;
            this.Pergunta1_resposta2.TabStop = true;
            this.Pergunta1_resposta2.Text = "Filha";
            this.Pergunta1_resposta2.UseVisualStyleBackColor = true;
            // 
            // Pergunta1_resposta1
            // 
            this.Pergunta1_resposta1.AutoSize = true;
            this.Pergunta1_resposta1.Location = new System.Drawing.Point(9, 91);
            this.Pergunta1_resposta1.Name = "Pergunta1_resposta1";
            this.Pergunta1_resposta1.Size = new System.Drawing.Size(46, 17);
            this.Pergunta1_resposta1.TabIndex = 1;
            this.Pergunta1_resposta1.TabStop = true;
            this.Pergunta1_resposta1.Text = "Mãe";
            this.Pergunta1_resposta1.UseVisualStyleBackColor = true;
            this.Pergunta1_resposta1.CheckedChanged += new System.EventHandler(this.Pergunta1_resposta1_CheckedChanged);
            // 
            // lblpergunta1
            // 
            this.lblpergunta1.AutoSize = true;
            this.lblpergunta1.Location = new System.Drawing.Point(6, 29);
            this.lblpergunta1.Name = "lblpergunta1";
            this.lblpergunta1.Size = new System.Drawing.Size(259, 26);
            this.lblpergunta1.TabIndex = 0;
            this.lblpergunta1.Text = "Se a Filha de Tereza é a Filha da Mãe da Minha filha,\r\n O que Sou de Tereza ?";
            this.lblpergunta1.Click += new System.EventHandler(this.label1_Click);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // btnFinalizar
            // 
            this.btnFinalizar.Location = new System.Drawing.Point(363, 920);
            this.btnFinalizar.Name = "btnFinalizar";
            this.btnFinalizar.Size = new System.Drawing.Size(75, 23);
            this.btnFinalizar.TabIndex = 6;
            this.btnFinalizar.Text = "Finalizar";
            this.btnFinalizar.UseVisualStyleBackColor = true;
            this.btnFinalizar.Click += new System.EventHandler(this.btnFinalizar_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Pergunta2_resposta4);
            this.groupBox2.Controls.Add(this.Pergunta2_resposta3);
            this.groupBox2.Controls.Add(this.Pergunta2_resposta2);
            this.groupBox2.Controls.Add(this.Pergunta2_resposta1);
            this.groupBox2.Controls.Add(this.lblPergunta2);
            this.groupBox2.Location = new System.Drawing.Point(298, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(242, 196);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pergunta2";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // Pergunta2_resposta4
            // 
            this.Pergunta2_resposta4.AutoSize = true;
            this.Pergunta2_resposta4.Location = new System.Drawing.Point(14, 152);
            this.Pergunta2_resposta4.Name = "Pergunta2_resposta4";
            this.Pergunta2_resposta4.Size = new System.Drawing.Size(85, 17);
            this.Pergunta2_resposta4.TabIndex = 4;
            this.Pergunta2_resposta4.TabStop = true;
            this.Pergunta2_resposta4.Text = "radioButton4";
            this.Pergunta2_resposta4.UseVisualStyleBackColor = true;
            // 
            // Pergunta2_resposta3
            // 
            this.Pergunta2_resposta3.AutoSize = true;
            this.Pergunta2_resposta3.Location = new System.Drawing.Point(14, 129);
            this.Pergunta2_resposta3.Name = "Pergunta2_resposta3";
            this.Pergunta2_resposta3.Size = new System.Drawing.Size(85, 17);
            this.Pergunta2_resposta3.TabIndex = 3;
            this.Pergunta2_resposta3.TabStop = true;
            this.Pergunta2_resposta3.Text = "radioButton3";
            this.Pergunta2_resposta3.UseVisualStyleBackColor = true;
            // 
            // Pergunta2_resposta2
            // 
            this.Pergunta2_resposta2.AutoSize = true;
            this.Pergunta2_resposta2.Location = new System.Drawing.Point(14, 106);
            this.Pergunta2_resposta2.Name = "Pergunta2_resposta2";
            this.Pergunta2_resposta2.Size = new System.Drawing.Size(85, 17);
            this.Pergunta2_resposta2.TabIndex = 2;
            this.Pergunta2_resposta2.TabStop = true;
            this.Pergunta2_resposta2.Text = "radioButton2";
            this.Pergunta2_resposta2.UseVisualStyleBackColor = true;
            // 
            // Pergunta2_resposta1
            // 
            this.Pergunta2_resposta1.AutoSize = true;
            this.Pergunta2_resposta1.Location = new System.Drawing.Point(14, 83);
            this.Pergunta2_resposta1.Name = "Pergunta2_resposta1";
            this.Pergunta2_resposta1.Size = new System.Drawing.Size(85, 17);
            this.Pergunta2_resposta1.TabIndex = 1;
            this.Pergunta2_resposta1.TabStop = true;
            this.Pergunta2_resposta1.Text = "radioButton1";
            this.Pergunta2_resposta1.UseVisualStyleBackColor = true;
            // 
            // lblPergunta2
            // 
            this.lblPergunta2.AutoSize = true;
            this.lblPergunta2.Location = new System.Drawing.Point(6, 29);
            this.lblPergunta2.Name = "lblPergunta2";
            this.lblPergunta2.Size = new System.Drawing.Size(35, 13);
            this.lblPergunta2.TabIndex = 0;
            this.lblPergunta2.Text = "label1";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.Pergunta3_resposta4);
            this.groupBox3.Controls.Add(this.Pergunta3_resposta3);
            this.groupBox3.Controls.Add(this.Pergunta3_resposta2);
            this.groupBox3.Controls.Add(this.Pergunta3_resposta1);
            this.groupBox3.Controls.Add(this.lblpergunta3);
            this.groupBox3.Location = new System.Drawing.Point(559, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(242, 196);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Pergunta3";
            // 
            // Pergunta3_resposta4
            // 
            this.Pergunta3_resposta4.AutoSize = true;
            this.Pergunta3_resposta4.Location = new System.Drawing.Point(14, 152);
            this.Pergunta3_resposta4.Name = "Pergunta3_resposta4";
            this.Pergunta3_resposta4.Size = new System.Drawing.Size(85, 17);
            this.Pergunta3_resposta4.TabIndex = 4;
            this.Pergunta3_resposta4.TabStop = true;
            this.Pergunta3_resposta4.Text = "radioButton4";
            this.Pergunta3_resposta4.UseVisualStyleBackColor = true;
            // 
            // Pergunta3_resposta3
            // 
            this.Pergunta3_resposta3.AutoSize = true;
            this.Pergunta3_resposta3.Location = new System.Drawing.Point(14, 129);
            this.Pergunta3_resposta3.Name = "Pergunta3_resposta3";
            this.Pergunta3_resposta3.Size = new System.Drawing.Size(85, 17);
            this.Pergunta3_resposta3.TabIndex = 3;
            this.Pergunta3_resposta3.TabStop = true;
            this.Pergunta3_resposta3.Text = "radioButton3";
            this.Pergunta3_resposta3.UseVisualStyleBackColor = true;
            // 
            // Pergunta3_resposta2
            // 
            this.Pergunta3_resposta2.AutoSize = true;
            this.Pergunta3_resposta2.Location = new System.Drawing.Point(14, 106);
            this.Pergunta3_resposta2.Name = "Pergunta3_resposta2";
            this.Pergunta3_resposta2.Size = new System.Drawing.Size(85, 17);
            this.Pergunta3_resposta2.TabIndex = 2;
            this.Pergunta3_resposta2.TabStop = true;
            this.Pergunta3_resposta2.Text = "radioButton2";
            this.Pergunta3_resposta2.UseVisualStyleBackColor = true;
            // 
            // Pergunta3_resposta1
            // 
            this.Pergunta3_resposta1.AutoSize = true;
            this.Pergunta3_resposta1.Location = new System.Drawing.Point(14, 83);
            this.Pergunta3_resposta1.Name = "Pergunta3_resposta1";
            this.Pergunta3_resposta1.Size = new System.Drawing.Size(85, 17);
            this.Pergunta3_resposta1.TabIndex = 1;
            this.Pergunta3_resposta1.TabStop = true;
            this.Pergunta3_resposta1.Text = "radioButton1";
            this.Pergunta3_resposta1.UseVisualStyleBackColor = true;
            // 
            // lblpergunta3
            // 
            this.lblpergunta3.AutoSize = true;
            this.lblpergunta3.Location = new System.Drawing.Point(6, 34);
            this.lblpergunta3.Name = "lblpergunta3";
            this.lblpergunta3.Size = new System.Drawing.Size(35, 13);
            this.lblpergunta3.TabIndex = 0;
            this.lblpergunta3.Text = "label1";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.Pergunta6_resposta4);
            this.groupBox4.Controls.Add(this.Pergunta6_resposta3);
            this.groupBox4.Controls.Add(this.Pergunta6_resposta2);
            this.groupBox4.Controls.Add(this.Pergunta6_resposta1);
            this.groupBox4.Controls.Add(this.lblpergunta6);
            this.groupBox4.Location = new System.Drawing.Point(559, 221);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(242, 196);
            this.groupBox4.TabIndex = 9;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Pergunta6";
            this.groupBox4.Enter += new System.EventHandler(this.groupBox4_Enter);
            // 
            // Pergunta6_resposta4
            // 
            this.Pergunta6_resposta4.AutoSize = true;
            this.Pergunta6_resposta4.Location = new System.Drawing.Point(14, 152);
            this.Pergunta6_resposta4.Name = "Pergunta6_resposta4";
            this.Pergunta6_resposta4.Size = new System.Drawing.Size(85, 17);
            this.Pergunta6_resposta4.TabIndex = 4;
            this.Pergunta6_resposta4.TabStop = true;
            this.Pergunta6_resposta4.Text = "radioButton4";
            this.Pergunta6_resposta4.UseVisualStyleBackColor = true;
            // 
            // Pergunta6_resposta3
            // 
            this.Pergunta6_resposta3.AutoSize = true;
            this.Pergunta6_resposta3.Location = new System.Drawing.Point(14, 129);
            this.Pergunta6_resposta3.Name = "Pergunta6_resposta3";
            this.Pergunta6_resposta3.Size = new System.Drawing.Size(85, 17);
            this.Pergunta6_resposta3.TabIndex = 3;
            this.Pergunta6_resposta3.TabStop = true;
            this.Pergunta6_resposta3.Text = "radioButton3";
            this.Pergunta6_resposta3.UseVisualStyleBackColor = true;
            // 
            // Pergunta6_resposta2
            // 
            this.Pergunta6_resposta2.AutoSize = true;
            this.Pergunta6_resposta2.Location = new System.Drawing.Point(14, 106);
            this.Pergunta6_resposta2.Name = "Pergunta6_resposta2";
            this.Pergunta6_resposta2.Size = new System.Drawing.Size(85, 17);
            this.Pergunta6_resposta2.TabIndex = 2;
            this.Pergunta6_resposta2.TabStop = true;
            this.Pergunta6_resposta2.Text = "radioButton2";
            this.Pergunta6_resposta2.UseVisualStyleBackColor = true;
            // 
            // Pergunta6_resposta1
            // 
            this.Pergunta6_resposta1.AutoSize = true;
            this.Pergunta6_resposta1.Location = new System.Drawing.Point(14, 83);
            this.Pergunta6_resposta1.Name = "Pergunta6_resposta1";
            this.Pergunta6_resposta1.Size = new System.Drawing.Size(85, 17);
            this.Pergunta6_resposta1.TabIndex = 1;
            this.Pergunta6_resposta1.TabStop = true;
            this.Pergunta6_resposta1.Text = "radioButton1";
            this.Pergunta6_resposta1.UseVisualStyleBackColor = true;
            // 
            // lblpergunta6
            // 
            this.lblpergunta6.AutoSize = true;
            this.lblpergunta6.Location = new System.Drawing.Point(6, 34);
            this.lblpergunta6.Name = "lblpergunta6";
            this.lblpergunta6.Size = new System.Drawing.Size(35, 13);
            this.lblpergunta6.TabIndex = 0;
            this.lblpergunta6.Text = "label1";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.Pergunta5_resposta4);
            this.groupBox5.Controls.Add(this.Pergunta5_resposta3);
            this.groupBox5.Controls.Add(this.Pergunta5_resposta2);
            this.groupBox5.Controls.Add(this.Pergunta5_resposta1);
            this.groupBox5.Controls.Add(this.lblpergunta5);
            this.groupBox5.Location = new System.Drawing.Point(298, 221);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(242, 196);
            this.groupBox5.TabIndex = 9;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Pergunta5";
            // 
            // Pergunta5_resposta4
            // 
            this.Pergunta5_resposta4.AutoSize = true;
            this.Pergunta5_resposta4.Location = new System.Drawing.Point(14, 152);
            this.Pergunta5_resposta4.Name = "Pergunta5_resposta4";
            this.Pergunta5_resposta4.Size = new System.Drawing.Size(85, 17);
            this.Pergunta5_resposta4.TabIndex = 4;
            this.Pergunta5_resposta4.TabStop = true;
            this.Pergunta5_resposta4.Text = "radioButton4";
            this.Pergunta5_resposta4.UseVisualStyleBackColor = true;
            // 
            // Pergunta5_resposta3
            // 
            this.Pergunta5_resposta3.AutoSize = true;
            this.Pergunta5_resposta3.Location = new System.Drawing.Point(14, 129);
            this.Pergunta5_resposta3.Name = "Pergunta5_resposta3";
            this.Pergunta5_resposta3.Size = new System.Drawing.Size(85, 17);
            this.Pergunta5_resposta3.TabIndex = 3;
            this.Pergunta5_resposta3.TabStop = true;
            this.Pergunta5_resposta3.Text = "radioButton3";
            this.Pergunta5_resposta3.UseVisualStyleBackColor = true;
            // 
            // Pergunta5_resposta2
            // 
            this.Pergunta5_resposta2.AutoSize = true;
            this.Pergunta5_resposta2.Location = new System.Drawing.Point(14, 106);
            this.Pergunta5_resposta2.Name = "Pergunta5_resposta2";
            this.Pergunta5_resposta2.Size = new System.Drawing.Size(85, 17);
            this.Pergunta5_resposta2.TabIndex = 2;
            this.Pergunta5_resposta2.TabStop = true;
            this.Pergunta5_resposta2.Text = "radioButton2";
            this.Pergunta5_resposta2.UseVisualStyleBackColor = true;
            // 
            // Pergunta5_resposta1
            // 
            this.Pergunta5_resposta1.AutoSize = true;
            this.Pergunta5_resposta1.Location = new System.Drawing.Point(14, 83);
            this.Pergunta5_resposta1.Name = "Pergunta5_resposta1";
            this.Pergunta5_resposta1.Size = new System.Drawing.Size(85, 17);
            this.Pergunta5_resposta1.TabIndex = 1;
            this.Pergunta5_resposta1.TabStop = true;
            this.Pergunta5_resposta1.Text = "radioButton1";
            this.Pergunta5_resposta1.UseVisualStyleBackColor = true;
            // 
            // lblpergunta5
            // 
            this.lblpergunta5.AutoSize = true;
            this.lblpergunta5.Location = new System.Drawing.Point(6, 34);
            this.lblpergunta5.Name = "lblpergunta5";
            this.lblpergunta5.Size = new System.Drawing.Size(35, 13);
            this.lblpergunta5.TabIndex = 0;
            this.lblpergunta5.Text = "label1";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.Pergunta4_resposta4);
            this.groupBox6.Controls.Add(this.Pergunta4_resposta3);
            this.groupBox6.Controls.Add(this.Pergunta4_resposta2);
            this.groupBox6.Controls.Add(this.Pergunta4_resposta1);
            this.groupBox6.Controls.Add(this.lblpergunta4);
            this.groupBox6.Location = new System.Drawing.Point(36, 221);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(256, 196);
            this.groupBox6.TabIndex = 9;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Pergunta4";
            // 
            // Pergunta4_resposta4
            // 
            this.Pergunta4_resposta4.AutoSize = true;
            this.Pergunta4_resposta4.Location = new System.Drawing.Point(14, 152);
            this.Pergunta4_resposta4.Name = "Pergunta4_resposta4";
            this.Pergunta4_resposta4.Size = new System.Drawing.Size(85, 17);
            this.Pergunta4_resposta4.TabIndex = 4;
            this.Pergunta4_resposta4.TabStop = true;
            this.Pergunta4_resposta4.Text = "radioButton4";
            this.Pergunta4_resposta4.UseVisualStyleBackColor = true;
            // 
            // Pergunta4_resposta3
            // 
            this.Pergunta4_resposta3.AutoSize = true;
            this.Pergunta4_resposta3.Location = new System.Drawing.Point(14, 129);
            this.Pergunta4_resposta3.Name = "Pergunta4_resposta3";
            this.Pergunta4_resposta3.Size = new System.Drawing.Size(85, 17);
            this.Pergunta4_resposta3.TabIndex = 3;
            this.Pergunta4_resposta3.TabStop = true;
            this.Pergunta4_resposta3.Text = "radioButton3";
            this.Pergunta4_resposta3.UseVisualStyleBackColor = true;
            // 
            // Pergunta4_resposta2
            // 
            this.Pergunta4_resposta2.AutoSize = true;
            this.Pergunta4_resposta2.Location = new System.Drawing.Point(14, 106);
            this.Pergunta4_resposta2.Name = "Pergunta4_resposta2";
            this.Pergunta4_resposta2.Size = new System.Drawing.Size(85, 17);
            this.Pergunta4_resposta2.TabIndex = 2;
            this.Pergunta4_resposta2.TabStop = true;
            this.Pergunta4_resposta2.Text = "radioButton2";
            this.Pergunta4_resposta2.UseVisualStyleBackColor = true;
            // 
            // Pergunta4_resposta1
            // 
            this.Pergunta4_resposta1.AutoSize = true;
            this.Pergunta4_resposta1.Location = new System.Drawing.Point(14, 83);
            this.Pergunta4_resposta1.Name = "Pergunta4_resposta1";
            this.Pergunta4_resposta1.Size = new System.Drawing.Size(85, 17);
            this.Pergunta4_resposta1.TabIndex = 1;
            this.Pergunta4_resposta1.TabStop = true;
            this.Pergunta4_resposta1.Text = "radioButton1";
            this.Pergunta4_resposta1.UseVisualStyleBackColor = true;
            // 
            // lblpergunta4
            // 
            this.lblpergunta4.AutoSize = true;
            this.lblpergunta4.Location = new System.Drawing.Point(6, 34);
            this.lblpergunta4.Name = "lblpergunta4";
            this.lblpergunta4.Size = new System.Drawing.Size(35, 13);
            this.lblpergunta4.TabIndex = 0;
            this.lblpergunta4.Text = "label1";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.Pergunta8_resposta4);
            this.groupBox7.Controls.Add(this.Pergunta8_resposta3);
            this.groupBox7.Controls.Add(this.Pergunta8_resposta2);
            this.groupBox7.Controls.Add(this.Pergunta8_resposta1);
            this.groupBox7.Controls.Add(this.lblpergunta8);
            this.groupBox7.Location = new System.Drawing.Point(312, 443);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(305, 196);
            this.groupBox7.TabIndex = 10;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Pergunta8";
            // 
            // Pergunta8_resposta4
            // 
            this.Pergunta8_resposta4.AutoSize = true;
            this.Pergunta8_resposta4.Location = new System.Drawing.Point(14, 152);
            this.Pergunta8_resposta4.Name = "Pergunta8_resposta4";
            this.Pergunta8_resposta4.Size = new System.Drawing.Size(85, 17);
            this.Pergunta8_resposta4.TabIndex = 4;
            this.Pergunta8_resposta4.TabStop = true;
            this.Pergunta8_resposta4.Text = "radioButton4";
            this.Pergunta8_resposta4.UseVisualStyleBackColor = true;
            // 
            // Pergunta8_resposta3
            // 
            this.Pergunta8_resposta3.AutoSize = true;
            this.Pergunta8_resposta3.Location = new System.Drawing.Point(14, 129);
            this.Pergunta8_resposta3.Name = "Pergunta8_resposta3";
            this.Pergunta8_resposta3.Size = new System.Drawing.Size(85, 17);
            this.Pergunta8_resposta3.TabIndex = 3;
            this.Pergunta8_resposta3.TabStop = true;
            this.Pergunta8_resposta3.Text = "radioButton3";
            this.Pergunta8_resposta3.UseVisualStyleBackColor = true;
            // 
            // Pergunta8_resposta2
            // 
            this.Pergunta8_resposta2.AutoSize = true;
            this.Pergunta8_resposta2.Location = new System.Drawing.Point(14, 106);
            this.Pergunta8_resposta2.Name = "Pergunta8_resposta2";
            this.Pergunta8_resposta2.Size = new System.Drawing.Size(85, 17);
            this.Pergunta8_resposta2.TabIndex = 2;
            this.Pergunta8_resposta2.TabStop = true;
            this.Pergunta8_resposta2.Text = "radioButton2";
            this.Pergunta8_resposta2.UseVisualStyleBackColor = true;
            // 
            // Pergunta8_resposta1
            // 
            this.Pergunta8_resposta1.AutoSize = true;
            this.Pergunta8_resposta1.Location = new System.Drawing.Point(14, 83);
            this.Pergunta8_resposta1.Name = "Pergunta8_resposta1";
            this.Pergunta8_resposta1.Size = new System.Drawing.Size(85, 17);
            this.Pergunta8_resposta1.TabIndex = 1;
            this.Pergunta8_resposta1.TabStop = true;
            this.Pergunta8_resposta1.Text = "radioButton1";
            this.Pergunta8_resposta1.UseVisualStyleBackColor = true;
            // 
            // lblpergunta8
            // 
            this.lblpergunta8.AutoSize = true;
            this.lblpergunta8.Location = new System.Drawing.Point(6, 34);
            this.lblpergunta8.Name = "lblpergunta8";
            this.lblpergunta8.Size = new System.Drawing.Size(35, 13);
            this.lblpergunta8.TabIndex = 0;
            this.lblpergunta8.Text = "label1";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.Pergunta9_resposta4);
            this.groupBox8.Controls.Add(this.Pergunta9_resposta3);
            this.groupBox8.Controls.Add(this.Pergunta9_resposta2);
            this.groupBox8.Controls.Add(this.Pergunta9_resposta1);
            this.groupBox8.Controls.Add(this.lblpergunta9);
            this.groupBox8.Location = new System.Drawing.Point(36, 645);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(331, 196);
            this.groupBox8.TabIndex = 9;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Pergunta9";
            // 
            // Pergunta9_resposta4
            // 
            this.Pergunta9_resposta4.AutoSize = true;
            this.Pergunta9_resposta4.Location = new System.Drawing.Point(14, 152);
            this.Pergunta9_resposta4.Name = "Pergunta9_resposta4";
            this.Pergunta9_resposta4.Size = new System.Drawing.Size(85, 17);
            this.Pergunta9_resposta4.TabIndex = 4;
            this.Pergunta9_resposta4.TabStop = true;
            this.Pergunta9_resposta4.Text = "radioButton4";
            this.Pergunta9_resposta4.UseVisualStyleBackColor = true;
            // 
            // Pergunta9_resposta3
            // 
            this.Pergunta9_resposta3.AutoSize = true;
            this.Pergunta9_resposta3.Location = new System.Drawing.Point(14, 129);
            this.Pergunta9_resposta3.Name = "Pergunta9_resposta3";
            this.Pergunta9_resposta3.Size = new System.Drawing.Size(85, 17);
            this.Pergunta9_resposta3.TabIndex = 3;
            this.Pergunta9_resposta3.TabStop = true;
            this.Pergunta9_resposta3.Text = "radioButton3";
            this.Pergunta9_resposta3.UseVisualStyleBackColor = true;
            // 
            // Pergunta9_resposta2
            // 
            this.Pergunta9_resposta2.AutoSize = true;
            this.Pergunta9_resposta2.Location = new System.Drawing.Point(14, 106);
            this.Pergunta9_resposta2.Name = "Pergunta9_resposta2";
            this.Pergunta9_resposta2.Size = new System.Drawing.Size(85, 17);
            this.Pergunta9_resposta2.TabIndex = 2;
            this.Pergunta9_resposta2.TabStop = true;
            this.Pergunta9_resposta2.Text = "radioButton2";
            this.Pergunta9_resposta2.UseVisualStyleBackColor = true;
            // 
            // Pergunta9_resposta1
            // 
            this.Pergunta9_resposta1.AutoSize = true;
            this.Pergunta9_resposta1.Location = new System.Drawing.Point(14, 83);
            this.Pergunta9_resposta1.Name = "Pergunta9_resposta1";
            this.Pergunta9_resposta1.Size = new System.Drawing.Size(85, 17);
            this.Pergunta9_resposta1.TabIndex = 1;
            this.Pergunta9_resposta1.TabStop = true;
            this.Pergunta9_resposta1.Text = "radioButton1";
            this.Pergunta9_resposta1.UseVisualStyleBackColor = true;
            // 
            // lblpergunta9
            // 
            this.lblpergunta9.AutoSize = true;
            this.lblpergunta9.Location = new System.Drawing.Point(6, 34);
            this.lblpergunta9.Name = "lblpergunta9";
            this.lblpergunta9.Size = new System.Drawing.Size(35, 13);
            this.lblpergunta9.TabIndex = 0;
            this.lblpergunta9.Text = "label1";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.Pergunta7_resposta4);
            this.groupBox9.Controls.Add(this.Pergunta7_resposta3);
            this.groupBox9.Controls.Add(this.Pergunta7_resposta2);
            this.groupBox9.Controls.Add(this.Pergunta7_resposta1);
            this.groupBox9.Controls.Add(this.lblpergunta7);
            this.groupBox9.Location = new System.Drawing.Point(36, 443);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(265, 196);
            this.groupBox9.TabIndex = 9;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Pergunta7";
            this.groupBox9.Enter += new System.EventHandler(this.groupBox9_Enter);
            // 
            // Pergunta7_resposta4
            // 
            this.Pergunta7_resposta4.AutoSize = true;
            this.Pergunta7_resposta4.Location = new System.Drawing.Point(14, 152);
            this.Pergunta7_resposta4.Name = "Pergunta7_resposta4";
            this.Pergunta7_resposta4.Size = new System.Drawing.Size(85, 17);
            this.Pergunta7_resposta4.TabIndex = 4;
            this.Pergunta7_resposta4.TabStop = true;
            this.Pergunta7_resposta4.Text = "radioButton4";
            this.Pergunta7_resposta4.UseVisualStyleBackColor = true;
            // 
            // Pergunta7_resposta3
            // 
            this.Pergunta7_resposta3.AutoSize = true;
            this.Pergunta7_resposta3.Location = new System.Drawing.Point(14, 129);
            this.Pergunta7_resposta3.Name = "Pergunta7_resposta3";
            this.Pergunta7_resposta3.Size = new System.Drawing.Size(85, 17);
            this.Pergunta7_resposta3.TabIndex = 3;
            this.Pergunta7_resposta3.TabStop = true;
            this.Pergunta7_resposta3.Text = "radioButton3";
            this.Pergunta7_resposta3.UseVisualStyleBackColor = true;
            // 
            // Pergunta7_resposta2
            // 
            this.Pergunta7_resposta2.AutoSize = true;
            this.Pergunta7_resposta2.Location = new System.Drawing.Point(14, 106);
            this.Pergunta7_resposta2.Name = "Pergunta7_resposta2";
            this.Pergunta7_resposta2.Size = new System.Drawing.Size(85, 17);
            this.Pergunta7_resposta2.TabIndex = 2;
            this.Pergunta7_resposta2.TabStop = true;
            this.Pergunta7_resposta2.Text = "radioButton2";
            this.Pergunta7_resposta2.UseVisualStyleBackColor = true;
            // 
            // Pergunta7_resposta1
            // 
            this.Pergunta7_resposta1.AutoSize = true;
            this.Pergunta7_resposta1.Location = new System.Drawing.Point(14, 83);
            this.Pergunta7_resposta1.Name = "Pergunta7_resposta1";
            this.Pergunta7_resposta1.Size = new System.Drawing.Size(85, 17);
            this.Pergunta7_resposta1.TabIndex = 1;
            this.Pergunta7_resposta1.TabStop = true;
            this.Pergunta7_resposta1.Text = "radioButton1";
            this.Pergunta7_resposta1.UseVisualStyleBackColor = true;
            // 
            // lblpergunta7
            // 
            this.lblpergunta7.AutoSize = true;
            this.lblpergunta7.Location = new System.Drawing.Point(6, 34);
            this.lblpergunta7.Name = "lblpergunta7";
            this.lblpergunta7.Size = new System.Drawing.Size(35, 13);
            this.lblpergunta7.TabIndex = 0;
            this.lblpergunta7.Text = "label1";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.Pergunta10_resposta4);
            this.groupBox10.Controls.Add(this.Pergunta10_resposta3);
            this.groupBox10.Controls.Add(this.Pergunta10_resposta2);
            this.groupBox10.Controls.Add(this.Pergunta10_resposta1);
            this.groupBox10.Controls.Add(this.lblpergunta10);
            this.groupBox10.Location = new System.Drawing.Point(418, 645);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(303, 196);
            this.groupBox10.TabIndex = 10;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Pergunta10";
            // 
            // Pergunta10_resposta4
            // 
            this.Pergunta10_resposta4.AutoSize = true;
            this.Pergunta10_resposta4.Location = new System.Drawing.Point(14, 152);
            this.Pergunta10_resposta4.Name = "Pergunta10_resposta4";
            this.Pergunta10_resposta4.Size = new System.Drawing.Size(85, 17);
            this.Pergunta10_resposta4.TabIndex = 4;
            this.Pergunta10_resposta4.TabStop = true;
            this.Pergunta10_resposta4.Text = "radioButton4";
            this.Pergunta10_resposta4.UseVisualStyleBackColor = true;
            // 
            // Pergunta10_resposta3
            // 
            this.Pergunta10_resposta3.AutoSize = true;
            this.Pergunta10_resposta3.Location = new System.Drawing.Point(14, 129);
            this.Pergunta10_resposta3.Name = "Pergunta10_resposta3";
            this.Pergunta10_resposta3.Size = new System.Drawing.Size(85, 17);
            this.Pergunta10_resposta3.TabIndex = 3;
            this.Pergunta10_resposta3.TabStop = true;
            this.Pergunta10_resposta3.Text = "radioButton3";
            this.Pergunta10_resposta3.UseVisualStyleBackColor = true;
            // 
            // Pergunta10_resposta2
            // 
            this.Pergunta10_resposta2.AutoSize = true;
            this.Pergunta10_resposta2.Location = new System.Drawing.Point(14, 106);
            this.Pergunta10_resposta2.Name = "Pergunta10_resposta2";
            this.Pergunta10_resposta2.Size = new System.Drawing.Size(85, 17);
            this.Pergunta10_resposta2.TabIndex = 2;
            this.Pergunta10_resposta2.TabStop = true;
            this.Pergunta10_resposta2.Text = "radioButton2";
            this.Pergunta10_resposta2.UseVisualStyleBackColor = true;
            // 
            // Pergunta10_resposta1
            // 
            this.Pergunta10_resposta1.AutoSize = true;
            this.Pergunta10_resposta1.Location = new System.Drawing.Point(14, 83);
            this.Pergunta10_resposta1.Name = "Pergunta10_resposta1";
            this.Pergunta10_resposta1.Size = new System.Drawing.Size(85, 17);
            this.Pergunta10_resposta1.TabIndex = 1;
            this.Pergunta10_resposta1.TabStop = true;
            this.Pergunta10_resposta1.Text = "radioButton1";
            this.Pergunta10_resposta1.UseVisualStyleBackColor = true;
            // 
            // lblpergunta10
            // 
            this.lblpergunta10.AutoSize = true;
            this.lblpergunta10.Location = new System.Drawing.Point(6, 34);
            this.lblpergunta10.Name = "lblpergunta10";
            this.lblpergunta10.Size = new System.Drawing.Size(35, 13);
            this.lblpergunta10.TabIndex = 0;
            this.lblpergunta10.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMargin = new System.Drawing.Size(0, 1200);
            this.ClientSize = new System.Drawing.Size(920, 484);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnFinalizar);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblpergunta1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.RadioButton Pergunta1_resposta4;
        private System.Windows.Forms.RadioButton Pergunta1_resposta3;
        private System.Windows.Forms.RadioButton Pergunta1_resposta2;
        private System.Windows.Forms.RadioButton Pergunta1_resposta1;
        private System.Windows.Forms.Button btnFinalizar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton Pergunta2_resposta4;
        private System.Windows.Forms.RadioButton Pergunta2_resposta3;
        private System.Windows.Forms.RadioButton Pergunta2_resposta2;
        private System.Windows.Forms.RadioButton Pergunta2_resposta1;
        private System.Windows.Forms.Label lblPergunta2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton Pergunta3_resposta4;
        private System.Windows.Forms.RadioButton Pergunta3_resposta3;
        private System.Windows.Forms.RadioButton Pergunta3_resposta2;
        private System.Windows.Forms.RadioButton Pergunta3_resposta1;
        private System.Windows.Forms.Label lblpergunta3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton Pergunta6_resposta4;
        private System.Windows.Forms.RadioButton Pergunta6_resposta3;
        private System.Windows.Forms.RadioButton Pergunta6_resposta2;
        private System.Windows.Forms.RadioButton Pergunta6_resposta1;
        private System.Windows.Forms.Label lblpergunta6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton Pergunta5_resposta4;
        private System.Windows.Forms.RadioButton Pergunta5_resposta3;
        private System.Windows.Forms.RadioButton Pergunta5_resposta2;
        private System.Windows.Forms.RadioButton Pergunta5_resposta1;
        private System.Windows.Forms.Label lblpergunta5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton Pergunta4_resposta4;
        private System.Windows.Forms.RadioButton Pergunta4_resposta3;
        private System.Windows.Forms.RadioButton Pergunta4_resposta2;
        private System.Windows.Forms.RadioButton Pergunta4_resposta1;
        private System.Windows.Forms.Label lblpergunta4;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton Pergunta8_resposta4;
        private System.Windows.Forms.RadioButton Pergunta8_resposta3;
        private System.Windows.Forms.RadioButton Pergunta8_resposta2;
        private System.Windows.Forms.RadioButton Pergunta8_resposta1;
        private System.Windows.Forms.Label lblpergunta8;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.RadioButton Pergunta9_resposta4;
        private System.Windows.Forms.RadioButton Pergunta9_resposta3;
        private System.Windows.Forms.RadioButton Pergunta9_resposta2;
        private System.Windows.Forms.RadioButton Pergunta9_resposta1;
        private System.Windows.Forms.Label lblpergunta9;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.RadioButton Pergunta7_resposta4;
        private System.Windows.Forms.RadioButton Pergunta7_resposta3;
        private System.Windows.Forms.RadioButton Pergunta7_resposta2;
        private System.Windows.Forms.RadioButton Pergunta7_resposta1;
        private System.Windows.Forms.Label lblpergunta7;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.RadioButton Pergunta10_resposta4;
        private System.Windows.Forms.RadioButton Pergunta10_resposta3;
        private System.Windows.Forms.RadioButton Pergunta10_resposta2;
        private System.Windows.Forms.RadioButton Pergunta10_resposta1;
        private System.Windows.Forms.Label lblpergunta10;
    }
}

