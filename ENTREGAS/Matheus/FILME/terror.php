<!DOCTYPE HTML>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title> MV FILMES </title>
		<meta name="description" content="Site de filmes gratuitos, e de todos os generos.">
		<meta name="keywords" content="Filmes, Series, Outros">
		<meta name="robots" content="index,follow">
		<meta name="author" content="Matheus Vanzelli">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">
		<link rel="icon" href="img/Documents.png">
			<script src="https://code.jquery.com/jquery-3.2.1.min.js"> </script>
	</head>
	<body>
		<header class="cabecalho">
			<a href="index.html"> <h1 class="logo"> MV FILMES </h1> </a>
			<button class="btn-menu"> <img src="https://png.icons8.com/menu/ios7/25"> </button>
			<nav class="menu">
				<a class="btn-close"> x </a>
				<ul>
					<li> <a href="index.html"> INICIO             </a> </li>
					<li> <a href="acao.php"> AÇÃO               </a> </li>
					<li> <a href="animacao.php"> ANIMAÇÃO           </a> </li>
					<li> <a href="comedia.php"> COMÉDIA 			 </a> </li>
					<li> <a href="ficcao.php"> FICÇÃO CIENTIFICA  </a> </li>
					<li> <a href="guerra.php"> GUERRA             </a> </li>
					<li> <a href="romance.php"> ROMANCE/DRAMA            </a> </li>
					<li> <a href="terror.php"> TERROR             </a> </li>
			</nav>
		</header>
		<div class="banner">
			<div class="title">
				<h2> TERROR/HORROR </h2>
				<div id="galeria">
					<a href="pages/invocacao2.php"> <img src="img/invocacao2.jpg" alt="teste"></img> </a>
					<a href="pages/ouijaojogodosespiritos.php"> <img src="img/ouija.jpg" alt="teste"></img> </a>
					<a href="pages/chamado3.php"> <img src="img/chamado3.jpg" alt="teste"></img> </a>
					<a href="pages/fragmentado.php"> <img src="img/fragmentado.jpg" alt="teste"></img> </a>
					<a href="pages/realidade.php"> <img src="img/realidade.jpg" alt="teste"></img> </a>
					<a href="pages/asluzes.php"> <img src="img/asluzes.jpg" alt="teste"></img> </a>
					<a href="pages/jessebelle.php"> <img src="img/jessebelle.jpg" alt="teste"></img> </a>
					<a href="pages/aocairdanoite.php"> <img src="img/aocairdanoite.jpg" alt="teste"></img> </a>
					<a href="pages/acura.php"> <img src="img/acura.jpg" alt="teste"></img> </a>
					<a href="pages/american.php"> <img src="img/american.jpg" alt="teste"></img> </a>
					<a href="pages/ababa.php"> <img src="img/ababa.jpg" alt="teste"></img> </a>
					<a href="pages/lavender.php"> <img src="img/lavender.jpg" alt="teste"></img> </a>
					<a href="pages/oultimoexorcismo.php"> <img src="img/oultimoexorcismo.jpg" alt="teste"></img> </a>
					<a href="pages/annabelle2.php"> <img src="img/annabelle2.jpg" alt="teste"></img> </a>
					<a href="pages/amityvilleodespertar.php"> <img src="img/amityvilleodespertar.jpg" alt="teste"></img> </a>
				</div>
				
			</div>
		</div>

	<script>
	$(".btn-menu").on('click', function(){
		$('.menu').show();
	});
	$(".btn-close").on('click', function(){
		$(".menu").hide();
	});
	</script>
	</body>
</html>