<!DOCTYPE HTML>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title> MV FILMES </title>
		<meta name="description" content="Site de filmes gratuitos, e de todos os generos.">
		<meta name="keywords" content="Filmes, Series, Outros">
		<meta name="robots" content="index,follow">
		<meta name="author" content="Matheus Vanzelli">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">
		<link rel="icon" href="img/Documents.png">
			<script src="https://code.jquery.com/jquery-3.2.1.min.js"> </script>
	</head>
	<body>
		<header class="cabecalho">
			<a href="index.html"> <h1 class="logo"> MV FILMES </h1> </a>
			<button class="btn-menu"> <img src="https://png.icons8.com/menu/ios7/25"> </button>
			<nav class="menu">
				<a class="btn-close"> x </a>
				<ul>
					<li> <a href="index.html"> INICIO             </a> </li>
					<li> <a href="acao.php"> AÇÃO               </a> </li>
					<li> <a href="animacao.php"> ANIMAÇÃO           </a> </li>
					<li> <a href="comedia.php"> COMÉDIA 			 </a> </li>
					<li> <a href="ficcao.php"> FICÇÃO CIENTIFICA  </a> </li>
					<li> <a href="guerra.php"> GUERRA             </a> </li>
					<li> <a href="romance.php"> ROMANCE/DRAMA            </a> </li>
					<li> <a href="terror.php"> TERROR             </a> </li>
			</nav>
		</header>
		<div class="banner">
			<div class="title">
				<h2> ANIMAÇÃO </h2>
				<div id="galeria">
					<a href="pages/guardioesdagalaxia.php"> <img src="img/guardioesdagalaxia.jpg" alt="teste"></img> </a>
					<a href="pages/bailarina.php"> <img src="img/bailarina.jpg" alt="teste"></img> </a>
					<a href="pages/sing.php"> <img src="img/sing.jpg" alt="teste"></img> </a>
					<a href="pages/monster.php"> <img src="img/monster.jpg" alt="teste"></img> </a>
					<a href="pages/powerrangers.php"> <img src="img/powerrangers.jpg" alt="teste"></img> </a>
					<a href="pages/homemaranhadevoltaaolar.php"> <img src="img/homemaranhadevoltaaolar.jpg" alt="teste"></img> </a>
					<a href="pages/legobatmanofilme.php"> <img src="img/legobatmanofilme.jpg" alt="teste"></img> </a>
					<a href="pages/opoderosochefinho.php"> <img src="img/opoderosochefinho.jpg" alt="teste"></img> </a>
					<a href="pages/carros3.php"> <img src="img/carros3.jpg" alt="teste"></img> </a>
					<a href="pages/capitaocueca.php"> <img src="img/capitaocueca.jpg" alt="teste"></img> </a>
					<a href="pages/pixels.php"> <img src="img/pixels.jpg" alt="teste"></img> </a>
					<a href="pages/emoji.php"> <img src="img/emojiofilme.jpg" alt="teste"></img> </a>
					<a href="pages/meumalvadofavorito3.php"> <img src="img/meumalvadofavorito3.jpg" alt="teste"></img> </a>
					<a href="pages/bobesponja.php"> <img src="img/bobesponja.jpg" alt="teste"></img> </a>
					<a href="pages/zootopia.php"> <img src="img/zootopia.jpg" alt="teste"></img> </a>
				</div>
				
			</div>
		</div>

	<script>
	$(".btn-menu").on('click', function(){
		$('.menu').show();
	});
	$(".btn-close").on('click', function(){
		$(".menu").hide();
	});
	</script>
	</body>
</html>