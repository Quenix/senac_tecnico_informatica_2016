<!DOCTYPE HTML>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title> MV FILMES </title>
		<meta name="description" content="Site de filmes gratuitos, e de todos os generos.">
		<meta name="keywords" content="Filmes, Series, Outros">
		<meta name="robots" content="index,follow">
		<meta name="author" content="Matheus Vanzelli">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">
		<link rel="icon" href="img/Documents.png">
			<script src="https://code.jquery.com/jquery-3.2.1.min.js"> </script>
	</head>
	<body>
		<header class="cabecalho">
			<a href="index.html"> <h1 class="logo"> MV FILMES </h1> </a>
			<button class="btn-menu"> <img src="https://png.icons8.com/menu/ios7/25"> </button>
			<nav class="menu">
				<a class="btn-close"> x </a>
				<ul>
					<li> <a href="index.html"> INICIO             </a> </li>
					<li> <a href="acao.php"> AÇÃO               </a> </li>
					<li> <a href="animacao.php"> ANIMAÇÃO           </a> </li>
					<li> <a href="comedia.php"> COMÉDIA 			 </a> </li>
					<li> <a href="ficcao.php"> FICÇÃO CIENTIFICA  </a> </li>
					<li> <a href="guerra.php"> GUERRA             </a> </li>
					<li> <a href="romance.php"> ROMANCE/DRAMA           </a> </li>
					<li> <a href="terror.php"> TERROR             </a> </li>
			</nav>
		</header>
		<div class="banner">
			<div class="title">
				<h2> FICÇÃO CIENTÍFICA </h2>
				<div id="galeria">
					<a href="pages/atorrenegra.php"> <img src="img/atorrenegra.jpg" alt="teste"></img> </a>
					<a href="pages/planetadosmacacos.php"> <img src="img/planetadosmacacos.jpg" alt="teste"></img> </a>
					<a href="pages/ondeestasegunda.php"> <img src="img/ondeestasegunda.jpg" alt="teste"></img> </a>
					<a href="pages/kong.php"> <img src="img/kong.jpg" alt="teste"></img> </a>
					<a href="pages/creed.php"> <img src="img/creed.jpg" alt="teste"></img> </a>
					<a href="pages/esquadrao.php"> <img src="img/esquadrao.jpg" alt="teste"></img> </a>
					<a href="pages/descoberta.php"> <img src="img/descoberta.jpg" alt="teste"></img> </a>
					<a href="pages/vida.php"> <img src="img/vida.jpg" alt="teste"></img> </a>
					<a href="pages/lucy.php"> <img src="img/lucy.jpg" alt="teste"></img> </a>
					<a href="pages/umanoitedecrime.php"> <img src="img/umanoitedecrime.jpg" alt="teste"></img> </a>
					<a href="pages/olegadodeosiris.php"> <img src="img/olegadodeosiris.jpg" alt="teste"></img> </a>
					<a href="pages/quartetofantastico4.php"> <img src="img/quartetofantastico4.jpg" alt="teste"></img> </a>
					<a href="pages/acura.php"> <img src="img/acura.jpg" alt="teste"></img> </a>
					<a href="pages/powerrangers.php"> <img src="img/powerrangers.jpg" alt="teste"></img> </a>
					<a href="pages/alemdaescuridao.php"> <img src="img/alemdaescuridao.jpg" alt="teste"></img> </a>
					<a href="pages/oguerreiro.php"> <img src="img/guerreiro.jpg" alt="teste"></img> </a>
				</div>
				
			</div>
		</div>

	<script>
	$(".btn-menu").on('click', function(){
		$('.menu').show();
	});
	$(".btn-close").on('click', function(){
		$(".menu").hide();
	});
	</script>
	</body>
</html>