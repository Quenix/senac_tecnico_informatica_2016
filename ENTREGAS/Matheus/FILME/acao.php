<!DOCTYPE HTML>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title> MV FILMES </title>
		<meta name="description" content="Site de filmes gratuitos, e de todos os generos.">
		<meta name="keywords" content="Filmes, Series, Outros">
		<meta name="robots" content="index,follow">
		<meta name="author" content="Matheus Vanzelli">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">
		<link rel="icon" href="img/Documents.png">
			<script src="https://code.jquery.com/jquery-3.2.1.min.js"> </script>
	</head>
	<body>
		<header class="cabecalho">
			<a href="index.html"> <h1 class="logo"> MV FILMES </h1> </a>
			<button class="btn-menu"> <img src="https://png.icons8.com/menu/ios7/25"> </button>
			<nav class="menu">
				<a class="btn-close"> x </a>
				<ul>
					<li> <a href="index.html"> INICIO             </a> </li>
					<li> <a href="acao.php"> AÇÃO               </a> </li>
					<li> <a href="animacao.php"> ANIMAÇÃO           </a> </li>
					<li> <a href="comedia.php"> COMÉDIA 			 </a> </li>
					<li> <a href="ficcao.php"> FICÇÃO CIENTIFICA  </a> </li>
					<li> <a href="guerra.php"> GUERRA             </a> </li>
					<li> <a href="romance.php"> ROMANCE/DRAMA          </a> </li>
					<li> <a href="terror.php"> TERROR             </a> </li>
			</nav>
		</header>
		<div class="banner">
			<div class="title">
				<h2> AÇÃO </h2>
				<div id="galeria">
					<a href="pages/segurancaemrisco.php"> <img src="img/segurancaemrisco.jpg" alt="teste"></img> </a>
					<a href="pages/malibu.php"> <img src="img/malibu.jpg" alt="teste"></img> </a>
					<a href="pages/logan.php"> <img src="img/logan.jpg" alt="teste"></img> </a>
					<a href="pages/vizinhos.php"> <img src="img/vizinhos.jpg" alt="teste"></img> </a>
					<a href="pages/acidadeperdida.php"> <img src="img/acidadeperdida.jpg" alt="teste"></img> </a>
					<a href="pages/vl8.php"> <img src="img/vl8.jpg" alt="teste"></img> </a>
					<a href="pages/rev.php"> <img src="img/rev.jpg" alt="teste"></img> </a>
					<a href="pages/oguerreiro.php"> <img src="img/guerreiro.jpg" alt="teste"></img> </a>
					<a href="pages/alemdaescuridao.php"> <img src="img/alemdaescuridao.jpg" alt="teste"></img> </a>
					<a href="pages/pixels.php"> <img src="img/pixels.jpg" alt="teste"></img> </a>
					<a href="pages/bushwick.php"> <img src="img/bushwick.jpg" alt="teste"></img> </a>
					<a href="pages/nolimite.php"> <img src="img/nolimite.jpg" alt="teste"></img> </a>
					<a href="pages/wheelman.php"> <img src="img/wheelman.jpg" alt="teste"></img> </a>
					<a href="pages/mulhermaravilha.php"> <img src="img/mulhermaravilha.jpg" alt="teste"></img> </a>
					<a href="pages/transformersoultimocavaleiro.php"> <img src="img/transformersoultimocavaleiro.jpg" alt="teste"></img> </a>
					<a href="pages/planetadosmacacos.php"> <img src="img/planetadosmacacos.jpg" alt="teste"></img> </a>
					<a href="pages/piratasdocaribeavingancadesalazar.php"> <img src="img/piratasdocaribeavingancadesalazar.jpg" alt="teste"></img> </a>
					<a href="pages/guerracivil.php"> <img src="img/guerracivil.jpg" alt="teste"></img> </a>

				</div>
				
			</div>
		</div>

	<script>
	$(".btn-menu").on('click', function(){
		$('.menu').show();
	});
	$(".btn-close").on('click', function(){
		$(".menu").hide();
	});
	</script>
	</body>
</html>