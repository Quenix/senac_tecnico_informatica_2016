<!DOCTYPE HTML>
<html lang="pt-br">
	<head>
			<link rel="stylesheet" type="text/css" href="../css/style.css">
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title> MV FILMES </title>
		<meta name="description" content="Site de filmes gratuitos, e de todos os generos.">
		<meta name="keywords" content="Filmes, Series, Outros">
		<meta name="robots" content="index,follow">
		<meta name="author" content="Matheus Vanzelli">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">
		<link rel="icon" href="img/Documents.png">
			<script src="https://code.jquery.com/jquery-3.2.1.min.js"> </script>
	</head>
	<body>
		<header class="stcabecalho">
			<a href="index.html"> <h1 class="stlogo"> MV FILMES </h1> </a>
			<button class="btn-menu"> <img src="https://png.icons8.com/menu/ios7/25"> </button>
			<nav class="menu">
				<a class="btn-close"> x </a>
				<ul>
					<li> <a href="index.html"> INICIO             </a> </li>
					<li> <a href="acao.php"> AÇÃO               </a> </li>
					<li> <a href="animacao.php"> ANIMAÇÃO           </a> </li>
					<li> <a href="comedia.php"> COMÉDIA 			 </a> </li>
					<li> <a href="ficcao.php"> FICÇÃO CIENTIFICA  </a> </li>
					<li> <a href="guerra.php"> GUERRA             </a> </li>
					<li> <a href="romance.php"> ROMANCE/DRAMA           </a> </li>
					<li> <a href="terror.php"> TERROR             </a> </li>
			</nav>
		</header>
		<div class="banner">
			<div class="title">
				<h2> STRANGER THINGS  </h2>
				<div id="galeria">
					<div class="title">
					<h3> TEMPORADA 1 </h3>
					</div>
						<ul>
							<li> <a href="../series/agentecateretep1.php"> <img src="../img/agentecater.jpg" alt="teste"> </img> </a> </l1>
					
					
				</div>	
			</div>
		</div>

	<script>
	$(".btn-menu").on('click', function(){
		$('.menu').show();
	});
	$(".btn-close").on('click', function(){
		$(".menu").hide();
	});
	</script>
	</body>
</html>