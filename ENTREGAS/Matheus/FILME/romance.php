<!DOCTYPE HTML>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title> MV FILMES </title>
		<meta name="description" content="Site de filmes gratuitos, e de todos os generos.">
		<meta name="keywords" content="Filmes, Series, Outros">
		<meta name="robots" content="index,follow">
		<meta name="author" content="Matheus Vanzelli">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">
		<link rel="icon" href="img/Documents.png">
			<script src="https://code.jquery.com/jquery-3.2.1.min.js"> </script>
	</head>
	<body>
		<header class="cabecalho">
			<a href="index.html"> <h1 class="logo"> MV FILMES </h1> </a>
			<button class="btn-menu"> <img src="https://png.icons8.com/menu/ios7/25"> </button>
			<nav class="menu">
				<a class="btn-close"> x </a>
				<ul>
					<li> <a href="index.html"> INICIO             </a> </li>
					<li> <a href="acao.php"> AÇÃO               </a> </li>
					<li> <a href="animacao.php"> ANIMAÇÃO           </a> </li>
					<li> <a href="comedia.php"> COMÉDIA 			 </a> </li>
					<li> <a href="ficcao.php"> FICÇÃO CIENTIFICA  </a> </li>
					<li> <a href="guerra.php"> GUERRA             </a> </li>
					<li> <a href="romance.php"> ROMANCE/DRAMA            </a> </li>
					<li> <a href="terror.php"> TERROR             </a> </li>
			</nav>
		</header>
		<div class="banner">
			<div class="title">
				<h2> ROMANCE/DRAMA </h2>
				<div id="galeria">
					<a href="pages/abelaeafera.php"> <img src="img/abelaeafera.jpg" alt="teste"></img> </a>
					<a href="pages/ovendedordesonhos.php"> <img src="img/ovendedordesonhos.jpg" alt="teste"></img> </a>
					<a href="pages/acabana.php"> <img src="img/acabana.jpg" alt="teste"></img> </a>
					<a href="pages/nos.php"> <img src="img/nos.jpg" alt="teste"></img> </a>
					<a href="pages/ele.php"> <img src="img/ele.png" alt="teste"></img> </a>
					<a href="pages/sete.php"> <img src="img/sete.jpg" alt="teste"></img> </a>
					<a href="pages/fome.php"> <img src="img/fome.jpg" alt="teste"></img> </a>
					<a href="pages/mar.php"> <img src="img/mar.png" alt="teste"></img> </a>
					<a href="pages/amantes.php"> <img src="img/amantes.jpg" alt="teste"></img> </a>
					<a href="pages/peles.php"> <img src="img/peles.jpg" alt="teste"></img> </a>
					<a href="pages/arma.php"> <img src="img/arma.jpg" alt="teste"></img> </a>
					<a href="pages/descoberta.php"> <img src="img/descoberta.jpg" alt="teste"></img> </a>
					<a href="pages/okja.php"> <img src="img/okja.jpg" alt="teste"></img> </a>
					<a href="pages/campo.php"> <img src="img/campo.jpg" alt="teste"></img> </a>
					<a href="pages/parispodeesperar.php"> <img src="img/parispodeesperar.jpg" alt="teste"></img> </a>			
					<a href="pages/vencendoopassado.php"> <img src="img/vencendoopassado.jpg" alt="teste"></img> </a>
					<a href="pages/cinderela.php"> <img src="img/cinderela.jpg" alt="teste"></img> </a>
					<a href="pages/nossasnoites.php"> <img src="img/nossasnoites.jpg" alt="teste"></img> </a>
				</div>
				
			</div>
		</div>

	<script>
	$(".btn-menu").on('click', function(){
		$('.menu').show();
	});
	$(".btn-close").on('click', function(){
		$(".menu").hide();
	});
	</script>
	</body>
</html>