<!DOCTYPE HTML>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title> MV FILMES </title>
		<meta name="description" content="Site de filmes gratuitos, e de todos os generos.">
		<meta name="keywords" content="Filmes, Series, Outros">
		<meta name="robots" content="index,follow">
		<meta name="author" content="Matheus Vanzelli">
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">
		<link rel="icon" href="img/Documents.png">
			<script src="https://code.jquery.com/jquery-3.2.1.min.js"> </script>
	</head>
	<body>
		<header class="cabecalho">
			<a href="index.html"> <h1 class="logo"> MV FILMES </h1> </a>
			<button class="btn-menu"> <img src="https://png.icons8.com/menu/ios7/25"> </button>
			<nav class="menu">
				<a class="btn-close"> x </a>
				<ul>
					<li> <a href="index.html"> INICIO             </a> </li>
					<li> <a href="acao.php"> AÇÃO               </a> </li>
					<li> <a href="animacao.php"> ANIMAÇÃO           </a> </li>
					<li> <a href="comedia.php"> COMÉDIA 			 </a> </li>
					<li> <a href="ficcao.php"> FICÇÃO CIENTIFICA  </a> </li>
					<li> <a href="guerra.php"> GUERRA             </a> </li>
					<li> <a href="romance.php"> ROMANCE/DRAMA            </a> </li>
					<li> <a href="terror.php"> TERROR             </a> </li>
			</nav>
		</header>
		<div class="banner">
			<div class="title">
				<h2> COMÉDIA </h2>
				<div id="galeria">
					<a href="pages/entrasai.php"> <img src="img/entrasai.jpg" alt="teste"></img> </a>
					<a href="pages/internetofilme.php"> <img src="img/internetofilme.jpg" alt="teste"></img> </a>
					<a href="pages/loko.php"> <img src="img/loko.jpg" alt="teste"></img> </a>
					<a href="pages/peca.php"> <img src="img/peca.jpg" alt="teste"></img> </a>
					<a href="pages/afabulosa.php"> <img src="img/afabulosa.png" alt="teste"></img> </a>
					<a href="pages/t2.php"> <img src="img/t2.jpg" alt="teste"></img> </a>
					<a href="pages/vizinhos.php"> <img src="img/vizinhos.jpg" alt="teste"></img> </a>
					<a href="pages/ele.php"> <img src="img/ele.png" alt="teste"></img> </a>
					<a href="pages/tudo.php"> <img src="img/tudo.jpg" alt="teste"></img> </a>
					<a href="pages/emoji.php"> <img src="img/emojiofilme.jpg" alt="teste"></img> </a>
					<a href="pages/carros3.php"> <img src="img/carros3.jpg" alt="teste"></img> </a>
					<a href="pages/opoderosochefinho.php"> <img src="img/opoderosochefinho.jpg" alt="teste"></img> </a>
					<a href="pages/offline.php"> <img src="img/offline.jpg" alt="teste"></img> </a>
					<a href="pages/zootopia.php"> <img src="img/zootopia.jpg" alt="teste"></img> </a>
					<a href="pages/pequenodemonio.php"> <img src="img/pequenodemonio.jpg" alt="teste"></img> </a>
					<a href="pages/finalmente18.php"> <img src="img/finalmente18.jpg" alt="teste"></img> </a>
					<a href="pages/ted2.php"> <img src="img/ted2.jpg" alt="teste"></img> </a>
					<a href="pages/geniosdocrime.php"> <img src="img/geniosdocrime.jpg" alt="teste"></img> </a>
					<a href="pages/capitaofantastico.php"> <img src="img/capitaofantastico.jpg" alt="teste"></img> </a>
					<a href="pages/capitaocueca.php"> <img src="img/capitaocueca.jpg" alt="teste"></img> </a>
					<a href="pages/meumalvadofavorito3.php"> <img src="img/meumalvadofavorito3.jpg" alt="teste"></img> </a>
				</div>
				
			</div>
		</div>

	<script>
	$(".btn-menu").on('click', function(){
		$('.menu').show();
	});
	$(".btn-close").on('click', function(){
		$(".menu").hide();
	});
	</script>
	</body>
</html>