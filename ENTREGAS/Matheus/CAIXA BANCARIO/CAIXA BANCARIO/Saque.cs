﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CAIXA_BANCARIO
{
    public partial class Saque : Form
    {
        public Saque()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double valor = Convert.ToDouble(txtSaque.Text);

            if (valor > 0)
            {
                Form1.saldo -= valor;
                MessageBox.Show("SAQUE EFETUADO COM SUCESSO" + "\n" + "SEU SALDO É DE: " + Form1.saldo);
            }
                else
            {
                MessageBox.Show("VALOR INVALIDO");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
