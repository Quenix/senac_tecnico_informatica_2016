﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CAIXA_BANCARIO
{
    public partial class Deposito : Form
    {
        public Deposito()
        {
            InitializeComponent();
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDepositar_Click(object sender, EventArgs e)
        {
            double valor = Convert.ToDouble(txtDeposito.Text);

            if (valor > 0)
            {
                Form1.saldo += valor;
                MessageBox.Show("DEPOSITO EFETUADO COM SUCESSO" + "\n" + "SEU SALDO É DE: " + Form1.saldo);
            }
                else
            {
                MessageBox.Show("VALOR INVALIDO");
            }
        }
    }
}
