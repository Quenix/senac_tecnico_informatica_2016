﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CAIXA_BANCARIO
{
    public partial class Form1 : Form
    {
        public static double saldo = 1000;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnSaldo_Click(object sender, EventArgs e)
        {
            var saldo = new Saldo();
            saldo.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var saque = new Saque();
            saque.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var deposito = new Deposito();
            deposito.ShowDialog();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
