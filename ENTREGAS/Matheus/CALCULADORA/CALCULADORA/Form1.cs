﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CALCULADORA
{
    public partial class Form1 : Form
    {
        string operacao;
        int valorA, valorB, resultado;

        public Form1()
        {

            InitializeComponent();
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            valorA = Convert.ToInt32(txtValorA.Text);
            valorB = Convert.ToInt32(txtValorB.Text);


            if (operacao == "soma")
            {
                resultado = valorA + valorB;
                MessageBox.Show("Seu resultado é :" + resultado);
            }

            if (operacao == "subtracao")
            {
                resultado = valorA - valorB;
                MessageBox.Show("Seu resultado é :" + resultado);
            }

            if (operacao == "divisão")
            {
                resultado = valorA / valorB;
                MessageBox.Show("Seu resultado é :" + resultado);
            }

            if (operacao == "multiplicação")
            {
                resultado = valorA * valorB;
                MessageBox.Show("Seu resultado é :" + resultado);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            operacao = "subtracao";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            operacao = "divisão";
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnMultiplicação_Click(object sender, EventArgs e)
        {
            operacao = "multiplicação";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            operacao = "soma";
            
        }
    }
}
