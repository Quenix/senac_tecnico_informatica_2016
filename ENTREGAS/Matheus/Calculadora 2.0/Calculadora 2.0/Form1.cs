﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora_2._0
{
    public partial class Form1 : Form
    {
        double valor1, valor2, resultado;
        string operacao;

        private void btnUm_Click(object sender, EventArgs e)
        {
            txtValor.Text += "1";
        }

        private void btnDois_Click(object sender, EventArgs e)
        {
            txtValor.Text += "2";
        }

        private void btnTrez_Click(object sender, EventArgs e)
        {
            txtValor.Text += "3";
        }

        private void btnQuatro_Click(object sender, EventArgs e)
        {
            txtValor.Text += "4";
        }

        private void btnCinco_Click(object sender, EventArgs e)
        {
            txtValor.Text += "5";
        }

        private void btnSeis_Click(object sender, EventArgs e)
        {
            txtValor.Text += "6";
        }

        private void btnSete_Click(object sender, EventArgs e)
        {
            txtValor.Text += "7";
        }

        private void btnOito_Click(object sender, EventArgs e)
        {
            txtValor.Text += "8";
        }

        private void btnNove_Click(object sender, EventArgs e)
        {
            txtValor.Text += "9";
        }

        private void btnSoma_Click(object sender, EventArgs e)
        {
            operacao = "soma";
            valor1 = Convert.ToDouble(txtValor.Text);
            txtValor.Text = "";
        }

        private void btnSubtracao_Click(object sender, EventArgs e)
        {
            operacao = "subtracao";
            valor1 = Convert.ToDouble(txtValor.Text);
            txtValor.Text = "";
        }

        private void btnMultiplicacao_Click(object sender, EventArgs e)
        {
            operacao = "multiplicacao";
            valor1 = Convert.ToDouble(txtValor.Text);
            txtValor.Text = "";
        }

        private void btnDivisao_Click(object sender, EventArgs e)
        {
            operacao = "divisao";
            valor1 = Convert.ToDouble(txtValor.Text);
            txtValor.Text = "";
        }

        private void btnVirgula_Click(object sender, EventArgs e)
        {
            txtValor.Text += ",";
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            valor1 = 0;
            valor2 = 0;
            operacao = "";
            txtValor.Text = "";
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            valor2 = Convert.ToDouble(txtValor.Text);

            if(operacao == "soma")
            {
                resultado = valor1 + valor2;
            }

            if(operacao == "subtracao")
            {
                resultado = valor1 - valor2;
            }

            if(operacao == "multiplicacao")
            {
                resultado = valor1 * valor2;
            }

            if(operacao == "divisao")
            {
                resultado = valor1 / valor2;
            }

            txtValor.Text = Convert.ToString(resultado);
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void btnZero_Click(object sender, EventArgs e)
        {
            txtValor.Text += "0";
        }
    }
}
