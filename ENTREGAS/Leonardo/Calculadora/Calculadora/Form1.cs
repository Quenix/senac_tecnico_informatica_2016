﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Form1 : Form
    {
        //Declaração das variáveis
        string operacao;
        int valor1, valor2;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnSoma_Click(object sender, EventArgs e)
        {
            
            operacao = "soma";
        }

        private void btnResultado_Click(object sender, EventArgs e)
        {
            if (operacao == "soma")
            {
                MessageBox.Show("Isto é uma soma.");
            }
            else
            {
                MessageBox.Show("Não clicou em nada, rapaz!");
            }
        }
    }
}
