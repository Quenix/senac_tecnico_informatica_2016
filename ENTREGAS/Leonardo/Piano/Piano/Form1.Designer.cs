﻿namespace Piano
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEscalaDiatonica = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnEscalaDiatonica
            // 
            this.btnEscalaDiatonica.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEscalaDiatonica.Location = new System.Drawing.Point(91, 86);
            this.btnEscalaDiatonica.Name = "btnEscalaDiatonica";
            this.btnEscalaDiatonica.Size = new System.Drawing.Size(254, 61);
            this.btnEscalaDiatonica.TabIndex = 0;
            this.btnEscalaDiatonica.Text = "Escala Diatônica";
            this.btnEscalaDiatonica.UseVisualStyleBackColor = true;
            this.btnEscalaDiatonica.Click += new System.EventHandler(this.btnEscalaDiatonica_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 224);
            this.Controls.Add(this.btnEscalaDiatonica);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnEscalaDiatonica;
    }
}

