﻿namespace Calculadora
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_adição = new System.Windows.Forms.Button();
            this.btn_divisão = new System.Windows.Forms.Button();
            this.btn_Subtraçao = new System.Windows.Forms.Button();
            this.btn_multiplicação = new System.Windows.Forms.Button();
            this.txtValor1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtValor2 = new System.Windows.Forms.TextBox();
            this.btn_igual = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_adição
            // 
            this.btn_adição.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_adição.Location = new System.Drawing.Point(12, 137);
            this.btn_adição.Name = "btn_adição";
            this.btn_adição.Size = new System.Drawing.Size(65, 50);
            this.btn_adição.TabIndex = 0;
            this.btn_adição.Text = "+";
            this.btn_adição.UseVisualStyleBackColor = true;
            this.btn_adição.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_divisão
            // 
            this.btn_divisão.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_divisão.Location = new System.Drawing.Point(12, 208);
            this.btn_divisão.Name = "btn_divisão";
            this.btn_divisão.Size = new System.Drawing.Size(65, 50);
            this.btn_divisão.TabIndex = 1;
            this.btn_divisão.Text = "/";
            this.btn_divisão.UseVisualStyleBackColor = true;
            this.btn_divisão.Click += new System.EventHandler(this.button2_Click);
            // 
            // btn_Subtraçao
            // 
            this.btn_Subtraçao.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Subtraçao.Location = new System.Drawing.Point(184, 137);
            this.btn_Subtraçao.Name = "btn_Subtraçao";
            this.btn_Subtraçao.Size = new System.Drawing.Size(66, 50);
            this.btn_Subtraçao.TabIndex = 2;
            this.btn_Subtraçao.Text = "-";
            this.btn_Subtraçao.UseVisualStyleBackColor = true;
            this.btn_Subtraçao.Click += new System.EventHandler(this.btn_Subtraçao_Click);
            // 
            // btn_multiplicação
            // 
            this.btn_multiplicação.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_multiplicação.Location = new System.Drawing.Point(184, 208);
            this.btn_multiplicação.Name = "btn_multiplicação";
            this.btn_multiplicação.Size = new System.Drawing.Size(66, 50);
            this.btn_multiplicação.TabIndex = 3;
            this.btn_multiplicação.Text = "*";
            this.btn_multiplicação.UseVisualStyleBackColor = true;
            this.btn_multiplicação.Click += new System.EventHandler(this.btn_multiplicação_Click);
            // 
            // txtValor1
            // 
            this.txtValor1.Location = new System.Drawing.Point(12, 80);
            this.txtValor1.Name = "txtValor1";
            this.txtValor1.Size = new System.Drawing.Size(93, 20);
            this.txtValor1.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(56, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 29);
            this.label1.TabIndex = 6;
            this.label1.Text = "Calculadora";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtValor2
            // 
            this.txtValor2.Location = new System.Drawing.Point(147, 80);
            this.txtValor2.Name = "txtValor2";
            this.txtValor2.Size = new System.Drawing.Size(103, 20);
            this.txtValor2.TabIndex = 7;
            // 
            // btn_igual
            // 
            this.btn_igual.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_igual.Location = new System.Drawing.Point(92, 162);
            this.btn_igual.Name = "btn_igual";
            this.btn_igual.Size = new System.Drawing.Size(75, 57);
            this.btn_igual.TabIndex = 8;
            this.btn_igual.Text = "=";
            this.btn_igual.UseVisualStyleBackColor = true;
            this.btn_igual.Click += new System.EventHandler(this.btn_igual_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(287, 286);
            this.Controls.Add(this.btn_igual);
            this.Controls.Add(this.txtValor2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtValor1);
            this.Controls.Add(this.btn_multiplicação);
            this.Controls.Add(this.btn_Subtraçao);
            this.Controls.Add(this.btn_divisão);
            this.Controls.Add(this.btn_adição);
            this.Name = "Form1";
            this.Text = "Calculadora";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_adição;
        private System.Windows.Forms.Button btn_divisão;
        private System.Windows.Forms.Button btn_Subtraçao;
        private System.Windows.Forms.Button btn_multiplicação;
        private System.Windows.Forms.TextBox txtValor1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtValor2;
        private System.Windows.Forms.Button btn_igual;
    }
}

