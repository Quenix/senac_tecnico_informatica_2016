﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Form1 : Form
    {
        int valor1, valor2;
        string
        operacao;


        public Form1()
        {
            InitializeComponent();
        }
        
        private void button2_Click(object sender, EventArgs e)
        {

            operacao = "divisao";
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            operacao = "soma";
        }

        private void btn_Subtraçao_Click(object sender, EventArgs e)
        {
            operacao = "subtração";
        }

        private void btn_igual_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btn_multiplicação_Click(object sender, EventArgs e)

        {
            operacao = "multiplicacao";
        }

        private void btn_igual_Click_1(object sender, EventArgs e)
        {
          try
          { 
            valor1 = Convert.ToInt32(txtValor1.Text);
            valor2 = Convert.ToInt32(txtValor2.Text);

                switch (operacao)
                {

                    case "soma":
                        MessageBox.Show("Resultado: " + (valor1 + valor2));
                        break;

                    case "subtração":
                        MessageBox.Show("Resultado: " + (valor1 - valor2));
                        break;

                    case "multiplicacao":
                        MessageBox.Show("Resultado: " + (valor1 * valor2));
                        break;

                    case "divisao":
                        if (valor2 == 0)
                        {
                            label1.Text = "Tentou dividir por zero";
                        }
                        else
                        {
                            MessageBox.Show("Resultado: " + (valor1 / valor2));

                        }
                        break;
                }
                
          }
            catch (Exception)
            {
                MessageBox.Show("Errou!");
            }
               

          
        }
    }
}