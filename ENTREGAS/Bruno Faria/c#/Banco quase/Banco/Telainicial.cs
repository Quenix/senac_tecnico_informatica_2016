﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Banco
{
    public partial class Telainicial : Form
    {
        public Telainicial()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var Saque = new Saque();
            Saque.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var Deposito = new Deposito();
            Deposito.ShowDialog();
        
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Seu saldo é R$" + FMenu.saldo);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
