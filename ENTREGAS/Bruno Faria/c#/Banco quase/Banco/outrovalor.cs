﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Banco
{
    public partial class outrovalor : Form
    {
        public outrovalor()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FMenu.saque = Convert.ToDouble(txtSValor.Text);
            FMenu.saldo = (FMenu.saque - FMenu.saldo);
            MessageBox.Show(FMenu.saque + " Reais foram retirados");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
