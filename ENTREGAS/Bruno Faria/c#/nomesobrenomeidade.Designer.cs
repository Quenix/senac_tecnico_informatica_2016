﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblnome = new System.Windows.Forms.Label();
            this.lblSobrenome = new System.Windows.Forms.Label();
            this.lblDatanasc = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.txtsobrenome = new System.Windows.Forms.TextBox();
            this.txtidade = new System.Windows.Forms.TextBox();
            this.lblmostranome = new System.Windows.Forms.Label();
            this.lblmostraidade = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblnome
            // 
            this.lblnome.AutoSize = true;
            this.lblnome.Location = new System.Drawing.Point(12, 26);
            this.lblnome.Name = "lblnome";
            this.lblnome.Size = new System.Drawing.Size(38, 13);
            this.lblnome.TabIndex = 0;
            this.lblnome.Text = "Nome:";
            this.lblnome.Click += new System.EventHandler(this.lblnome_Click);
            // 
            // lblSobrenome
            // 
            this.lblSobrenome.AutoSize = true;
            this.lblSobrenome.Location = new System.Drawing.Point(124, 26);
            this.lblSobrenome.Name = "lblSobrenome";
            this.lblSobrenome.Size = new System.Drawing.Size(64, 13);
            this.lblSobrenome.TabIndex = 2;
            this.lblSobrenome.Text = "Sobrenome:";
            // 
            // lblDatanasc
            // 
            this.lblDatanasc.AutoSize = true;
            this.lblDatanasc.Location = new System.Drawing.Point(16, 62);
            this.lblDatanasc.Name = "lblDatanasc";
            this.lblDatanasc.Size = new System.Drawing.Size(105, 13);
            this.lblDatanasc.TabIndex = 3;
            this.lblDatanasc.Text = "Data de nascimento:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(27, 203);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(221, 49);
            this.button1.TabIndex = 4;
            this.button1.Text = "Gravar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtnome
            // 
            this.txtnome.Location = new System.Drawing.Point(48, 23);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(70, 20);
            this.txtnome.TabIndex = 1;
            this.txtnome.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // txtsobrenome
            // 
            this.txtsobrenome.Location = new System.Drawing.Point(191, 23);
            this.txtsobrenome.Name = "txtsobrenome";
            this.txtsobrenome.Size = new System.Drawing.Size(74, 20);
            this.txtsobrenome.TabIndex = 2;
            // 
            // txtidade
            // 
            this.txtidade.Location = new System.Drawing.Point(127, 59);
            this.txtidade.Name = "txtidade";
            this.txtidade.Size = new System.Drawing.Size(133, 20);
            this.txtidade.TabIndex = 3;
            // 
            // lblmostranome
            // 
            this.lblmostranome.AutoSize = true;
            this.lblmostranome.Location = new System.Drawing.Point(111, 140);
            this.lblmostranome.Name = "lblmostranome";
            this.lblmostranome.Size = new System.Drawing.Size(0, 13);
            this.lblmostranome.TabIndex = 8;
            // 
            // lblmostraidade
            // 
            this.lblmostraidade.AutoSize = true;
            this.lblmostraidade.Location = new System.Drawing.Point(111, 178);
            this.lblmostraidade.Name = "lblmostraidade";
            this.lblmostraidade.Size = new System.Drawing.Size(0, 13);
            this.lblmostraidade.TabIndex = 9;
            this.lblmostraidade.Click += new System.EventHandler(this.lblmostraidade_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.lblmostraidade);
            this.Controls.Add(this.lblmostranome);
            this.Controls.Add(this.txtidade);
            this.Controls.Add(this.txtsobrenome);
            this.Controls.Add(this.txtnome);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblDatanasc);
            this.Controls.Add(this.lblSobrenome);
            this.Controls.Add(this.lblnome);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblnome;
        private System.Windows.Forms.Label lblSobrenome;
        private System.Windows.Forms.Label lblDatanasc;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.TextBox txtsobrenome;
        private System.Windows.Forms.TextBox txtidade;
        private System.Windows.Forms.Label lblmostranome;
        private System.Windows.Forms.Label lblmostraidade;
    }
}

