﻿namespace atividade_19_09_2017
{
    partial class Inverter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblfraseinvert = new System.Windows.Forms.Label();
            this.txtInvert = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblfraseinvert
            // 
            this.lblfraseinvert.AutoSize = true;
            this.lblfraseinvert.Location = new System.Drawing.Point(122, 137);
            this.lblfraseinvert.Name = "lblfraseinvert";
            this.lblfraseinvert.Size = new System.Drawing.Size(0, 13);
            this.lblfraseinvert.TabIndex = 0;
            // 
            // txtInvert
            // 
            this.txtInvert.Location = new System.Drawing.Point(41, 58);
            this.txtInvert.Name = "txtInvert";
            this.txtInvert.Size = new System.Drawing.Size(185, 20);
            this.txtInvert.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(269, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "Digite algo e clique em Inverter";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(83, 207);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 42);
            this.button1.TabIndex = 3;
            this.button1.Text = "Inverter";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Inverter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtInvert);
            this.Controls.Add(this.lblfraseinvert);
            this.Name = "Inverter";
            this.Text = "Inverter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblfraseinvert;
        private System.Windows.Forms.TextBox txtInvert;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
    }
}