﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace atividade_19_09_2017
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
       
        
    
        private void fraseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frase Frase = new Frase();
            Frase.Show();
        }

        private void nomeIdadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Nome_idade Nome = new Nome_idade();
            Nome.Show();
        }

        private void inverterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Inverter Inverter = new Inverter();
            Inverter.Show();
        }
    }
}
