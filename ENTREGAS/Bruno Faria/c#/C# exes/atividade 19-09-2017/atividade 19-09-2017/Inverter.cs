﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace atividade_19_09_2017
{
    public partial class Inverter : Form
    {
        
        public Inverter()
        {
            InitializeComponent();
        }
        String Frase;
        private void button1_Click(object sender, EventArgs e)
        {
            char[] arrChar = txtInvert.Text.ToCharArray();
            Array.Reverse(arrChar);
            string Frase = new String(arrChar);

            lblfraseinvert.Text = Frase;
        }
    }
}
