﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Banco
{
    public partial class Deposito : Form
    {
        public Deposito()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FMenu.deposito = Convert.ToDouble(txtDValor.Text);
                if (FMenu.deposito > 0)
                {
                    FMenu.saldo = (FMenu.saldo + FMenu.deposito);
                    MessageBox.Show("Deposito de R$" + FMenu.deposito + ", feito com sucesso");
                    txtDValor.Text = "";
                }
                else
                {
                    MessageBox.Show("Valor invalido !");
                }
            }
            
            catch (Exception)
            {
                MessageBox.Show("Formato do valor invalido !");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
