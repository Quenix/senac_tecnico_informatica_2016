﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Banco
{
    public partial class outrovalor : Form
    {
        public outrovalor()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FMenu.saque = Convert.ToDouble(txtSValor.Text);
                if (FMenu.saldo > 0)
                {
                    FMenu.saldo = (FMenu.saldo - FMenu.saque);
                    MessageBox.Show(FMenu.saque + " Reais foram retirados");

                }
                else
                {
                    MessageBox.Show("Saldo insuficiente !");

                }

            }
            catch (Exception)
            {
                MessageBox.Show("Formato do valor invalido !");
            }
                
            
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
    }
}
