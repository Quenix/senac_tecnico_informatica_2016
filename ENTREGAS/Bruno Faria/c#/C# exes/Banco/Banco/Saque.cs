﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Banco
{
    public partial class Saque : Form
    {
        public Saque()
        {
            InitializeComponent();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            var outrovalor = new outrovalor();
            outrovalor.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (FMenu.saldo >= 10)
            {
                FMenu.saque = 10;
                FMenu.saldo = (FMenu.saldo - FMenu.saque);
                MessageBox.Show(" Retirada efetuada com sucesso !");

            }
            
             else
            {
                MessageBox.Show("Saldo insuficiente !");

            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (FMenu.saldo >= 20)
            {
                FMenu.saque = 20;
                FMenu.saldo = (FMenu.saldo - FMenu.saque);
                MessageBox.Show(" Retirada efetuada com sucesso !");

            }

            else
            {
                MessageBox.Show("Saldo insuficiente !");

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (FMenu.saldo >= 50)
            {
                FMenu.saque = 50;
                FMenu.saldo = (FMenu.saldo - FMenu.saque);
                MessageBox.Show(" Retirada efetuada com sucesso !");

            }

            else
            {
                MessageBox.Show("Saldo insuficiente !");

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (FMenu.saldo >= 100)
            {
                FMenu.saque = 100;
                FMenu.saldo = (FMenu.saldo - FMenu.saque);
                MessageBox.Show(" Retirada efetuada com sucesso !");

            }

            else
            {
                MessageBox.Show("Saldo insuficiente !");

            }

        }
    }
}
