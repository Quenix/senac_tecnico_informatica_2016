﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculo_IMC
{
    public partial class Form1 : Form
    {
        double Peso, Altura, Resultado;
        string Nivel;

        public Form1()
        {
            InitializeComponent();
        }

        private void txtPeso_TextChanged(object sender, EventArgs e)
        {
           

        }

        private void txtAltura_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            Peso = Convert.ToDouble(txtPeso.Text);
            Altura = Convert.ToDouble(txtAltura.Text);

            Resultado = Peso /(Altura * Altura);
          lblImc.Text = Convert.ToString(Math.Round(Resultado, 2));
            //lblNivel.Text = Nivel;

                if ((Resultado >= 16) && (Resultado <= 16.9))
                    {
                    lblNivel.Text = "Muito Abaixo do Peso";
                MessageBox.Show (" Cuidado! \n Pode ocorrer: Queda de Cabelo, Infertilidade, Ausência Menstrual");
                    }

                if ((Resultado >= 17) && (Resultado <= 18.5)) 
                    {
                lblNivel.Text = "Abaixo do Peso";
                    MessageBox.Show(" Cuidado! \n Pode ocorrer: Fadiga, Stress, Ansiedade ");

                    }

                if ((Resultado >= 18.8) && (Resultado <= 24.9)) 
                    {
                lblNivel.Text = "Peso Normal";

                    }

                if ((Resultado >= 25) && (Resultado <= 29.9)) 
                    {
                lblNivel.Text = "Acima do Peso";
                        MessageBox.Show(" Cuidado! \n Pode ocorrer: Fadiga, Má circulação, Varizes");


                    }

                if ((Resultado >= 30) && (Resultado <= 34.9))
                    {
                lblNivel.Text = "Obesidade Grau I";
                        MessageBox.Show(" Cuidado! \n Pode ocorrer: Diabetes, Angina ,Infarto, Aterosclerose");

                    }

                if ((Resultado >= 35) && (Resultado <= 40)) 
                    {
                lblNivel.Text = "Obesidade Grau II";
                        MessageBox.Show(" Cuidado! \n Pode ocorrer: Apneia do Sono, Falta de Ar");
                
                    }

                if (Resultado > 40)

                    {
                lblNivel.Text = "Obesidade Grau III";
                    MessageBox.Show(" Cuidado! \n Pode ocorrer: Refluxo, Dificuldade para se mover \n Escaras, Diabete, Infarto, AVC");
            
                    }
                    
            }

        } 
    }

