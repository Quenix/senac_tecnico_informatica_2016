﻿namespace Quizz_sobre_Informatica
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RB15 = new System.Windows.Forms.RadioButton();
            this.RB14 = new System.Windows.Forms.RadioButton();
            this.RB13 = new System.Windows.Forms.RadioButton();
            this.RB12 = new System.Windows.Forms.RadioButton();
            this.RB11 = new System.Windows.Forms.RadioButton();
            this.lblPergunta1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.RB25 = new System.Windows.Forms.RadioButton();
            this.RB24 = new System.Windows.Forms.RadioButton();
            this.RB23 = new System.Windows.Forms.RadioButton();
            this.RB22 = new System.Windows.Forms.RadioButton();
            this.RB21 = new System.Windows.Forms.RadioButton();
            this.lblPergunta2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.RB35 = new System.Windows.Forms.RadioButton();
            this.RB34 = new System.Windows.Forms.RadioButton();
            this.RB33 = new System.Windows.Forms.RadioButton();
            this.RB32 = new System.Windows.Forms.RadioButton();
            this.RB31 = new System.Windows.Forms.RadioButton();
            this.lblPergunta3 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.RB45 = new System.Windows.Forms.RadioButton();
            this.RB44 = new System.Windows.Forms.RadioButton();
            this.RB43 = new System.Windows.Forms.RadioButton();
            this.RB42 = new System.Windows.Forms.RadioButton();
            this.RB41 = new System.Windows.Forms.RadioButton();
            this.lblPergunta4 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.RB55 = new System.Windows.Forms.RadioButton();
            this.lblPergunta5 = new System.Windows.Forms.Label();
            this.RB54 = new System.Windows.Forms.RadioButton();
            this.RB51 = new System.Windows.Forms.RadioButton();
            this.RB53 = new System.Windows.Forms.RadioButton();
            this.RB52 = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.RB65 = new System.Windows.Forms.RadioButton();
            this.RB64 = new System.Windows.Forms.RadioButton();
            this.RB63 = new System.Windows.Forms.RadioButton();
            this.RB62 = new System.Windows.Forms.RadioButton();
            this.RB61 = new System.Windows.Forms.RadioButton();
            this.lblPergunta6 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.RB75 = new System.Windows.Forms.RadioButton();
            this.lblPergunta7 = new System.Windows.Forms.Label();
            this.RB74 = new System.Windows.Forms.RadioButton();
            this.RB71 = new System.Windows.Forms.RadioButton();
            this.RB73 = new System.Windows.Forms.RadioButton();
            this.RB72 = new System.Windows.Forms.RadioButton();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.RB85 = new System.Windows.Forms.RadioButton();
            this.lblPergunta8 = new System.Windows.Forms.Label();
            this.RB84 = new System.Windows.Forms.RadioButton();
            this.RB81 = new System.Windows.Forms.RadioButton();
            this.RB83 = new System.Windows.Forms.RadioButton();
            this.RB82 = new System.Windows.Forms.RadioButton();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.RB95 = new System.Windows.Forms.RadioButton();
            this.lblPergunta9 = new System.Windows.Forms.Label();
            this.RB94 = new System.Windows.Forms.RadioButton();
            this.RB91 = new System.Windows.Forms.RadioButton();
            this.RB93 = new System.Windows.Forms.RadioButton();
            this.RB92 = new System.Windows.Forms.RadioButton();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.RB105 = new System.Windows.Forms.RadioButton();
            this.lblPergunta10 = new System.Windows.Forms.Label();
            this.RB104 = new System.Windows.Forms.RadioButton();
            this.RB101 = new System.Windows.Forms.RadioButton();
            this.RB103 = new System.Windows.Forms.RadioButton();
            this.RB102 = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RB15);
            this.groupBox1.Controls.Add(this.RB14);
            this.groupBox1.Controls.Add(this.RB13);
            this.groupBox1.Controls.Add(this.RB12);
            this.groupBox1.Controls.Add(this.RB11);
            this.groupBox1.Controls.Add(this.lblPergunta1);
            this.groupBox1.Location = new System.Drawing.Point(12, 30);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 195);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // RB15
            // 
            this.RB15.AutoSize = true;
            this.RB15.Location = new System.Drawing.Point(9, 162);
            this.RB15.Name = "RB15";
            this.RB15.Size = new System.Drawing.Size(85, 17);
            this.RB15.TabIndex = 5;
            this.RB15.TabStop = true;
            this.RB15.Text = "radioButton5";
            this.RB15.UseVisualStyleBackColor = true;
            // 
            // RB14
            // 
            this.RB14.AutoSize = true;
            this.RB14.Location = new System.Drawing.Point(9, 139);
            this.RB14.Name = "RB14";
            this.RB14.Size = new System.Drawing.Size(85, 17);
            this.RB14.TabIndex = 4;
            this.RB14.TabStop = true;
            this.RB14.Text = "radioButton4";
            this.RB14.UseVisualStyleBackColor = true;
            // 
            // RB13
            // 
            this.RB13.AutoSize = true;
            this.RB13.Location = new System.Drawing.Point(9, 116);
            this.RB13.Name = "RB13";
            this.RB13.Size = new System.Drawing.Size(85, 17);
            this.RB13.TabIndex = 3;
            this.RB13.TabStop = true;
            this.RB13.Text = "radioButton3";
            this.RB13.UseVisualStyleBackColor = true;
            // 
            // RB12
            // 
            this.RB12.AutoSize = true;
            this.RB12.Location = new System.Drawing.Point(9, 93);
            this.RB12.Name = "RB12";
            this.RB12.Size = new System.Drawing.Size(85, 17);
            this.RB12.TabIndex = 2;
            this.RB12.TabStop = true;
            this.RB12.Text = "radioButton2";
            this.RB12.UseVisualStyleBackColor = true;
            // 
            // RB11
            // 
            this.RB11.AutoSize = true;
            this.RB11.Location = new System.Drawing.Point(9, 70);
            this.RB11.Name = "RB11";
            this.RB11.Size = new System.Drawing.Size(85, 17);
            this.RB11.TabIndex = 1;
            this.RB11.TabStop = true;
            this.RB11.Text = "radioButton1";
            this.RB11.UseVisualStyleBackColor = true;
            // 
            // lblPergunta1
            // 
            this.lblPergunta1.AutoSize = true;
            this.lblPergunta1.Location = new System.Drawing.Point(6, 27);
            this.lblPergunta1.Name = "lblPergunta1";
            this.lblPergunta1.Size = new System.Drawing.Size(35, 13);
            this.lblPergunta1.TabIndex = 0;
            this.lblPergunta1.Text = "label1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.RB25);
            this.groupBox2.Controls.Add(this.RB24);
            this.groupBox2.Controls.Add(this.RB23);
            this.groupBox2.Controls.Add(this.RB22);
            this.groupBox2.Controls.Add(this.RB21);
            this.groupBox2.Controls.Add(this.lblPergunta2);
            this.groupBox2.Location = new System.Drawing.Point(218, 30);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 195);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // RB25
            // 
            this.RB25.AutoSize = true;
            this.RB25.Location = new System.Drawing.Point(9, 162);
            this.RB25.Name = "RB25";
            this.RB25.Size = new System.Drawing.Size(85, 17);
            this.RB25.TabIndex = 10;
            this.RB25.TabStop = true;
            this.RB25.Text = "radioButton6";
            this.RB25.UseVisualStyleBackColor = true;
            // 
            // RB24
            // 
            this.RB24.AutoSize = true;
            this.RB24.Location = new System.Drawing.Point(9, 139);
            this.RB24.Name = "RB24";
            this.RB24.Size = new System.Drawing.Size(85, 17);
            this.RB24.TabIndex = 9;
            this.RB24.TabStop = true;
            this.RB24.Text = "radioButton7";
            this.RB24.UseVisualStyleBackColor = true;
            // 
            // RB23
            // 
            this.RB23.AutoSize = true;
            this.RB23.Location = new System.Drawing.Point(9, 116);
            this.RB23.Name = "RB23";
            this.RB23.Size = new System.Drawing.Size(85, 17);
            this.RB23.TabIndex = 8;
            this.RB23.TabStop = true;
            this.RB23.Text = "radioButton8";
            this.RB23.UseVisualStyleBackColor = true;
            // 
            // RB22
            // 
            this.RB22.AutoSize = true;
            this.RB22.Location = new System.Drawing.Point(9, 93);
            this.RB22.Name = "RB22";
            this.RB22.Size = new System.Drawing.Size(85, 17);
            this.RB22.TabIndex = 7;
            this.RB22.TabStop = true;
            this.RB22.Text = "radioButton9";
            this.RB22.UseVisualStyleBackColor = true;
            // 
            // RB21
            // 
            this.RB21.AutoSize = true;
            this.RB21.Location = new System.Drawing.Point(9, 70);
            this.RB21.Name = "RB21";
            this.RB21.Size = new System.Drawing.Size(91, 17);
            this.RB21.TabIndex = 6;
            this.RB21.TabStop = true;
            this.RB21.Text = "radioButton10";
            this.RB21.UseVisualStyleBackColor = true;
            // 
            // lblPergunta2
            // 
            this.lblPergunta2.AutoSize = true;
            this.lblPergunta2.Location = new System.Drawing.Point(6, 27);
            this.lblPergunta2.Name = "lblPergunta2";
            this.lblPergunta2.Size = new System.Drawing.Size(35, 13);
            this.lblPergunta2.TabIndex = 1;
            this.lblPergunta2.Text = "label2";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.RB35);
            this.groupBox3.Controls.Add(this.RB34);
            this.groupBox3.Controls.Add(this.RB33);
            this.groupBox3.Controls.Add(this.RB32);
            this.groupBox3.Controls.Add(this.RB31);
            this.groupBox3.Controls.Add(this.lblPergunta3);
            this.groupBox3.Location = new System.Drawing.Point(424, 30);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 195);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // RB35
            // 
            this.RB35.AutoSize = true;
            this.RB35.Location = new System.Drawing.Point(9, 162);
            this.RB35.Name = "RB35";
            this.RB35.Size = new System.Drawing.Size(91, 17);
            this.RB35.TabIndex = 10;
            this.RB35.TabStop = true;
            this.RB35.Text = "radioButton11";
            this.RB35.UseVisualStyleBackColor = true;
            // 
            // RB34
            // 
            this.RB34.AutoSize = true;
            this.RB34.Location = new System.Drawing.Point(9, 139);
            this.RB34.Name = "RB34";
            this.RB34.Size = new System.Drawing.Size(91, 17);
            this.RB34.TabIndex = 9;
            this.RB34.TabStop = true;
            this.RB34.Text = "radioButton12";
            this.RB34.UseVisualStyleBackColor = true;
            // 
            // RB33
            // 
            this.RB33.AutoSize = true;
            this.RB33.Location = new System.Drawing.Point(9, 116);
            this.RB33.Name = "RB33";
            this.RB33.Size = new System.Drawing.Size(91, 17);
            this.RB33.TabIndex = 8;
            this.RB33.TabStop = true;
            this.RB33.Text = "radioButton13";
            this.RB33.UseVisualStyleBackColor = true;
            // 
            // RB32
            // 
            this.RB32.AutoSize = true;
            this.RB32.Location = new System.Drawing.Point(9, 93);
            this.RB32.Name = "RB32";
            this.RB32.Size = new System.Drawing.Size(91, 17);
            this.RB32.TabIndex = 7;
            this.RB32.TabStop = true;
            this.RB32.Text = "radioButton14";
            this.RB32.UseVisualStyleBackColor = true;
            // 
            // RB31
            // 
            this.RB31.AutoSize = true;
            this.RB31.Location = new System.Drawing.Point(9, 70);
            this.RB31.Name = "RB31";
            this.RB31.Size = new System.Drawing.Size(91, 17);
            this.RB31.TabIndex = 6;
            this.RB31.TabStop = true;
            this.RB31.Text = "radioButton15";
            this.RB31.UseVisualStyleBackColor = true;
            // 
            // lblPergunta3
            // 
            this.lblPergunta3.AutoSize = true;
            this.lblPergunta3.Location = new System.Drawing.Point(6, 27);
            this.lblPergunta3.Name = "lblPergunta3";
            this.lblPergunta3.Size = new System.Drawing.Size(35, 13);
            this.lblPergunta3.TabIndex = 1;
            this.lblPergunta3.Text = "label3";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.RB45);
            this.groupBox4.Controls.Add(this.RB44);
            this.groupBox4.Controls.Add(this.RB43);
            this.groupBox4.Controls.Add(this.RB42);
            this.groupBox4.Controls.Add(this.RB41);
            this.groupBox4.Controls.Add(this.lblPergunta4);
            this.groupBox4.Location = new System.Drawing.Point(630, 30);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(200, 195);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "groupBox4";
            // 
            // RB45
            // 
            this.RB45.AutoSize = true;
            this.RB45.Location = new System.Drawing.Point(9, 162);
            this.RB45.Name = "RB45";
            this.RB45.Size = new System.Drawing.Size(91, 17);
            this.RB45.TabIndex = 10;
            this.RB45.TabStop = true;
            this.RB45.Text = "radioButton16";
            this.RB45.UseVisualStyleBackColor = true;
            // 
            // RB44
            // 
            this.RB44.AutoSize = true;
            this.RB44.Location = new System.Drawing.Point(9, 139);
            this.RB44.Name = "RB44";
            this.RB44.Size = new System.Drawing.Size(91, 17);
            this.RB44.TabIndex = 9;
            this.RB44.TabStop = true;
            this.RB44.Text = "radioButton17";
            this.RB44.UseVisualStyleBackColor = true;
            // 
            // RB43
            // 
            this.RB43.AutoSize = true;
            this.RB43.Location = new System.Drawing.Point(9, 116);
            this.RB43.Name = "RB43";
            this.RB43.Size = new System.Drawing.Size(91, 17);
            this.RB43.TabIndex = 8;
            this.RB43.TabStop = true;
            this.RB43.Text = "radioButton18";
            this.RB43.UseVisualStyleBackColor = true;
            // 
            // RB42
            // 
            this.RB42.AutoSize = true;
            this.RB42.Location = new System.Drawing.Point(9, 93);
            this.RB42.Name = "RB42";
            this.RB42.Size = new System.Drawing.Size(91, 17);
            this.RB42.TabIndex = 7;
            this.RB42.TabStop = true;
            this.RB42.Text = "radioButton19";
            this.RB42.UseVisualStyleBackColor = true;
            // 
            // RB41
            // 
            this.RB41.AutoSize = true;
            this.RB41.Location = new System.Drawing.Point(9, 70);
            this.RB41.Name = "RB41";
            this.RB41.Size = new System.Drawing.Size(91, 17);
            this.RB41.TabIndex = 6;
            this.RB41.TabStop = true;
            this.RB41.Text = "radioButton20";
            this.RB41.UseVisualStyleBackColor = true;
            // 
            // lblPergunta4
            // 
            this.lblPergunta4.AutoSize = true;
            this.lblPergunta4.Location = new System.Drawing.Point(6, 27);
            this.lblPergunta4.Name = "lblPergunta4";
            this.lblPergunta4.Size = new System.Drawing.Size(35, 13);
            this.lblPergunta4.TabIndex = 2;
            this.lblPergunta4.Text = "label4";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.RB55);
            this.groupBox5.Controls.Add(this.lblPergunta5);
            this.groupBox5.Controls.Add(this.RB54);
            this.groupBox5.Controls.Add(this.RB51);
            this.groupBox5.Controls.Add(this.RB53);
            this.groupBox5.Controls.Add(this.RB52);
            this.groupBox5.Location = new System.Drawing.Point(836, 30);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(200, 195);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "groupBox5";
            // 
            // RB55
            // 
            this.RB55.AutoSize = true;
            this.RB55.Location = new System.Drawing.Point(9, 162);
            this.RB55.Name = "RB55";
            this.RB55.Size = new System.Drawing.Size(91, 17);
            this.RB55.TabIndex = 15;
            this.RB55.TabStop = true;
            this.RB55.Text = "radioButton21";
            this.RB55.UseVisualStyleBackColor = true;
            // 
            // lblPergunta5
            // 
            this.lblPergunta5.AutoSize = true;
            this.lblPergunta5.Location = new System.Drawing.Point(6, 27);
            this.lblPergunta5.Name = "lblPergunta5";
            this.lblPergunta5.Size = new System.Drawing.Size(35, 13);
            this.lblPergunta5.TabIndex = 3;
            this.lblPergunta5.Text = "label5";
            // 
            // RB54
            // 
            this.RB54.AutoSize = true;
            this.RB54.Location = new System.Drawing.Point(9, 139);
            this.RB54.Name = "RB54";
            this.RB54.Size = new System.Drawing.Size(91, 17);
            this.RB54.TabIndex = 14;
            this.RB54.TabStop = true;
            this.RB54.Text = "radioButton22";
            this.RB54.UseVisualStyleBackColor = true;
            // 
            // RB51
            // 
            this.RB51.AutoSize = true;
            this.RB51.Location = new System.Drawing.Point(9, 70);
            this.RB51.Name = "RB51";
            this.RB51.Size = new System.Drawing.Size(91, 17);
            this.RB51.TabIndex = 11;
            this.RB51.TabStop = true;
            this.RB51.Text = "radioButton25";
            this.RB51.UseVisualStyleBackColor = true;
            // 
            // RB53
            // 
            this.RB53.AutoSize = true;
            this.RB53.Location = new System.Drawing.Point(9, 116);
            this.RB53.Name = "RB53";
            this.RB53.Size = new System.Drawing.Size(91, 17);
            this.RB53.TabIndex = 13;
            this.RB53.TabStop = true;
            this.RB53.Text = "radioButton23";
            this.RB53.UseVisualStyleBackColor = true;
            // 
            // RB52
            // 
            this.RB52.AutoSize = true;
            this.RB52.Location = new System.Drawing.Point(9, 93);
            this.RB52.Name = "RB52";
            this.RB52.Size = new System.Drawing.Size(91, 17);
            this.RB52.TabIndex = 12;
            this.RB52.TabStop = true;
            this.RB52.Text = "radioButton24";
            this.RB52.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.RB65);
            this.groupBox6.Controls.Add(this.RB64);
            this.groupBox6.Controls.Add(this.RB63);
            this.groupBox6.Controls.Add(this.RB62);
            this.groupBox6.Controls.Add(this.RB61);
            this.groupBox6.Controls.Add(this.lblPergunta6);
            this.groupBox6.Location = new System.Drawing.Point(12, 329);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(200, 195);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "groupBox6";
            // 
            // RB65
            // 
            this.RB65.AutoSize = true;
            this.RB65.Location = new System.Drawing.Point(9, 158);
            this.RB65.Name = "RB65";
            this.RB65.Size = new System.Drawing.Size(91, 17);
            this.RB65.TabIndex = 10;
            this.RB65.TabStop = true;
            this.RB65.Text = "radioButton26";
            this.RB65.UseVisualStyleBackColor = true;
            // 
            // RB64
            // 
            this.RB64.AutoSize = true;
            this.RB64.Location = new System.Drawing.Point(9, 135);
            this.RB64.Name = "RB64";
            this.RB64.Size = new System.Drawing.Size(91, 17);
            this.RB64.TabIndex = 9;
            this.RB64.TabStop = true;
            this.RB64.Text = "radioButton27";
            this.RB64.UseVisualStyleBackColor = true;
            // 
            // RB63
            // 
            this.RB63.AutoSize = true;
            this.RB63.Location = new System.Drawing.Point(9, 112);
            this.RB63.Name = "RB63";
            this.RB63.Size = new System.Drawing.Size(91, 17);
            this.RB63.TabIndex = 8;
            this.RB63.TabStop = true;
            this.RB63.Text = "radioButton28";
            this.RB63.UseVisualStyleBackColor = true;
            // 
            // RB62
            // 
            this.RB62.AutoSize = true;
            this.RB62.Location = new System.Drawing.Point(9, 89);
            this.RB62.Name = "RB62";
            this.RB62.Size = new System.Drawing.Size(91, 17);
            this.RB62.TabIndex = 7;
            this.RB62.TabStop = true;
            this.RB62.Text = "radioButton29";
            this.RB62.UseVisualStyleBackColor = true;
            // 
            // RB61
            // 
            this.RB61.AutoSize = true;
            this.RB61.Location = new System.Drawing.Point(9, 66);
            this.RB61.Name = "RB61";
            this.RB61.Size = new System.Drawing.Size(91, 17);
            this.RB61.TabIndex = 6;
            this.RB61.TabStop = true;
            this.RB61.Text = "radioButton30";
            this.RB61.UseVisualStyleBackColor = true;
            // 
            // lblPergunta6
            // 
            this.lblPergunta6.AutoSize = true;
            this.lblPergunta6.Location = new System.Drawing.Point(6, 30);
            this.lblPergunta6.Name = "lblPergunta6";
            this.lblPergunta6.Size = new System.Drawing.Size(35, 13);
            this.lblPergunta6.TabIndex = 4;
            this.lblPergunta6.Text = "label6";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.RB75);
            this.groupBox7.Controls.Add(this.lblPergunta7);
            this.groupBox7.Controls.Add(this.RB74);
            this.groupBox7.Controls.Add(this.RB71);
            this.groupBox7.Controls.Add(this.RB73);
            this.groupBox7.Controls.Add(this.RB72);
            this.groupBox7.Location = new System.Drawing.Point(218, 329);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(200, 195);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "groupBox7";
            // 
            // RB75
            // 
            this.RB75.AutoSize = true;
            this.RB75.Location = new System.Drawing.Point(9, 158);
            this.RB75.Name = "RB75";
            this.RB75.Size = new System.Drawing.Size(91, 17);
            this.RB75.TabIndex = 15;
            this.RB75.TabStop = true;
            this.RB75.Text = "radioButton31";
            this.RB75.UseVisualStyleBackColor = true;
            // 
            // lblPergunta7
            // 
            this.lblPergunta7.AutoSize = true;
            this.lblPergunta7.Location = new System.Drawing.Point(6, 30);
            this.lblPergunta7.Name = "lblPergunta7";
            this.lblPergunta7.Size = new System.Drawing.Size(35, 13);
            this.lblPergunta7.TabIndex = 5;
            this.lblPergunta7.Text = "label7";
            // 
            // RB74
            // 
            this.RB74.AutoSize = true;
            this.RB74.Location = new System.Drawing.Point(9, 135);
            this.RB74.Name = "RB74";
            this.RB74.Size = new System.Drawing.Size(91, 17);
            this.RB74.TabIndex = 14;
            this.RB74.TabStop = true;
            this.RB74.Text = "radioButton32";
            this.RB74.UseVisualStyleBackColor = true;
            // 
            // RB71
            // 
            this.RB71.AutoSize = true;
            this.RB71.Location = new System.Drawing.Point(9, 66);
            this.RB71.Name = "RB71";
            this.RB71.Size = new System.Drawing.Size(91, 17);
            this.RB71.TabIndex = 11;
            this.RB71.TabStop = true;
            this.RB71.Text = "radioButton35";
            this.RB71.UseVisualStyleBackColor = true;
            // 
            // RB73
            // 
            this.RB73.AutoSize = true;
            this.RB73.Location = new System.Drawing.Point(9, 112);
            this.RB73.Name = "RB73";
            this.RB73.Size = new System.Drawing.Size(91, 17);
            this.RB73.TabIndex = 13;
            this.RB73.TabStop = true;
            this.RB73.Text = "radioButton33";
            this.RB73.UseVisualStyleBackColor = true;
            // 
            // RB72
            // 
            this.RB72.AutoSize = true;
            this.RB72.Location = new System.Drawing.Point(9, 89);
            this.RB72.Name = "RB72";
            this.RB72.Size = new System.Drawing.Size(91, 17);
            this.RB72.TabIndex = 12;
            this.RB72.TabStop = true;
            this.RB72.Text = "radioButton34";
            this.RB72.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.RB85);
            this.groupBox8.Controls.Add(this.lblPergunta8);
            this.groupBox8.Controls.Add(this.RB84);
            this.groupBox8.Controls.Add(this.RB81);
            this.groupBox8.Controls.Add(this.RB83);
            this.groupBox8.Controls.Add(this.RB82);
            this.groupBox8.Location = new System.Drawing.Point(424, 329);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(200, 195);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "groupBox8";
            // 
            // RB85
            // 
            this.RB85.AutoSize = true;
            this.RB85.Location = new System.Drawing.Point(9, 158);
            this.RB85.Name = "RB85";
            this.RB85.Size = new System.Drawing.Size(91, 17);
            this.RB85.TabIndex = 20;
            this.RB85.TabStop = true;
            this.RB85.Text = "radioButton36";
            this.RB85.UseVisualStyleBackColor = true;
            // 
            // lblPergunta8
            // 
            this.lblPergunta8.AutoSize = true;
            this.lblPergunta8.Location = new System.Drawing.Point(6, 30);
            this.lblPergunta8.Name = "lblPergunta8";
            this.lblPergunta8.Size = new System.Drawing.Size(35, 13);
            this.lblPergunta8.TabIndex = 6;
            this.lblPergunta8.Text = "label8";
            // 
            // RB84
            // 
            this.RB84.AutoSize = true;
            this.RB84.Location = new System.Drawing.Point(9, 135);
            this.RB84.Name = "RB84";
            this.RB84.Size = new System.Drawing.Size(91, 17);
            this.RB84.TabIndex = 19;
            this.RB84.TabStop = true;
            this.RB84.Text = "radioButton37";
            this.RB84.UseVisualStyleBackColor = true;
            // 
            // RB81
            // 
            this.RB81.AutoSize = true;
            this.RB81.Location = new System.Drawing.Point(9, 66);
            this.RB81.Name = "RB81";
            this.RB81.Size = new System.Drawing.Size(91, 17);
            this.RB81.TabIndex = 16;
            this.RB81.TabStop = true;
            this.RB81.Text = "radioButton40";
            this.RB81.UseVisualStyleBackColor = true;
            // 
            // RB83
            // 
            this.RB83.AutoSize = true;
            this.RB83.Location = new System.Drawing.Point(9, 112);
            this.RB83.Name = "RB83";
            this.RB83.Size = new System.Drawing.Size(91, 17);
            this.RB83.TabIndex = 18;
            this.RB83.TabStop = true;
            this.RB83.Text = "radioButton38";
            this.RB83.UseVisualStyleBackColor = true;
            // 
            // RB82
            // 
            this.RB82.AutoSize = true;
            this.RB82.Location = new System.Drawing.Point(9, 89);
            this.RB82.Name = "RB82";
            this.RB82.Size = new System.Drawing.Size(91, 17);
            this.RB82.TabIndex = 17;
            this.RB82.TabStop = true;
            this.RB82.Text = "radioButton39";
            this.RB82.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.RB95);
            this.groupBox9.Controls.Add(this.lblPergunta9);
            this.groupBox9.Controls.Add(this.RB94);
            this.groupBox9.Controls.Add(this.RB91);
            this.groupBox9.Controls.Add(this.RB93);
            this.groupBox9.Controls.Add(this.RB92);
            this.groupBox9.Location = new System.Drawing.Point(630, 329);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(200, 195);
            this.groupBox9.TabIndex = 1;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "groupBox9";
            // 
            // RB95
            // 
            this.RB95.AutoSize = true;
            this.RB95.Location = new System.Drawing.Point(9, 158);
            this.RB95.Name = "RB95";
            this.RB95.Size = new System.Drawing.Size(91, 17);
            this.RB95.TabIndex = 25;
            this.RB95.TabStop = true;
            this.RB95.Text = "radioButton41";
            this.RB95.UseVisualStyleBackColor = true;
            // 
            // lblPergunta9
            // 
            this.lblPergunta9.AutoSize = true;
            this.lblPergunta9.Location = new System.Drawing.Point(6, 30);
            this.lblPergunta9.Name = "lblPergunta9";
            this.lblPergunta9.Size = new System.Drawing.Size(35, 13);
            this.lblPergunta9.TabIndex = 7;
            this.lblPergunta9.Text = "label9";
            // 
            // RB94
            // 
            this.RB94.AutoSize = true;
            this.RB94.Location = new System.Drawing.Point(9, 135);
            this.RB94.Name = "RB94";
            this.RB94.Size = new System.Drawing.Size(91, 17);
            this.RB94.TabIndex = 24;
            this.RB94.TabStop = true;
            this.RB94.Text = "radioButton42";
            this.RB94.UseVisualStyleBackColor = true;
            // 
            // RB91
            // 
            this.RB91.AutoSize = true;
            this.RB91.Location = new System.Drawing.Point(9, 66);
            this.RB91.Name = "RB91";
            this.RB91.Size = new System.Drawing.Size(91, 17);
            this.RB91.TabIndex = 21;
            this.RB91.TabStop = true;
            this.RB91.Text = "radioButton45";
            this.RB91.UseVisualStyleBackColor = true;
            // 
            // RB93
            // 
            this.RB93.AutoSize = true;
            this.RB93.Location = new System.Drawing.Point(9, 112);
            this.RB93.Name = "RB93";
            this.RB93.Size = new System.Drawing.Size(91, 17);
            this.RB93.TabIndex = 23;
            this.RB93.TabStop = true;
            this.RB93.Text = "radioButton43";
            this.RB93.UseVisualStyleBackColor = true;
            // 
            // RB92
            // 
            this.RB92.AutoSize = true;
            this.RB92.Location = new System.Drawing.Point(9, 89);
            this.RB92.Name = "RB92";
            this.RB92.Size = new System.Drawing.Size(91, 17);
            this.RB92.TabIndex = 22;
            this.RB92.TabStop = true;
            this.RB92.Text = "radioButton44";
            this.RB92.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.RB105);
            this.groupBox10.Controls.Add(this.lblPergunta10);
            this.groupBox10.Controls.Add(this.RB104);
            this.groupBox10.Controls.Add(this.RB101);
            this.groupBox10.Controls.Add(this.RB103);
            this.groupBox10.Controls.Add(this.RB102);
            this.groupBox10.Location = new System.Drawing.Point(836, 329);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(200, 195);
            this.groupBox10.TabIndex = 1;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "groupBox10";
            // 
            // RB105
            // 
            this.RB105.AutoSize = true;
            this.RB105.Location = new System.Drawing.Point(9, 158);
            this.RB105.Name = "RB105";
            this.RB105.Size = new System.Drawing.Size(91, 17);
            this.RB105.TabIndex = 30;
            this.RB105.TabStop = true;
            this.RB105.Text = "radioButton46";
            this.RB105.UseVisualStyleBackColor = true;
            // 
            // lblPergunta10
            // 
            this.lblPergunta10.AutoSize = true;
            this.lblPergunta10.Location = new System.Drawing.Point(6, 30);
            this.lblPergunta10.Name = "lblPergunta10";
            this.lblPergunta10.Size = new System.Drawing.Size(41, 13);
            this.lblPergunta10.TabIndex = 8;
            this.lblPergunta10.Text = "label10";
            // 
            // RB104
            // 
            this.RB104.AutoSize = true;
            this.RB104.Location = new System.Drawing.Point(9, 135);
            this.RB104.Name = "RB104";
            this.RB104.Size = new System.Drawing.Size(91, 17);
            this.RB104.TabIndex = 29;
            this.RB104.TabStop = true;
            this.RB104.Text = "radioButton47";
            this.RB104.UseVisualStyleBackColor = true;
            // 
            // RB101
            // 
            this.RB101.AutoSize = true;
            this.RB101.Location = new System.Drawing.Point(9, 66);
            this.RB101.Name = "RB101";
            this.RB101.Size = new System.Drawing.Size(91, 17);
            this.RB101.TabIndex = 26;
            this.RB101.TabStop = true;
            this.RB101.Text = "radioButton50";
            this.RB101.UseVisualStyleBackColor = true;
            // 
            // RB103
            // 
            this.RB103.AutoSize = true;
            this.RB103.Location = new System.Drawing.Point(9, 112);
            this.RB103.Name = "RB103";
            this.RB103.Size = new System.Drawing.Size(91, 17);
            this.RB103.TabIndex = 28;
            this.RB103.TabStop = true;
            this.RB103.Text = "radioButton48";
            this.RB103.UseVisualStyleBackColor = true;
            // 
            // RB102
            // 
            this.RB102.AutoSize = true;
            this.RB102.Location = new System.Drawing.Point(9, 89);
            this.RB102.Name = "RB102";
            this.RB102.Size = new System.Drawing.Size(91, 17);
            this.RB102.TabIndex = 27;
            this.RB102.TabStop = true;
            this.RB102.Text = "radioButton49";
            this.RB102.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(411, 546);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(213, 89);
            this.button1.TabIndex = 4;
            this.button1.Text = "Resultado";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1066, 647);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Quiz de Informatica";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton RB15;
        private System.Windows.Forms.RadioButton RB14;
        private System.Windows.Forms.RadioButton RB13;
        private System.Windows.Forms.RadioButton RB12;
        private System.Windows.Forms.RadioButton RB11;
        private System.Windows.Forms.Label lblPergunta1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton RB25;
        private System.Windows.Forms.RadioButton RB24;
        private System.Windows.Forms.RadioButton RB23;
        private System.Windows.Forms.RadioButton RB22;
        private System.Windows.Forms.RadioButton RB21;
        private System.Windows.Forms.Label lblPergunta2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton RB35;
        private System.Windows.Forms.RadioButton RB34;
        private System.Windows.Forms.RadioButton RB33;
        private System.Windows.Forms.RadioButton RB32;
        private System.Windows.Forms.RadioButton RB31;
        private System.Windows.Forms.Label lblPergunta3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton RB45;
        private System.Windows.Forms.RadioButton RB44;
        private System.Windows.Forms.RadioButton RB43;
        private System.Windows.Forms.RadioButton RB42;
        private System.Windows.Forms.RadioButton RB41;
        private System.Windows.Forms.Label lblPergunta4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton RB55;
        private System.Windows.Forms.Label lblPergunta5;
        private System.Windows.Forms.RadioButton RB54;
        private System.Windows.Forms.RadioButton RB51;
        private System.Windows.Forms.RadioButton RB53;
        private System.Windows.Forms.RadioButton RB52;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton RB65;
        private System.Windows.Forms.RadioButton RB64;
        private System.Windows.Forms.RadioButton RB63;
        private System.Windows.Forms.RadioButton RB62;
        private System.Windows.Forms.RadioButton RB61;
        private System.Windows.Forms.Label lblPergunta6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton RB75;
        private System.Windows.Forms.Label lblPergunta7;
        private System.Windows.Forms.RadioButton RB74;
        private System.Windows.Forms.RadioButton RB71;
        private System.Windows.Forms.RadioButton RB73;
        private System.Windows.Forms.RadioButton RB72;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.RadioButton RB85;
        private System.Windows.Forms.Label lblPergunta8;
        private System.Windows.Forms.RadioButton RB84;
        private System.Windows.Forms.RadioButton RB81;
        private System.Windows.Forms.RadioButton RB83;
        private System.Windows.Forms.RadioButton RB82;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.RadioButton RB95;
        private System.Windows.Forms.Label lblPergunta9;
        private System.Windows.Forms.RadioButton RB94;
        private System.Windows.Forms.RadioButton RB91;
        private System.Windows.Forms.RadioButton RB93;
        private System.Windows.Forms.RadioButton RB92;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.RadioButton RB105;
        private System.Windows.Forms.Label lblPergunta10;
        private System.Windows.Forms.RadioButton RB104;
        private System.Windows.Forms.RadioButton RB101;
        private System.Windows.Forms.RadioButton RB103;
        private System.Windows.Forms.RadioButton RB102;
        private System.Windows.Forms.Button button1;
    }
}

