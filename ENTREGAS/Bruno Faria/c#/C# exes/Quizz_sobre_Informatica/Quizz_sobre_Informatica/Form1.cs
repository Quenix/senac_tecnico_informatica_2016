﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quizz_sobre_Informatica
{
    public partial class Form1 : Form
    {
        double nota;


        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            groupBox1.Text = ("Pergunta 1");
            lblPergunta1.Text = ("Qual é o principal componente de um computador?");
            RB11.Text = ("Monitor");
            RB12.Text = ("Memória");
            RB13.Text = ("Teclado");
            RB14.Text = ("Placa Mãe");//Correta
            RB15.Text = ("Processador");
            //É a placa onde todos os componentes estão interligados.

            groupBox2.Text = ("Pergunta 2");
            lblPergunta2.Text = ("Na falta de quais destes componentes, você não consegue utilizar um computador?");
            RB21.Text = ("Mouse");
            RB22.Text = ("Teclado");
            RB23.Text = ("Processador");//Correta
            RB24.Text = ("Leitor de CD ou DVD");
            RB25.Text = ("Caixas de Som");
            //O processador é um componente fundamental para o funcionamento do micro.

            groupBox3.Text = ("Pergunta 3");
            lblPergunta3.Text = ("As imagens em sites às vezes aparecem tão rápido, por quê?");
            RB31.Text = ("Porque o servidor que você esta acessando está na sua cidade");
            RB32.Text = ("Porque tem pouca gente usando o site");
            RB33.Text = ("Porque a sua internet é banda larga (conexão rápida)");
            RB34.Text = ("Porque o seu computador costuma guardar, num lugar chamado 'cache', as imagens que você busca costumeiramente");//Correta
            RB35.Text = ("Porque seu computador é bom");
            //No sistema operacional fica armazenado informações de sites freqüentados.

            groupBox4.Text = ("Pergunta 4");
            lblPergunta4.Text = ("A evolução dos computadores se baseia em quais pontos principais?");
            RB41.Text = ("Memórias e facilidade de uso");
            RB42.Text = ("Processador e facilidade de uso");
            RB43.Text = ("Interatividade e rapidez");
            RB44.Text = ("Processamento e armazenamento");//Correta
            RB45.Text = ("Processador e memória RAM");
            //A evolução de qualquer equipamento está baseada na velocidade de processamento e capacidade de armazenamento.

            groupBox5.Text = ("Pergunta 5");
            lblPergunta5.Text = ("São computadores usados em ambiente de rede para aumentar a produtividade dos usuários em uma empresa, normalmente disponibilizados a esses funcionários dentro de um departamento. Uma das características é o não uso de disco rígido. Refere-se a qual tecnologia?");
            RB51.Text = ("Notebook");
            RB52.Text = ("Thinclient");//Correta
            RB53.Text = ("PALM");
            RB54.Text = ("Servidor");
            RB55.Text = ("Mainframe");
            //Computadores deste tipo são aqueles que não possuem disco rígido, quando conectados a computadores do tipo servidor, prestam-se a trabalhar de forma corporativa.

            groupBox6.Text = ("Pergunta 6");
            lblPergunta6.Text = ("Qual o conceito que envolve pessoas e procedimentos que auxiliam usuários quando estes possuem alguma dificuldade no ambiente de trabalho quando usam a informática?");
            RB61.Text = ("Treinamento");
            RB62.Text = ("Serviços Tercerizado");
            RB63.Text = ("Help Desk");//Correta
            RB64.Text = ("Assistência Técnica");
            RB65.Text = ("Ajuda Online");
            //Help Desk, ou mais modernamente Sevice Desk/Central de Serviço, é o local onde as pessoas recorrem quando se deparam com problemas no ambiente de informática.

            groupBox7.Text = ("Pergunta 7");
            lblPergunta7.Text = ("São exemplos de hardware, somente:");
            RB71.Text = ("Placa de video, Scanner e OCR");
            RB72.Text = ("Mouse, Placa de video e Windows");
            RB73.Text = ("Memória, Leitor de código de barras e Placa de rede");//Correta
            RB74.Text = ("Word, impressora e monitor");
            RB75.Text = ("Windows, Linux e DOS");
            //Hardware é a parte física, ou seja, qualquer parte que integra as partes e componentes de um computador.

            groupBox8.Text = ("Pergunta 8");
            lblPergunta8.Text = ("É um tipo de computador que tem como função controlar as demais estações de trabalho quando ligados em uma rede de computadores:");
            RB81.Text = ("Desktop");
            RB82.Text = ("Notebook");
            RB83.Text = ("PDA");
            RB84.Text = ("UMPC");
            RB85.Text = ("Servidor");//Correta
            //Normalmente, uma rede de computadores possui um computador especial, com melhores recursos de processamento e armazenamento de dados, além de ser um equipamento com maior robustez, que controla serviços e outros computadores em uma rede.

            groupBox9.Text = ("Pergunta 9");
            lblPergunta9.Text = ("Na terminologia usual de microinformática, os dados Pentium IV, 3.0 Ghz, 256 Mb RAM, HD 40 Gb, teclado padrão ABNT, placa de vídeo 128 mb AGP, constituem um modelo de:");
            RB91.Text = ("Organização");
            RB92.Text = ("Layout");
            RB93.Text = ("Configuração");//Correta
            RB94.Text = ("Estruturação");
            RB95.Text = ("Programação");
            //Os itens descritos acima são a configuração, ou seja, a descrição do micro.

            groupBox10.Text = ("Pergunta 10");
            lblPergunta10.Text = ("As partes físicas do microcomputador só funcionam de maneira lógica quando executam ordens contidas em um programa ou em um conjunto de programas. A parte composta pelos programas – que transforma as partes físicas do microcomputador em uma unidade lógica de processamento – é chamada de:");
            RB101.Text = ("Hardware");
            RB102.Text = ("Selfware");
            RB103.Text = ("Netware");
            RB104.Text = ("Software");//Correta
            RB105.Text = ("Firmware");
            //É o programa que você executa, tais como: Firefox, Word, Excell, etc.









        }
    }
}
