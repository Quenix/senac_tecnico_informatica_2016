﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace calc
{
    public partial class Form1 : Form
    {
        double valor1, valor2, total;
        string operacao;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnZero_Click(object sender, EventArgs e)
        {
            txtTela.Text += "0";
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            txtTela.Text += "1";
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            txtTela.Text += "2";
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            txtTela.Text += "3";
        }

        private void btnPonto_Click(object sender, EventArgs e)
        {
            txtTela.Text += ",";
        }

        private void btnIgual_Click(object sender, EventArgs e)
        {
            valor2 = Convert.ToDouble(txtTela.Text);
            txtTela2.Text = "";
         switch (operacao)
            {
                case "Somar":
                    total = (valor1 + valor2);
                    txtTela.Text = Convert.ToString(total);
                        break;
                case "Subtrair":
                    total = (valor1 - valor2);
                    txtTela.Text = Convert.ToString(total);
                    break;
                case "Multiplicar":
                    total = (valor1 * valor2);
                    txtTela.Text = Convert.ToString(total);
                    break;
                case "Dividir":
                    if(valor1 != 0 && valor2 != 0) //Valor1 difere de 0 e valor2 difere de 0
                    {
                        total = (valor1 / valor2);
                        txtTela.Text = Convert.ToString(total);
                    }
                    else
                    {
                        MessageBox.Show("Operação invalida!");
                    }
                    
                    break;
                
                case "Porcento":
                    total = (valor1/100)*valor2;
                    txtTela.Text = Convert.ToString(total);
                    break;
            }
        

        }

        private void btnRaiz_Click(object sender, EventArgs e)
        {
            valor1 = Convert.ToDouble(txtTela.Text);
            txtTela.Text =  Convert.ToString(Math.Sqrt(valor1));
            txtTela2.Text = "√(" + valor1 + ")";
            operacao = "Raiz";
        }

        private void btnPorcento_Click(object sender, EventArgs e)
        {
            valor1 = Convert.ToDouble(txtTela.Text);
            txtTela2.Text = valor1 + " % ";
            txtTela.Text = "";
            operacao = "Porcento";
        }

        private void btnDividir_Click(object sender, EventArgs e)
        {
            valor1 = Convert.ToDouble(txtTela.Text);
            txtTela2.Text = valor1 + " / ";
            txtTela.Text = "";
            operacao = "Dividir";
        }

        private void btnSomar_Click(object sender, EventArgs e)
        {
            valor1 = Convert.ToDouble(txtTela.Text);
            txtTela2.Text = valor1 + " + " ;
            txtTela.Text = "";
            operacao = "Somar";

        }

        private void btnSubtrair_Click(object sender, EventArgs e)
        {
            valor1 = Convert.ToDouble(txtTela.Text);
            txtTela2.Text = valor1 + " - ";
            txtTela.Text = "";
            operacao = "Subtrair";
        }

        private void btnMultiplicar_Click(object sender, EventArgs e)
        {
            valor1 = Convert.ToDouble(txtTela.Text);
            txtTela2.Text = valor1 + " * ";
            txtTela.Text = "";
            operacao = "Multiplicar";
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            txtTela.Text += "7";
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            txtTela.Text += "8";
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            txtTela.Text += "9";
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            txtTela.Text += "4";
        }

        private void btnCE_Click(object sender, EventArgs e)
        {
            txtTela.Text = "";
            txtTela2.Text = "";
            
        }

       
        private void btn5_Click(object sender, EventArgs e)
        {
            txtTela.Text += "5";
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            txtTela.Text += "6";
        }
    }
}
