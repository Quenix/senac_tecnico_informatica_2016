﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calcular.IMC
{
    
    public partial class Form1 : Form
    {
        Double peso, altura, resultado;
        

        public Form1()
        {
            InitializeComponent();
        }

        private void blnCalcular_Click(object sender, EventArgs e)
        {
            peso = Convert.ToDouble(txtPeso.Text);
            altura = Convert.ToDouble(txtAltura.Text);

            resultado = (peso / (altura * altura));

            lblResultado.Text = ("O Imc é: " + Math.Round(resultado, 2));

            if (resultado <= 18.5)
                 lblCondicao.Text= ("Abaixo do Peso");

            if (resultado >= 18.6 && resultado <= 24.9)
                lblCondicao.Text = ("Peso Ideal Parabéns");

            if (resultado >= 25.0 && resultado <= 29.9)
            lblCondicao.Text = ("Levemente Acima do Peso");

            if (resultado >= 30.0 && resultado <= 34.9)
                lblCondicao.Text=("Obesidade Grau I");

            if (resultado >= 35.0 && resultado <= 39.9)
                lblCondicao.Text = ("Obesidade Grau II (SERVERA)");

            if (resultado >= 40)
                lblCondicao.Text = ("Obesidade Grau III (MÓRBIDA)");

            


        }
    }
}
