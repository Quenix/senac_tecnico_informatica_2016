﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculoIMC
{
    public partial class Form1 : Form
    {
        double peso, altura, imc;

        public Form1()
        {
            InitializeComponent();
        }

        private void CalcularIMC_Click(object sender, EventArgs e)
        {
            peso = Convert.ToDouble(textBoxPeso.Text);
            altura = Convert.ToDouble(textBoxAltura.Text);
            imc = peso / (altura * altura);
            lblResultado.Text = ("Seu IMC é: " + Math.Round(imc, 2));

            if(imc <= 18.5)
            {
                lblimc.Text = ("Abaixo do peso");
            }

            if(imc >= 18.6 && imc <= 24.9)
            {
                lblimc.Text = ("Peso ideal");
            }

            if(imc >= 25.0 && imc <= 29.9)
            {
                lblimc.Text = ("Levemente acima do peso");
            }

            if(imc >= 30.0 && imc <= 34.9)
            {
                lblimc.Text = ("Obesidade grau I");
            }

            if(imc >= 35.0 && imc <= 39.9)
            {
                lblimc.Text = ("Obesidade grau II (severa)");
            }

            if(imc >= 40.0)
            {
                lblimc.Text = ("Obesidade gra III (morbida)");
            }
        }
    }
}
