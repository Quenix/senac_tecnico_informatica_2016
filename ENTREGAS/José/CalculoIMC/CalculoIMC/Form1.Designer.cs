﻿namespace CalculoIMC
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.Peso = new System.Windows.Forms.Label();
            this.Altura = new System.Windows.Forms.Label();
            this.textBoxPeso = new System.Windows.Forms.TextBox();
            this.textBoxAltura = new System.Windows.Forms.TextBox();
            this.CalcularIMC = new System.Windows.Forms.Button();
            this.lblResultado = new System.Windows.Forms.Label();
            this.lblimc = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Peso
            // 
            this.Peso.AutoSize = true;
            this.Peso.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Peso.Location = new System.Drawing.Point(24, 24);
            this.Peso.Name = "Peso";
            this.Peso.Size = new System.Drawing.Size(76, 31);
            this.Peso.TabIndex = 0;
            this.Peso.Text = "Peso";
            // 
            // Altura
            // 
            this.Altura.AutoSize = true;
            this.Altura.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Altura.Location = new System.Drawing.Point(24, 85);
            this.Altura.Name = "Altura";
            this.Altura.Size = new System.Drawing.Size(85, 31);
            this.Altura.TabIndex = 1;
            this.Altura.Text = "Altura";
            // 
            // textBoxPeso
            // 
            this.textBoxPeso.Location = new System.Drawing.Point(136, 35);
            this.textBoxPeso.Name = "textBoxPeso";
            this.textBoxPeso.Size = new System.Drawing.Size(100, 20);
            this.textBoxPeso.TabIndex = 2;
            // 
            // textBoxAltura
            // 
            this.textBoxAltura.Location = new System.Drawing.Point(136, 96);
            this.textBoxAltura.Name = "textBoxAltura";
            this.textBoxAltura.Size = new System.Drawing.Size(100, 20);
            this.textBoxAltura.TabIndex = 3;
            // 
            // CalcularIMC
            // 
            this.CalcularIMC.Location = new System.Drawing.Point(67, 178);
            this.CalcularIMC.Name = "CalcularIMC";
            this.CalcularIMC.Size = new System.Drawing.Size(138, 41);
            this.CalcularIMC.TabIndex = 4;
            this.CalcularIMC.Text = "Calcular IMC";
            this.CalcularIMC.UseVisualStyleBackColor = true;
            this.CalcularIMC.Click += new System.EventHandler(this.CalcularIMC_Click);
            // 
            // lblResultado
            // 
            this.lblResultado.AutoSize = true;
            this.lblResultado.Location = new System.Drawing.Point(27, 141);
            this.lblResultado.Name = "lblResultado";
            this.lblResultado.Size = new System.Drawing.Size(26, 13);
            this.lblResultado.TabIndex = 5;
            this.lblResultado.Text = "IMC";
            // 
            // lblimc
            // 
            this.lblimc.AutoSize = true;
            this.lblimc.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblimc.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.lblimc.Location = new System.Drawing.Point(136, 140);
            this.lblimc.Name = "lblimc";
            this.lblimc.Size = new System.Drawing.Size(0, 13);
            this.lblimc.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(268, 252);
            this.Controls.Add(this.lblimc);
            this.Controls.Add(this.lblResultado);
            this.Controls.Add(this.CalcularIMC);
            this.Controls.Add(this.textBoxAltura);
            this.Controls.Add(this.textBoxPeso);
            this.Controls.Add(this.Altura);
            this.Controls.Add(this.Peso);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Peso;
        private System.Windows.Forms.Label Altura;
        private System.Windows.Forms.TextBox textBoxPeso;
        private System.Windows.Forms.TextBox textBoxAltura;
        private System.Windows.Forms.Button CalcularIMC;
        private System.Windows.Forms.Label lblResultado;
        private System.Windows.Forms.Label lblimc;
    }
}

