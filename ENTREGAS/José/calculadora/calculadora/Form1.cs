﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace calculadora
{    
    public partial class Form1 : Form
    {
        Double val1, val2, resultado;
        String operacao;

        public Form1()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            val1 = Convert.ToDouble(txtVal1.Text);
            val2 = Convert.ToDouble(txtVal2.Text);

            if (operacao == "soma")
            {
                resultado = val1 + val2;
                MessageBox.Show("Resultado: " + resultado);
            }
            if (operacao == "subtracao")
            {
                resultado = val1 - val2;
                MessageBox.Show("resultado: " + resultado);
            }
            if (operacao == "multiplicacao")
            {
                resultado = val1 * val2;
                MessageBox.Show("resultado: " + resultado);
            }
            if (operacao == "divisao")
            {
                if (val1 == 0 || val2 == 0)
                {
                    MessageBox.Show("Não é possivel dividir por zero");
                }
                else
                {
                    resultado = val1 / val2;
                    MessageBox.Show("resultado: " + resultado);
                }
               
            }
    
        }

        private void btnSubtracao_Click(object sender, EventArgs e)
        {
            operacao = "subtracao";
        }

        private void btnMultipilcacao_Click(object sender, EventArgs e)
        {
            operacao = "multiplicacao";
        }

        private void btnDivisao_Click(object sender, EventArgs e)
        {
            operacao = "divisao";
        }

        private void Val2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtVal1_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void txtVal1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente numero e virgula");
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("este campo aceita somente uma virgula");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            operacao = "soma";
        }
    }
}
