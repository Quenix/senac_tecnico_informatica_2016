﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Toub.Sound.Midi;

namespace Pentatonica
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            MidiPlayer.OpenMidi();

        }


        private void button1_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "C4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "D#4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "F4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "G4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "A#4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "C4", 127));
            System.Threading.Thread.Sleep(500);
        }
    }
}
