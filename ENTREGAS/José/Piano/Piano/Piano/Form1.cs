﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Toub.Sound.Midi;

namespace Piano
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            MidiPlayer.OpenMidi();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnDo_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "C4", 127));
        }

        private void btnRe_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "D4", 127));
        }

        private void btnMi_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "E4", 127));
        }

        private void btnFa_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "F4", 127));
        }

        private void btnSol_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "G4", 127));
        }

        private void btnLa_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "A4", 127));
        }

        private void btnSi_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "B#4", 127));
        }

        private void btcC_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "B4", 127));
        }

        private void btnBb_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "B4", 127));
        }
    }
}
