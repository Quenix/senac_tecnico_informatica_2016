﻿namespace Piano
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDo = new System.Windows.Forms.Button();
            this.btnSi = new System.Windows.Forms.Button();
            this.btnLa = new System.Windows.Forms.Button();
            this.btnSol = new System.Windows.Forms.Button();
            this.btnFa = new System.Windows.Forms.Button();
            this.btnMi = new System.Windows.Forms.Button();
            this.btnRe = new System.Windows.Forms.Button();
            this.btcC = new System.Windows.Forms.Button();
            this.btnBb = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnDo
            // 
            this.btnDo.Location = new System.Drawing.Point(2, 12);
            this.btnDo.Name = "btnDo";
            this.btnDo.Size = new System.Drawing.Size(35, 123);
            this.btnDo.TabIndex = 0;
            this.btnDo.Text = "C";
            this.btnDo.UseVisualStyleBackColor = true;
            this.btnDo.Click += new System.EventHandler(this.btnDo_Click);
            // 
            // btnSi
            // 
            this.btnSi.Location = new System.Drawing.Point(289, 12);
            this.btnSi.Name = "btnSi";
            this.btnSi.Size = new System.Drawing.Size(35, 123);
            this.btnSi.TabIndex = 3;
            this.btnSi.Text = "B#";
            this.btnSi.UseVisualStyleBackColor = true;
            this.btnSi.Click += new System.EventHandler(this.btnSi_Click);
            // 
            // btnLa
            // 
            this.btnLa.Location = new System.Drawing.Point(207, 12);
            this.btnLa.Name = "btnLa";
            this.btnLa.Size = new System.Drawing.Size(35, 123);
            this.btnLa.TabIndex = 4;
            this.btnLa.Text = "A";
            this.btnLa.UseVisualStyleBackColor = true;
            this.btnLa.Click += new System.EventHandler(this.btnLa_Click);
            // 
            // btnSol
            // 
            this.btnSol.Location = new System.Drawing.Point(166, 12);
            this.btnSol.Name = "btnSol";
            this.btnSol.Size = new System.Drawing.Size(35, 123);
            this.btnSol.TabIndex = 5;
            this.btnSol.Text = "G";
            this.btnSol.UseVisualStyleBackColor = true;
            this.btnSol.Click += new System.EventHandler(this.btnSol_Click);
            // 
            // btnFa
            // 
            this.btnFa.Location = new System.Drawing.Point(125, 12);
            this.btnFa.Name = "btnFa";
            this.btnFa.Size = new System.Drawing.Size(35, 123);
            this.btnFa.TabIndex = 6;
            this.btnFa.Text = "F";
            this.btnFa.UseVisualStyleBackColor = true;
            this.btnFa.Click += new System.EventHandler(this.btnFa_Click);
            // 
            // btnMi
            // 
            this.btnMi.Location = new System.Drawing.Point(84, 12);
            this.btnMi.Name = "btnMi";
            this.btnMi.Size = new System.Drawing.Size(35, 123);
            this.btnMi.TabIndex = 7;
            this.btnMi.Text = "E";
            this.btnMi.UseVisualStyleBackColor = true;
            this.btnMi.Click += new System.EventHandler(this.btnMi_Click);
            // 
            // btnRe
            // 
            this.btnRe.Location = new System.Drawing.Point(43, 12);
            this.btnRe.Name = "btnRe";
            this.btnRe.Size = new System.Drawing.Size(35, 123);
            this.btnRe.TabIndex = 8;
            this.btnRe.Text = "D";
            this.btnRe.UseVisualStyleBackColor = true;
            this.btnRe.Click += new System.EventHandler(this.btnRe_Click);
            // 
            // btcC
            // 
            this.btcC.Location = new System.Drawing.Point(248, 12);
            this.btcC.Name = "btcC";
            this.btcC.Size = new System.Drawing.Size(35, 123);
            this.btcC.TabIndex = 9;
            this.btcC.Text = "B";
            this.btcC.UseVisualStyleBackColor = true;
            this.btcC.Click += new System.EventHandler(this.btcC_Click);
            // 
            // btnBb
            // 
            this.btnBb.Location = new System.Drawing.Point(330, 12);
            this.btnBb.Name = "btnBb";
            this.btnBb.Size = new System.Drawing.Size(35, 123);
            this.btnBb.TabIndex = 10;
            this.btnBb.Text = "Bb";
            this.btnBb.UseVisualStyleBackColor = true;
            this.btnBb.Click += new System.EventHandler(this.btnBb_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(368, 137);
            this.Controls.Add(this.btnBb);
            this.Controls.Add(this.btcC);
            this.Controls.Add(this.btnRe);
            this.Controls.Add(this.btnMi);
            this.Controls.Add(this.btnFa);
            this.Controls.Add(this.btnSol);
            this.Controls.Add(this.btnLa);
            this.Controls.Add(this.btnSi);
            this.Controls.Add(this.btnDo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDo;
        private System.Windows.Forms.Button btnSi;
        private System.Windows.Forms.Button btnLa;
        private System.Windows.Forms.Button btnSol;
        private System.Windows.Forms.Button btnFa;
        private System.Windows.Forms.Button btnMi;
        private System.Windows.Forms.Button btnRe;
        private System.Windows.Forms.Button btcC;
        private System.Windows.Forms.Button btnBb;
    }
}

