﻿namespace Calculadora_Entrega
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.bmais = new System.Windows.Forms.Button();
            this.bmenos = new System.Windows.Forms.Button();
            this.bmultiplicar = new System.Windows.Forms.Button();
            this.bdividir = new System.Windows.Forms.Button();
            this.bigual = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // bmais
            // 
            this.bmais.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bmais.Location = new System.Drawing.Point(14, 166);
            this.bmais.Name = "bmais";
            this.bmais.Size = new System.Drawing.Size(107, 153);
            this.bmais.TabIndex = 0;
            this.bmais.Text = "+\r\n";
            this.bmais.UseVisualStyleBackColor = true;
            // 
            // bmenos
            // 
            this.bmenos.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bmenos.Location = new System.Drawing.Point(142, 166);
            this.bmenos.Name = "bmenos";
            this.bmenos.Size = new System.Drawing.Size(106, 153);
            this.bmenos.TabIndex = 1;
            this.bmenos.Text = "-";
            this.bmenos.UseVisualStyleBackColor = true;
            // 
            // bmultiplicar
            // 
            this.bmultiplicar.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bmultiplicar.Location = new System.Drawing.Point(269, 166);
            this.bmultiplicar.Name = "bmultiplicar";
            this.bmultiplicar.Size = new System.Drawing.Size(107, 153);
            this.bmultiplicar.TabIndex = 2;
            this.bmultiplicar.Text = "*";
            this.bmultiplicar.UseVisualStyleBackColor = true;
            // 
            // bdividir
            // 
            this.bdividir.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bdividir.Location = new System.Drawing.Point(399, 166);
            this.bdividir.Name = "bdividir";
            this.bdividir.Size = new System.Drawing.Size(111, 153);
            this.bdividir.TabIndex = 3;
            this.bdividir.Text = "/";
            this.bdividir.UseVisualStyleBackColor = true;
            // 
            // bigual
            // 
            this.bigual.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bigual.Location = new System.Drawing.Point(142, 325);
            this.bigual.Name = "bigual";
            this.bigual.Size = new System.Drawing.Size(234, 85);
            this.bigual.TabIndex = 4;
            this.bigual.Text = "=";
            this.bigual.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(14, 120);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(234, 40);
            this.textBox1.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Showcard Gothic", 36F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(80, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(357, 60);
            this.label1.TabIndex = 6;
            this.label1.Text = "Calculadora";
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(269, 120);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(240, 40);
            this.textBox2.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 422);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.bigual);
            this.Controls.Add(this.bdividir);
            this.Controls.Add(this.bmultiplicar);
            this.Controls.Add(this.bmenos);
            this.Controls.Add(this.bmais);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Calculadora";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bmais;
        private System.Windows.Forms.Button bmenos;
        private System.Windows.Forms.Button bmultiplicar;
        private System.Windows.Forms.Button bdividir;
        private System.Windows.Forms.Button bigual;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox2;
    }
}

