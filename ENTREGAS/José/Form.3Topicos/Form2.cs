﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForm
{
    public partial class Form2 : Form
    {
        int idade;

        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            idade = Convert.ToInt32(txtIdade.Text);
            lblNomeIdade.Text = ("Seu nome é: "+ txtNome.Text + "\n E sua idade é: " + idade);
            if (idade >= 18)
            {
                MessageBox.Show("Você é maior de idade");
            }
            else
            {

                MessageBox.Show("Você é menor de idade");
            }
        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {

        }
    }
}