﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void cadastroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var janela = new Form2();
            janela.ShowDialog();
        }

        private void fraseColoridaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var janelaColor = new Form3();
            janelaColor.ShowDialog();
        }

        private void inveterFraseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var janelaFrase = new Form4();
            janelaFrase.ShowDialog();
        }
    }
}
