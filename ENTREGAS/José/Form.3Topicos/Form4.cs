﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForm
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void btnInverter_Click(object sender, EventArgs e)
        {
            char[] invertfon = txtFrase.Text.ToCharArray();
            Array.Reverse(invertfon);
            string revert = new string(invertfon);
            MessageBox.Show(revert);
        }
    }
}
