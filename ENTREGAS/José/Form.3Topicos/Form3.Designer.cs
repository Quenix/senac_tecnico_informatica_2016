﻿namespace WindowsForm
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFrase = new System.Windows.Forms.Label();
            this.btnRoxo = new System.Windows.Forms.Button();
            this.btnVerde = new System.Windows.Forms.Button();
            this.btnCinza = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblFrase
            // 
            this.lblFrase.AutoSize = true;
            this.lblFrase.Font = new System.Drawing.Font("Ravie", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrase.Location = new System.Drawing.Point(12, 71);
            this.lblFrase.Name = "lblFrase";
            this.lblFrase.Size = new System.Drawing.Size(431, 43);
            this.lblFrase.TabIndex = 0;
            this.lblFrase.Text = "ISSO MUDA DE COR";
            // 
            // btnRoxo
            // 
            this.btnRoxo.Location = new System.Drawing.Point(12, 134);
            this.btnRoxo.Name = "btnRoxo";
            this.btnRoxo.Size = new System.Drawing.Size(145, 52);
            this.btnRoxo.TabIndex = 1;
            this.btnRoxo.Text = "Roxo";
            this.btnRoxo.UseVisualStyleBackColor = true;
            this.btnRoxo.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnVerde
            // 
            this.btnVerde.Location = new System.Drawing.Point(163, 134);
            this.btnVerde.Name = "btnVerde";
            this.btnVerde.Size = new System.Drawing.Size(154, 52);
            this.btnVerde.TabIndex = 2;
            this.btnVerde.Text = "Verde";
            this.btnVerde.UseVisualStyleBackColor = true;
            this.btnVerde.Click += new System.EventHandler(this.btnVerde_Click);
            // 
            // btnCinza
            // 
            this.btnCinza.Location = new System.Drawing.Point(323, 134);
            this.btnCinza.Name = "btnCinza";
            this.btnCinza.Size = new System.Drawing.Size(142, 52);
            this.btnCinza.TabIndex = 3;
            this.btnCinza.Text = "Cinza";
            this.btnCinza.UseVisualStyleBackColor = true;
            this.btnCinza.Click += new System.EventHandler(this.btnCinza_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(477, 198);
            this.Controls.Add(this.btnCinza);
            this.Controls.Add(this.btnVerde);
            this.Controls.Add(this.btnRoxo);
            this.Controls.Add(this.lblFrase);
            this.Name = "Form3";
            this.Text = "Form3";
            this.Load += new System.EventHandler(this.Form3_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFrase;
        private System.Windows.Forms.Button btnRoxo;
        private System.Windows.Forms.Button btnVerde;
        private System.Windows.Forms.Button btnCinza;
    }
}