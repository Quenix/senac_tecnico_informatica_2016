﻿namespace calculadora
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.mostraresultado = new System.Windows.Forms.TextBox();
            this.um = new System.Windows.Forms.Button();
            this.multiplicacao = new System.Windows.Forms.Button();
            this.subtracao = new System.Windows.Forms.Button();
            this.divisao = new System.Windows.Forms.Button();
            this.resultado = new System.Windows.Forms.Button();
            this.quatro = new System.Windows.Forms.Button();
            this.seis = new System.Windows.Forms.Button();
            this.cinco = new System.Windows.Forms.Button();
            this.zero = new System.Windows.Forms.Button();
            this.nove = new System.Windows.Forms.Button();
            this.oito = new System.Windows.Forms.Button();
            this.sete = new System.Windows.Forms.Button();
            this.tres = new System.Windows.Forms.Button();
            this.dois = new System.Windows.Forms.Button();
            this.virgula = new System.Windows.Forms.Button();
            this.soma = new System.Windows.Forms.Button();
            this.clear = new System.Windows.Forms.Button();
            this.lblaparece = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // mostraresultado
            // 
            this.mostraresultado.Cursor = System.Windows.Forms.Cursors.Default;
            this.mostraresultado.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mostraresultado.Location = new System.Drawing.Point(96, 29);
            this.mostraresultado.Name = "mostraresultado";
            this.mostraresultado.Size = new System.Drawing.Size(202, 31);
            this.mostraresultado.TabIndex = 0;
            this.mostraresultado.TextChanged += new System.EventHandler(this.mostraresultado_TextChanged);
            this.mostraresultado.KeyUp += new System.Windows.Forms.KeyEventHandler(this.mostraresultado_KeyUp);
            // 
            // um
            // 
            this.um.BackColor = System.Drawing.SystemColors.Window;
            this.um.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.um.ForeColor = System.Drawing.Color.Black;
            this.um.Location = new System.Drawing.Point(96, 83);
            this.um.Name = "um";
            this.um.Size = new System.Drawing.Size(41, 39);
            this.um.TabIndex = 1;
            this.um.Text = "1";
            this.um.UseVisualStyleBackColor = false;
            this.um.Click += new System.EventHandler(this.um_Click);
            // 
            // multiplicacao
            // 
            this.multiplicacao.BackColor = System.Drawing.SystemColors.Window;
            this.multiplicacao.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.multiplicacao.Location = new System.Drawing.Point(24, 202);
            this.multiplicacao.Name = "multiplicacao";
            this.multiplicacao.Size = new System.Drawing.Size(39, 37);
            this.multiplicacao.TabIndex = 10;
            this.multiplicacao.Text = "x";
            this.multiplicacao.UseVisualStyleBackColor = false;
            this.multiplicacao.Click += new System.EventHandler(this.multiplicacao_Click);
            // 
            // subtracao
            // 
            this.subtracao.BackColor = System.Drawing.SystemColors.Window;
            this.subtracao.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subtracao.Location = new System.Drawing.Point(24, 140);
            this.subtracao.Name = "subtracao";
            this.subtracao.Size = new System.Drawing.Size(39, 39);
            this.subtracao.TabIndex = 12;
            this.subtracao.Text = "-";
            this.subtracao.UseVisualStyleBackColor = false;
            this.subtracao.Click += new System.EventHandler(this.subtracao_Click);
            // 
            // divisao
            // 
            this.divisao.BackColor = System.Drawing.SystemColors.Window;
            this.divisao.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.divisao.Location = new System.Drawing.Point(24, 266);
            this.divisao.Name = "divisao";
            this.divisao.Size = new System.Drawing.Size(39, 36);
            this.divisao.TabIndex = 13;
            this.divisao.Text = "/";
            this.divisao.UseVisualStyleBackColor = false;
            this.divisao.Click += new System.EventHandler(this.divisao_Click);
            // 
            // resultado
            // 
            this.resultado.BackColor = System.Drawing.SystemColors.Window;
            this.resultado.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultado.ForeColor = System.Drawing.SystemColors.InfoText;
            this.resultado.Location = new System.Drawing.Point(257, 83);
            this.resultado.Name = "resultado";
            this.resultado.Size = new System.Drawing.Size(41, 39);
            this.resultado.TabIndex = 15;
            this.resultado.Text = "=";
            this.resultado.UseVisualStyleBackColor = false;
            this.resultado.Click += new System.EventHandler(this.resultado_Click);
            // 
            // quatro
            // 
            this.quatro.BackColor = System.Drawing.SystemColors.Window;
            this.quatro.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quatro.ForeColor = System.Drawing.Color.Black;
            this.quatro.Location = new System.Drawing.Point(96, 140);
            this.quatro.Name = "quatro";
            this.quatro.Size = new System.Drawing.Size(41, 39);
            this.quatro.TabIndex = 16;
            this.quatro.Text = "4";
            this.quatro.UseVisualStyleBackColor = false;
            this.quatro.Click += new System.EventHandler(this.quatro_Click);
            // 
            // seis
            // 
            this.seis.BackColor = System.Drawing.SystemColors.Window;
            this.seis.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seis.ForeColor = System.Drawing.Color.Black;
            this.seis.Location = new System.Drawing.Point(190, 140);
            this.seis.Name = "seis";
            this.seis.Size = new System.Drawing.Size(41, 39);
            this.seis.TabIndex = 17;
            this.seis.Text = "6";
            this.seis.UseVisualStyleBackColor = false;
            this.seis.Click += new System.EventHandler(this.seis_Click);
            // 
            // cinco
            // 
            this.cinco.BackColor = System.Drawing.SystemColors.Window;
            this.cinco.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cinco.ForeColor = System.Drawing.Color.Black;
            this.cinco.Location = new System.Drawing.Point(143, 140);
            this.cinco.Name = "cinco";
            this.cinco.Size = new System.Drawing.Size(41, 39);
            this.cinco.TabIndex = 18;
            this.cinco.Text = "5";
            this.cinco.UseMnemonic = false;
            this.cinco.UseVisualStyleBackColor = false;
            this.cinco.Click += new System.EventHandler(this.cinco_Click);
            // 
            // zero
            // 
            this.zero.BackColor = System.Drawing.SystemColors.Window;
            this.zero.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zero.ForeColor = System.Drawing.Color.Black;
            this.zero.Location = new System.Drawing.Point(257, 202);
            this.zero.Name = "zero";
            this.zero.Size = new System.Drawing.Size(41, 39);
            this.zero.TabIndex = 19;
            this.zero.Text = "0";
            this.zero.UseVisualStyleBackColor = false;
            this.zero.Click += new System.EventHandler(this.zero_Click);
            // 
            // nove
            // 
            this.nove.BackColor = System.Drawing.SystemColors.Window;
            this.nove.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nove.ForeColor = System.Drawing.Color.Black;
            this.nove.Location = new System.Drawing.Point(190, 202);
            this.nove.Name = "nove";
            this.nove.Size = new System.Drawing.Size(41, 39);
            this.nove.TabIndex = 20;
            this.nove.Text = "9";
            this.nove.UseVisualStyleBackColor = false;
            this.nove.Click += new System.EventHandler(this.nove_Click);
            // 
            // oito
            // 
            this.oito.BackColor = System.Drawing.SystemColors.Window;
            this.oito.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.oito.ForeColor = System.Drawing.Color.Black;
            this.oito.Location = new System.Drawing.Point(143, 202);
            this.oito.Name = "oito";
            this.oito.Size = new System.Drawing.Size(41, 39);
            this.oito.TabIndex = 21;
            this.oito.Text = "8";
            this.oito.UseVisualStyleBackColor = false;
            this.oito.Click += new System.EventHandler(this.oito_Click);
            // 
            // sete
            // 
            this.sete.BackColor = System.Drawing.SystemColors.Window;
            this.sete.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sete.ForeColor = System.Drawing.Color.Black;
            this.sete.Location = new System.Drawing.Point(96, 202);
            this.sete.Name = "sete";
            this.sete.Size = new System.Drawing.Size(41, 39);
            this.sete.TabIndex = 22;
            this.sete.Text = "7";
            this.sete.UseVisualStyleBackColor = false;
            this.sete.Click += new System.EventHandler(this.sete_Click);
            // 
            // tres
            // 
            this.tres.BackColor = System.Drawing.SystemColors.Window;
            this.tres.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tres.ForeColor = System.Drawing.Color.Black;
            this.tres.Location = new System.Drawing.Point(190, 83);
            this.tres.Name = "tres";
            this.tres.Size = new System.Drawing.Size(41, 39);
            this.tres.TabIndex = 23;
            this.tres.Text = "3";
            this.tres.UseVisualStyleBackColor = false;
            this.tres.Click += new System.EventHandler(this.tres_Click);
            // 
            // dois
            // 
            this.dois.BackColor = System.Drawing.SystemColors.Window;
            this.dois.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dois.ForeColor = System.Drawing.Color.Black;
            this.dois.Location = new System.Drawing.Point(143, 83);
            this.dois.Name = "dois";
            this.dois.Size = new System.Drawing.Size(41, 39);
            this.dois.TabIndex = 24;
            this.dois.Text = "2";
            this.dois.UseVisualStyleBackColor = false;
            this.dois.Click += new System.EventHandler(this.dois_Click);
            // 
            // virgula
            // 
            this.virgula.BackColor = System.Drawing.SystemColors.Window;
            this.virgula.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.virgula.Location = new System.Drawing.Point(257, 140);
            this.virgula.Name = "virgula";
            this.virgula.Size = new System.Drawing.Size(41, 39);
            this.virgula.TabIndex = 25;
            this.virgula.Text = ",";
            this.virgula.UseVisualStyleBackColor = false;
            this.virgula.Click += new System.EventHandler(this.virgula_Click);
            // 
            // soma
            // 
            this.soma.BackColor = System.Drawing.SystemColors.Window;
            this.soma.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.soma.Location = new System.Drawing.Point(24, 83);
            this.soma.Name = "soma";
            this.soma.Size = new System.Drawing.Size(39, 39);
            this.soma.TabIndex = 11;
            this.soma.Text = "+";
            this.soma.UseVisualStyleBackColor = false;
            this.soma.Click += new System.EventHandler(this.soma_Click);
            // 
            // clear
            // 
            this.clear.BackColor = System.Drawing.SystemColors.Window;
            this.clear.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clear.Location = new System.Drawing.Point(96, 265);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(202, 35);
            this.clear.TabIndex = 26;
            this.clear.Text = "Limpar";
            this.clear.UseVisualStyleBackColor = false;
            this.clear.Click += new System.EventHandler(this.limpa_Click);
            // 
            // lblaparece
            // 
            this.lblaparece.AutoSize = true;
            this.lblaparece.Location = new System.Drawing.Point(210, 8);
            this.lblaparece.Name = "lblaparece";
            this.lblaparece.Size = new System.Drawing.Size(0, 13);
            this.lblaparece.TabIndex = 27;
            this.lblaparece.Click += new System.EventHandler(this.lblaparece_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(324, 321);
            this.Controls.Add(this.lblaparece);
            this.Controls.Add(this.clear);
            this.Controls.Add(this.virgula);
            this.Controls.Add(this.dois);
            this.Controls.Add(this.tres);
            this.Controls.Add(this.sete);
            this.Controls.Add(this.oito);
            this.Controls.Add(this.nove);
            this.Controls.Add(this.zero);
            this.Controls.Add(this.cinco);
            this.Controls.Add(this.seis);
            this.Controls.Add(this.quatro);
            this.Controls.Add(this.resultado);
            this.Controls.Add(this.divisao);
            this.Controls.Add(this.subtracao);
            this.Controls.Add(this.soma);
            this.Controls.Add(this.multiplicacao);
            this.Controls.Add(this.um);
            this.Controls.Add(this.mostraresultado);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Calculadora";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox mostraresultado;
        private System.Windows.Forms.Button um;
        private System.Windows.Forms.Button multiplicacao;
        private System.Windows.Forms.Button subtracao;
        private System.Windows.Forms.Button divisao;
        private System.Windows.Forms.Button resultado;
        private System.Windows.Forms.Button quatro;
        private System.Windows.Forms.Button seis;
        private System.Windows.Forms.Button cinco;
        private System.Windows.Forms.Button zero;
        private System.Windows.Forms.Button nove;
        private System.Windows.Forms.Button oito;
        private System.Windows.Forms.Button sete;
        private System.Windows.Forms.Button tres;
        private System.Windows.Forms.Button dois;
        private System.Windows.Forms.Button virgula;
        private System.Windows.Forms.Button soma;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.Label lblaparece;
    }
}

