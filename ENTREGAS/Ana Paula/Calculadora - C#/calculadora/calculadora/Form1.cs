﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace calculadora
{
    public partial class Form1 : Form
    {
        double valorUm, valorDois, valorTres, valorQuatro, valorCinco, valorSeis, valorSete, valorOito, guardavalor;
        string operador;
        Boolean teclado;
       
  
        private void cinco_Click(object sender, EventArgs e)
        {
            mostraresultado.Text += "5";
        }

        private void seis_Click(object sender, EventArgs e)
        {
            mostraresultado.Text += "6";
        }

        private void sete_Click(object sender, EventArgs e)
        {
            mostraresultado.Text += "7";
        }

        private void oito_Click(object sender, EventArgs e)
        {
            mostraresultado.Text += "8";
        }

        private void nove_Click(object sender, EventArgs e)
        {
            mostraresultado.Text += "9";
        }

        private void subtracao_Click(object sender, EventArgs e)
        {
            try
            {
                if (teclado)
                    mostraresultado.Text = mostraresultado.Text.Remove(mostraresultado.Text.Length - 1);

                valorTres = Convert.ToDouble(mostraresultado.Text);

                lblaparece.Text = Convert.ToString(valorTres) + " - ";

                mostraresultado.Text = "";
                operador = "subtracao";
            }
            catch (Exception)
            {
                MessageBox.Show("Por favor, tente novamente. Essa operação não é válida.");
            }

        }

        private void multiplicacao_Click(object sender, EventArgs e)
        {
            try
            {
                if (teclado)
                    mostraresultado.Text = mostraresultado.Text.Remove(mostraresultado.Text.Length - 1);

                valorCinco = Convert.ToDouble(mostraresultado.Text);

                lblaparece.Text = Convert.ToString(valorCinco) + " * ";

                mostraresultado.Text = "";
                operador = "multiplicacao";
            }
            catch (Exception)
            {
                MessageBox.Show("Por favor, tente novamente. Essa operação não é válida.");
            }

        }

        private void divisao_Click(object sender, EventArgs e)
        {
            try
            {
                if (teclado)
                    mostraresultado.Text = mostraresultado.Text.Remove(mostraresultado.Text.Length - 1);

                valorSete = Convert.ToDouble(mostraresultado.Text);

                lblaparece.Text = Convert.ToString(valorSete) + " / ";

                mostraresultado.Text = "";
                operador = "divisao";
            }
            catch (Exception)
            {
                MessageBox.Show("Por favor, tente novamente. Essa operação não é válida.");
            }

        }



        private void zero_Click(object sender, EventArgs e)
        {
            mostraresultado.Text += "0";
        }

        private void quatro_Click(object sender, EventArgs e)
        {
            mostraresultado.Text += "4";
        }

        private void tres_Click(object sender, EventArgs e)
        {
            mostraresultado.Text += "3";
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void virgula_Click(object sender, EventArgs e)
        {
            mostraresultado.Text += ",";
        }

        private void mostraresultado_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Add || e.KeyCode == Keys.Oemplus)
            {
                teclado = true;
                soma_Click(this, null);
            }
            if (e.KeyCode == Keys.Subtract || e.KeyCode == Keys.OemMinus)
            {
                teclado = true;
                subtracao_Click(this, null);
            }
            if (e.KeyCode == Keys.Multiply)
            {
                teclado = true;
                multiplicacao_Click(this, null);
            }
            if (e.KeyCode == Keys.Divide)
            {
                teclado = true;
                divisao_Click(this, null);
            }
            if (e.KeyCode == Keys.Enter)
            {
                teclado = true;
                resultado_Click(this, null);

            }
            if (e.KeyCode == Keys.Escape)
            {
                teclado = true;
                limpa_Click(this, null);
            }
            if (e.KeyCode == Keys.OemPeriod)
            {
                teclado = true;
            
                mostraresultado.Text = mostraresultado.Text.Remove(mostraresultado.Text.Length -1);

                mostraresultado.Text += (",");

                mostraresultado.SelectionStart = mostraresultado.Text.Length;
                mostraresultado.SelectionLength = 0;

            }


        }

        private void limpa_Click(object sender, EventArgs e)
        {
            mostraresultado.Text = "";
            lblaparece.Text = "";
        }

        private void lblaparece_Click(object sender, EventArgs e)
        {

        }

        public Form1()
        {

            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void um_Click(object sender, EventArgs e)
        {

            mostraresultado.Text += "1";
        }

        private void dois_Click(object sender, EventArgs e)
        {
            mostraresultado.Text += "2";
        }

        private void resultado_Click(object sender, EventArgs e)
        {
            
            {
                if (operador == "soma")
            {
                valorDois = Convert.ToDouble(mostraresultado.Text);
                lblaparece.Text += Convert.ToString(valorDois) + " =";
                mostraresultado.Text = Convert.ToString(valorUm + valorDois);
            
                mostraresultado.SelectionStart = mostraresultado.Text.Length;
                mostraresultado.SelectionLength = 0;

            }

            if (operador == "subtracao")
            {
                valorQuatro = Convert.ToDouble(mostraresultado.Text);
                lblaparece.Text += Convert.ToString(valorQuatro) + " =";
                mostraresultado.Text = Convert.ToString(valorTres - valorQuatro);

                mostraresultado.SelectionStart = mostraresultado.Text.Length;
                mostraresultado.SelectionLength = 0;

            }

            if (operador == "multiplicacao")
            {
                valorSeis = Convert.ToDouble(mostraresultado.Text);
                lblaparece.Text += Convert.ToString(valorSeis) + " =";
                mostraresultado.Text = Convert.ToString(valorCinco * valorSeis);

                mostraresultado.SelectionStart = mostraresultado.Text.Length;
                mostraresultado.SelectionLength = 0;

            }
                if (operador == "divisao")
                {
                    valorOito = Convert.ToDouble(mostraresultado.Text);
                    lblaparece.Text += Convert.ToString(valorOito) + " =";
                    if (valorSete == 0 || valorOito == 0)
                    {
                        mostraresultado.Text = "Não é possível dividir por zero";
                    }
                    else
                    {
                        mostraresultado.Text = Convert.ToString(valorSete / valorOito);
                    }

                    mostraresultado.SelectionStart = mostraresultado.Text.Length;
                    mostraresultado.SelectionLength = 0;
                }


            }


        }

        private void mostraresultado_TextChanged(object sender, EventArgs e)
        {

        }

        private void soma_Click(object sender, EventArgs e)
        {
            try
            {
                if (teclado)
                    mostraresultado.Text = mostraresultado.Text.Remove(mostraresultado.Text.Length - 1);

                valorUm = Convert.ToDouble(mostraresultado.Text);

                lblaparece.Text = Convert.ToString(valorUm) + " + ";

                mostraresultado.Text = "";
                operador = "soma";
            }
            catch (Exception)
            {
                MessageBox.Show("Por favor, tente novamente. Essa operação não é válida.");
            }




        }
    }
}
