﻿namespace TelaFinal
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.recebernome = new System.Windows.Forms.Label();
            this.textonome = new System.Windows.Forms.TextBox();
            this.mostrarsobrenome = new System.Windows.Forms.Label();
            this.textosobrenome = new System.Windows.Forms.TextBox();
            this.button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // recebernome
            // 
            this.recebernome.AutoSize = true;
            this.recebernome.Location = new System.Drawing.Point(12, 56);
            this.recebernome.Name = "recebernome";
            this.recebernome.Size = new System.Drawing.Size(38, 13);
            this.recebernome.TabIndex = 0;
            this.recebernome.Text = "Nome:";
            // 
            // textonome
            // 
            this.textonome.Location = new System.Drawing.Point(56, 53);
            this.textonome.Name = "textonome";
            this.textonome.Size = new System.Drawing.Size(157, 20);
            this.textonome.TabIndex = 1;
            this.textonome.TextChanged += new System.EventHandler(this.textonome_TextChanged);
            // 
            // mostrarsobrenome
            // 
            this.mostrarsobrenome.AutoSize = true;
            this.mostrarsobrenome.Location = new System.Drawing.Point(11, 89);
            this.mostrarsobrenome.Name = "mostrarsobrenome";
            this.mostrarsobrenome.Size = new System.Drawing.Size(64, 13);
            this.mostrarsobrenome.TabIndex = 4;
            this.mostrarsobrenome.Text = "Sobrenome:";
            // 
            // textosobrenome
            // 
            this.textosobrenome.Location = new System.Drawing.Point(87, 86);
            this.textosobrenome.Name = "textosobrenome";
            this.textosobrenome.Size = new System.Drawing.Size(167, 20);
            this.textosobrenome.TabIndex = 5;
            // 
            // button
            // 
            this.button.Location = new System.Drawing.Point(87, 133);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(75, 23);
            this.button.TabIndex = 6;
            this.button.Text = "Clique aqui.";
            this.button.UseVisualStyleBackColor = true;
            this.button.Click += new System.EventHandler(this.button_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.button);
            this.Controls.Add(this.textosobrenome);
            this.Controls.Add(this.mostrarsobrenome);
            this.Controls.Add(this.textonome);
            this.Controls.Add(this.recebernome);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label recebernome;
        private System.Windows.Forms.TextBox textonome;
        private System.Windows.Forms.Label mostrarsobrenome;
        private System.Windows.Forms.TextBox textosobrenome;
        private System.Windows.Forms.Button button;
    }
}

