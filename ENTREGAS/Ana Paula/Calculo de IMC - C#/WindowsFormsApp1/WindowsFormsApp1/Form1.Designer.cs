﻿namespace WindowsFormsApp1
{
    partial class tela
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(tela));
            this.lblpeso = new System.Windows.Forms.Label();
            this.lblaltura = new System.Windows.Forms.Label();
            this.lblidade = new System.Windows.Forms.Label();
            this.lblsexo = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtpeso = new System.Windows.Forms.TextBox();
            this.txtaltura = new System.Windows.Forms.TextBox();
            this.txtidade = new System.Windows.Forms.TextBox();
            this.cmbsexo = new System.Windows.Forms.ComboBox();
            this.cmbatividade = new System.Windows.Forms.ComboBox();
            this.buttoncalculo = new System.Windows.Forms.Button();
            this.lblkg = new System.Windows.Forms.Label();
            this.lblmetros = new System.Windows.Forms.Label();
            this.lblanos = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblpeso
            // 
            this.lblpeso.AutoSize = true;
            this.lblpeso.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblpeso.Location = new System.Drawing.Point(94, 215);
            this.lblpeso.Name = "lblpeso";
            this.lblpeso.Size = new System.Drawing.Size(45, 16);
            this.lblpeso.TabIndex = 0;
            this.lblpeso.Text = "Peso:";
            // 
            // lblaltura
            // 
            this.lblaltura.AutoSize = true;
            this.lblaltura.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblaltura.Location = new System.Drawing.Point(87, 257);
            this.lblaltura.Name = "lblaltura";
            this.lblaltura.Size = new System.Drawing.Size(53, 16);
            this.lblaltura.TabIndex = 1;
            this.lblaltura.Text = "Altura:";
            // 
            // lblidade
            // 
            this.lblidade.AutoSize = true;
            this.lblidade.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblidade.Location = new System.Drawing.Point(89, 300);
            this.lblidade.Name = "lblidade";
            this.lblidade.Size = new System.Drawing.Size(51, 16);
            this.lblidade.TabIndex = 2;
            this.lblidade.Text = "Idade:";
            // 
            // lblsexo
            // 
            this.lblsexo.AutoSize = true;
            this.lblsexo.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsexo.Location = new System.Drawing.Point(94, 348);
            this.lblsexo.Name = "lblsexo";
            this.lblsexo.Size = new System.Drawing.Size(46, 16);
            this.lblsexo.TabIndex = 3;
            this.lblsexo.Text = "Sexo:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 391);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "Atividades físicas:";
            // 
            // txtpeso
            // 
            this.txtpeso.Location = new System.Drawing.Point(171, 214);
            this.txtpeso.Name = "txtpeso";
            this.txtpeso.Size = new System.Drawing.Size(167, 20);
            this.txtpeso.TabIndex = 6;
            this.txtpeso.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // txtaltura
            // 
            this.txtaltura.Location = new System.Drawing.Point(171, 259);
            this.txtaltura.Name = "txtaltura";
            this.txtaltura.Size = new System.Drawing.Size(167, 20);
            this.txtaltura.TabIndex = 7;
            // 
            // txtidade
            // 
            this.txtidade.Location = new System.Drawing.Point(171, 299);
            this.txtidade.Name = "txtidade";
            this.txtidade.Size = new System.Drawing.Size(167, 20);
            this.txtidade.TabIndex = 8;
            // 
            // cmbsexo
            // 
            this.cmbsexo.FormattingEnabled = true;
            this.cmbsexo.Items.AddRange(new object[] {
            "Feminino ",
            "Masculino"});
            this.cmbsexo.Location = new System.Drawing.Point(171, 343);
            this.cmbsexo.Name = "cmbsexo";
            this.cmbsexo.Size = new System.Drawing.Size(167, 21);
            this.cmbsexo.TabIndex = 9;
            // 
            // cmbatividade
            // 
            this.cmbatividade.FormattingEnabled = true;
            this.cmbatividade.Items.AddRange(new object[] {
            "Sedentário",
            "Moderada",
            "Intensa"});
            this.cmbatividade.Location = new System.Drawing.Point(171, 390);
            this.cmbatividade.Name = "cmbatividade";
            this.cmbatividade.Size = new System.Drawing.Size(167, 21);
            this.cmbatividade.TabIndex = 10;
            this.cmbatividade.SelectedIndexChanged += new System.EventHandler(this.cmbatividade_SelectedIndexChanged);
            // 
            // buttoncalculo
            // 
            this.buttoncalculo.BackColor = System.Drawing.SystemColors.Window;
            this.buttoncalculo.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttoncalculo.Location = new System.Drawing.Point(171, 444);
            this.buttoncalculo.Name = "buttoncalculo";
            this.buttoncalculo.Size = new System.Drawing.Size(167, 44);
            this.buttoncalculo.TabIndex = 11;
            this.buttoncalculo.Text = "Calcular IMC";
            this.buttoncalculo.UseVisualStyleBackColor = false;
            this.buttoncalculo.Click += new System.EventHandler(this.buttoncalculo_Click);
            // 
            // lblkg
            // 
            this.lblkg.AutoSize = true;
            this.lblkg.Location = new System.Drawing.Point(344, 218);
            this.lblkg.Name = "lblkg";
            this.lblkg.Size = new System.Drawing.Size(19, 13);
            this.lblkg.TabIndex = 12;
            this.lblkg.Text = "kg";
            // 
            // lblmetros
            // 
            this.lblmetros.AutoSize = true;
            this.lblmetros.Location = new System.Drawing.Point(344, 262);
            this.lblmetros.Name = "lblmetros";
            this.lblmetros.Size = new System.Drawing.Size(38, 13);
            this.lblmetros.TabIndex = 13;
            this.lblmetros.Text = "metros";
            this.lblmetros.Click += new System.EventHandler(this.lblmetros_Click);
            // 
            // lblanos
            // 
            this.lblanos.AutoSize = true;
            this.lblanos.Location = new System.Drawing.Point(344, 303);
            this.lblanos.Name = "lblanos";
            this.lblanos.Size = new System.Drawing.Size(30, 13);
            this.lblanos.TabIndex = 14;
            this.lblanos.Text = "anos";
            // 
            // tela
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(509, 522);
            this.Controls.Add(this.lblanos);
            this.Controls.Add(this.lblmetros);
            this.Controls.Add(this.lblkg);
            this.Controls.Add(this.buttoncalculo);
            this.Controls.Add(this.cmbatividade);
            this.Controls.Add(this.cmbsexo);
            this.Controls.Add(this.txtidade);
            this.Controls.Add(this.txtaltura);
            this.Controls.Add(this.txtpeso);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblsexo);
            this.Controls.Add(this.lblidade);
            this.Controls.Add(this.lblaltura);
            this.Controls.Add(this.lblpeso);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "tela";
            this.Text = "Cálculo de Índice de Massa Corporea";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblpeso;
        private System.Windows.Forms.Label lblaltura;
        private System.Windows.Forms.Label lblidade;
        private System.Windows.Forms.Label lblsexo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtpeso;
        private System.Windows.Forms.TextBox txtaltura;
        private System.Windows.Forms.TextBox txtidade;
        private System.Windows.Forms.ComboBox cmbsexo;
        private System.Windows.Forms.ComboBox cmbatividade;
        private System.Windows.Forms.Button buttoncalculo;
        private System.Windows.Forms.Label lblkg;
        private System.Windows.Forms.Label lblmetros;
        private System.Windows.Forms.Label lblanos;
    }
}

