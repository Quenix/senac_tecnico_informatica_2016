﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Banco
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void btncad_Click(object sender, EventArgs e)
        {
            var cadastro = new Cadastro();
            cadastro.ShowDialog(); 
        }

        private void btnentrar_Click(object sender, EventArgs e)
        {
            if((txtlog.Text == Convert.ToString("Admin") && txtpass.Text == Convert.ToString("Admin")) || (txtlog.Text == Convert.ToString(Cadastro.cpf) && txtpass.Text == Convert.ToString("123")))
            {
                var menu = new Menu();
                menu.ShowDialog();

            }
            else
            {
                MessageBox.Show("DADOS INCORRETOS!.TENTE NOVAMENTE.");
                lblcpf.ForeColor = System.Drawing.Color.Red;
                lblsenha.ForeColor = System.Drawing.Color.Red;
                lblcod.ForeColor = System.Drawing.Color.Red;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void txtlog_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
