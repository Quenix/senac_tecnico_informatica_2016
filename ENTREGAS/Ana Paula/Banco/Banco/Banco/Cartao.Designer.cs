﻿namespace Banco
{
    partial class Cartao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Cartao));
            this.lblsaldocartao = new System.Windows.Forms.Label();
            this.lblsaldocredito = new System.Windows.Forms.Label();
            this.lblsaldodebito = new System.Windows.Forms.Label();
            this.lblvalordebito = new System.Windows.Forms.Label();
            this.lblvalorcredito = new System.Windows.Forms.Label();
            this.btnextratocartaocred = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cartãoAdicionakToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.solicitarSegundaViaDoCartãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imagemDaFaturaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnretornemenu = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblsaldocartao
            // 
            this.lblsaldocartao.AutoSize = true;
            this.lblsaldocartao.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsaldocartao.Location = new System.Drawing.Point(52, 39);
            this.lblsaldocartao.Name = "lblsaldocartao";
            this.lblsaldocartao.Size = new System.Drawing.Size(147, 14);
            this.lblsaldocartao.TabIndex = 0;
            this.lblsaldocartao.Text = "Saldo atual do cartão:";
            this.lblsaldocartao.Click += new System.EventHandler(this.lblsaldocartao_Click);
            // 
            // lblsaldocredito
            // 
            this.lblsaldocredito.AutoSize = true;
            this.lblsaldocredito.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsaldocredito.Location = new System.Drawing.Point(52, 72);
            this.lblsaldocredito.Name = "lblsaldocredito";
            this.lblsaldocredito.Size = new System.Drawing.Size(58, 14);
            this.lblsaldocredito.TabIndex = 1;
            this.lblsaldocredito.Text = "Crédito:";
            // 
            // lblsaldodebito
            // 
            this.lblsaldodebito.AutoSize = true;
            this.lblsaldodebito.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsaldodebito.Location = new System.Drawing.Point(12, 171);
            this.lblsaldodebito.Name = "lblsaldodebito";
            this.lblsaldodebito.Size = new System.Drawing.Size(216, 14);
            this.lblsaldodebito.TabIndex = 2;
            this.lblsaldodebito.Text = "Seu saldo na função débito é de:";
            this.lblsaldodebito.Click += new System.EventHandler(this.lblsaldodebito_Click);
            // 
            // lblvalordebito
            // 
            this.lblvalordebito.AutoSize = true;
            this.lblvalordebito.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblvalordebito.Location = new System.Drawing.Point(254, 171);
            this.lblvalordebito.Name = "lblvalordebito";
            this.lblvalordebito.Size = new System.Drawing.Size(40, 14);
            this.lblvalordebito.TabIndex = 3;
            this.lblvalordebito.Text = "teste";
            this.lblvalordebito.Click += new System.EventHandler(this.lblvalordebito_Click);
            // 
            // lblvalorcredito
            // 
            this.lblvalorcredito.AutoSize = true;
            this.lblvalorcredito.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblvalorcredito.Location = new System.Drawing.Point(151, 72);
            this.lblvalorcredito.Name = "lblvalorcredito";
            this.lblvalorcredito.Size = new System.Drawing.Size(79, 14);
            this.lblvalorcredito.TabIndex = 4;
            this.lblvalorcredito.Text = "R$ 1751,93";
            this.lblvalorcredito.Click += new System.EventHandler(this.lblvalorcredito_Click);
            // 
            // btnextratocartaocred
            // 
            this.btnextratocartaocred.BackColor = System.Drawing.SystemColors.Window;
            this.btnextratocartaocred.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnextratocartaocred.Location = new System.Drawing.Point(55, 120);
            this.btnextratocartaocred.Name = "btnextratocartaocred";
            this.btnextratocartaocred.Size = new System.Drawing.Size(136, 23);
            this.btnextratocartaocred.TabIndex = 5;
            this.btnextratocartaocred.Text = "Visualizar Extrato";
            this.btnextratocartaocred.UseVisualStyleBackColor = false;
            this.btnextratocartaocred.Click += new System.EventHandler(this.btnextratocartaocred_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(423, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cartãoAdicionakToolStripMenuItem,
            this.solicitarSegundaViaDoCartãoToolStripMenuItem,
            this.imagemDaFaturaToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(89, 20);
            this.toolStripMenuItem1.Text = "Mais serviços";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // cartãoAdicionakToolStripMenuItem
            // 
            this.cartãoAdicionakToolStripMenuItem.Name = "cartãoAdicionakToolStripMenuItem";
            this.cartãoAdicionakToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.cartãoAdicionakToolStripMenuItem.Text = "Cartão Adicional";
            this.cartãoAdicionakToolStripMenuItem.Click += new System.EventHandler(this.cartãoAdicionakToolStripMenuItem_Click);
            // 
            // solicitarSegundaViaDoCartãoToolStripMenuItem
            // 
            this.solicitarSegundaViaDoCartãoToolStripMenuItem.Name = "solicitarSegundaViaDoCartãoToolStripMenuItem";
            this.solicitarSegundaViaDoCartãoToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.solicitarSegundaViaDoCartãoToolStripMenuItem.Text = "Solicitar segunda via do cartão";
            this.solicitarSegundaViaDoCartãoToolStripMenuItem.Click += new System.EventHandler(this.solicitarSegundaViaDoCartãoToolStripMenuItem_Click);
            // 
            // imagemDaFaturaToolStripMenuItem
            // 
            this.imagemDaFaturaToolStripMenuItem.Name = "imagemDaFaturaToolStripMenuItem";
            this.imagemDaFaturaToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.imagemDaFaturaToolStripMenuItem.Text = "Imagem da fatura";
            this.imagemDaFaturaToolStripMenuItem.Click += new System.EventHandler(this.imagemDaFaturaToolStripMenuItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(311, 120);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 128);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // btnretornemenu
            // 
            this.btnretornemenu.BackColor = System.Drawing.SystemColors.Window;
            this.btnretornemenu.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnretornemenu.Location = new System.Drawing.Point(55, 214);
            this.btnretornemenu.Name = "btnretornemenu";
            this.btnretornemenu.Size = new System.Drawing.Size(136, 23);
            this.btnretornemenu.TabIndex = 9;
            this.btnretornemenu.Text = "Menu";
            this.btnretornemenu.UseVisualStyleBackColor = false;
            // 
            // Cartao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 249);
            this.Controls.Add(this.btnretornemenu);
            this.Controls.Add(this.lblvalordebito);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnextratocartaocred);
            this.Controls.Add(this.lblvalorcredito);
            this.Controls.Add(this.lblsaldodebito);
            this.Controls.Add(this.lblsaldocredito);
            this.Controls.Add(this.lblsaldocartao);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Cartao";
            this.Text = "Cartão";
            this.Load += new System.EventHandler(this.Cartao_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblsaldocartao;
        private System.Windows.Forms.Label lblsaldocredito;
        private System.Windows.Forms.Label lblsaldodebito;
        private System.Windows.Forms.Label lblvalordebito;
        private System.Windows.Forms.Label lblvalorcredito;
        private System.Windows.Forms.Button btnextratocartaocred;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cartãoAdicionakToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem solicitarSegundaViaDoCartãoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imagemDaFaturaToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnretornemenu;
    }
}