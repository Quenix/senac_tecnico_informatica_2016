﻿namespace Banco
{
    partial class Deposito
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbldepositar = new System.Windows.Forms.Label();
            this.lblcedente = new System.Windows.Forms.Label();
            this.lbltel = new System.Windows.Forms.Label();
            this.btnfinalizar = new System.Windows.Forms.Button();
            this.txtdeposito = new System.Windows.Forms.TextBox();
            this.txtCPFConta = new System.Windows.Forms.TextBox();
            this.txttelefone = new System.Windows.Forms.TextBox();
            this.btncancelar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbldepositar
            // 
            this.lbldepositar.AutoSize = true;
            this.lbldepositar.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldepositar.Location = new System.Drawing.Point(51, 62);
            this.lbldepositar.Name = "lbldepositar";
            this.lbldepositar.Size = new System.Drawing.Size(182, 16);
            this.lbldepositar.TabIndex = 0;
            this.lbldepositar.Text = "Insira o valor do depósito:";
            // 
            // lblcedente
            // 
            this.lblcedente.AutoSize = true;
            this.lblcedente.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcedente.Location = new System.Drawing.Point(130, 101);
            this.lblcedente.Name = "lblcedente";
            this.lblcedente.Size = new System.Drawing.Size(103, 16);
            this.lblcedente.TabIndex = 1;
            this.lblcedente.Text = "CPF da conta:";
            // 
            // lbltel
            // 
            this.lbltel.AutoSize = true;
            this.lbltel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltel.Location = new System.Drawing.Point(72, 148);
            this.lbltel.Name = "lbltel";
            this.lbltel.Size = new System.Drawing.Size(161, 16);
            this.lbltel.TabIndex = 2;
            this.lbltel.Text = "Telefone para contato:";
            // 
            // btnfinalizar
            // 
            this.btnfinalizar.BackColor = System.Drawing.SystemColors.Window;
            this.btnfinalizar.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfinalizar.Location = new System.Drawing.Point(324, 199);
            this.btnfinalizar.Name = "btnfinalizar";
            this.btnfinalizar.Size = new System.Drawing.Size(130, 31);
            this.btnfinalizar.TabIndex = 3;
            this.btnfinalizar.Text = "Finalizar";
            this.btnfinalizar.UseVisualStyleBackColor = false;
            this.btnfinalizar.Click += new System.EventHandler(this.btnfinal_Click);
            // 
            // txtdeposito
            // 
            this.txtdeposito.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdeposito.Location = new System.Drawing.Point(275, 55);
            this.txtdeposito.Name = "txtdeposito";
            this.txtdeposito.Size = new System.Drawing.Size(145, 23);
            this.txtdeposito.TabIndex = 4;
            // 
            // txtCPFConta
            // 
            this.txtCPFConta.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCPFConta.Location = new System.Drawing.Point(275, 101);
            this.txtCPFConta.Name = "txtCPFConta";
            this.txtCPFConta.Size = new System.Drawing.Size(145, 23);
            this.txtCPFConta.TabIndex = 5;
            // 
            // txttelefone
            // 
            this.txttelefone.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttelefone.Location = new System.Drawing.Point(275, 145);
            this.txttelefone.Name = "txttelefone";
            this.txttelefone.Size = new System.Drawing.Size(145, 23);
            this.txttelefone.TabIndex = 6;
            // 
            // btncancelar
            // 
            this.btncancelar.BackColor = System.Drawing.SystemColors.Window;
            this.btncancelar.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncancelar.Location = new System.Drawing.Point(167, 199);
            this.btncancelar.Name = "btncancelar";
            this.btncancelar.Size = new System.Drawing.Size(130, 31);
            this.btncancelar.TabIndex = 7;
            this.btncancelar.Text = "Cancelar";
            this.btncancelar.UseVisualStyleBackColor = false;
            this.btncancelar.Click += new System.EventHandler(this.btncancelar_Click);
            // 
            // Deposito
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 257);
            this.Controls.Add(this.btncancelar);
            this.Controls.Add(this.txttelefone);
            this.Controls.Add(this.txtCPFConta);
            this.Controls.Add(this.txtdeposito);
            this.Controls.Add(this.btnfinalizar);
            this.Controls.Add(this.lbltel);
            this.Controls.Add(this.lblcedente);
            this.Controls.Add(this.lbldepositar);
            this.Name = "Deposito";
            this.Text = "Deposito";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbldepositar;
        private System.Windows.Forms.Label lblcedente;
        private System.Windows.Forms.Label lbltel;
        private System.Windows.Forms.Button btnfinalizar;
        private System.Windows.Forms.TextBox txtdeposito;
        private System.Windows.Forms.TextBox txtCPFConta;
        private System.Windows.Forms.TextBox txttelefone;
        private System.Windows.Forms.Button btncancelar;
    }
}