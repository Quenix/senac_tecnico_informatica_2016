﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Banco
{
    public partial class Cartao : Form
    {
        public Cartao()
        {
            InitializeComponent();
        }

        private void lblsaldocartao_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void lblsaldodebito_Click(object sender, EventArgs e)
        {

        }

        private void cartãoAdicionakToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var cartaoadc = new CartaoAdc();
            this.Visible = false;

        }

        private void btnextratocartaocred_Click(object sender, EventArgs e)
        {
            MessageBox.Show ("01/09 - PAGSEGURO UOL PIZZARIA E CHOPPERIA          R$57,80\n06/09 - CASAS BAHIA DEDICACAO TOTAL A VOCE          R$870,55\n11/09 - KABUM COMERCIO DE ELETRONICOS LTDA          R$582,75\n12/09 - MARISA LOJA DE ROUPAS PRA NAMORADI          R$156,23\n17/09 - TRIBOS ROCKSTORE EM ARARAQUARACITY          R$84,60\nTotal: R$1751,93\n Cotação do dólar (21/09): R$ 3,144\nPagamento Total da Fatura (21/09): R$ 1751,93\nValor mínimo da Fatura: R$ 350,38");
        }

        private void lblvalorcredito_Click(object sender, EventArgs e)
        {
            lblvalorcredito.Text = ("R$ 1751,93");
        }

        private void lblvalordebito_Click(object sender, EventArgs e)
        {

        }

        private void imagemDaFaturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("01/09 - PAGSEGURO UOL PIZZARIA E CHOPPERIA          R$57,80\n06/09 - CASAS BAHIA DEDICACAO TOTAL A VOCE          R$870,55\n11/09 - KABUM COMERCIO DE ELETRONICOS LTDA          R$582,75\n12/09 - MARISA LOJA DE ROUPAS PRA NAMORADI          R$156,23\n17/09 - TRIBOS ROCKSTORE EM ARARAQUARACITY          R$84,60\nTotal: R$1751,93\n Cotação do dólar (21/09): R$ 3,144\nPagamento Total da Fatura (21/09): R$ 1751,93\nValor mínimo da Fatura: R$ 350,38");
        }

        private void solicitarSegundaViaDoCartãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Não consta o bloqueio do cartão final 7385 em nosso sistema. Contate primeiramente o nosso SAC: 0800 777 7777");
        }

        private void Cartao_Load(object sender, EventArgs e)
        {

        }
    }
}
