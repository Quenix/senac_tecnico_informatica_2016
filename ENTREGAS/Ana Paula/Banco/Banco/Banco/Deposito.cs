﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Banco
{
    public partial class Deposito : Form
    {
        public double saldo ;
        public string teldeposito;

        public Deposito()
        {
            InitializeComponent();
        }

        private void btnfinal_Click(object sender, EventArgs e)
        {

            Double deposito = Convert.ToDouble(txtdeposito.Text);
           

            if (deposito > 0 && txtCPFConta.Text==Cadastro.cpf) 
            {
                Saldo.saldo = Saldo.saldo + deposito;
                MessageBox.Show("O valor depositado foi de: R$ " + deposito);

                teldeposito = txttelefone.Text;
                
            }
            else
            {
                MessageBox.Show("Informações incorretas");
            }
            this.Close();
        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
