﻿namespace Banco
{
    partial class CartaoAdc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CartaoAdc));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtnomeadc = new System.Windows.Forms.TextBox();
            this.lblcpf = new System.Windows.Forms.Label();
            this.txtcpfadc = new System.Windows.Forms.TextBox();
            this.lbllimiteadc = new System.Windows.Forms.Label();
            this.txtlimiteadc = new System.Windows.Forms.TextBox();
            this.btnsolicitaradc = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(88, 82);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(141, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 14);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nome:";
            // 
            // txtnomeadc
            // 
            this.txtnomeadc.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnomeadc.Location = new System.Drawing.Point(207, 35);
            this.txtnomeadc.Name = "txtnomeadc";
            this.txtnomeadc.Size = new System.Drawing.Size(219, 23);
            this.txtnomeadc.TabIndex = 2;
            // 
            // lblcpf
            // 
            this.lblcpf.AutoSize = true;
            this.lblcpf.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcpf.Location = new System.Drawing.Point(153, 86);
            this.lblcpf.Name = "lblcpf";
            this.lblcpf.Size = new System.Drawing.Size(36, 14);
            this.lblcpf.TabIndex = 3;
            this.lblcpf.Text = "CPF:";
            this.lblcpf.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtcpfadc
            // 
            this.txtcpfadc.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcpfadc.Location = new System.Drawing.Point(207, 82);
            this.txtcpfadc.Name = "txtcpfadc";
            this.txtcpfadc.Size = new System.Drawing.Size(219, 23);
            this.txtcpfadc.TabIndex = 4;
            // 
            // lbllimiteadc
            // 
            this.lbllimiteadc.AutoSize = true;
            this.lbllimiteadc.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllimiteadc.Location = new System.Drawing.Point(77, 140);
            this.lbllimiteadc.Name = "lbllimiteadc";
            this.lbllimiteadc.Size = new System.Drawing.Size(112, 14);
            this.lbllimiteadc.TabIndex = 5;
            this.lbllimiteadc.Text = "Limite desejado:";
            // 
            // txtlimiteadc
            // 
            this.txtlimiteadc.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlimiteadc.Location = new System.Drawing.Point(207, 136);
            this.txtlimiteadc.Name = "txtlimiteadc";
            this.txtlimiteadc.Size = new System.Drawing.Size(219, 23);
            this.txtlimiteadc.TabIndex = 6;
            // 
            // btnsolicitaradc
            // 
            this.btnsolicitaradc.BackColor = System.Drawing.SystemColors.Window;
            this.btnsolicitaradc.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsolicitaradc.Location = new System.Drawing.Point(144, 193);
            this.btnsolicitaradc.Name = "btnsolicitaradc";
            this.btnsolicitaradc.Size = new System.Drawing.Size(167, 25);
            this.btnsolicitaradc.TabIndex = 7;
            this.btnsolicitaradc.Text = "Solicitar";
            this.btnsolicitaradc.UseVisualStyleBackColor = false;
            this.btnsolicitaradc.Click += new System.EventHandler(this.btnsolicitaradc_Click);
            // 
            // CartaoAdc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 241);
            this.Controls.Add(this.btnsolicitaradc);
            this.Controls.Add(this.txtlimiteadc);
            this.Controls.Add(this.lbllimiteadc);
            this.Controls.Add(this.txtcpfadc);
            this.Controls.Add(this.lblcpf);
            this.Controls.Add(this.txtnomeadc);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CartaoAdc";
            this.Text = "Cartão Adicional";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtnomeadc;
        private System.Windows.Forms.Label lblcpf;
        private System.Windows.Forms.TextBox txtcpfadc;
        private System.Windows.Forms.Label lbllimiteadc;
        private System.Windows.Forms.TextBox txtlimiteadc;
        private System.Windows.Forms.Button btnsolicitaradc;
    }
}