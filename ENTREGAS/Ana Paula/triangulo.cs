﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            int firstvalue;
            int secondvalue;
            int thirdvalue;

            Console.WriteLine("Seja bem-vindo ao classificador de triângulos");
            Console.WriteLine("Qual é a medida do lado A?");
            firstvalue = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Qual é a medida do lado B?");
            secondvalue = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Qual é a medida do lado C?");
            thirdvalue = Convert.ToInt32(Console.ReadLine());

            if (firstvalue <= secondvalue + thirdvalue && secondvalue <= thirdvalue + firstvalue && thirdvalue <= firstvalue + secondvalue)
            {
                Console.WriteLine("Essa é uma figura triangular.");
            }


            if (firstvalue == secondvalue && thirdvalue == secondvalue)
            {
                Console.WriteLine("E é equilátero: três lados com medidas iguais.");
            }
            if (firstvalue == secondvalue || firstvalue == secondvalue || thirdvalue == secondvalue)
            {
                Console.WriteLine("E é isósceles: dois lados com medidas iguais e uma diferente.");

            }
            if (secondvalue != thirdvalue && firstvalue != secondvalue)
            {
                Console.WriteLine("E é escaleno: três lados com medidas diferentes.");
            }
            else
            {
                Console.WriteLine("Não é uma figura triângular.");
            }

        }
    }
}
