﻿namespace CadastroUsuario
{
    partial class menu
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(menu));
            this.lbltitulo = new System.Windows.Forms.Label();
            this.lblinformacao = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblnome = new System.Windows.Forms.Label();
            this.lblidade = new System.Windows.Forms.Label();
            this.lbldepartamento = new System.Windows.Forms.Label();
            this.lblcargo = new System.Windows.Forms.Label();
            this.lblsalario = new System.Windows.Forms.Label();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.txtidade = new System.Windows.Forms.TextBox();
            this.txtcargo = new System.Windows.Forms.TextBox();
            this.txtsalario = new System.Windows.Forms.TextBox();
            this.buttoncadastro = new System.Windows.Forms.Button();
            this.buttonsair = new System.Windows.Forms.Button();
            this.cmbdepartamento = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lbltitulo
            // 
            this.lbltitulo.AutoSize = true;
            this.lbltitulo.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltitulo.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lbltitulo.Location = new System.Drawing.Point(314, 40);
            this.lbltitulo.Name = "lbltitulo";
            this.lbltitulo.Size = new System.Drawing.Size(87, 16);
            this.lbltitulo.TabIndex = 0;
            this.lbltitulo.Text = "Bem-vindo. ";
            this.lbltitulo.Click += new System.EventHandler(this.lbltitulo_Click);
            // 
            // lblinformacao
            // 
            this.lblinformacao.AutoSize = true;
            this.lblinformacao.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblinformacao.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblinformacao.Location = new System.Drawing.Point(228, 107);
            this.lblinformacao.Name = "lblinformacao";
            this.lblinformacao.Size = new System.Drawing.Size(242, 16);
            this.lblinformacao.TabIndex = 1;
            this.lblinformacao.Text = "Insira suas informações cadastrais:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(24, 40);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(132, 123);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click_1);
            // 
            // lblnome
            // 
            this.lblnome.AutoSize = true;
            this.lblnome.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnome.ForeColor = System.Drawing.SystemColors.MenuText;
            this.lblnome.Location = new System.Drawing.Point(29, 182);
            this.lblnome.Name = "lblnome";
            this.lblnome.Size = new System.Drawing.Size(109, 14);
            this.lblnome.TabIndex = 3;
            this.lblnome.Text = "Nome completo:";
            this.lblnome.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblidade
            // 
            this.lblidade.AutoSize = true;
            this.lblidade.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblidade.ForeColor = System.Drawing.SystemColors.MenuText;
            this.lblidade.Location = new System.Drawing.Point(83, 211);
            this.lblidade.Name = "lblidade";
            this.lblidade.Size = new System.Drawing.Size(49, 14);
            this.lblidade.TabIndex = 4;
            this.lblidade.Text = "Idade:";
            this.lblidade.Click += new System.EventHandler(this.lblidade_Click);
            // 
            // lbldepartamento
            // 
            this.lbldepartamento.AutoSize = true;
            this.lbldepartamento.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldepartamento.ForeColor = System.Drawing.SystemColors.MenuText;
            this.lbldepartamento.Location = new System.Drawing.Point(29, 239);
            this.lbldepartamento.Name = "lbldepartamento";
            this.lbldepartamento.Size = new System.Drawing.Size(103, 14);
            this.lbldepartamento.TabIndex = 5;
            this.lbldepartamento.Text = "Departamento:";
            // 
            // lblcargo
            // 
            this.lblcargo.AutoSize = true;
            this.lblcargo.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcargo.ForeColor = System.Drawing.SystemColors.MenuText;
            this.lblcargo.Location = new System.Drawing.Point(82, 264);
            this.lblcargo.Name = "lblcargo";
            this.lblcargo.Size = new System.Drawing.Size(50, 14);
            this.lblcargo.TabIndex = 6;
            this.lblcargo.Text = "Cargo:";
            // 
            // lblsalario
            // 
            this.lblsalario.AutoSize = true;
            this.lblsalario.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsalario.ForeColor = System.Drawing.SystemColors.MenuText;
            this.lblsalario.Location = new System.Drawing.Point(77, 290);
            this.lblsalario.Name = "lblsalario";
            this.lblsalario.Size = new System.Drawing.Size(55, 14);
            this.lblsalario.TabIndex = 7;
            this.lblsalario.Text = "Salário:";
            // 
            // txtnome
            // 
            this.txtnome.Location = new System.Drawing.Point(173, 180);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(228, 20);
            this.txtnome.TabIndex = 8;
            this.txtnome.TextChanged += new System.EventHandler(this.txtnome_TextChanged);
            // 
            // txtidade
            // 
            this.txtidade.Location = new System.Drawing.Point(173, 209);
            this.txtidade.Name = "txtidade";
            this.txtidade.Size = new System.Drawing.Size(228, 20);
            this.txtidade.TabIndex = 9;
            // 
            // txtcargo
            // 
            this.txtcargo.Location = new System.Drawing.Point(173, 264);
            this.txtcargo.Name = "txtcargo";
            this.txtcargo.Size = new System.Drawing.Size(228, 20);
            this.txtcargo.TabIndex = 11;
            // 
            // txtsalario
            // 
            this.txtsalario.Location = new System.Drawing.Point(173, 290);
            this.txtsalario.Name = "txtsalario";
            this.txtsalario.Size = new System.Drawing.Size(228, 20);
            this.txtsalario.TabIndex = 12;
            // 
            // buttoncadastro
            // 
            this.buttoncadastro.BackColor = System.Drawing.Color.White;
            this.buttoncadastro.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttoncadastro.ForeColor = System.Drawing.SystemColors.MenuText;
            this.buttoncadastro.Location = new System.Drawing.Point(356, 379);
            this.buttoncadastro.Name = "buttoncadastro";
            this.buttoncadastro.Size = new System.Drawing.Size(170, 23);
            this.buttoncadastro.TabIndex = 13;
            this.buttoncadastro.Text = "Efetuar cadastro";
            this.buttoncadastro.UseVisualStyleBackColor = false;
            this.buttoncadastro.Click += new System.EventHandler(this.buttoncadastro_Click);
            // 
            // buttonsair
            // 
            this.buttonsair.BackColor = System.Drawing.Color.White;
            this.buttonsair.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonsair.ForeColor = System.Drawing.SystemColors.MenuText;
            this.buttonsair.Location = new System.Drawing.Point(80, 379);
            this.buttonsair.Name = "buttonsair";
            this.buttonsair.Size = new System.Drawing.Size(170, 23);
            this.buttonsair.TabIndex = 14;
            this.buttonsair.Text = "Sair do sistema";
            this.buttonsair.UseVisualStyleBackColor = false;
            this.buttonsair.Click += new System.EventHandler(this.buttonsair_Click);
            // 
            // cmbdepartamento
            // 
            this.cmbdepartamento.FormattingEnabled = true;
            this.cmbdepartamento.Items.AddRange(new object[] {
            "Administração",
            "Contabilidade",
            "Diretoria",
            "Logística",
            "Financeiro\t",
            "Tecnologia da Informação",
            "Recursos Humanos"});
            this.cmbdepartamento.Location = new System.Drawing.Point(173, 235);
            this.cmbdepartamento.Name = "cmbdepartamento";
            this.cmbdepartamento.Size = new System.Drawing.Size(228, 21);
            this.cmbdepartamento.TabIndex = 15;
            // 
            // menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(611, 447);
            this.Controls.Add(this.cmbdepartamento);
            this.Controls.Add(this.buttonsair);
            this.Controls.Add(this.buttoncadastro);
            this.Controls.Add(this.txtsalario);
            this.Controls.Add(this.txtcargo);
            this.Controls.Add(this.txtidade);
            this.Controls.Add(this.txtnome);
            this.Controls.Add(this.lblsalario);
            this.Controls.Add(this.lblcargo);
            this.Controls.Add(this.lbldepartamento);
            this.Controls.Add(this.lblidade);
            this.Controls.Add(this.lblnome);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblinformacao);
            this.Controls.Add(this.lbltitulo);
            this.ForeColor = System.Drawing.SystemColors.Window;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "menu";
            this.RightToLeftLayout = true;
            this.Text = "Cadastro de usuário";
            this.Load += new System.EventHandler(this.menu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbltitulo;
        private System.Windows.Forms.Label lblinformacao;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblnome;
        private System.Windows.Forms.Label lblidade;
        private System.Windows.Forms.Label lbldepartamento;
        private System.Windows.Forms.Label lblcargo;
        private System.Windows.Forms.Label lblsalario;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.TextBox txtidade;
        private System.Windows.Forms.TextBox txtcargo;
        private System.Windows.Forms.TextBox txtsalario;
        private System.Windows.Forms.Button buttoncadastro;
        private System.Windows.Forms.Button buttonsair;
        private System.Windows.Forms.ComboBox cmbdepartamento;
    }
}

