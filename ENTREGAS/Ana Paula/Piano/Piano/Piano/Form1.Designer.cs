﻿namespace Piano
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnc = new System.Windows.Forms.Button();
            this.btnb = new System.Windows.Forms.Button();
            this.btna = new System.Windows.Forms.Button();
            this.btng = new System.Windows.Forms.Button();
            this.btnf = new System.Windows.Forms.Button();
            this.btne = new System.Windows.Forms.Button();
            this.btncsus = new System.Windows.Forms.Button();
            this.btnd = new System.Windows.Forms.Button();
            this.btndsus = new System.Windows.Forms.Button();
            this.btnfsus = new System.Windows.Forms.Button();
            this.btngsus = new System.Windows.Forms.Button();
            this.btnasus = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnc
            // 
            this.btnc.BackColor = System.Drawing.Color.White;
            this.btnc.Location = new System.Drawing.Point(153, 202);
            this.btnc.Name = "btnc";
            this.btnc.Size = new System.Drawing.Size(35, 161);
            this.btnc.TabIndex = 0;
            this.btnc.UseVisualStyleBackColor = false;
            this.btnc.Click += new System.EventHandler(this.btnpiano_Click);
            // 
            // btnb
            // 
            this.btnb.BackColor = System.Drawing.Color.White;
            this.btnb.Location = new System.Drawing.Point(355, 202);
            this.btnb.Name = "btnb";
            this.btnb.Size = new System.Drawing.Size(35, 161);
            this.btnb.TabIndex = 1;
            this.btnb.UseVisualStyleBackColor = false;
            this.btnb.Click += new System.EventHandler(this.btnb_Click);
            // 
            // btna
            // 
            this.btna.BackColor = System.Drawing.Color.White;
            this.btna.Location = new System.Drawing.Point(321, 202);
            this.btna.Name = "btna";
            this.btna.Size = new System.Drawing.Size(35, 161);
            this.btna.TabIndex = 2;
            this.btna.UseVisualStyleBackColor = false;
            this.btna.Click += new System.EventHandler(this.btna_Click);
            // 
            // btng
            // 
            this.btng.BackColor = System.Drawing.Color.White;
            this.btng.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btng.Location = new System.Drawing.Point(287, 202);
            this.btng.Name = "btng";
            this.btng.Size = new System.Drawing.Size(35, 161);
            this.btng.TabIndex = 3;
            this.btng.UseVisualStyleBackColor = false;
            this.btng.Click += new System.EventHandler(this.btng_Click);
            // 
            // btnf
            // 
            this.btnf.BackColor = System.Drawing.Color.White;
            this.btnf.Location = new System.Drawing.Point(254, 202);
            this.btnf.Name = "btnf";
            this.btnf.Size = new System.Drawing.Size(35, 161);
            this.btnf.TabIndex = 4;
            this.btnf.UseVisualStyleBackColor = false;
            this.btnf.Click += new System.EventHandler(this.btnf_Click);
            // 
            // btne
            // 
            this.btne.BackColor = System.Drawing.Color.White;
            this.btne.Location = new System.Drawing.Point(220, 202);
            this.btne.Name = "btne";
            this.btne.Size = new System.Drawing.Size(35, 161);
            this.btne.TabIndex = 5;
            this.btne.UseVisualStyleBackColor = false;
            this.btne.Click += new System.EventHandler(this.btne_Click);
            // 
            // btncsus
            // 
            this.btncsus.BackColor = System.Drawing.Color.Black;
            this.btncsus.Location = new System.Drawing.Point(173, 167);
            this.btncsus.Name = "btncsus";
            this.btncsus.Size = new System.Drawing.Size(22, 128);
            this.btncsus.TabIndex = 11;
            this.btncsus.UseVisualStyleBackColor = false;
            this.btncsus.Click += new System.EventHandler(this.button11_Click);
            // 
            // btnd
            // 
            this.btnd.BackColor = System.Drawing.Color.White;
            this.btnd.Location = new System.Drawing.Point(186, 202);
            this.btnd.Name = "btnd";
            this.btnd.Size = new System.Drawing.Size(35, 161);
            this.btnd.TabIndex = 12;
            this.btnd.UseVisualStyleBackColor = false;
            this.btnd.Click += new System.EventHandler(this.btnd_Click);
            // 
            // btndsus
            // 
            this.btndsus.BackColor = System.Drawing.Color.Black;
            this.btndsus.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.btndsus.Location = new System.Drawing.Point(208, 167);
            this.btndsus.Name = "btndsus";
            this.btndsus.Size = new System.Drawing.Size(22, 128);
            this.btndsus.TabIndex = 13;
            this.btndsus.UseVisualStyleBackColor = false;
            this.btndsus.Click += new System.EventHandler(this.btndsus_Click);
            // 
            // btnfsus
            // 
            this.btnfsus.BackColor = System.Drawing.Color.Black;
            this.btnfsus.Location = new System.Drawing.Point(276, 166);
            this.btnfsus.Name = "btnfsus";
            this.btnfsus.Size = new System.Drawing.Size(22, 128);
            this.btnfsus.TabIndex = 14;
            this.btnfsus.UseVisualStyleBackColor = false;
            this.btnfsus.Click += new System.EventHandler(this.btnfsus_Click);
            // 
            // btngsus
            // 
            this.btngsus.BackColor = System.Drawing.Color.Black;
            this.btngsus.Location = new System.Drawing.Point(308, 166);
            this.btngsus.Name = "btngsus";
            this.btngsus.Size = new System.Drawing.Size(22, 128);
            this.btngsus.TabIndex = 15;
            this.btngsus.UseVisualStyleBackColor = false;
            this.btngsus.Click += new System.EventHandler(this.btngsus_Click);
            // 
            // btnasus
            // 
            this.btnasus.BackColor = System.Drawing.Color.Black;
            this.btnasus.Location = new System.Drawing.Point(343, 166);
            this.btnasus.Name = "btnasus";
            this.btnasus.Size = new System.Drawing.Size(22, 128);
            this.btnasus.TabIndex = 16;
            this.btnasus.UseVisualStyleBackColor = false;
            this.btnasus.Click += new System.EventHandler(this.button15_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(427, 365);
            this.Controls.Add(this.btngsus);
            this.Controls.Add(this.btnasus);
            this.Controls.Add(this.btna);
            this.Controls.Add(this.btnfsus);
            this.Controls.Add(this.btndsus);
            this.Controls.Add(this.btncsus);
            this.Controls.Add(this.btnd);
            this.Controls.Add(this.btne);
            this.Controls.Add(this.btnf);
            this.Controls.Add(this.btng);
            this.Controls.Add(this.btnb);
            this.Controls.Add(this.btnc);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Piano";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnc;
        private System.Windows.Forms.Button btnb;
        private System.Windows.Forms.Button btna;
        private System.Windows.Forms.Button btng;
        private System.Windows.Forms.Button btnf;
        private System.Windows.Forms.Button btne;
        private System.Windows.Forms.Button btncsus;
        private System.Windows.Forms.Button btnd;
        private System.Windows.Forms.Button btndsus;
        private System.Windows.Forms.Button btnfsus;
        private System.Windows.Forms.Button btngsus;
        private System.Windows.Forms.Button btnasus;
    }
}

