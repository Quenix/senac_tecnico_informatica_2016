﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Toub.Sound.Midi;

namespace Piano
{
    public partial class Form1 : Form
    {
        Boolean teclado;
        public Form1()
        {
            InitializeComponent();
            MidiPlayer.OpenMidi();
            
            
        }

        private void btnpiano_Click(object sender, EventArgs e)
        {
            MidiPlayer.OpenMidi();

            MidiPlayer.Play(new NoteOn(0, 1, "C4", 127));
        }

        private void button11_Click(object sender, EventArgs e)
        {
            MidiPlayer.OpenMidi();

            MidiPlayer.Play(new NoteOn(0, 1, "C#4", 127));
        }

        private void btnd_Click(object sender, EventArgs e)
        {
            MidiPlayer.OpenMidi();

            MidiPlayer.Play(new NoteOn(0, 1, "D4", 127));
        }

        private void btne_Click(object sender, EventArgs e)
        {
            MidiPlayer.OpenMidi();

            MidiPlayer.Play(new NoteOn(0, 1, "E4", 127));
        }

        private void btnf_Click(object sender, EventArgs e)
        {
            MidiPlayer.OpenMidi();

            MidiPlayer.Play(new NoteOn(0, 1, "F4", 127));
        }

        private void btng_Click(object sender, EventArgs e)
        {
            MidiPlayer.OpenMidi();

            MidiPlayer.Play(new NoteOn(0, 1, "G4", 127));

        }

        private void btna_Click(object sender, EventArgs e)
        {
            MidiPlayer.OpenMidi();

            MidiPlayer.Play(new NoteOn(0, 1, "A4", 127));
        }

        private void btnb_Click(object sender, EventArgs e)
        {
            MidiPlayer.OpenMidi();

            MidiPlayer.Play(new NoteOn(0, 1, "B4", 127));
        }

        private void btndsus_Click(object sender, EventArgs e)
        {
            MidiPlayer.OpenMidi();

            MidiPlayer.Play(new NoteOn(0, 1, "D#4", 127));
        }

        private void btnfsus_Click(object sender, EventArgs e)
        {
            MidiPlayer.OpenMidi();

            MidiPlayer.Play(new NoteOn(0, 1, "F#4", 127));
        }

        private void btngsus_Click(object sender, EventArgs e)
        {
            MidiPlayer.OpenMidi();

            MidiPlayer.Play(new NoteOn(0, 1, "G#4", 127));
        }

        private void button15_Click(object sender, EventArgs e)
        {
            MidiPlayer.OpenMidi();

            MidiPlayer.Play(new NoteOn(0, 1, "A#4", 127));
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.F1)
            {
                teclado = true;
                button11_Click(this, null);

            }
            if (e.KeyCode == Keys.F2)
            {
                teclado = true;
                btndsus_Click(this, null);
            }
            if (e.KeyCode == Keys.F3)
            {
                teclado = true;
                btnfsus_Click(this, null);
            }
            if (e.KeyCode == Keys.F4)
            {
                teclado = true;
                btngsus_Click(this, null);
            }
            if (e.KeyCode == Keys.F5)
            {
                teclado = true;
                button15_Click(this, null);
            }

            if (e.KeyCode == Keys.NumPad1 || e.KeyCode == Keys.D1)
            {
                teclado = true;
                btnpiano_Click(this, null);

            }
            if (e.KeyCode == Keys.NumPad2 || e.KeyCode == Keys.D2)
            {
                teclado = true;
                btnd_Click(this, null);
            }
            if (e.KeyCode == Keys.NumPad3 || e.KeyCode == Keys.D3)
            {
                teclado = true;
                btne_Click(this, null);
            }
            if (e.KeyCode == Keys.NumPad4 || e.KeyCode == Keys.D4)
            {
                teclado = true;
                btnf_Click(this, null);
            }
            if (e.KeyCode == Keys.NumPad5 || e.KeyCode == Keys.D5)
            {
                teclado = true;
                btng_Click(this, null);
            }
            if (e.KeyCode == Keys.NumPad6 || e.KeyCode == Keys.D6)
            {
                teclado = true;
                btna_Click(this, null);
            }
            if (e.KeyCode == Keys.NumPad7 || e.KeyCode == Keys.D7)
            {
                teclado = true;
                btna_Click(this, null);
            }
            if (e.KeyCode == Keys.NumPad8 || e.KeyCode == Keys.D8)
            {
                teclado = true;
                btnb_Click(this, null);
            }
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void OnKeyUpHandler(object sender, KeyEventArgs e)
        {
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}





