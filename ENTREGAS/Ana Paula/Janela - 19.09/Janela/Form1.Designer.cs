﻿namespace Janela
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.cadastroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verDadosCadastraisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.corDeBotãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verificarCorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fraseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verificarFraseInversaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastroToolStripMenuItem,
            this.corDeBotãoToolStripMenuItem,
            this.fraseToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(337, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // cadastroToolStripMenuItem
            // 
            this.cadastroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoToolStripMenuItem,
            this.verDadosCadastraisToolStripMenuItem});
            this.cadastroToolStripMenuItem.Name = "cadastroToolStripMenuItem";
            this.cadastroToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.cadastroToolStripMenuItem.Text = "Cadastro";
            this.cadastroToolStripMenuItem.Click += new System.EventHandler(this.cadastroToolStripMenuItem_Click);
            // 
            // novoToolStripMenuItem
            // 
            this.novoToolStripMenuItem.Name = "novoToolStripMenuItem";
            this.novoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.novoToolStripMenuItem.Text = "Novo";
            this.novoToolStripMenuItem.Click += new System.EventHandler(this.novoToolStripMenuItem_Click);
            // 
            // verDadosCadastraisToolStripMenuItem
            // 
            this.verDadosCadastraisToolStripMenuItem.Name = "verDadosCadastraisToolStripMenuItem";
            this.verDadosCadastraisToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.verDadosCadastraisToolStripMenuItem.Text = "Ver dados cadastrais";
            this.verDadosCadastraisToolStripMenuItem.Click += new System.EventHandler(this.verDadosCadastraisToolStripMenuItem_Click);
            // 
            // corDeBotãoToolStripMenuItem
            // 
            this.corDeBotãoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verificarCorToolStripMenuItem});
            this.corDeBotãoToolStripMenuItem.Name = "corDeBotãoToolStripMenuItem";
            this.corDeBotãoToolStripMenuItem.Size = new System.Drawing.Size(88, 20);
            this.corDeBotãoToolStripMenuItem.Text = "Cor de botão";
            this.corDeBotãoToolStripMenuItem.Click += new System.EventHandler(this.corDeBotãoToolStripMenuItem_Click);
            // 
            // verificarCorToolStripMenuItem
            // 
            this.verificarCorToolStripMenuItem.Name = "verificarCorToolStripMenuItem";
            this.verificarCorToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.verificarCorToolStripMenuItem.Text = "Verificar cor";
            this.verificarCorToolStripMenuItem.Click += new System.EventHandler(this.verificarCorToolStripMenuItem_Click);
            // 
            // fraseToolStripMenuItem
            // 
            this.fraseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verificarFraseInversaToolStripMenuItem});
            this.fraseToolStripMenuItem.Name = "fraseToolStripMenuItem";
            this.fraseToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.fraseToolStripMenuItem.Text = "Frase";
            this.fraseToolStripMenuItem.Click += new System.EventHandler(this.fraseToolStripMenuItem_Click);
            // 
            // verificarFraseInversaToolStripMenuItem
            // 
            this.verificarFraseInversaToolStripMenuItem.Name = "verificarFraseInversaToolStripMenuItem";
            this.verificarFraseInversaToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.verificarFraseInversaToolStripMenuItem.Text = "Ver o inverso de uma frase";
            this.verificarFraseInversaToolStripMenuItem.Click += new System.EventHandler(this.verificarFraseInversaToolStripMenuItem_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(337, 202);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Menu";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cadastroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verDadosCadastraisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem corDeBotãoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verificarCorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fraseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verificarFraseInversaToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
    }
}