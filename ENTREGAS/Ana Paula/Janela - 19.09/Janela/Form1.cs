﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Janela
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void cadastroToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void novoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var cadastro = new cadastro();
            cadastro.ShowDialog();
        }

        private void verDadosCadastraisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dados = new dados();
            dados.ShowDialog();
        }

        private void corDeBotãoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void verificarCorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var cor = new cor();
            cor.ShowDialog();   

        }

        private void fraseToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void verificarFraseInversaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frase = new frase();
            frase.ShowDialog();
        }
    }
}
