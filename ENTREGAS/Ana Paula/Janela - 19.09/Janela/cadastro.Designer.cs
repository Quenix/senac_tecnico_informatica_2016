﻿namespace Janela
{
    partial class cadastro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cadastro));
            this.lblnome = new System.Windows.Forms.Label();
            this.lblidade = new System.Windows.Forms.Label();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.txtidade = new System.Windows.Forms.TextBox();
            this.btnmaioridade = new System.Windows.Forms.Button();
            this.lblmaioridade = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblnome
            // 
            this.lblnome.AutoSize = true;
            this.lblnome.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnome.Location = new System.Drawing.Point(40, 49);
            this.lblnome.Name = "lblnome";
            this.lblnome.Size = new System.Drawing.Size(50, 16);
            this.lblnome.TabIndex = 0;
            this.lblnome.Text = "Nome:";
            // 
            // lblidade
            // 
            this.lblidade.AutoSize = true;
            this.lblidade.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblidade.Location = new System.Drawing.Point(41, 103);
            this.lblidade.Name = "lblidade";
            this.lblidade.Size = new System.Drawing.Size(49, 14);
            this.lblidade.TabIndex = 1;
            this.lblidade.Text = "Idade:";
            // 
            // txtnome
            // 
            this.txtnome.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnome.Location = new System.Drawing.Point(96, 46);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(173, 23);
            this.txtnome.TabIndex = 2;
            this.txtnome.TextChanged += new System.EventHandler(this.txtnome_TextChanged);
            // 
            // txtidade
            // 
            this.txtidade.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtidade.Location = new System.Drawing.Point(96, 99);
            this.txtidade.Name = "txtidade";
            this.txtidade.Size = new System.Drawing.Size(173, 23);
            this.txtidade.TabIndex = 3;
            // 
            // btnmaioridade
            // 
            this.btnmaioridade.BackColor = System.Drawing.SystemColors.Window;
            this.btnmaioridade.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmaioridade.Location = new System.Drawing.Point(96, 146);
            this.btnmaioridade.Name = "btnmaioridade";
            this.btnmaioridade.Size = new System.Drawing.Size(173, 23);
            this.btnmaioridade.TabIndex = 4;
            this.btnmaioridade.Text = "Verificar maioridade";
            this.btnmaioridade.UseVisualStyleBackColor = false;
            this.btnmaioridade.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblmaioridade
            // 
            this.lblmaioridade.AutoSize = true;
            this.lblmaioridade.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmaioridade.Location = new System.Drawing.Point(41, 190);
            this.lblmaioridade.Name = "lblmaioridade";
            this.lblmaioridade.Size = new System.Drawing.Size(0, 13);
            this.lblmaioridade.TabIndex = 5;
            // 
            // cadastro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(370, 246);
            this.Controls.Add(this.lblmaioridade);
            this.Controls.Add(this.btnmaioridade);
            this.Controls.Add(this.txtidade);
            this.Controls.Add(this.txtnome);
            this.Controls.Add(this.lblidade);
            this.Controls.Add(this.lblnome);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "cadastro";
            this.Text = "Cadastro";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblnome;
        private System.Windows.Forms.Label lblidade;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.TextBox txtidade;
        private System.Windows.Forms.Button btnmaioridade;
        private System.Windows.Forms.Label lblmaioridade;
    }
}