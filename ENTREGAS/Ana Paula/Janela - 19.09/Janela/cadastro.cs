﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Janela
{
    public partial class cadastro : Form
    {
        Double idade;
        string nome;

        public cadastro()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            idade = Convert.ToDouble (txtidade.Text);
            nome = Convert.ToString(txtnome.Text);
            dados.nome = nome;
            dados.idade = Convert.ToString(idade);
            if (idade < 18)
            {
                lblmaioridade.Text = "De acordo com o cadastro, " + nome + " é menor de idade.";
            }
            else
            {
                lblmaioridade.Text = "De acordo com o cadastro, " + nome + " é maior de idade.";
            }

        }

        private void txtnome_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
