﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Janela
{
    public partial class cor : Form
    {
        public cor()
        {
            InitializeComponent();
        }

        private void lblcor_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            lblcor.ForeColor = System.Drawing.Color.Black;
        }

        private void btncor2_Click(object sender, EventArgs e)
        {
            lblcor.ForeColor = System.Drawing.Color.Red;
        }

        private void btncor3_Click(object sender, EventArgs e)
        {
            lblcor.ForeColor = System.Drawing.Color.Blue;
        }

        private void btncor4_Click(object sender, EventArgs e)
        {
            lblcor.ForeColor = System.Drawing.Color.DarkGreen;
        }
    }
}
