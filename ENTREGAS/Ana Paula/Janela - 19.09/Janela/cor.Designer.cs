﻿namespace Janela
{
    partial class cor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cor));
            this.lblcor = new System.Windows.Forms.Label();
            this.btncor1 = new System.Windows.Forms.Button();
            this.btncor3 = new System.Windows.Forms.Button();
            this.btncor2 = new System.Windows.Forms.Button();
            this.btncor4 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblcor
            // 
            this.lblcor.AutoSize = true;
            this.lblcor.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcor.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblcor.Location = new System.Drawing.Point(77, 91);
            this.lblcor.Name = "lblcor";
            this.lblcor.Size = new System.Drawing.Size(233, 16);
            this.lblcor.TabIndex = 0;
            this.lblcor.Text = "Aqui vai aparecer a cor escolhida.";
            this.lblcor.Click += new System.EventHandler(this.lblcor_Click);
            // 
            // btncor1
            // 
            this.btncor1.BackColor = System.Drawing.SystemColors.Window;
            this.btncor1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncor1.Location = new System.Drawing.Point(12, 35);
            this.btncor1.Name = "btncor1";
            this.btncor1.Size = new System.Drawing.Size(92, 27);
            this.btncor1.TabIndex = 1;
            this.btncor1.Text = "Preto";
            this.btncor1.UseVisualStyleBackColor = false;
            this.btncor1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btncor3
            // 
            this.btncor3.BackColor = System.Drawing.SystemColors.Window;
            this.btncor3.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncor3.Location = new System.Drawing.Point(208, 35);
            this.btncor3.Name = "btncor3";
            this.btncor3.Size = new System.Drawing.Size(92, 27);
            this.btncor3.TabIndex = 2;
            this.btncor3.Text = "Azul";
            this.btncor3.UseVisualStyleBackColor = false;
            this.btncor3.Click += new System.EventHandler(this.btncor3_Click);
            // 
            // btncor2
            // 
            this.btncor2.BackColor = System.Drawing.SystemColors.Window;
            this.btncor2.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncor2.Location = new System.Drawing.Point(110, 35);
            this.btncor2.Name = "btncor2";
            this.btncor2.Size = new System.Drawing.Size(92, 27);
            this.btncor2.TabIndex = 3;
            this.btncor2.Text = "Vermelho";
            this.btncor2.UseVisualStyleBackColor = false;
            this.btncor2.Click += new System.EventHandler(this.btncor2_Click);
            // 
            // btncor4
            // 
            this.btncor4.BackColor = System.Drawing.SystemColors.Window;
            this.btncor4.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncor4.Location = new System.Drawing.Point(308, 35);
            this.btncor4.Name = "btncor4";
            this.btncor4.Size = new System.Drawing.Size(92, 27);
            this.btncor4.TabIndex = 4;
            this.btncor4.Text = "Verde";
            this.btncor4.UseVisualStyleBackColor = false;
            this.btncor4.Click += new System.EventHandler(this.btncor4_Click);
            // 
            // cor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 140);
            this.Controls.Add(this.btncor4);
            this.Controls.Add(this.btncor2);
            this.Controls.Add(this.btncor3);
            this.Controls.Add(this.btncor1);
            this.Controls.Add(this.lblcor);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "cor";
            this.Text = "Cor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblcor;
        private System.Windows.Forms.Button btncor1;
        private System.Windows.Forms.Button btncor3;
        private System.Windows.Forms.Button btncor2;
        private System.Windows.Forms.Button btncor4;
    }
}