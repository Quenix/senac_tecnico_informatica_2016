﻿namespace Janela
{
    partial class dados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(dados));
            this.lblcadastronome = new System.Windows.Forms.Label();
            this.lblcadastroidade = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblcadastronome
            // 
            this.lblcadastronome.AutoSize = true;
            this.lblcadastronome.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcadastronome.Location = new System.Drawing.Point(30, 47);
            this.lblcadastronome.Name = "lblcadastronome";
            this.lblcadastronome.Size = new System.Drawing.Size(0, 16);
            this.lblcadastronome.TabIndex = 0;
            this.lblcadastronome.Click += new System.EventHandler(this.lblcadastronome_Click);
            // 
            // lblcadastroidade
            // 
            this.lblcadastroidade.AutoSize = true;
            this.lblcadastroidade.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcadastroidade.Location = new System.Drawing.Point(30, 93);
            this.lblcadastroidade.Name = "lblcadastroidade";
            this.lblcadastroidade.Size = new System.Drawing.Size(0, 16);
            this.lblcadastroidade.TabIndex = 1;
            // 
            // dados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 162);
            this.Controls.Add(this.lblcadastroidade);
            this.Controls.Add(this.lblcadastronome);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "dados";
            this.Text = "Dados cadastrados";
            this.Load += new System.EventHandler(this.dados_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblcadastronome;
        private System.Windows.Forms.Label lblcadastroidade;
    }
}