﻿namespace Janela
{
    partial class frase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frase));
            this.txtfrase = new System.Windows.Forms.TextBox();
            this.lblfraseinverter = new System.Windows.Forms.Label();
            this.btninvert = new System.Windows.Forms.Button();
            this.txtfraseinvertida = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtfrase
            // 
            this.txtfrase.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfrase.Location = new System.Drawing.Point(68, 82);
            this.txtfrase.Name = "txtfrase";
            this.txtfrase.Size = new System.Drawing.Size(267, 23);
            this.txtfrase.TabIndex = 0;
            // 
            // lblfraseinverter
            // 
            this.lblfraseinverter.AutoSize = true;
            this.lblfraseinverter.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfraseinverter.Location = new System.Drawing.Point(120, 30);
            this.lblfraseinverter.Name = "lblfraseinverter";
            this.lblfraseinverter.Size = new System.Drawing.Size(148, 16);
            this.lblfraseinverter.TabIndex = 1;
            this.lblfraseinverter.Text = "Insira a frase abaixo:";
            // 
            // btninvert
            // 
            this.btninvert.BackColor = System.Drawing.SystemColors.Window;
            this.btninvert.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btninvert.Location = new System.Drawing.Point(157, 140);
            this.btninvert.Name = "btninvert";
            this.btninvert.Size = new System.Drawing.Size(75, 23);
            this.btninvert.TabIndex = 2;
            this.btninvert.Text = "Inverter";
            this.btninvert.UseVisualStyleBackColor = false;
            this.btninvert.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtfraseinvertida
            // 
            this.txtfraseinvertida.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfraseinvertida.Location = new System.Drawing.Point(68, 185);
            this.txtfraseinvertida.Name = "txtfraseinvertida";
            this.txtfraseinvertida.Size = new System.Drawing.Size(267, 23);
            this.txtfraseinvertida.TabIndex = 3;
            // 
            // frase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(390, 270);
            this.Controls.Add(this.txtfraseinvertida);
            this.Controls.Add(this.btninvert);
            this.Controls.Add(this.lblfraseinverter);
            this.Controls.Add(this.txtfrase);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frase";
            this.Text = "Frase inversa";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtfrase;
        private System.Windows.Forms.Label lblfraseinverter;
        private System.Windows.Forms.Button btninvert;
        private System.Windows.Forms.TextBox txtfraseinvertida;
    }
}