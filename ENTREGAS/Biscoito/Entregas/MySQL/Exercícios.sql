/* 1- selecione todos os resultados da tabela COUNTRY */

select * from country;

/*2- selecione os 10 primeiros resultados da tabela CITY */

select * from city limit 10;

/* 3- selecione todos os resultados da tabela ACTOR ordenando do último ao primeiro */

select * from actor order by actor_id desc;