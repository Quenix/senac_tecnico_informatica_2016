﻿namespace quizz
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pergunta1_resposta5 = new System.Windows.Forms.RadioButton();
            this.pergunta1_resposta4 = new System.Windows.Forms.RadioButton();
            this.pergunta1_resposta3 = new System.Windows.Forms.RadioButton();
            this.pergunta1_resposta2 = new System.Windows.Forms.RadioButton();
            this.lblPergunta1 = new System.Windows.Forms.Label();
            this.pergunta1_resposta1 = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pergunta2_resposta5 = new System.Windows.Forms.RadioButton();
            this.pergunta2_resposta4 = new System.Windows.Forms.RadioButton();
            this.pergunta2_resposta3 = new System.Windows.Forms.RadioButton();
            this.pergunta2_resposta2 = new System.Windows.Forms.RadioButton();
            this.lblPergunta2 = new System.Windows.Forms.Label();
            this.pergunta2_resposta1 = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pergunta3_resposta5 = new System.Windows.Forms.RadioButton();
            this.pergunta3_resposta4 = new System.Windows.Forms.RadioButton();
            this.pergunta3_resposta3 = new System.Windows.Forms.RadioButton();
            this.pergunta3_resposta2 = new System.Windows.Forms.RadioButton();
            this.lblPergunta3 = new System.Windows.Forms.Label();
            this.pergunta3_resposta1 = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.pergunta4_resposta5 = new System.Windows.Forms.RadioButton();
            this.pergunta4_resposta4 = new System.Windows.Forms.RadioButton();
            this.pergunta4_resposta3 = new System.Windows.Forms.RadioButton();
            this.pergunta4_resposta2 = new System.Windows.Forms.RadioButton();
            this.lblPergunta4 = new System.Windows.Forms.Label();
            this.pergunta4_resposta1 = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.pergunta5_resposta5 = new System.Windows.Forms.RadioButton();
            this.pergunta5_resposta4 = new System.Windows.Forms.RadioButton();
            this.pergunta5_resposta3 = new System.Windows.Forms.RadioButton();
            this.pergunta5_resposta2 = new System.Windows.Forms.RadioButton();
            this.lblPergunta5 = new System.Windows.Forms.Label();
            this.pergunta5_resposta1 = new System.Windows.Forms.RadioButton();
            this.btnFinal = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pergunta1_resposta5);
            this.groupBox1.Controls.Add(this.pergunta1_resposta4);
            this.groupBox1.Controls.Add(this.pergunta1_resposta3);
            this.groupBox1.Controls.Add(this.pergunta1_resposta2);
            this.groupBox1.Controls.Add(this.lblPergunta1);
            this.groupBox1.Controls.Add(this.pergunta1_resposta1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(467, 231);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pergunta1";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // pergunta1_resposta5
            // 
            this.pergunta1_resposta5.AutoSize = true;
            this.pergunta1_resposta5.Location = new System.Drawing.Point(6, 197);
            this.pergunta1_resposta5.Name = "pergunta1_resposta5";
            this.pergunta1_resposta5.Size = new System.Drawing.Size(85, 17);
            this.pergunta1_resposta5.TabIndex = 5;
            this.pergunta1_resposta5.TabStop = true;
            this.pergunta1_resposta5.Text = "radioButton1";
            this.pergunta1_resposta5.UseVisualStyleBackColor = true;
            this.pergunta1_resposta5.CheckedChanged += new System.EventHandler(this.pergunta1_resposta5_CheckedChanged);
            // 
            // pergunta1_resposta4
            // 
            this.pergunta1_resposta4.AutoSize = true;
            this.pergunta1_resposta4.Location = new System.Drawing.Point(6, 174);
            this.pergunta1_resposta4.Name = "pergunta1_resposta4";
            this.pergunta1_resposta4.Size = new System.Drawing.Size(85, 17);
            this.pergunta1_resposta4.TabIndex = 4;
            this.pergunta1_resposta4.TabStop = true;
            this.pergunta1_resposta4.Text = "radioButton1";
            this.pergunta1_resposta4.UseVisualStyleBackColor = true;
            this.pergunta1_resposta4.CheckedChanged += new System.EventHandler(this.pergunta1_resposta4_CheckedChanged);
            // 
            // pergunta1_resposta3
            // 
            this.pergunta1_resposta3.AutoSize = true;
            this.pergunta1_resposta3.Location = new System.Drawing.Point(6, 151);
            this.pergunta1_resposta3.Name = "pergunta1_resposta3";
            this.pergunta1_resposta3.Size = new System.Drawing.Size(85, 17);
            this.pergunta1_resposta3.TabIndex = 3;
            this.pergunta1_resposta3.TabStop = true;
            this.pergunta1_resposta3.Text = "radioButton1";
            this.pergunta1_resposta3.UseVisualStyleBackColor = true;
            this.pergunta1_resposta3.CheckedChanged += new System.EventHandler(this.pergunta1_resposta3_CheckedChanged);
            // 
            // pergunta1_resposta2
            // 
            this.pergunta1_resposta2.AutoSize = true;
            this.pergunta1_resposta2.Location = new System.Drawing.Point(6, 128);
            this.pergunta1_resposta2.Name = "pergunta1_resposta2";
            this.pergunta1_resposta2.Size = new System.Drawing.Size(85, 17);
            this.pergunta1_resposta2.TabIndex = 2;
            this.pergunta1_resposta2.TabStop = true;
            this.pergunta1_resposta2.Text = "radioButton1";
            this.pergunta1_resposta2.UseVisualStyleBackColor = true;
            this.pergunta1_resposta2.CheckedChanged += new System.EventHandler(this.pergunta1_resposta2_CheckedChanged);
            // 
            // lblPergunta1
            // 
            this.lblPergunta1.AutoSize = true;
            this.lblPergunta1.Location = new System.Drawing.Point(31, 27);
            this.lblPergunta1.Name = "lblPergunta1";
            this.lblPergunta1.Size = new System.Drawing.Size(35, 13);
            this.lblPergunta1.TabIndex = 1;
            this.lblPergunta1.Text = "label1";
            // 
            // pergunta1_resposta1
            // 
            this.pergunta1_resposta1.AutoSize = true;
            this.pergunta1_resposta1.Location = new System.Drawing.Point(6, 105);
            this.pergunta1_resposta1.Name = "pergunta1_resposta1";
            this.pergunta1_resposta1.Size = new System.Drawing.Size(85, 17);
            this.pergunta1_resposta1.TabIndex = 0;
            this.pergunta1_resposta1.TabStop = true;
            this.pergunta1_resposta1.Text = "radioButton1";
            this.pergunta1_resposta1.UseVisualStyleBackColor = true;
            this.pergunta1_resposta1.CheckedChanged += new System.EventHandler(this.pergunta1_resposta1_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pergunta2_resposta5);
            this.groupBox2.Controls.Add(this.pergunta2_resposta4);
            this.groupBox2.Controls.Add(this.pergunta2_resposta3);
            this.groupBox2.Controls.Add(this.pergunta2_resposta2);
            this.groupBox2.Controls.Add(this.lblPergunta2);
            this.groupBox2.Controls.Add(this.pergunta2_resposta1);
            this.groupBox2.Location = new System.Drawing.Point(12, 249);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(467, 231);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pergunta2";
            // 
            // pergunta2_resposta5
            // 
            this.pergunta2_resposta5.AutoSize = true;
            this.pergunta2_resposta5.Location = new System.Drawing.Point(6, 197);
            this.pergunta2_resposta5.Name = "pergunta2_resposta5";
            this.pergunta2_resposta5.Size = new System.Drawing.Size(85, 17);
            this.pergunta2_resposta5.TabIndex = 5;
            this.pergunta2_resposta5.TabStop = true;
            this.pergunta2_resposta5.Text = "radioButton2";
            this.pergunta2_resposta5.UseVisualStyleBackColor = true;
            this.pergunta2_resposta5.CheckedChanged += new System.EventHandler(this.pergunta2_resposta5_CheckedChanged);
            // 
            // pergunta2_resposta4
            // 
            this.pergunta2_resposta4.AutoSize = true;
            this.pergunta2_resposta4.Location = new System.Drawing.Point(6, 174);
            this.pergunta2_resposta4.Name = "pergunta2_resposta4";
            this.pergunta2_resposta4.Size = new System.Drawing.Size(85, 17);
            this.pergunta2_resposta4.TabIndex = 4;
            this.pergunta2_resposta4.TabStop = true;
            this.pergunta2_resposta4.Text = "radioButton2";
            this.pergunta2_resposta4.UseVisualStyleBackColor = true;
            this.pergunta2_resposta4.CheckedChanged += new System.EventHandler(this.pergunta2_resposta4_CheckedChanged);
            // 
            // pergunta2_resposta3
            // 
            this.pergunta2_resposta3.AutoSize = true;
            this.pergunta2_resposta3.Location = new System.Drawing.Point(6, 151);
            this.pergunta2_resposta3.Name = "pergunta2_resposta3";
            this.pergunta2_resposta3.Size = new System.Drawing.Size(85, 17);
            this.pergunta2_resposta3.TabIndex = 3;
            this.pergunta2_resposta3.TabStop = true;
            this.pergunta2_resposta3.Text = "radioButton2";
            this.pergunta2_resposta3.UseVisualStyleBackColor = true;
            this.pergunta2_resposta3.CheckedChanged += new System.EventHandler(this.pergunta2_resposta3_CheckedChanged);
            // 
            // pergunta2_resposta2
            // 
            this.pergunta2_resposta2.AutoSize = true;
            this.pergunta2_resposta2.Location = new System.Drawing.Point(6, 128);
            this.pergunta2_resposta2.Name = "pergunta2_resposta2";
            this.pergunta2_resposta2.Size = new System.Drawing.Size(85, 17);
            this.pergunta2_resposta2.TabIndex = 2;
            this.pergunta2_resposta2.TabStop = true;
            this.pergunta2_resposta2.Text = "radioButton2";
            this.pergunta2_resposta2.UseVisualStyleBackColor = true;
            this.pergunta2_resposta2.CheckedChanged += new System.EventHandler(this.pergunta2_resposta2_CheckedChanged);
            // 
            // lblPergunta2
            // 
            this.lblPergunta2.AutoSize = true;
            this.lblPergunta2.Location = new System.Drawing.Point(31, 27);
            this.lblPergunta2.Name = "lblPergunta2";
            this.lblPergunta2.Size = new System.Drawing.Size(35, 13);
            this.lblPergunta2.TabIndex = 1;
            this.lblPergunta2.Text = "label2";
            // 
            // pergunta2_resposta1
            // 
            this.pergunta2_resposta1.AutoSize = true;
            this.pergunta2_resposta1.Location = new System.Drawing.Point(6, 105);
            this.pergunta2_resposta1.Name = "pergunta2_resposta1";
            this.pergunta2_resposta1.Size = new System.Drawing.Size(85, 17);
            this.pergunta2_resposta1.TabIndex = 0;
            this.pergunta2_resposta1.TabStop = true;
            this.pergunta2_resposta1.Text = "radioButton2";
            this.pergunta2_resposta1.UseVisualStyleBackColor = true;
            this.pergunta2_resposta1.CheckedChanged += new System.EventHandler(this.pergunta2_resposta1_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.pergunta3_resposta5);
            this.groupBox3.Controls.Add(this.pergunta3_resposta4);
            this.groupBox3.Controls.Add(this.pergunta3_resposta3);
            this.groupBox3.Controls.Add(this.pergunta3_resposta2);
            this.groupBox3.Controls.Add(this.lblPergunta3);
            this.groupBox3.Controls.Add(this.pergunta3_resposta1);
            this.groupBox3.Location = new System.Drawing.Point(12, 500);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(467, 231);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Pergunta3";
            // 
            // pergunta3_resposta5
            // 
            this.pergunta3_resposta5.AutoSize = true;
            this.pergunta3_resposta5.Location = new System.Drawing.Point(6, 197);
            this.pergunta3_resposta5.Name = "pergunta3_resposta5";
            this.pergunta3_resposta5.Size = new System.Drawing.Size(85, 17);
            this.pergunta3_resposta5.TabIndex = 5;
            this.pergunta3_resposta5.TabStop = true;
            this.pergunta3_resposta5.Text = "radioButton3";
            this.pergunta3_resposta5.UseVisualStyleBackColor = true;
            this.pergunta3_resposta5.CheckedChanged += new System.EventHandler(this.pergunta3_resposta5_CheckedChanged);
            // 
            // pergunta3_resposta4
            // 
            this.pergunta3_resposta4.AutoSize = true;
            this.pergunta3_resposta4.Location = new System.Drawing.Point(6, 174);
            this.pergunta3_resposta4.Name = "pergunta3_resposta4";
            this.pergunta3_resposta4.Size = new System.Drawing.Size(85, 17);
            this.pergunta3_resposta4.TabIndex = 4;
            this.pergunta3_resposta4.TabStop = true;
            this.pergunta3_resposta4.Text = "radioButton3";
            this.pergunta3_resposta4.UseVisualStyleBackColor = true;
            this.pergunta3_resposta4.CheckedChanged += new System.EventHandler(this.pergunta3_resposta4_CheckedChanged);
            // 
            // pergunta3_resposta3
            // 
            this.pergunta3_resposta3.AutoSize = true;
            this.pergunta3_resposta3.Location = new System.Drawing.Point(6, 151);
            this.pergunta3_resposta3.Name = "pergunta3_resposta3";
            this.pergunta3_resposta3.Size = new System.Drawing.Size(85, 17);
            this.pergunta3_resposta3.TabIndex = 3;
            this.pergunta3_resposta3.TabStop = true;
            this.pergunta3_resposta3.Text = "radioButton3";
            this.pergunta3_resposta3.UseVisualStyleBackColor = true;
            this.pergunta3_resposta3.CheckedChanged += new System.EventHandler(this.pergunta3_resposta3_CheckedChanged);
            // 
            // pergunta3_resposta2
            // 
            this.pergunta3_resposta2.AutoSize = true;
            this.pergunta3_resposta2.Location = new System.Drawing.Point(6, 128);
            this.pergunta3_resposta2.Name = "pergunta3_resposta2";
            this.pergunta3_resposta2.Size = new System.Drawing.Size(85, 17);
            this.pergunta3_resposta2.TabIndex = 2;
            this.pergunta3_resposta2.TabStop = true;
            this.pergunta3_resposta2.Text = "radioButton3";
            this.pergunta3_resposta2.UseVisualStyleBackColor = true;
            this.pergunta3_resposta2.CheckedChanged += new System.EventHandler(this.pergunta3_resposta2_CheckedChanged);
            // 
            // lblPergunta3
            // 
            this.lblPergunta3.AutoSize = true;
            this.lblPergunta3.Location = new System.Drawing.Point(31, 27);
            this.lblPergunta3.Name = "lblPergunta3";
            this.lblPergunta3.Size = new System.Drawing.Size(35, 13);
            this.lblPergunta3.TabIndex = 1;
            this.lblPergunta3.Text = "label3";
            // 
            // pergunta3_resposta1
            // 
            this.pergunta3_resposta1.AutoSize = true;
            this.pergunta3_resposta1.Location = new System.Drawing.Point(6, 105);
            this.pergunta3_resposta1.Name = "pergunta3_resposta1";
            this.pergunta3_resposta1.Size = new System.Drawing.Size(85, 17);
            this.pergunta3_resposta1.TabIndex = 0;
            this.pergunta3_resposta1.TabStop = true;
            this.pergunta3_resposta1.Text = "radioButton3";
            this.pergunta3_resposta1.UseVisualStyleBackColor = true;
            this.pergunta3_resposta1.CheckedChanged += new System.EventHandler(this.pergunta3_resposta1_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.pergunta4_resposta5);
            this.groupBox4.Controls.Add(this.pergunta4_resposta4);
            this.groupBox4.Controls.Add(this.pergunta4_resposta3);
            this.groupBox4.Controls.Add(this.pergunta4_resposta2);
            this.groupBox4.Controls.Add(this.lblPergunta4);
            this.groupBox4.Controls.Add(this.pergunta4_resposta1);
            this.groupBox4.Location = new System.Drawing.Point(12, 748);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(467, 231);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Pergunta4";
            // 
            // pergunta4_resposta5
            // 
            this.pergunta4_resposta5.AutoSize = true;
            this.pergunta4_resposta5.Location = new System.Drawing.Point(6, 197);
            this.pergunta4_resposta5.Name = "pergunta4_resposta5";
            this.pergunta4_resposta5.Size = new System.Drawing.Size(85, 17);
            this.pergunta4_resposta5.TabIndex = 5;
            this.pergunta4_resposta5.TabStop = true;
            this.pergunta4_resposta5.Text = "radioButton4";
            this.pergunta4_resposta5.UseVisualStyleBackColor = true;
            this.pergunta4_resposta5.CheckedChanged += new System.EventHandler(this.pergunta4_resposta5_CheckedChanged);
            // 
            // pergunta4_resposta4
            // 
            this.pergunta4_resposta4.AutoSize = true;
            this.pergunta4_resposta4.Location = new System.Drawing.Point(6, 174);
            this.pergunta4_resposta4.Name = "pergunta4_resposta4";
            this.pergunta4_resposta4.Size = new System.Drawing.Size(85, 17);
            this.pergunta4_resposta4.TabIndex = 4;
            this.pergunta4_resposta4.TabStop = true;
            this.pergunta4_resposta4.Text = "radioButton4";
            this.pergunta4_resposta4.UseVisualStyleBackColor = true;
            this.pergunta4_resposta4.CheckedChanged += new System.EventHandler(this.pergunta4_resposta4_CheckedChanged);
            // 
            // pergunta4_resposta3
            // 
            this.pergunta4_resposta3.AutoSize = true;
            this.pergunta4_resposta3.Location = new System.Drawing.Point(6, 151);
            this.pergunta4_resposta3.Name = "pergunta4_resposta3";
            this.pergunta4_resposta3.Size = new System.Drawing.Size(85, 17);
            this.pergunta4_resposta3.TabIndex = 3;
            this.pergunta4_resposta3.TabStop = true;
            this.pergunta4_resposta3.Text = "radioButton4";
            this.pergunta4_resposta3.UseVisualStyleBackColor = true;
            this.pergunta4_resposta3.CheckedChanged += new System.EventHandler(this.radioButton8_CheckedChanged);
            // 
            // pergunta4_resposta2
            // 
            this.pergunta4_resposta2.AutoSize = true;
            this.pergunta4_resposta2.Location = new System.Drawing.Point(6, 128);
            this.pergunta4_resposta2.Name = "pergunta4_resposta2";
            this.pergunta4_resposta2.Size = new System.Drawing.Size(85, 17);
            this.pergunta4_resposta2.TabIndex = 2;
            this.pergunta4_resposta2.TabStop = true;
            this.pergunta4_resposta2.Text = "radioButton4";
            this.pergunta4_resposta2.UseVisualStyleBackColor = true;
            this.pergunta4_resposta2.CheckedChanged += new System.EventHandler(this.pergunta4_resposta2_CheckedChanged);
            // 
            // lblPergunta4
            // 
            this.lblPergunta4.AutoSize = true;
            this.lblPergunta4.Location = new System.Drawing.Point(31, 27);
            this.lblPergunta4.Name = "lblPergunta4";
            this.lblPergunta4.Size = new System.Drawing.Size(35, 13);
            this.lblPergunta4.TabIndex = 1;
            this.lblPergunta4.Text = "label4";
            // 
            // pergunta4_resposta1
            // 
            this.pergunta4_resposta1.AutoSize = true;
            this.pergunta4_resposta1.Location = new System.Drawing.Point(6, 105);
            this.pergunta4_resposta1.Name = "pergunta4_resposta1";
            this.pergunta4_resposta1.Size = new System.Drawing.Size(85, 17);
            this.pergunta4_resposta1.TabIndex = 0;
            this.pergunta4_resposta1.TabStop = true;
            this.pergunta4_resposta1.Text = "radioButton4";
            this.pergunta4_resposta1.UseVisualStyleBackColor = true;
            this.pergunta4_resposta1.CheckedChanged += new System.EventHandler(this.pergunta4_resposta1_CheckedChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.pergunta5_resposta5);
            this.groupBox5.Controls.Add(this.pergunta5_resposta4);
            this.groupBox5.Controls.Add(this.pergunta5_resposta3);
            this.groupBox5.Controls.Add(this.pergunta5_resposta2);
            this.groupBox5.Controls.Add(this.lblPergunta5);
            this.groupBox5.Controls.Add(this.pergunta5_resposta1);
            this.groupBox5.Location = new System.Drawing.Point(12, 985);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(467, 231);
            this.groupBox5.TabIndex = 9;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Pergunta5";
            // 
            // pergunta5_resposta5
            // 
            this.pergunta5_resposta5.AutoSize = true;
            this.pergunta5_resposta5.Location = new System.Drawing.Point(6, 197);
            this.pergunta5_resposta5.Name = "pergunta5_resposta5";
            this.pergunta5_resposta5.Size = new System.Drawing.Size(85, 17);
            this.pergunta5_resposta5.TabIndex = 5;
            this.pergunta5_resposta5.TabStop = true;
            this.pergunta5_resposta5.Text = "radioButton5";
            this.pergunta5_resposta5.UseVisualStyleBackColor = true;
            this.pergunta5_resposta5.CheckedChanged += new System.EventHandler(this.pergunta5_resposta5_CheckedChanged);
            // 
            // pergunta5_resposta4
            // 
            this.pergunta5_resposta4.AutoSize = true;
            this.pergunta5_resposta4.Location = new System.Drawing.Point(6, 174);
            this.pergunta5_resposta4.Name = "pergunta5_resposta4";
            this.pergunta5_resposta4.Size = new System.Drawing.Size(85, 17);
            this.pergunta5_resposta4.TabIndex = 4;
            this.pergunta5_resposta4.TabStop = true;
            this.pergunta5_resposta4.Text = "radioButton5";
            this.pergunta5_resposta4.UseVisualStyleBackColor = true;
            this.pergunta5_resposta4.CheckedChanged += new System.EventHandler(this.pergunta5_resposta4_CheckedChanged);
            // 
            // pergunta5_resposta3
            // 
            this.pergunta5_resposta3.AutoSize = true;
            this.pergunta5_resposta3.Location = new System.Drawing.Point(6, 151);
            this.pergunta5_resposta3.Name = "pergunta5_resposta3";
            this.pergunta5_resposta3.Size = new System.Drawing.Size(85, 17);
            this.pergunta5_resposta3.TabIndex = 3;
            this.pergunta5_resposta3.TabStop = true;
            this.pergunta5_resposta3.Text = "radioButton5";
            this.pergunta5_resposta3.UseVisualStyleBackColor = true;
            this.pergunta5_resposta3.CheckedChanged += new System.EventHandler(this.pergunta5_resposta3_CheckedChanged);
            // 
            // pergunta5_resposta2
            // 
            this.pergunta5_resposta2.AutoSize = true;
            this.pergunta5_resposta2.Location = new System.Drawing.Point(6, 128);
            this.pergunta5_resposta2.Name = "pergunta5_resposta2";
            this.pergunta5_resposta2.Size = new System.Drawing.Size(85, 17);
            this.pergunta5_resposta2.TabIndex = 2;
            this.pergunta5_resposta2.TabStop = true;
            this.pergunta5_resposta2.Text = "radioButton5";
            this.pergunta5_resposta2.UseVisualStyleBackColor = true;
            this.pergunta5_resposta2.CheckedChanged += new System.EventHandler(this.pergunta5_resposta2_CheckedChanged);
            // 
            // lblPergunta5
            // 
            this.lblPergunta5.AutoSize = true;
            this.lblPergunta5.Location = new System.Drawing.Point(31, 27);
            this.lblPergunta5.Name = "lblPergunta5";
            this.lblPergunta5.Size = new System.Drawing.Size(35, 13);
            this.lblPergunta5.TabIndex = 1;
            this.lblPergunta5.Text = "label5";
            // 
            // pergunta5_resposta1
            // 
            this.pergunta5_resposta1.AutoSize = true;
            this.pergunta5_resposta1.Location = new System.Drawing.Point(6, 105);
            this.pergunta5_resposta1.Name = "pergunta5_resposta1";
            this.pergunta5_resposta1.Size = new System.Drawing.Size(85, 17);
            this.pergunta5_resposta1.TabIndex = 0;
            this.pergunta5_resposta1.TabStop = true;
            this.pergunta5_resposta1.Text = "radioButton5";
            this.pergunta5_resposta1.UseVisualStyleBackColor = true;
            this.pergunta5_resposta1.CheckedChanged += new System.EventHandler(this.pergunta5_resposta1_CheckedChanged);
            // 
            // btnFinal
            // 
            this.btnFinal.Location = new System.Drawing.Point(206, 1231);
            this.btnFinal.Name = "btnFinal";
            this.btnFinal.Size = new System.Drawing.Size(104, 40);
            this.btnFinal.TabIndex = 10;
            this.btnFinal.Text = "Finalizar";
            this.btnFinal.UseVisualStyleBackColor = true;
            this.btnFinal.Click += new System.EventHandler(this.btnFinal_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMargin = new System.Drawing.Size(0, 80);
            this.ClientSize = new System.Drawing.Size(531, 495);
            this.Controls.Add(this.btnFinal);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton pergunta1_resposta4;
        private System.Windows.Forms.RadioButton pergunta1_resposta3;
        private System.Windows.Forms.RadioButton pergunta1_resposta2;
        private System.Windows.Forms.Label lblPergunta1;
        private System.Windows.Forms.RadioButton pergunta1_resposta1;
        private System.Windows.Forms.RadioButton pergunta1_resposta5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton pergunta2_resposta5;
        private System.Windows.Forms.RadioButton pergunta2_resposta4;
        private System.Windows.Forms.RadioButton pergunta2_resposta3;
        private System.Windows.Forms.RadioButton pergunta2_resposta2;
        private System.Windows.Forms.Label lblPergunta2;
        private System.Windows.Forms.RadioButton pergunta2_resposta1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton pergunta3_resposta5;
        private System.Windows.Forms.RadioButton pergunta3_resposta4;
        private System.Windows.Forms.RadioButton pergunta3_resposta3;
        private System.Windows.Forms.RadioButton pergunta3_resposta2;
        private System.Windows.Forms.Label lblPergunta3;
        private System.Windows.Forms.RadioButton pergunta3_resposta1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton pergunta4_resposta5;
        private System.Windows.Forms.RadioButton pergunta4_resposta4;
        private System.Windows.Forms.RadioButton pergunta4_resposta3;
        private System.Windows.Forms.RadioButton pergunta4_resposta2;
        private System.Windows.Forms.Label lblPergunta4;
        private System.Windows.Forms.RadioButton pergunta4_resposta1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton pergunta5_resposta5;
        private System.Windows.Forms.RadioButton pergunta5_resposta4;
        private System.Windows.Forms.RadioButton pergunta5_resposta3;
        private System.Windows.Forms.RadioButton pergunta5_resposta2;
        private System.Windows.Forms.Label lblPergunta5;
        private System.Windows.Forms.RadioButton pergunta5_resposta1;
        private System.Windows.Forms.Button btnFinal;
    }
}

