﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace quizz
{
    public partial class Form1 : Form
    {
        int p1, p2, p3, p4, p5, resultado;

        private void pergunta1_resposta1_CheckedChanged(object sender, EventArgs e)
        {
            p1 = 1;
        }

        private void pergunta1_resposta2_CheckedChanged(object sender, EventArgs e)
        {
            p1 = 2;
        }

        private void pergunta1_resposta3_CheckedChanged(object sender, EventArgs e)
        {
            p1 = 3;
        }

        private void pergunta1_resposta4_CheckedChanged(object sender, EventArgs e)
        {
            p1 = 4;
        }

        private void pergunta1_resposta5_CheckedChanged(object sender, EventArgs e)
        {
            p1 = 5;
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void pergunta2_resposta1_CheckedChanged(object sender, EventArgs e)
        {
            p2 = 1;
        }

        private void pergunta2_resposta2_CheckedChanged(object sender, EventArgs e)
        {
            p2 = 2;
        }

        private void pergunta2_resposta3_CheckedChanged(object sender, EventArgs e)
        {
            p2 = 3;
        }

        private void pergunta2_resposta4_CheckedChanged(object sender, EventArgs e)
        {
            p2 = 4;
        }

        private void pergunta2_resposta5_CheckedChanged(object sender, EventArgs e)
        {
            p2 = 5;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void pergunta3_resposta1_CheckedChanged(object sender, EventArgs e)
        {
            p3 = 1;
        }

        private void pergunta3_resposta2_CheckedChanged(object sender, EventArgs e)
        {
            p3 = 2;
        }

        private void pergunta3_resposta3_CheckedChanged(object sender, EventArgs e)
        {
            p3 = 3;
        }

        private void pergunta3_resposta4_CheckedChanged(object sender, EventArgs e)
        {
            p3 = 4;
        }

        private void pergunta3_resposta5_CheckedChanged(object sender, EventArgs e)
        {
            p3 = 5;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lblPergunta1.Text = "Qual a chance de, ao rolar um d20, o resultado obtido ser par?";
            pergunta1_resposta1.Text = "1/20";
            pergunta1_resposta2.Text = "1/10";
            pergunta1_resposta3.Text = "1/2"; //resposta certa
            pergunta1_resposta4.Text = "2/5";
            pergunta1_resposta5.Text = "2/15";

            lblPergunta2.Text = "Qual a chance de, ao rolar um d20, o resultado ser 15?";
            pergunta2_resposta1.Text = "15/20";
            pergunta2_resposta2.Text = "1/20"; //resposta certa
            pergunta2_resposta3.Text = "1/8";
            pergunta2_resposta4.Text = "15/18";
            pergunta2_resposta5.Text = "15/19";

            lblPergunta3.Text = "Uma sala quadrada possui um gato em cada canto,\n cada gato vê três gatos, quantos gatos há naquela sala?";
            pergunta3_resposta1.Text = "12";
            pergunta3_resposta2.Text = "5";
            pergunta3_resposta3.Text = "3";
            pergunta3_resposta4.Text = "4"; //resposta certa
            pergunta3_resposta5.Text = "Prefiro cachorros";

            lblPergunta4.Text = "O resultado de 3^2+2^2-(10x3-23) é:";
            pergunta4_resposta1.Text = "6!";
            pergunta4_resposta2.Text = "3!";//resposta correta
            pergunta4_resposta3.Text = "7!";
            pergunta4_resposta4.Text = "2!";
            pergunta4_resposta5.Text = "4!";

            lblPergunta5.Text = "Qual a chance de, ao rolar um d100, o resultado ser 15 e em seguida 40?";
            pergunta5_resposta1.Text = "1/2";
            pergunta5_resposta2.Text = "10/9";
            pergunta5_resposta3.Text = "2/3";
            pergunta5_resposta4.Text = "2/5";
            pergunta5_resposta5.Text = "1/4";//resposta certa
        }

        private void radioButton8_CheckedChanged(object sender, EventArgs e)
        {
            p4 = 3;
        }

        private void btnFinal_Click(object sender, EventArgs e)
        {
            resultado = 0;

            switch (p1)
            {
                case 1:
                    limparPergunta1();
                    pergunta1_resposta1.ForeColor = Color.Red;
                    break;
                case 2:
                    limparPergunta1();
                    pergunta1_resposta2.ForeColor = Color.Red;
                    break;

                case 3:
                    limparPergunta1();
                    resultado += 10;
                    pergunta1_resposta3.ForeColor = Color.Green;
                    break;
                case 4:
                    limparPergunta1();
                    pergunta1_resposta4.ForeColor = Color.Red;
                    break;
                case 5:
                    limparPergunta1();
                    pergunta1_resposta5.ForeColor = Color.Red;
                    break; 
            }

            switch (p2)
            {
                case 1:
                    limparPergunta2();
                    pergunta2_resposta1.ForeColor = Color.Red;
                    break;
                case 2:
                    limparPergunta2();
                    resultado += 10;
                    pergunta2_resposta2.ForeColor = Color.Green;
                    break;

                case 3:
                    limparPergunta2();
                    pergunta2_resposta3.ForeColor = Color.Red;
                    break;
                case 4:
                    limparPergunta2();
                    pergunta2_resposta4.ForeColor = Color.Red;
                    break;
                case 5:
                    limparPergunta2();
                    pergunta2_resposta5.ForeColor = Color.Red;
                    break;
            }

            switch (p3)
            {
                case 1:
                    limparPergunta3();
                    pergunta3_resposta1.ForeColor = Color.Red;
                    break;
                case 2:
                    limparPergunta3();
                    pergunta3_resposta2.ForeColor = Color.Red;
                    break;

                case 3:
                    limparPergunta3();
                    pergunta3_resposta3.ForeColor = Color.Red;
                    break;
                case 4:
                    limparPergunta3();
                    resultado += 10;
                    pergunta3_resposta4.ForeColor = Color.Green;
                    break;
                case 5:
                    limparPergunta3();
                    resultado += 8000;
                    pergunta3_resposta5.ForeColor = Color.Gold;
                    break;
            }

            switch (p4)
            {
                case 1:
                    limparPergunta4();
                    pergunta4_resposta1.ForeColor = Color.Red;
                    break;
                case 2:
                    limparPergunta4();
                    resultado += 10;
                    pergunta4_resposta2.ForeColor = Color.Green;
                    break;

                case 3:
                    limparPergunta4();
                    pergunta4_resposta3.ForeColor = Color.Red;
                    break;
                case 4:
                    limparPergunta4();
                    pergunta4_resposta4.ForeColor = Color.Red;
                    break;
                case 5:
                    limparPergunta4();
                    pergunta4_resposta5.ForeColor = Color.Red;
                    break;
            }

            switch (p5)
            {
                case 1:
                    limparPergunta5();
                    pergunta5_resposta1.ForeColor = Color.Red;
                    break;
                case 2:
                    limparPergunta5();                    
                    pergunta5_resposta2.ForeColor = Color.Red;
                    break;

                case 3:
                    limparPergunta5();
                    pergunta5_resposta3.ForeColor = Color.Red;
                    break;
                case 4:
                    limparPergunta5();
                    pergunta5_resposta4.ForeColor = Color.Red;
                    break;
                case 5:
                    limparPergunta5();
                    resultado += 10;
                    pergunta5_resposta5.ForeColor = Color.Green;
                    break;
            }



            MessageBox.Show("Você fez " + resultado + " pontos");


        }

        private void pergunta4_resposta1_CheckedChanged(object sender, EventArgs e)
        {
            p4 = 1;
        }

        private void pergunta4_resposta2_CheckedChanged(object sender, EventArgs e)
        {
            p4 = 2;
        }

        private void pergunta4_resposta4_CheckedChanged(object sender, EventArgs e)
        {
            p4 = 4;
        }

        private void pergunta4_resposta5_CheckedChanged(object sender, EventArgs e)
        {
            p4 = 5;
        }

        private void pergunta5_resposta1_CheckedChanged(object sender, EventArgs e)
        {
            p5 = 1;
        }

        private void pergunta5_resposta2_CheckedChanged(object sender, EventArgs e)
        {
            p5 = 2;
        }

        private void pergunta5_resposta3_CheckedChanged(object sender, EventArgs e)
        {
            p5 = 3;
        }

        private void pergunta5_resposta4_CheckedChanged(object sender, EventArgs e)
        {
            p5 = 4;
        }

        private void pergunta5_resposta5_CheckedChanged(object sender, EventArgs e)
        {
            p5 = 5;
        }

        public void limparPergunta1()
        {
            pergunta1_resposta1.ForeColor = Color.Black;
            pergunta1_resposta2.ForeColor = Color.Black;
            pergunta1_resposta3.ForeColor = Color.Black;
            pergunta1_resposta4.ForeColor = Color.Black;
            pergunta1_resposta5.ForeColor = Color.Black;
        }

        public void limparPergunta2()
        {
            pergunta2_resposta1.ForeColor = Color.Black;
            pergunta2_resposta2.ForeColor = Color.Black;
            pergunta2_resposta3.ForeColor = Color.Black;
            pergunta2_resposta4.ForeColor = Color.Black;
            pergunta2_resposta5.ForeColor = Color.Black;
        }

        public void limparPergunta3()
        {
            pergunta3_resposta1.ForeColor = Color.Black;
            pergunta3_resposta2.ForeColor = Color.Black;
            pergunta3_resposta3.ForeColor = Color.Black;
            pergunta3_resposta4.ForeColor = Color.Black;
            pergunta3_resposta5.ForeColor = Color.Black;
        }

        public void limparPergunta4()
        {
            pergunta4_resposta1.ForeColor = Color.Black;
            pergunta4_resposta2.ForeColor = Color.Black;
            pergunta4_resposta3.ForeColor = Color.Black;
            pergunta4_resposta4.ForeColor = Color.Black;
            pergunta4_resposta5.ForeColor = Color.Black;
        }

        public void limparPergunta5()
        {
            pergunta5_resposta1.ForeColor = Color.Black;
            pergunta5_resposta2.ForeColor = Color.Black;
            pergunta5_resposta3.ForeColor = Color.Black;
            pergunta5_resposta4.ForeColor = Color.Black;
            pergunta5_resposta5.ForeColor = Color.Black;
        }
    }
}
