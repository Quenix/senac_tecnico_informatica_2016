﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnCalc_Click(object sender, EventArgs e)
        {
            double peso, altura, resultado;

            peso = Convert.ToDouble(txtPeso.Text);
            altura = Convert.ToDouble(txtAltura.Text);

            resultado = peso / Math.Pow(altura, 2);

            lblIMC.Text = ("O resultado é: " + Math.Round(resultado, 2));

            if (resultado < 17)
            {
                lblMess.Text = ("Você está muito abaixo do peso");
            }
            else
            {
                if (resultado < 18.5)
                {
                    lblMess.Text = ("Você está abaixo do peso");
                }
                else
                {
                    if (resultado < 25)
                    {
                        lblMess.Text = ("Você está no peso ideal");
                    }
                    else
                    {
                        if(resultado < 30)
                        {
                            lblMess.Text = ("Você está acima do peso");
                        }
                        else
                        {
                            if(resultado< 35)
                            {
                                lblMess.Text = ("Você está com obesidade de Grau I");
                            }
                            else
                            {
                                if(resultado < 41)
                                {
                                    lblMess.Text = ("Você está com obesidade de Grau II");
                                }
                                else
                                {
                                    lblMess.Text = ("Você está com obesidade de Grau III\n ou obesidade mórbida");
                                    
                                }
                            }
                        }
                    }
                }

            }



        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
