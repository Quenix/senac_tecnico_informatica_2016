﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Toub.Sound.Midi;

namespace RoletaRussa
{
    public partial class Form1 : Form
    {
        int game;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            game = random.Next(1, 11);
            switch (game)
            {
                case 1:
                    MidiPlayer.Play(@"C:\Users\vinicius.momoraes\Downloads\IronMaiden-TheTrooper.mp3");
                    break;

                case 2:
                    MidiPlayer.Play(@"C:\Users\vinicius.momoraes\Downloads\SAW-JogosMortaisTrilhaSonora.mp3");
                    break;

                case 3:
                    MidiPlayer.Play(@"C:\Users\vinicius.momoraes\Downloads\gemidaodozap.mp3");
                    break;

                case 4:
                    MidiPlayer.Play(@"C:\Users\vinicius.momoraes\Downloads\SABATON-Sparta.mp3");
                    break;

                case 5:
                    MidiPlayer.Play(@"C:\Users\vinicius.momoraes\Downloads\Ghost-SquareHammer.mp3");
                    break;

                case 6:
                    MidiPlayer.Play(@"C:\Users\vinicius.momoraes\Downloads\Trivium-Strife.mp3");
                    break;

                case 7:
                    MidiPlayer.Play(@"C:\Users\vinicius.momoraes\Downloads\Trivium-SilenceInTheSnow.mp3");
                    break;

                case 8:
                    MidiPlayer.Play(@"C:\Users\vinicius.momoraes\Downloads\gemidaodozap.mp3");
                    break;

                case 9:
                    MidiPlayer.Play(@"C:\Users\vinicius.momoraes\Downloads\SaidesgraçaEuquerodormir.mp3");
                    break;

                case 10:
                    MidiPlayer.Play(@"C:\Users\vinicius.momoraes\Downloads\Ai.mp3");
                    break;

            }
        }
    }
}
