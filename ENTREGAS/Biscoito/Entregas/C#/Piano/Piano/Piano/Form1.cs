﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Toub.Sound.Midi;

namespace Piano
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            MidiPlayer.OpenMidi();
        }

        private void btnKeyC_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "C4", 127));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "D4", 127));
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "E4", 127));
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "F4", 127));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "G4", 127));
        }

        private void button5_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "A4", 127));
        }

        private void button6_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "B4", 127));
        }

        private void button7_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(@"C:\Users\vinicius.momoraes\Downloads\IronMaiden-TheTrooper.mp3");

        }

        private void button8_Click(object sender, EventArgs e)
        {
            MidiPlayer.Play(new NoteOn(0, 1, "C4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "D4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "E4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "F4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "F4", 127));
            System.Threading.Thread.Sleep(250);

            MidiPlayer.Play(new NoteOn(0, 1, "F4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "C4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "D4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "C4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "D4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "D4", 127));
            System.Threading.Thread.Sleep(250);

            MidiPlayer.Play(new NoteOn(0, 1, "D4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "C4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "G4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "F4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "E4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "E4", 127));
            System.Threading.Thread.Sleep(250);

            MidiPlayer.Play(new NoteOn(0, 1, "E4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "C4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "D4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "E4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "F4", 127));
            System.Threading.Thread.Sleep(500);

            MidiPlayer.Play(new NoteOn(0, 1, "F4", 127));
            System.Threading.Thread.Sleep(250);

            MidiPlayer.Play(new NoteOn(0, 1, "F4", 127));
            System.Threading.Thread.Sleep(500);
        }
    }
}
