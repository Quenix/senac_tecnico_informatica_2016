﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Multi_janela
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
             
        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void novoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var janela = new Form2();
            janela.ShowDialog();
        }

        private void labelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var janela = new Form3();
            janela.ShowDialog();
        }

        private void inverterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var janela = new Form4();
            janela.ShowDialog();
        }
    }
}
