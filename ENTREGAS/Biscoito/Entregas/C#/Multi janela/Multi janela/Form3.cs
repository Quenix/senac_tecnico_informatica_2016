﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Multi_janela
{
    public partial class Form3 : Form
    {   
        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lblMarilene.ForeColor = Color.Red;
        }

        private void btnBlue_Click(object sender, EventArgs e)
        {
            lblMarilene.ForeColor = Color.Blue;
        }

        private void btnGreen_Click(object sender, EventArgs e)
        {
            lblMarilene.ForeColor = Color.Green;
        }

        private void btnOrange_Click(object sender, EventArgs e)
        {
            lblMarilene.ForeColor = Color.Orange;
        }

        private void btnBlack_Click(object sender, EventArgs e)
        {
            lblMarilene.ForeColor = Color.Black;
        }
    }
}
