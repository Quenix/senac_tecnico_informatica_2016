﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Multi_janela
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void btnInverter_Click(object sender, EventArgs e)
        {

            char[] arrChar = txtText.Text.ToCharArray();
            Array.Reverse(arrChar);
            string invertida = new String(arrChar);

            txtText.Text = invertida;    

        }

        private void Form4_Load(object sender, EventArgs e)
        {

        }
    }
}
