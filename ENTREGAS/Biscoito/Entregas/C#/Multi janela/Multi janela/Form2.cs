﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Multi_janela
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void btnVerify_Click(object sender, EventArgs e)
        {
            string nome;
            int idade;
            try
            {


                nome = txtName.Text;
                idade = Convert.ToInt32(txtAge.Text);

                if (idade >= 18)
                {
                    MessageBox.Show(nome + " tem " + idade + " anos e é maior de idade");
                }
                else
                {
                    MessageBox.Show(nome + " tem " + idade + " anos e não é maior de idade");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Apenas valores válidos");
            }
        }
    }
}
