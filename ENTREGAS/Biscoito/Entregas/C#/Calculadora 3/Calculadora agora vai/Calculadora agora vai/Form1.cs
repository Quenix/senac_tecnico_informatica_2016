﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora_agora_vai
{
    public partial class Form1 : Form
    {
        //Var
        string opr;
        double valor1, valor2;

        public Form1()
        {
            InitializeComponent();
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            txtN1.Text += btn1.Text;

        }

        private void txt1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnSoma_Click(object sender, EventArgs e)
        {
            opr = "1";
            txtOpr.Text = btnSoma.Text;
        }

        private void btnSub_Click(object sender, EventArgs e)
        {
            opr = "2";
            txtOpr.Text = btnSub.Text;
        }

        private void btnMult_Click(object sender, EventArgs e)
        {
            opr = "3";
            txtOpr.Text = btnMult.Text;
        }

        private void btnDiv_Click(object sender, EventArgs e)
        {    
            opr = "4";
                txtOpr.Text = btnDiv.Text;
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            txtN1.Text += btn3.Text;
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            txtN1.Text += btn4.Text;
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            txtN1.Text += btn5.Text;
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            txtN1.Text += btn6.Text;
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            txtN1.Text += btn7.Text;
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            txtN1.Text += btn8.Text;
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            txtN1.Text += btn9.Text;
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            txtN1.Text += btn0.Text;
        }

        private void btnIg_Click(object sender, EventArgs e)
        {


            if (txtN1.Text == "")
            {
                MessageBox.Show("Primeiro adicione um número ao valor 1.");
            }
            else
            {
                if (txtOpr.Text == "")
                {
                    MessageBox.Show("Escolha uma operação.");
                }
                else
                {


                    if (txtN2.Text == "")
                    {
                        MessageBox.Show("Primeiro adicione um número ao valor 2.");
                    }
                    else
                    {
                        try
                        {
                            valor1 = Convert.ToDouble(txtN1.Text);
                            valor2 = Convert.ToDouble(txtN2.Text);
                            switch (opr)
                            {

                                case "1":
                                    MessageBox.Show(valor1 + " + " + valor2 + " = " + (valor1 + valor2));
                                    break;

                                case "2":
                                    MessageBox.Show(valor1 + " - " + valor2 + " = " + (valor1 - valor2));
                                    break;

                                case "3":
                                    MessageBox.Show(valor1 + " x " + valor2 + " = " + (valor1 * valor2));
                                    break;

                                case "4":
                                    if (valor2 != 0)
                                    {

                                        MessageBox.Show(valor1 + " / " + valor2 + " = " + (valor1 / valor2));

                                    }
                                    else
                                    {
                                        MessageBox.Show("Porra aspira, dividir por zero não!");

                                    }
                                    break;
                            }
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Apenas uma vírgula por valor!");
                        }
                    }


            }   }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn1N2_Click(object sender, EventArgs e)
        {
            txtN2.Text += btn1N2.Text;
        }

        private void btn2N2_Click(object sender, EventArgs e)
        {
            txtN2.Text += btn2N2.Text;
        }

        private void btn3N2_Click(object sender, EventArgs e)
        {
            txtN2.Text += btn3N2.Text;
        }

        private void btn4N2_Click(object sender, EventArgs e)
        {
            txtN2.Text += btn4N2.Text;
        }

        private void btn5N2_Click(object sender, EventArgs e)
        {
            txtN2.Text += btn5N2.Text;
        }

        private void btn6N2_Click(object sender, EventArgs e)
        {
            txtN2.Text += btn6N2.Text;
        }

        private void btn7N2_Click(object sender, EventArgs e)
        {
            txtN2.Text += btn7N2.Text;
        }

        private void btn8N2_Click(object sender, EventArgs e)
        {
            txtN2.Text += btn8N2.Text;
        }

        private void btn9N2_Click(object sender, EventArgs e)
        {
            txtN2.Text += btn9N2.Text;
        }

        private void btn0N2_Click(object sender, EventArgs e)
        {
            txtN2.Text += btn0N2.Text;
        }

        private void btnVirg_Click(object sender, EventArgs e)
        {
            txtN1.Text += btnVirg.Text;
        }

        private void btnVirgN2_Click(object sender, EventArgs e)
        {
            txtN2.Text += btnVirgN2.Text;
        }

        private void btnApagar_Click(object sender, EventArgs e)
        {
            if(txtN1.Text.Length > 0)
                txtN1.Text = txtN1.Text.Remove(txtN1.Text.Length - 1);

        }

        private void btnApagarN2_Click(object sender, EventArgs e)
        {
            if (txtN2.Text.Length > 0)
                txtN2.Text = txtN2.Text.Remove(txtN2.Text.Length - 1);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtN1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtOpr_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn2_Click(object sender, EventArgs e)
        {
            txtN1.Text += btn2.Text;
        }
    }
}
