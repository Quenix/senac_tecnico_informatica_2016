﻿namespace Calculadora_agora_vai
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btnVirg = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.btnApagar = new System.Windows.Forms.Button();
            this.btnApagarN2 = new System.Windows.Forms.Button();
            this.btn0N2 = new System.Windows.Forms.Button();
            this.btnVirgN2 = new System.Windows.Forms.Button();
            this.btn9N2 = new System.Windows.Forms.Button();
            this.btn8N2 = new System.Windows.Forms.Button();
            this.btn7N2 = new System.Windows.Forms.Button();
            this.btn6N2 = new System.Windows.Forms.Button();
            this.btn5N2 = new System.Windows.Forms.Button();
            this.btn4N2 = new System.Windows.Forms.Button();
            this.btn3N2 = new System.Windows.Forms.Button();
            this.btn2N2 = new System.Windows.Forms.Button();
            this.btn1N2 = new System.Windows.Forms.Button();
            this.btnSub = new System.Windows.Forms.Button();
            this.btnSoma = new System.Windows.Forms.Button();
            this.btnIg = new System.Windows.Forms.Button();
            this.btnSair = new System.Windows.Forms.Button();
            this.txtN1 = new System.Windows.Forms.TextBox();
            this.txtN2 = new System.Windows.Forms.TextBox();
            this.txtOpr = new System.Windows.Forms.TextBox();
            this.btnDiv = new System.Windows.Forms.Button();
            this.btnMult = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn1
            // 
            this.btn1.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btn1.FlatAppearance.BorderSize = 100;
            this.btn1.Location = new System.Drawing.Point(87, 187);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(39, 35);
            this.btn1.TabIndex = 0;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn2
            // 
            this.btn2.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btn2.FlatAppearance.BorderSize = 100;
            this.btn2.Location = new System.Drawing.Point(132, 187);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(39, 35);
            this.btn2.TabIndex = 1;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btn3
            // 
            this.btn3.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btn3.FlatAppearance.BorderSize = 100;
            this.btn3.Location = new System.Drawing.Point(177, 187);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(39, 35);
            this.btn3.TabIndex = 2;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = true;
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btn4
            // 
            this.btn4.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btn4.FlatAppearance.BorderSize = 100;
            this.btn4.Location = new System.Drawing.Point(87, 228);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(39, 35);
            this.btn4.TabIndex = 3;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = true;
            this.btn4.Click += new System.EventHandler(this.btn4_Click);
            // 
            // btn5
            // 
            this.btn5.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btn5.FlatAppearance.BorderSize = 100;
            this.btn5.Location = new System.Drawing.Point(132, 228);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(39, 35);
            this.btn5.TabIndex = 4;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = true;
            this.btn5.Click += new System.EventHandler(this.btn5_Click);
            // 
            // btn6
            // 
            this.btn6.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btn6.FlatAppearance.BorderSize = 100;
            this.btn6.Location = new System.Drawing.Point(177, 228);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(39, 35);
            this.btn6.TabIndex = 5;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = true;
            this.btn6.Click += new System.EventHandler(this.btn6_Click);
            // 
            // btn7
            // 
            this.btn7.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btn7.FlatAppearance.BorderSize = 100;
            this.btn7.Location = new System.Drawing.Point(87, 269);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(39, 35);
            this.btn7.TabIndex = 6;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = true;
            this.btn7.Click += new System.EventHandler(this.btn7_Click);
            // 
            // btn8
            // 
            this.btn8.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btn8.FlatAppearance.BorderSize = 100;
            this.btn8.Location = new System.Drawing.Point(132, 269);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(39, 35);
            this.btn8.TabIndex = 7;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = true;
            this.btn8.Click += new System.EventHandler(this.btn8_Click);
            // 
            // btn9
            // 
            this.btn9.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btn9.FlatAppearance.BorderSize = 100;
            this.btn9.Location = new System.Drawing.Point(177, 269);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(39, 35);
            this.btn9.TabIndex = 8;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = true;
            this.btn9.Click += new System.EventHandler(this.btn9_Click);
            // 
            // btnVirg
            // 
            this.btnVirg.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btnVirg.FlatAppearance.BorderSize = 100;
            this.btnVirg.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVirg.Location = new System.Drawing.Point(87, 310);
            this.btnVirg.Name = "btnVirg";
            this.btnVirg.Size = new System.Drawing.Size(39, 35);
            this.btnVirg.TabIndex = 9;
            this.btnVirg.Text = ",";
            this.btnVirg.UseVisualStyleBackColor = true;
            this.btnVirg.Click += new System.EventHandler(this.btnVirg_Click);
            // 
            // btn0
            // 
            this.btn0.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btn0.FlatAppearance.BorderSize = 100;
            this.btn0.Location = new System.Drawing.Point(132, 310);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(39, 35);
            this.btn0.TabIndex = 10;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = true;
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btnApagar
            // 
            this.btnApagar.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btnApagar.FlatAppearance.BorderSize = 100;
            this.btnApagar.Location = new System.Drawing.Point(177, 310);
            this.btnApagar.Name = "btnApagar";
            this.btnApagar.Size = new System.Drawing.Size(39, 35);
            this.btnApagar.TabIndex = 11;
            this.btnApagar.Text = "<-";
            this.btnApagar.UseVisualStyleBackColor = true;
            this.btnApagar.Click += new System.EventHandler(this.btnApagar_Click);
            // 
            // btnApagarN2
            // 
            this.btnApagarN2.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btnApagarN2.FlatAppearance.BorderSize = 100;
            this.btnApagarN2.Location = new System.Drawing.Point(492, 310);
            this.btnApagarN2.Name = "btnApagarN2";
            this.btnApagarN2.Size = new System.Drawing.Size(39, 35);
            this.btnApagarN2.TabIndex = 27;
            this.btnApagarN2.Text = "<-";
            this.btnApagarN2.UseVisualStyleBackColor = true;
            this.btnApagarN2.Click += new System.EventHandler(this.btnApagarN2_Click);
            // 
            // btn0N2
            // 
            this.btn0N2.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btn0N2.FlatAppearance.BorderSize = 100;
            this.btn0N2.Location = new System.Drawing.Point(446, 310);
            this.btn0N2.Name = "btn0N2";
            this.btn0N2.Size = new System.Drawing.Size(39, 35);
            this.btn0N2.TabIndex = 26;
            this.btn0N2.Text = "0";
            this.btn0N2.UseVisualStyleBackColor = true;
            this.btn0N2.Click += new System.EventHandler(this.btn0N2_Click);
            // 
            // btnVirgN2
            // 
            this.btnVirgN2.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btnVirgN2.FlatAppearance.BorderSize = 100;
            this.btnVirgN2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVirgN2.Location = new System.Drawing.Point(402, 310);
            this.btnVirgN2.Name = "btnVirgN2";
            this.btnVirgN2.Size = new System.Drawing.Size(39, 35);
            this.btnVirgN2.TabIndex = 25;
            this.btnVirgN2.Text = ",";
            this.btnVirgN2.UseVisualStyleBackColor = true;
            this.btnVirgN2.Click += new System.EventHandler(this.btnVirgN2_Click);
            // 
            // btn9N2
            // 
            this.btn9N2.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btn9N2.FlatAppearance.BorderSize = 100;
            this.btn9N2.Location = new System.Drawing.Point(492, 269);
            this.btn9N2.Name = "btn9N2";
            this.btn9N2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btn9N2.Size = new System.Drawing.Size(39, 35);
            this.btn9N2.TabIndex = 24;
            this.btn9N2.Text = "9";
            this.btn9N2.UseVisualStyleBackColor = true;
            this.btn9N2.Click += new System.EventHandler(this.btn9N2_Click);
            // 
            // btn8N2
            // 
            this.btn8N2.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btn8N2.FlatAppearance.BorderSize = 100;
            this.btn8N2.Location = new System.Drawing.Point(447, 269);
            this.btn8N2.Name = "btn8N2";
            this.btn8N2.Size = new System.Drawing.Size(39, 35);
            this.btn8N2.TabIndex = 23;
            this.btn8N2.Text = "8";
            this.btn8N2.UseVisualStyleBackColor = true;
            this.btn8N2.Click += new System.EventHandler(this.btn8N2_Click);
            // 
            // btn7N2
            // 
            this.btn7N2.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btn7N2.FlatAppearance.BorderSize = 100;
            this.btn7N2.Location = new System.Drawing.Point(402, 269);
            this.btn7N2.Name = "btn7N2";
            this.btn7N2.Size = new System.Drawing.Size(39, 35);
            this.btn7N2.TabIndex = 22;
            this.btn7N2.Text = "7";
            this.btn7N2.UseVisualStyleBackColor = true;
            this.btn7N2.Click += new System.EventHandler(this.btn7N2_Click);
            // 
            // btn6N2
            // 
            this.btn6N2.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btn6N2.FlatAppearance.BorderSize = 100;
            this.btn6N2.Location = new System.Drawing.Point(492, 228);
            this.btn6N2.Name = "btn6N2";
            this.btn6N2.Size = new System.Drawing.Size(39, 35);
            this.btn6N2.TabIndex = 21;
            this.btn6N2.Text = "6";
            this.btn6N2.UseVisualStyleBackColor = true;
            this.btn6N2.Click += new System.EventHandler(this.btn6N2_Click);
            // 
            // btn5N2
            // 
            this.btn5N2.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btn5N2.FlatAppearance.BorderSize = 100;
            this.btn5N2.Location = new System.Drawing.Point(447, 228);
            this.btn5N2.Name = "btn5N2";
            this.btn5N2.Size = new System.Drawing.Size(39, 35);
            this.btn5N2.TabIndex = 20;
            this.btn5N2.Text = "5";
            this.btn5N2.UseVisualStyleBackColor = true;
            this.btn5N2.Click += new System.EventHandler(this.btn5N2_Click);
            // 
            // btn4N2
            // 
            this.btn4N2.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btn4N2.FlatAppearance.BorderSize = 100;
            this.btn4N2.Location = new System.Drawing.Point(402, 228);
            this.btn4N2.Name = "btn4N2";
            this.btn4N2.Size = new System.Drawing.Size(39, 35);
            this.btn4N2.TabIndex = 19;
            this.btn4N2.Text = "4";
            this.btn4N2.UseVisualStyleBackColor = true;
            this.btn4N2.Click += new System.EventHandler(this.btn4N2_Click);
            // 
            // btn3N2
            // 
            this.btn3N2.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btn3N2.FlatAppearance.BorderSize = 100;
            this.btn3N2.Location = new System.Drawing.Point(492, 187);
            this.btn3N2.Name = "btn3N2";
            this.btn3N2.Size = new System.Drawing.Size(39, 35);
            this.btn3N2.TabIndex = 18;
            this.btn3N2.Text = "3";
            this.btn3N2.UseVisualStyleBackColor = true;
            this.btn3N2.Click += new System.EventHandler(this.btn3N2_Click);
            // 
            // btn2N2
            // 
            this.btn2N2.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btn2N2.FlatAppearance.BorderSize = 100;
            this.btn2N2.Location = new System.Drawing.Point(447, 187);
            this.btn2N2.Name = "btn2N2";
            this.btn2N2.Size = new System.Drawing.Size(39, 35);
            this.btn2N2.TabIndex = 17;
            this.btn2N2.Text = "2";
            this.btn2N2.UseVisualStyleBackColor = true;
            this.btn2N2.Click += new System.EventHandler(this.btn2N2_Click);
            // 
            // btn1N2
            // 
            this.btn1N2.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btn1N2.FlatAppearance.BorderSize = 100;
            this.btn1N2.Location = new System.Drawing.Point(402, 187);
            this.btn1N2.Name = "btn1N2";
            this.btn1N2.Size = new System.Drawing.Size(39, 35);
            this.btn1N2.TabIndex = 16;
            this.btn1N2.Text = "1";
            this.btn1N2.UseVisualStyleBackColor = true;
            this.btn1N2.Click += new System.EventHandler(this.btn1N2_Click);
            // 
            // btnSub
            // 
            this.btnSub.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btnSub.FlatAppearance.BorderSize = 100;
            this.btnSub.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSub.Location = new System.Drawing.Point(312, 187);
            this.btnSub.Name = "btnSub";
            this.btnSub.Size = new System.Drawing.Size(39, 35);
            this.btnSub.TabIndex = 13;
            this.btnSub.Text = "-";
            this.btnSub.UseVisualStyleBackColor = true;
            this.btnSub.Click += new System.EventHandler(this.btnSub_Click);
            // 
            // btnSoma
            // 
            this.btnSoma.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btnSoma.FlatAppearance.BorderSize = 100;
            this.btnSoma.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSoma.Location = new System.Drawing.Point(267, 187);
            this.btnSoma.Name = "btnSoma";
            this.btnSoma.Size = new System.Drawing.Size(39, 35);
            this.btnSoma.TabIndex = 12;
            this.btnSoma.Text = "+";
            this.btnSoma.UseVisualStyleBackColor = true;
            this.btnSoma.Click += new System.EventHandler(this.btnSoma_Click);
            // 
            // btnIg
            // 
            this.btnIg.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btnIg.FlatAppearance.BorderSize = 100;
            this.btnIg.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIg.Location = new System.Drawing.Point(267, 287);
            this.btnIg.Name = "btnIg";
            this.btnIg.Size = new System.Drawing.Size(84, 35);
            this.btnIg.TabIndex = 28;
            this.btnIg.Text = "=";
            this.btnIg.UseVisualStyleBackColor = true;
            this.btnIg.Click += new System.EventHandler(this.btnIg_Click);
            // 
            // btnSair
            // 
            this.btnSair.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btnSair.FlatAppearance.BorderSize = 100;
            this.btnSair.Location = new System.Drawing.Point(447, 365);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(84, 35);
            this.btnSair.TabIndex = 29;
            this.btnSair.Text = "Sair";
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // txtN1
            // 
            this.txtN1.Location = new System.Drawing.Point(186, 122);
            this.txtN1.Name = "txtN1";
            this.txtN1.ReadOnly = true;
            this.txtN1.Size = new System.Drawing.Size(100, 20);
            this.txtN1.TabIndex = 30;
            this.txtN1.TextChanged += new System.EventHandler(this.txtN1_TextChanged);
            // 
            // txtN2
            // 
            this.txtN2.Location = new System.Drawing.Point(331, 122);
            this.txtN2.Name = "txtN2";
            this.txtN2.ReadOnly = true;
            this.txtN2.Size = new System.Drawing.Size(100, 20);
            this.txtN2.TabIndex = 31;
            // 
            // txtOpr
            // 
            this.txtOpr.Location = new System.Drawing.Point(292, 122);
            this.txtOpr.Name = "txtOpr";
            this.txtOpr.ReadOnly = true;
            this.txtOpr.Size = new System.Drawing.Size(33, 20);
            this.txtOpr.TabIndex = 32;
            this.txtOpr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtOpr.TextChanged += new System.EventHandler(this.txtOpr_TextChanged);
            // 
            // btnDiv
            // 
            this.btnDiv.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btnDiv.FlatAppearance.BorderSize = 100;
            this.btnDiv.Location = new System.Drawing.Point(312, 228);
            this.btnDiv.Name = "btnDiv";
            this.btnDiv.Size = new System.Drawing.Size(39, 35);
            this.btnDiv.TabIndex = 15;
            this.btnDiv.Text = "/";
            this.btnDiv.UseVisualStyleBackColor = true;
            this.btnDiv.Click += new System.EventHandler(this.btnDiv_Click);
            // 
            // btnMult
            // 
            this.btnMult.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText;
            this.btnMult.FlatAppearance.BorderSize = 100;
            this.btnMult.Location = new System.Drawing.Point(267, 228);
            this.btnMult.Name = "btnMult";
            this.btnMult.Size = new System.Drawing.Size(39, 35);
            this.btnMult.TabIndex = 14;
            this.btnMult.Text = "X";
            this.btnMult.UseVisualStyleBackColor = true;
            this.btnMult.Click += new System.EventHandler(this.btnMult_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(183, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 35;
            this.label1.Text = "Valor 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(328, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 36;
            this.label2.Text = "Valor 2";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Comic Sans MS", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(222, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 38);
            this.label3.TabIndex = 37;
            this.label3.Text = "Calculadora";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(599, 412);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnDiv);
            this.Controls.Add(this.btnMult);
            this.Controls.Add(this.txtOpr);
            this.Controls.Add(this.txtN2);
            this.Controls.Add(this.txtN1);
            this.Controls.Add(this.btnSair);
            this.Controls.Add(this.btnIg);
            this.Controls.Add(this.btnSub);
            this.Controls.Add(this.btnSoma);
            this.Controls.Add(this.btnApagarN2);
            this.Controls.Add(this.btn0N2);
            this.Controls.Add(this.btnVirgN2);
            this.Controls.Add(this.btn9N2);
            this.Controls.Add(this.btn8N2);
            this.Controls.Add(this.btn7N2);
            this.Controls.Add(this.btn6N2);
            this.Controls.Add(this.btn5N2);
            this.Controls.Add(this.btn4N2);
            this.Controls.Add(this.btn3N2);
            this.Controls.Add(this.btn2N2);
            this.Controls.Add(this.btn1N2);
            this.Controls.Add(this.btnApagar);
            this.Controls.Add(this.btn0);
            this.Controls.Add(this.btnVirg);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn7);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Name = "Form1";
            this.Text = " Calculadora";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btnVirg;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button btnApagar;
        private System.Windows.Forms.Button btnApagarN2;
        private System.Windows.Forms.Button btn0N2;
        private System.Windows.Forms.Button btnVirgN2;
        private System.Windows.Forms.Button btn9N2;
        private System.Windows.Forms.Button btn8N2;
        private System.Windows.Forms.Button btn7N2;
        private System.Windows.Forms.Button btn6N2;
        private System.Windows.Forms.Button btn5N2;
        private System.Windows.Forms.Button btn4N2;
        private System.Windows.Forms.Button btn3N2;
        private System.Windows.Forms.Button btn2N2;
        private System.Windows.Forms.Button btn1N2;
        private System.Windows.Forms.Button btnSub;
        private System.Windows.Forms.Button btnSoma;
        private System.Windows.Forms.Button btnIg;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.TextBox txtN1;
        private System.Windows.Forms.TextBox txtN2;
        private System.Windows.Forms.TextBox txtOpr;
        private System.Windows.Forms.Button btnDiv;
        private System.Windows.Forms.Button btnMult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

