﻿namespace WindowsFormsApp2
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btnVirg = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.btnApagar = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.btnSub = new System.Windows.Forms.Button();
            this.btnSoma = new System.Windows.Forms.Button();
            this.btnIg = new System.Windows.Forms.Button();
            this.btnSair = new System.Windows.Forms.Button();
            this.txtN1 = new System.Windows.Forms.TextBox();
            this.txtN2 = new System.Windows.Forms.TextBox();
            this.txtOpc = new System.Windows.Forms.TextBox();
            this.btnDiv = new System.Windows.Forms.Button();
            this.btnMult = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(87, 187);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(39, 35);
            this.btn1.TabIndex = 0;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = true;
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(132, 187);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(39, 35);
            this.btn2.TabIndex = 1;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = true;
            // 
            // btn3
            // 
            this.btn3.Location = new System.Drawing.Point(177, 187);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(39, 35);
            this.btn3.TabIndex = 2;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = true;
            // 
            // btn4
            // 
            this.btn4.Location = new System.Drawing.Point(87, 228);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(39, 35);
            this.btn4.TabIndex = 3;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = true;
            // 
            // btn5
            // 
            this.btn5.Location = new System.Drawing.Point(132, 228);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(39, 35);
            this.btn5.TabIndex = 4;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = true;
            // 
            // btn6
            // 
            this.btn6.Location = new System.Drawing.Point(177, 228);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(39, 35);
            this.btn6.TabIndex = 5;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = true;
            // 
            // btn7
            // 
            this.btn7.Location = new System.Drawing.Point(87, 269);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(39, 35);
            this.btn7.TabIndex = 6;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = true;
            // 
            // btn8
            // 
            this.btn8.Location = new System.Drawing.Point(132, 269);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(39, 35);
            this.btn8.TabIndex = 7;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = true;
            // 
            // btn9
            // 
            this.btn9.Location = new System.Drawing.Point(177, 269);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(39, 35);
            this.btn9.TabIndex = 8;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = true;
            // 
            // btnVirg
            // 
            this.btnVirg.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVirg.Location = new System.Drawing.Point(87, 310);
            this.btnVirg.Name = "btnVirg";
            this.btnVirg.Size = new System.Drawing.Size(39, 35);
            this.btnVirg.TabIndex = 9;
            this.btnVirg.Text = ",";
            this.btnVirg.UseVisualStyleBackColor = true;
            // 
            // btn0
            // 
            this.btn0.Location = new System.Drawing.Point(132, 310);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(39, 35);
            this.btn0.TabIndex = 10;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = true;
            // 
            // btnApagar
            // 
            this.btnApagar.Location = new System.Drawing.Point(177, 310);
            this.btnApagar.Name = "btnApagar";
            this.btnApagar.Size = new System.Drawing.Size(39, 35);
            this.btnApagar.TabIndex = 11;
            this.btnApagar.Text = "<-";
            this.btnApagar.UseVisualStyleBackColor = true;
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(492, 310);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(39, 35);
            this.button13.TabIndex = 23;
            this.button13.Text = "button13";
            this.button13.UseVisualStyleBackColor = true;
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(447, 310);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(39, 35);
            this.button14.TabIndex = 22;
            this.button14.Text = "button14";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(402, 310);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(39, 35);
            this.button15.TabIndex = 21;
            this.button15.Text = "button15";
            this.button15.UseVisualStyleBackColor = true;
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(492, 269);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(39, 35);
            this.button16.TabIndex = 20;
            this.button16.Text = "button16";
            this.button16.UseVisualStyleBackColor = true;
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(447, 269);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(39, 35);
            this.button17.TabIndex = 19;
            this.button17.Text = "button17";
            this.button17.UseVisualStyleBackColor = true;
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(402, 269);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(39, 35);
            this.button18.TabIndex = 18;
            this.button18.Text = "button18";
            this.button18.UseVisualStyleBackColor = true;
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(492, 228);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(39, 35);
            this.button19.TabIndex = 17;
            this.button19.Text = "button19";
            this.button19.UseVisualStyleBackColor = true;
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(447, 228);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(39, 35);
            this.button20.TabIndex = 16;
            this.button20.Text = "button20";
            this.button20.UseVisualStyleBackColor = true;
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(402, 228);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(39, 35);
            this.button21.TabIndex = 15;
            this.button21.Text = "button21";
            this.button21.UseVisualStyleBackColor = true;
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(492, 187);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(39, 35);
            this.button22.TabIndex = 14;
            this.button22.Text = "button22";
            this.button22.UseVisualStyleBackColor = true;
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(447, 187);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(39, 35);
            this.button23.TabIndex = 13;
            this.button23.Text = "button23";
            this.button23.UseVisualStyleBackColor = true;
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(402, 187);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(39, 35);
            this.button24.TabIndex = 12;
            this.button24.Text = "button24";
            this.button24.UseVisualStyleBackColor = true;
            // 
            // btnSub
            // 
            this.btnSub.Location = new System.Drawing.Point(312, 187);
            this.btnSub.Name = "btnSub";
            this.btnSub.Size = new System.Drawing.Size(39, 35);
            this.btnSub.TabIndex = 25;
            this.btnSub.Text = "-";
            this.btnSub.UseVisualStyleBackColor = true;
            // 
            // btnSoma
            // 
            this.btnSoma.Location = new System.Drawing.Point(267, 187);
            this.btnSoma.Name = "btnSoma";
            this.btnSoma.Size = new System.Drawing.Size(39, 35);
            this.btnSoma.TabIndex = 24;
            this.btnSoma.Text = "+";
            this.btnSoma.UseVisualStyleBackColor = true;
            // 
            // btnIg
            // 
            this.btnIg.Location = new System.Drawing.Point(267, 269);
            this.btnIg.Name = "btnIg";
            this.btnIg.Size = new System.Drawing.Size(84, 35);
            this.btnIg.TabIndex = 28;
            this.btnIg.Text = "=";
            this.btnIg.UseVisualStyleBackColor = true;
            // 
            // btnSair
            // 
            this.btnSair.Location = new System.Drawing.Point(267, 310);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(84, 35);
            this.btnSair.TabIndex = 29;
            this.btnSair.Text = "Sair";
            this.btnSair.UseVisualStyleBackColor = true;
            // 
            // txtN1
            // 
            this.txtN1.Location = new System.Drawing.Point(187, 61);
            this.txtN1.Name = "txtN1";
            this.txtN1.Size = new System.Drawing.Size(100, 20);
            this.txtN1.TabIndex = 30;
            // 
            // txtN2
            // 
            this.txtN2.Location = new System.Drawing.Point(332, 61);
            this.txtN2.Name = "txtN2";
            this.txtN2.Size = new System.Drawing.Size(100, 20);
            this.txtN2.TabIndex = 31;
            // 
            // txtOpc
            // 
            this.txtOpc.Location = new System.Drawing.Point(293, 61);
            this.txtOpc.Name = "txtOpc";
            this.txtOpc.Size = new System.Drawing.Size(33, 20);
            this.txtOpc.TabIndex = 32;
            // 
            // btnDiv
            // 
            this.btnDiv.Location = new System.Drawing.Point(312, 228);
            this.btnDiv.Name = "btnDiv";
            this.btnDiv.Size = new System.Drawing.Size(39, 35);
            this.btnDiv.TabIndex = 34;
            this.btnDiv.Text = "/";
            this.btnDiv.UseVisualStyleBackColor = true;
            // 
            // btnMult
            // 
            this.btnMult.Location = new System.Drawing.Point(267, 228);
            this.btnMult.Name = "btnMult";
            this.btnMult.Size = new System.Drawing.Size(39, 35);
            this.btnMult.TabIndex = 33;
            this.btnMult.Text = "X";
            this.btnMult.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(599, 412);
            this.Controls.Add(this.btnDiv);
            this.Controls.Add(this.btnMult);
            this.Controls.Add(this.txtOpc);
            this.Controls.Add(this.txtN2);
            this.Controls.Add(this.txtN1);
            this.Controls.Add(this.btnSair);
            this.Controls.Add(this.btnIg);
            this.Controls.Add(this.btnSub);
            this.Controls.Add(this.btnSoma);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.button22);
            this.Controls.Add(this.button23);
            this.Controls.Add(this.button24);
            this.Controls.Add(this.btnApagar);
            this.Controls.Add(this.btn0);
            this.Controls.Add(this.btnVirg);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn7);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Name = "Form1";
            this.Text = " Calculadora";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btnVirg;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button btnApagar;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button btnSub;
        private System.Windows.Forms.Button btnSoma;
        private System.Windows.Forms.Button btnIg;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.TextBox txtN1;
        private System.Windows.Forms.TextBox txtN2;
        private System.Windows.Forms.TextBox txtOpc;
        private System.Windows.Forms.Button btnDiv;
        private System.Windows.Forms.Button btnMult;
    }
}

