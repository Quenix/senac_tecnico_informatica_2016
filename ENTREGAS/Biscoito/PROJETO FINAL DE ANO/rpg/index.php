<!DOCTYPE html>
<html>
	<head>
		<title>Login</title>
		<link type="text/css" rel="stylesheet" href="css/style.css" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		</head>
	
	<body>	
		<div id="formLogin">
			<form id="frmlogin" method="POST" action="login.php">
				<label>ID:</label>
				<input name="username" type="text" id="userlogin"/>
				
				<label>Senha:</label>
				<input name="password" type="password"  id="userpassword"/>
				
				<input id="login" type="submit" name="check" value="Entrar">
			</form>
		</div>
		
		<!-- Trigger/Open The Modal -->
		<button id="myBtn">Cadastrar</button>

		<!-- The Modal -->
		<div id="myModal" class="modal">

		  <!-- Modal content -->
		  <div class="modal-content">
			<span class="close">&times;</span>
			<form id="cadastroEnviar" method="POST" action="cadastrar.php">
				<input id="cadastrarUser" type="hidden" name="userid" >
				<input id="cadastrarSenha" type="hidden" name="userpassw">			
			</form>
			
			<form id="frmregister">
				<label>ID:</label>
				<input type="text" id="userRegister"/>
				<label>Senha:</label>
				<input type="password"  id="passwordRegister"/>
				<label>Confirmar Senha:</label>
				<input name="confpassw" type="password"  id="passwordRegisterConfirm"/>
				
				<input id="btnregister" type="submit" value="Cadastrar">
			</form>
			
			
		  </div>

		</div>

	</body>
	<script>
		
	
			$('#frmregister').on('submit', function(e){
				e.preventDefault();
				var userRegister = $('#userRegister').val();
				var passwordRegister = $('#passwordRegister').val();
				var passwordRegisterConfirm = $('#passwordRegisterConfirm').val();
				
				if(passwordRegister == passwordRegisterConfirm){
				$('#cadastrarUser').val(userRegister);
				$('#cadastrarSenha').val(passwordRegister);
				
				$('#cadastroEnviar').submit();
				}else{
					alert('Senhas não coincidem');
				}
			});
		
		</script>
	<script src="register.js"></script>
	
</html>