<?PHP
	$user = $_POST['u'];
?>
<HTML>
	<HEAD>
		<title>Chat</title>
		<link rel='stylesheet' type='text/css' href='css/style.css' />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="javascript/Chat.js"></script>
	</HEAD>
	
	<BODY>
		<div class='chatContainer'>
			<div class='chatHeader'>
				<h3>Welcome <?php echo ucwords($user); ?></h3>
			</div>
			<div class='chatMessages'></div>
			<div class='chatBottom'>
				<form action='#' onSubmit='return nothing;' id='chatForm'>
					<input type='hidden' id='name' value='<?php echo$user; ?>' />
					<input type='text' name='text' id='text' value='' placeholder='type your chat message' />
					<input type='submit' name='submit' value='post' />
				</form>
			</div>
		</div>
	</BODY>
</HTML>