﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cadastro_usuário
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            string nome, sobrenome, departamento;
            int idade, salario;
    
            nome = txtNome.Text;
            sobrenome = txtSobrenome.Text;
            departamento = cmbDepartamento.Text;
            idade = Convert.ToInt32 (txtIdade.Text);
            salario = Convert.ToInt32(txtSalario.Text);

             MessageBox.Show("Nome: " + nome + " " + sobrenome + "\n" + "Idade: " + idade + "\n" + "Salário: " + salario + " Reais" + "\n" + "Departamento: " + departamento + "\n \n" + "Cadastro efetuado com sucesso");

        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
