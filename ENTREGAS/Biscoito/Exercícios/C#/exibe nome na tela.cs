﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            string nome, sobrenome;

            Console.WriteLine("Qual o seu nome?");
            nome = Console.ReadLine();
            Console.WriteLine("Qual o seu sobrenome?");
            sobrenome = Console.ReadLine();

            Console.WriteLine("Bem vindo " + nome + " " + sobrenome);
            
        }
    }
}
