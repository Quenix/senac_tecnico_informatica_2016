﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            string nome;
            int idade;
            Console.WriteLine("Qual o seu nome?");
            nome =Console.ReadLine();

            Console.WriteLine("Qual a sua idade?");
            idade = Convert.ToInt16(Console.ReadLine());

            Console.Write("seu nome é " + nome + ", tem " + idade + " anos");
            
            if((idade == 20) || (idade == 30))
            {
                Console.WriteLine(" e pode entrar");
            }
            else
            {
                Console.WriteLine(" e não pode entrar");
            }
            
        }
    }
}
