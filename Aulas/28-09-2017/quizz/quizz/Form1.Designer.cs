﻿namespace quizz
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pergunta1_resposta3 = new System.Windows.Forms.RadioButton();
            this.pergunta1_resposta2 = new System.Windows.Forms.RadioButton();
            this.pergunta1_resposta1 = new System.Windows.Forms.RadioButton();
            this.lblPergunta1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pergunta2_resposta3 = new System.Windows.Forms.RadioButton();
            this.pergunta2_resposta2 = new System.Windows.Forms.RadioButton();
            this.pergunta2_resposta1 = new System.Windows.Forms.RadioButton();
            this.lblPergunta2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pergunta1_resposta3);
            this.groupBox1.Controls.Add(this.pergunta1_resposta2);
            this.groupBox1.Controls.Add(this.pergunta1_resposta1);
            this.groupBox1.Controls.Add(this.lblPergunta1);
            this.groupBox1.Location = new System.Drawing.Point(20, 31);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(195, 192);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pergunta 1";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // pergunta1_resposta3
            // 
            this.pergunta1_resposta3.AutoSize = true;
            this.pergunta1_resposta3.Location = new System.Drawing.Point(7, 128);
            this.pergunta1_resposta3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pergunta1_resposta3.Name = "pergunta1_resposta3";
            this.pergunta1_resposta3.Size = new System.Drawing.Size(85, 17);
            this.pergunta1_resposta3.TabIndex = 3;
            this.pergunta1_resposta3.TabStop = true;
            this.pergunta1_resposta3.Text = "radioButton2";
            this.pergunta1_resposta3.UseVisualStyleBackColor = true;
            // 
            // pergunta1_resposta2
            // 
            this.pergunta1_resposta2.AutoSize = true;
            this.pergunta1_resposta2.Location = new System.Drawing.Point(7, 106);
            this.pergunta1_resposta2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pergunta1_resposta2.Name = "pergunta1_resposta2";
            this.pergunta1_resposta2.Size = new System.Drawing.Size(85, 17);
            this.pergunta1_resposta2.TabIndex = 2;
            this.pergunta1_resposta2.TabStop = true;
            this.pergunta1_resposta2.Text = "radioButton1";
            this.pergunta1_resposta2.UseVisualStyleBackColor = true;
            // 
            // pergunta1_resposta1
            // 
            this.pergunta1_resposta1.AutoSize = true;
            this.pergunta1_resposta1.Location = new System.Drawing.Point(7, 84);
            this.pergunta1_resposta1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pergunta1_resposta1.Name = "pergunta1_resposta1";
            this.pergunta1_resposta1.Size = new System.Drawing.Size(85, 17);
            this.pergunta1_resposta1.TabIndex = 1;
            this.pergunta1_resposta1.TabStop = true;
            this.pergunta1_resposta1.Text = "radioButton1";
            this.pergunta1_resposta1.UseVisualStyleBackColor = true;
            // 
            // lblPergunta1
            // 
            this.lblPergunta1.AutoSize = true;
            this.lblPergunta1.Location = new System.Drawing.Point(4, 24);
            this.lblPergunta1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPergunta1.Name = "lblPergunta1";
            this.lblPergunta1.Size = new System.Drawing.Size(33, 13);
            this.lblPergunta1.TabIndex = 0;
            this.lblPergunta1.Text = "Label";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pergunta2_resposta3);
            this.groupBox2.Controls.Add(this.pergunta2_resposta2);
            this.groupBox2.Controls.Add(this.pergunta2_resposta1);
            this.groupBox2.Controls.Add(this.lblPergunta2);
<<<<<<< HEAD
            this.groupBox2.Location = new System.Drawing.Point(248, 31);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
=======
            this.groupBox2.Location = new System.Drawing.Point(331, 38);
>>>>>>> 827184a4feb4bd7d278f5c915564e2467b9af44c
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Size = new System.Drawing.Size(195, 192);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pergunta 2";
            // 
            // pergunta2_resposta3
            // 
            this.pergunta2_resposta3.AutoSize = true;
            this.pergunta2_resposta3.Location = new System.Drawing.Point(7, 128);
            this.pergunta2_resposta3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pergunta2_resposta3.Name = "pergunta2_resposta3";
            this.pergunta2_resposta3.Size = new System.Drawing.Size(85, 17);
            this.pergunta2_resposta3.TabIndex = 3;
            this.pergunta2_resposta3.TabStop = true;
            this.pergunta2_resposta3.Text = "radioButton2";
            this.pergunta2_resposta3.UseVisualStyleBackColor = true;
            // 
            // pergunta2_resposta2
            // 
            this.pergunta2_resposta2.AutoSize = true;
            this.pergunta2_resposta2.Location = new System.Drawing.Point(7, 106);
            this.pergunta2_resposta2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pergunta2_resposta2.Name = "pergunta2_resposta2";
            this.pergunta2_resposta2.Size = new System.Drawing.Size(85, 17);
            this.pergunta2_resposta2.TabIndex = 2;
            this.pergunta2_resposta2.TabStop = true;
            this.pergunta2_resposta2.Text = "radioButton1";
            this.pergunta2_resposta2.UseVisualStyleBackColor = true;
            // 
            // pergunta2_resposta1
            // 
            this.pergunta2_resposta1.AutoSize = true;
            this.pergunta2_resposta1.Location = new System.Drawing.Point(7, 84);
            this.pergunta2_resposta1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pergunta2_resposta1.Name = "pergunta2_resposta1";
            this.pergunta2_resposta1.Size = new System.Drawing.Size(85, 17);
            this.pergunta2_resposta1.TabIndex = 1;
            this.pergunta2_resposta1.TabStop = true;
            this.pergunta2_resposta1.Text = "radioButton1";
            this.pergunta2_resposta1.UseVisualStyleBackColor = true;
            // 
            // lblPergunta2
            // 
            this.lblPergunta2.AutoSize = true;
            this.lblPergunta2.Location = new System.Drawing.Point(4, 24);
            this.lblPergunta2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPergunta2.Name = "lblPergunta2";
            this.lblPergunta2.Size = new System.Drawing.Size(33, 13);
            this.lblPergunta2.TabIndex = 0;
            this.lblPergunta2.Text = "Label";
            // 
            // groupBox3
            // 
<<<<<<< HEAD
            this.btnFinalizar.Location = new System.Drawing.Point(196, 242);
            this.btnFinalizar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnFinalizar.Name = "btnFinalizar";
            this.btnFinalizar.Size = new System.Drawing.Size(56, 19);
            this.btnFinalizar.TabIndex = 5;
            this.btnFinalizar.Text = "Finalizar!";
            this.btnFinalizar.UseVisualStyleBackColor = true;
            this.btnFinalizar.Click += new System.EventHandler(this.btnFinalizar_Click);
=======
            this.groupBox3.Controls.Add(this.radioButton1);
            this.groupBox3.Controls.Add(this.radioButton2);
            this.groupBox3.Controls.Add(this.radioButton3);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(26, 298);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(260, 236);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Pergunta 1";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(9, 157);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(110, 21);
            this.radioButton1.TabIndex = 3;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "radioButton2";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(9, 130);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(110, 21);
            this.radioButton2.TabIndex = 2;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "radioButton1";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(9, 103);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(110, 21);
            this.radioButton3.TabIndex = 1;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "radioButton1";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Label";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.radioButton4);
            this.groupBox4.Controls.Add(this.radioButton5);
            this.groupBox4.Controls.Add(this.radioButton6);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Location = new System.Drawing.Point(308, 327);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(260, 236);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Pergunta 2";
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(9, 157);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(110, 21);
            this.radioButton4.TabIndex = 3;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "radioButton2";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(9, 130);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(110, 21);
            this.radioButton5.TabIndex = 2;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "radioButton1";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(9, 103);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(110, 21);
            this.radioButton6.TabIndex = 1;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "radioButton1";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Label";
>>>>>>> 827184a4feb4bd7d278f5c915564e2467b9af44c
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
<<<<<<< HEAD
            this.ClientSize = new System.Drawing.Size(460, 279);
            this.Controls.Add(this.btnFinalizar);
=======
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(614, 347);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
>>>>>>> 827184a4feb4bd7d278f5c915564e2467b9af44c
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton pergunta1_resposta3;
        private System.Windows.Forms.RadioButton pergunta1_resposta2;
        private System.Windows.Forms.RadioButton pergunta1_resposta1;
        private System.Windows.Forms.Label lblPergunta1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton pergunta2_resposta3;
        private System.Windows.Forms.RadioButton pergunta2_resposta2;
        private System.Windows.Forms.RadioButton pergunta2_resposta1;
        private System.Windows.Forms.Label lblPergunta2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.Label label2;
    }
}

