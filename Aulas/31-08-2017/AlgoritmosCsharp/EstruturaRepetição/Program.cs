﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstruturaRepetição
{
    class Program
    {
        static void Main(string[] args)
        {
            Boolean lampada;
            string resposta;

            lampada = true;

            while (lampada == true)
            {
                Console.WriteLine("A lâmpada está ligada!");
                Console.WriteLine("Digite APAGAR para apagar a lâmpada.");
                resposta = Console.ReadLine();

                if(resposta == "APAGAR")
                {
                    lampada = false;
                    Console.WriteLine("A lâmpada foi desligada com sucesso!");
                }


            }
        }
    }
}
